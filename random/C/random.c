#include <assert.h>
#include <stdlib.h>



unsigned int
rand_from_until(unsigned int from, unsigned int until) {
  assert(from < until);
  assert(until - from <= RAND_MAX);

  until -= from;

  {
    const unsigned int MULTIPLE = (unsigned int)RAND_MAX + 1 - ((unsigned int)RAND_MAX + 1) % until;

    unsigned int n;

    do {
      n = rand();
    } while (n >= MULTIPLE);

    /*
      INV:
      n in one of those intervals:
      [0, 1, 2, ..., until - 1], [until, ... , 2*until - 1], ..., [(k - 1)*until, ... , k*until - 1], MULTIPLE_OF_UNTIL
    */

    return n % until + from;
  }
}
