#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <CUnit/Basic.h>  /* http://cunit.sourceforge.net/ */

#include "random.h"



void
test_rand_from_until() {
  const unsigned int FROM = 33;
  const unsigned int UNTIL = 100;
  const unsigned int NB = UNTIL*UNTIL;

  unsigned int from;

  for (from = 0; from < FROM; ++from) {
    unsigned int until;

    for (until = from + 1; until < from + UNTIL; ++until) {
      unsigned int i;
      unsigned int* distribution = calloc(until - from, sizeof(unsigned int));

      for (i = 0; i < NB; ++i) {
        const unsigned int N = rand_from_until(from, until);

        /* Check value */
        CU_ASSERT(from <= N);
        CU_ASSERT(N < until);

        if ((from <= N) && (N < until)) {
          distribution[N - from] += 1;
        }
      }

      /* Check all values have emerged */
      for (i = 0; i < until - from; ++i) {
        CU_ASSERT(distribution[i] > 0);
      }

      free(distribution);
    }
  }
}



/*
 * Main
 */
int
main() {
  CU_pSuite pSuite = NULL;

  if (CUE_SUCCESS != CU_initialize_registry()) {
    return CU_get_error();
  }

  pSuite = CU_add_suite("Suite_1", NULL, NULL);
  if (NULL == pSuite) {
    CU_cleanup_registry();

    return CU_get_error();
  }

  if (NULL == CU_add_test(pSuite, "test of rand_from_until()", test_rand_from_until)) {
    CU_cleanup_registry();

    return CU_get_error();
  }

  CU_basic_set_mode(CU_BRM_VERBOSE);


  printf("RAND_MAX: %u\n", RAND_MAX);

  srand(time(NULL));


  CU_basic_run_tests();
  CU_cleanup_registry();

  return CU_get_error();
}
