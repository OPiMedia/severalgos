/** \file random.h
 *
 * \brief Simple function to return random number.
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @version October 6, 2014
 * @author Olivier Pirson <olivier.pirson.opi@gmail.com>
 *
 *
 *
 * \mainpage random
 * Piece of severalgos.\n
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson\n
 * http://www.opimedia.be/
 */


#ifndef __RANDOM_HEADER
#define __RANDOM_HEADER "random --- October 6, 2014"


/**
 * Return a random number between from (included) and until (not included).
 *
 * Don't forget to seed the random number generator by srand().
 *
 * @param from < until
 * @param until <= from + RAND_MAX
 */
unsigned int
rand_from_until(unsigned int from, unsigned int until);


#endif  /* __RANDOM_HEADER */
