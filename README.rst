.. -*- restructuredtext -*-

==========
severalgos
==========
Several algorithms implemented in C, C++ and Python.

https://bitbucket.org/OPiMedia/severalgos

* 2D points (Python)

* 3np1 mod problem (C++)

* Fibonacci numbers (math, C++, C++ with GMP, Prolog, Python)

* graph: Dijkstra's shortest path first algorithm (Python)

* Primes (C++)

* Random (C)

* Sort (Python):

  - Insertion Sort

  - Merge Sort (merge)

  - Quick Sort (partition, partition 3-way)

  - Selection Sort

  - Shell Sort

  - Knuth Shuffle

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2013-2018, 2020, 2025 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png

|



Changes
=======
* August 3, 2020

  - Added Dijkstra's shortest path first algorithm.

* June, 2018

  - Added simple Fibonacci implementation in Prolog.

* April 30, 2017

  - Replaced Mathjax CDN for 3np1_mod_problem and C++ Fibonacci documentations.

* October 17, 2016

  - sort/Python: remover less_fct parameter to shuffle().

* February 29, 2016

  - Fibonacci: Corrected typos in mathematical document, removed "check" marks and added some relations.
  - Updated links to online documentations.

* March 15, 2015

  - Added C++ primes.

* February 24, 2015

  - Added GMP version of C++ Fibonacci.

* February 20, 2015

  - Added C++, Python and math document on Fibonacci numbers.

* October 6, 2014

  - Added ``random_c``.

* April 19, 2014

  - Added ``points2d_py``.

* September, 2013

  - Created this repository, with severall sorting algorithms ``sort_py``.
