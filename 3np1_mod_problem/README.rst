.. -*- restructuredtext -*-

=================================
severalgos/ 3n + 1 modulo problem
=================================

Statement of the problem of 3n + 1 problem
==========================================
.. math::
   \forall n \in \mathbb{N}_{\ast} :
   T(n) = \left\{
   \begin{array}{ll}
     \frac{n}{2} & \text{si }n\text{ pair}\\
     \frac{3n + 1}{2} & \text{si }n\text{ impair}\\
   \end{array}\right.

The 3n + 1 problem (Collatz **conjecture**)
state that

.. math::
   \forall n \in \mathbb{N}_{\ast}, \exists k \in \mathbb{N} : T^k(n) = 1


For more information, see :

* Collatz conjecture:
  https://en.wikipedia.org/wiki/Collatz_conjecture
* Problème 3n + 1:
  http://www.opimedia.be/3nP1/
* *The 3x+1 problem and its generalizations* (Jeff Lagarias):
  http://www.cecm.sfu.ca/organics/papers/lagarias/



In modulo arithmetic
====================
The C++ program *prog_3np1_mod* check the dynamic of T (and alternatives) function
in a **modulo arithmetic**.


The **C++ HTML online documentation**
http://www.opimedia.be/DS/online-documentations/severalgos/3np1-mod-problem-cpp/html/
