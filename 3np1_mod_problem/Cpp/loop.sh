#!/bin/bash

if [ -z "$1" ]
then
    N=8
else
    N=$1
fi

if [ -z "$2" ]
then
    OPE=T
else
    OPE=$2
fi


for I in `seq 1 $N`;
do
    echo
    ./prog_3np1_mod $I $OPE
done
