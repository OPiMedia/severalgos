#!/bin/bash

if [ -z "$1" ]
then
    OPE=T
else
    OPE=$1
fi

./prog_3np1_mod 8 $OPE
echo
./prog_3np1_mod 16 $OPE
echo
./prog_3np1_mod 24 $OPE
echo
./prog_3np1_mod 32 $OPE
echo
./prog_3np1_mod 48 $OPE
echo
./prog_3np1_mod 64 $OPE
