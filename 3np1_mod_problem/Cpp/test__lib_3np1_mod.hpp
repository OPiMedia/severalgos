/* -*- coding: latin-1 -*- */
/*
 * test__lib_3np1_mod.hpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <limits>
#include <set>
#include <vector>

#include <cxxtest/TestSuite.h>  // NOLINT // CxxTest http://cxxtest.com/

#include "lib_3np1_mod/lib_3np1_mod.hpp"



#define SHOW_FUNC_NAME std::cout << std::endl   \
  << "=== " << __func__ << " ===" << std::endl



class Test__lib_3np1_mod : public CxxTest::TestSuite {
 public:
  void test__C() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(lib_3np1_mod::C(0), 0);
    TS_ASSERT_EQUALS(lib_3np1_mod::C(1), 4);
    TS_ASSERT_EQUALS(lib_3np1_mod::C(2), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::C(3), 10);
    TS_ASSERT_EQUALS(lib_3np1_mod::C(4), 2);
    TS_ASSERT_EQUALS(lib_3np1_mod::C(5), 16);

    TS_ASSERT_EQUALS(lib_3np1_mod::C(99), 298);
    TS_ASSERT_EQUALS(lib_3np1_mod::C(100), 50);
    TS_ASSERT_EQUALS(lib_3np1_mod::C(101), 304);
  }


  void test__C_mask() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(lib_3np1_mod::C_mask(0, 7), 0);
    TS_ASSERT_EQUALS(lib_3np1_mod::C_mask(1, 7), 4);
    TS_ASSERT_EQUALS(lib_3np1_mod::C_mask(2, 7), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::C_mask(3, 7), 2);
    TS_ASSERT_EQUALS(lib_3np1_mod::C_mask(4, 7), 2);
    TS_ASSERT_EQUALS(lib_3np1_mod::C_mask(5, 7), 0);

    TS_ASSERT_EQUALS(lib_3np1_mod::C_mask(99, 127), 42);
    TS_ASSERT_EQUALS(lib_3np1_mod::C_mask(100, 127), 50);
    TS_ASSERT_EQUALS(lib_3np1_mod::C_mask(101, 127), 48);
  }


  void test__m2() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(lib_3np1_mod::m2(1), 0);
    TS_ASSERT_EQUALS(lib_3np1_mod::m2(2), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::m2(3), 0);
    TS_ASSERT_EQUALS(lib_3np1_mod::m2(4), 2);
    TS_ASSERT_EQUALS(lib_3np1_mod::m2(5), 0);

    TS_ASSERT_EQUALS(lib_3np1_mod::m2(98), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::m2(100), 2);
    TS_ASSERT_EQUALS(lib_3np1_mod::m2(102), 1);

    for (lib_3np1_mod::value_type n(1); n < 1000; n += 2) {  // odd numbers
      TS_ASSERT_EQUALS(lib_3np1_mod::m2(n), 0);
    }

    for (lib_3np1_mod::value_type n(2); n < 1000; n += 2) {  // even numbers
      if (n % 4 == 2) {
        TS_ASSERT_EQUALS(lib_3np1_mod::m2(n), 1);
      } else if (n % 8 == 4) {
        TS_ASSERT_EQUALS(lib_3np1_mod::m2(n), 2);
      } else if (n % 16 == 8) {
        TS_ASSERT_EQUALS(lib_3np1_mod::m2(n), 3);
      } else if (n % 32 == 16) {
        TS_ASSERT_EQUALS(lib_3np1_mod::m2(n), 4);
      } else if (n % 64 == 32) {
        TS_ASSERT_EQUALS(lib_3np1_mod::m2(n), 5);
      } else {
        TS_ASSERT_LESS_THAN_EQUALS(6, lib_3np1_mod::m2(n));
      }
    }

    for (unsigned int k(0);
         k < static_cast<unsigned int>
           (std::numeric_limits<lib_3np1_mod::value_type>::digits); ++k) {
      TS_ASSERT_EQUALS(lib_3np1_mod::m2
                       (static_cast<lib_3np1_mod::value_type>(1) << k), k);
    }
  }


  void test__F() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(lib_3np1_mod::F(0), 0);
    TS_ASSERT_EQUALS(lib_3np1_mod::F(1), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::F(2), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::F(3), 5);
    TS_ASSERT_EQUALS(lib_3np1_mod::F(4), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::F(5), 1);

    TS_ASSERT_EQUALS(lib_3np1_mod::F(99), 149);
    TS_ASSERT_EQUALS(lib_3np1_mod::F(100), 25);
    TS_ASSERT_EQUALS(lib_3np1_mod::F(101), 19);

    for (lib_3np1_mod::value_type n(1); n < 1000; ++n) {
      TS_ASSERT_EQUALS(lib_3np1_mod::F(n) % 2, 1);
    }
  }


  void test__F_mask() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(lib_3np1_mod::F_mask(0, 7), 0);
    TS_ASSERT_EQUALS(lib_3np1_mod::F_mask(1, 7), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::F_mask(2, 7), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::F_mask(3, 7), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::F_mask(4, 7), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::F_mask(5, 7), 0);

    TS_ASSERT_EQUALS(lib_3np1_mod::F_mask(99, 127), 21);
    TS_ASSERT_EQUALS(lib_3np1_mod::F_mask(100, 127), 25);
    TS_ASSERT_EQUALS(lib_3np1_mod::F_mask(101, 127), 3);

    for (lib_3np1_mod::value_type mask(0); mask < 1000; ++mask) {
      for (lib_3np1_mod::value_type n(1); n <= mask; ++n) {
        const lib_3np1_mod::value_type result(lib_3np1_mod::F_mask(n, mask));

        if (result != 0) {
          TS_ASSERT_EQUALS(result % 2, 1);
        }
      }
    }
  }


  void test__search_mask_exceptions() {
    SHOW_FUNC_NAME;

    {
      const std::set<lib_3np1_mod::value_type>
        exceptions(lib_3np1_mod::search_mask_exceptions(1));
      const std::set<lib_3np1_mod::value_type> correct{0, 1};

      TS_ASSERT_EQUALS(exceptions, correct);
    }

    {
      const std::set<lib_3np1_mod::value_type>
        exceptions(lib_3np1_mod::search_mask_exceptions(3));
      const std::set<lib_3np1_mod::value_type> correct{0, 1, 3};

      TS_ASSERT_EQUALS(exceptions, correct);
    }

    {
      const std::set<lib_3np1_mod::value_type>
        exceptions(lib_3np1_mod::search_mask_exceptions(7));
      const std::set<lib_3np1_mod::value_type> correct{0, 5};

      TS_ASSERT_EQUALS(exceptions, correct);
    }

    {
      const std::set<lib_3np1_mod::value_type>
        exceptions(lib_3np1_mod::search_mask_exceptions(15));
      const std::set<lib_3np1_mod::value_type> correct{0, 3, 5, 7, 9, 15};

      TS_ASSERT_EQUALS(exceptions, correct);
    }

    {
      const std::set<lib_3np1_mod::value_type>
        exceptions(lib_3np1_mod::search_mask_exceptions(31));
      const std::set<lib_3np1_mod::value_type> correct{0, 21};

      TS_ASSERT_EQUALS(exceptions, correct);
    }

    {
      const std::set<lib_3np1_mod::value_type>
        exceptions(lib_3np1_mod::search_mask_exceptions(255));
      const std::set<lib_3np1_mod::value_type> correct{0, 85, 227};

      TS_ASSERT_EQUALS(exceptions, correct);
    }

    {
      const std::set<lib_3np1_mod::value_type>
        exceptions(lib_3np1_mod::search_mask_exceptions(65535));
      const std::set<lib_3np1_mod::value_type>
        correct{0, 14563, 19417, 21845, 53399, 56635};

      TS_ASSERT_EQUALS(exceptions, correct);
    }
  }


  void test__search_mask_exceptions_32() {
#if false
    SHOW_FUNC_NAME;

    const std::set<lib_3np1_mod::value_type>
      exceptions(lib_3np1_mod::search_mask_exceptions_32());
    const std::set<lib_3np1_mod::value_type>
      correct{0, 1431655765u, 3817748707u};

    TS_ASSERT_EQUALS(exceptions, correct);
#endif
  }


  void test__search_mask_exceptions_64() {
#if false
    SHOW_FUNC_NAME;

    // ???
#endif
  }


  void test__start_path_mask__C() {
    SHOW_FUNC_NAME;

    {
      const std::vector<lib_3np1_mod::value_type> correct{0, 0};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(0,
                                                     lib_3np1_mod::C_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{1, 4, 2, 1};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(1,
                                                     lib_3np1_mod::C_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{2, 1};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(2,
                                                     lib_3np1_mod::C_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{3, 2};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(3,
                                                     lib_3np1_mod::C_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{4, 2};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(4,
                                                     lib_3np1_mod::C_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{5, 0};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(5,
                                                     lib_3np1_mod::C_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{6, 3};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(6,
                                                     lib_3np1_mod::C_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{7, 6};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(7,
                                                     lib_3np1_mod::C_mask, 7),
                       correct);
    }


    for (unsigned int i(1); i < 16; ++i) {
      const lib_3np1_mod::value_type mask((1u << i) - 1);

      for (lib_3np1_mod::value_type n(0); n <= mask; n += 2) {
        const std::vector<lib_3np1_mod::value_type> correct{n, n/2};

        TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(n,
                                                       lib_3np1_mod::C_mask,
                                                       mask),
                         correct);
      }
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{0, 0};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(0,
                                                     lib_3np1_mod::C_mask, 7),
                       correct);
    }

    for (unsigned int i(1); i < 32; ++i) {
      const std::vector<lib_3np1_mod::value_type> correct{0, 0};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(0, lib_3np1_mod::C_mask,
                                                     (1u << i) - 1),
                       correct);
    }
  }


  void test__start_path_mask__F() {
    SHOW_FUNC_NAME;

    {
      const std::vector<lib_3np1_mod::value_type> correct{0, 0};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(0,
                                                     lib_3np1_mod::F_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{1, 1};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(1,
                                                     lib_3np1_mod::F_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{2, 1};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(2,
                                                     lib_3np1_mod::F_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{3, 1};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(3,
                                                     lib_3np1_mod::F_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{4, 1};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(4,
                                                     lib_3np1_mod::F_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{5, 0};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(5,
                                                     lib_3np1_mod::F_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{6, 3};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(6,
                                                     lib_3np1_mod::F_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{7, 3};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(7,
                                                     lib_3np1_mod::F_mask, 7),
                       correct);
    }


    for (unsigned int i(1); i < 16; ++i) {
      const lib_3np1_mod::value_type mask((1u << i) - 1);

      for (lib_3np1_mod::value_type n(0); n <= mask; n += 2) {
        const std::vector<lib_3np1_mod::value_type>
          correct{n, (n == 0
                      ? 0
                      : n >> lib_3np1_mod::m2(n))};

        TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(n,
                                                       lib_3np1_mod::F_mask,
                                                       mask),
                         correct);
      }
    }
  }


  void test__start_path_mask__T() {
    SHOW_FUNC_NAME;

    {
      const std::vector<lib_3np1_mod::value_type> correct{0, 0};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(0,
                                                     lib_3np1_mod::T_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{1, 2, 1};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(1,
                                                     lib_3np1_mod::T_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{2, 1};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(2,
                                                     lib_3np1_mod::T_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{3, 1};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(3,
                                                     lib_3np1_mod::T_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{4, 2};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(4,
                                                     lib_3np1_mod::T_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{5, 0};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(5,
                                                     lib_3np1_mod::T_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{6, 3};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(6,
                                                     lib_3np1_mod::T_mask, 7),
                       correct);
    }
    {
      const std::vector<lib_3np1_mod::value_type> correct{7, 3};

      TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(7,
                                                     lib_3np1_mod::T_mask, 7),
                       correct);
    }


    for (unsigned int i(1); i < 16; ++i) {
      const lib_3np1_mod::value_type mask((1u << i) - 1);

      for (lib_3np1_mod::value_type n(0); n <= mask; n += 2) {
        const std::vector<lib_3np1_mod::value_type> correct{n, n/2};

        TS_ASSERT_EQUALS(lib_3np1_mod::start_path_mask(n,
                                                       lib_3np1_mod::T_mask,
                                                       mask),
                         correct);
      }
    }
  }


  void test__T() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(lib_3np1_mod::T(0), 0);
    TS_ASSERT_EQUALS(lib_3np1_mod::T(1), 2);
    TS_ASSERT_EQUALS(lib_3np1_mod::T(2), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::T(3), 5);
    TS_ASSERT_EQUALS(lib_3np1_mod::T(4), 2);
    TS_ASSERT_EQUALS(lib_3np1_mod::T(5), 8);

    TS_ASSERT_EQUALS(lib_3np1_mod::T(99), 149);
    TS_ASSERT_EQUALS(lib_3np1_mod::T(100), 50);
    TS_ASSERT_EQUALS(lib_3np1_mod::T(101), 152);
  }


  void test__T_mask() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(lib_3np1_mod::T_mask(0, 7), 0);
    TS_ASSERT_EQUALS(lib_3np1_mod::T_mask(1, 7), 2);
    TS_ASSERT_EQUALS(lib_3np1_mod::T_mask(2, 7), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::T_mask(3, 7), 1);
    TS_ASSERT_EQUALS(lib_3np1_mod::T_mask(4, 7), 2);
    TS_ASSERT_EQUALS(lib_3np1_mod::T_mask(5, 7), 0);

    TS_ASSERT_EQUALS(lib_3np1_mod::T_mask(99, 127), 21);
    TS_ASSERT_EQUALS(lib_3np1_mod::T_mask(100, 127), 50);
    TS_ASSERT_EQUALS(lib_3np1_mod::T_mask(101, 127), 24);
  }
};
