/* -*- coding: latin-1 -*- */
/** \file prog_3np1_mod.cpp
 * \brief
 * Usage: prog_3np1_mod k [C|F|T]
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iostream>
#include <limits>
#include <set>
#include <string>
#include <vector>

#include "lib_3np1_mod/lib_3np1_mod.hpp"



int
main(int argc, const char* const argv[]) {
  if (argc < 2) {
    return EXIT_FAILURE;
  }

  const unsigned int k(atoi(argv[1]));
  const lib_3np1_mod::value_type
    mask(k < static_cast<unsigned int>
             (std::numeric_limits<lib_3np1_mod::value_type>::digits)
         ? (static_cast<lib_3np1_mod::value_type>(1) << k) - 1
         : static_cast<lib_3np1_mod::value_type>(-1));

  std::string operation_name("T");

  lib_3np1_mod::operation_mask_type operation_mask(lib_3np1_mod::T_mask);

  if (argc >= 3) {
    operation_name = argv[2];

    if (operation_name == "C") {
      operation_mask = lib_3np1_mod::C_mask;
    } else if (operation_name == "F") {
      operation_mask = lib_3np1_mod::F_mask;
    } else if (operation_name != "T") {
      return EXIT_FAILURE;
    }
  }


  std::cout << "---------- Null and odd exceptions with modulo = 2^" << k
            << " = " << mask << " + 1 for " << operation_name
            << " function ----------" << std::endl;
  std::cout.flush();

  if ((k + 2 > static_cast<unsigned int>
       (std::numeric_limits<lib_3np1_mod::value_type>::digits))
      && (k != static_cast<unsigned int>
          (std::numeric_limits<lib_3np1_mod::value_type>::digits))) {
    std::cout << "! Require big integers. # digits precision: "
              << std::numeric_limits<lib_3np1_mod::value_type>::digits
              << std::endl;

    return EXIT_FAILURE;
  }

  const std::set<lib_3np1_mod::value_type>
    exceptions(lib_3np1_mod::search_mask_exceptions(mask));

  for (lib_3np1_mod::value_type n : exceptions) {
    std::cout << n;

    std::vector<lib_3np1_mod::value_type>
      path(lib_3np1_mod::start_path_mask(n, operation_mask, mask));

    for (auto it(path.cbegin() + 1); it != path.cend(); ++it) {
      std::cout << " -> " << *it;
    }

    std::cout << std::endl;
    std::cout.flush();
  }

  return EXIT_SUCCESS;
}
