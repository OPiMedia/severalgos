#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2015 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import os.path
import sys

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import sort


DATA_DIR = ('data/' if os.path.isdir('data/')
            else 'sort_py/data/')

data_14 = sort.read_data(DATA_DIR + '14.txt', convert=int)
data_14_str = sort.read_data(DATA_DIR + '14.txt')

data_10 = sort.read_data(DATA_DIR + '10.txt', convert=int)
data_100 = sort.read_data(DATA_DIR + '100.txt', convert=int)
data_1000 = sort.read_data(DATA_DIR + '1000.txt', convert=int)
data_10000 = sort.read_data(DATA_DIR + '10000.txt', convert=int)
data_100000 = sort.read_data(DATA_DIR + '100000.txt', convert=int)

data_10_duplicate = sort.read_data(DATA_DIR + '10_duplicate.txt',
                                   convert=int)
data_100_duplicate = sort.read_data(DATA_DIR + '100_duplicate.txt',
                                    convert=int)
data_1000_duplicate = sort.read_data(DATA_DIR + '1000_duplicate.txt',
                                     convert=int)
data_10000_duplicate = sort.read_data(DATA_DIR + '10000_duplicate.txt',
                                      convert=int)
data_100000_duplicate = sort.read_data(DATA_DIR + '100000_duplicate.txt',
                                       convert=int)

data_10_duplicate_nb_0 = data_10_duplicate.count(0)
data_100_duplicate_nb_0 = data_100_duplicate.count(0)
data_1000_duplicate_nb_0 = data_1000_duplicate.count(0)
data_10000_duplicate_nb_0 = data_10000_duplicate.count(0)
data_100000_duplicate_nb_0 = data_100000_duplicate.count(0)

data_20_double = sort.read_data(DATA_DIR + '20_double.txt', convert=int)
data_200_double = sort.read_data(DATA_DIR + '200_double.txt', convert=int)
data_2000_double = sort.read_data(DATA_DIR + '2000_double.txt', convert=int)
data_20000_double = sort.read_data(DATA_DIR + '20000_double.txt', convert=int)
data_200000_double = sort.read_data(DATA_DIR + '200000_double.txt',
                                    convert=int)

data_famous = sort.read_data(DATA_DIR + 'famous.txt')
data_films = sort.read_data(DATA_DIR + 'films.txt')


def test_read_data():
    assert data_14 == list(reversed(range(14)))
    assert data_14_str == [str(n) for n in reversed(range(14))]

    assert sorted(data_10) == list(range(10))
    assert sorted(data_100) == list(range(100))
    assert sorted(data_1000) == list(range(1000))
    assert sorted(data_10000) == list(range(10000))
    assert sorted(data_100000) == list(range(100000))
    assert sorted(sort.read_data(DATA_DIR + '100000.txt', convert=int)) \
        == list(range(100000))

    assert sorted(data_10_duplicate) == \
        [0]*data_10_duplicate_nb_0 + [1]*(10 - data_10_duplicate_nb_0)
    assert sorted(data_100_duplicate) == \
        [0]*data_100_duplicate_nb_0 + [1]*(100 - data_100_duplicate_nb_0)
    assert sorted(data_1000_duplicate) == \
        [0]*data_1000_duplicate_nb_0 + [1]*(1000 - data_1000_duplicate_nb_0)
    assert sorted(data_10000_duplicate) == \
        [0]*data_10000_duplicate_nb_0 + [1]*(10000 - data_10000_duplicate_nb_0)
    assert sorted(data_100000_duplicate) == \
        [0]*data_100000_duplicate_nb_0 + [1]*(100000
                                              - data_100000_duplicate_nb_0)
    data = sorted(sort.read_data(DATA_DIR + '100000_duplicate.txt',
                                 convert=int))
    data_nb_0 = data.count(0)
    assert data == [0]*data_nb_0 + [1]*(100000 - data_nb_0)

    del data

    assert sorted(data_20_double) == sorted(list(range(10)) + list(range(10)))
    assert sorted(data_200_double) == sorted(list(range(100))
                                             + list(range(100)))
    assert sorted(data_2000_double) == sorted(list(range(1000))
                                              + list(range(1000)))
    assert sorted(data_20000_double) == sorted(list(range(10000))
                                               + list(range(10000)))
    assert sorted(data_200000_double) == sorted(list(range(100000))
                                                + list(range(100000)))
    assert sorted(sort.read_data(DATA_DIR + '200000_double.txt',
                                 convert=int)) \
        == sorted(list(range(100000)) + list(range(100000)))


def test_is_sorted():
    data = []
    assert sort.is_sorted(data)

    data = [123]
    assert sort.is_sorted(data)

    data = [0, 1]
    assert sort.is_sorted(data)

    data = [1, 0]
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_14
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_14_str
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_10
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_100
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_1000
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_10000
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_100000
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_10_duplicate
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_100_duplicate
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_1000_duplicate
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_10000_duplicate
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_100000_duplicate
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_20_double
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_200_double
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_2000_double
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_20000_double
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_200000_double
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_famous
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))

    data = data_films
    assert not sort.is_sorted(data)
    assert sort.is_sorted(sorted(data))


def test_insertion_sort():
    data = []
    assert sort.insertion_sort(data) == (0, 0)
    assert data == []

    data = [123]
    assert sort.insertion_sort(data) == (0, 0)
    assert data == [123]

    data = [0, 1]
    assert sort.insertion_sort(data) == (1, 0)
    assert data == [0, 1]

    data = [1, 0]
    assert sort.insertion_sort(data) == (1, 1)
    assert data == [0, 1]

    data = list(data_14)
    sort.insertion_sort(data)
    assert data == list(range(14))
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    sort.insertion_sort(data, less_fct=sort.less_reverse)
    assert data == list(reversed(range(14)))
    assert sort.insertion_sort(data, less_fct=sort.less_reverse) \
        == (len(data) - 1, 0)

    data = list(data_14_str)
    sort.insertion_sort(data)
    assert data == '0 1 10 11 12 13 2 3 4 5 6 7 8 9'.split()
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_10)
    sort.insertion_sort(data)
    assert data == list(range(10))
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_100)
    sort.insertion_sort(data)
    assert data == list(range(100))
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_1000)
    sort.insertion_sort(data)
    assert data == list(range(1000))
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_10_duplicate)
    sort.insertion_sort(data)
    assert data == sorted(data_10_duplicate)
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_100_duplicate)
    sort.insertion_sort(data)
    assert data == sorted(data_100_duplicate)
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_20_double)
    sort.insertion_sort(data)
    assert data == sorted(list(range(10)) + list(range(10)))
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_200_double)
    sort.insertion_sort(data)
    assert data == sorted(list(range(100)) + list(range(100)))
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_2000_double)
    sort.insertion_sort(data)
    assert data == sorted(list(range(1000)) + list(range(1000)))
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_famous)
    sort.insertion_sort(data)
    assert data == sorted(data)
    assert sort.insertion_sort(data) == (len(data) - 1, 0)

    data = list(data_films)
    sort.insertion_sort(data)
    assert data == sorted(data)
    assert sort.insertion_sort(data) == (len(data) - 1, 0)


def test_merge():
    data1 = []
    data2 = []
    assert sort.merge(data1, data2) == ([], 0)
    assert data1 == []
    assert data2 == []

    for data in ([], [123], [0, 1],
                 data_14, data_14_str, data_100, data_1000,
                 data_famous, data_films):
        data = sorted(data)

        data1 = []
        data2 = list(data)

        assert sort.merge(data1, data2) == (data, 0)
        assert data1 == []
        assert data2 == data

        assert sort.merge(data2, data1) == (data, 0)
        assert data1 == []
        assert data2 == data

    data1 = [1]
    data2 = [0, 2]
    assert sort.merge(data1, data2)[0] == [0, 1, 2]
    assert data1 == [1]
    assert data2 == [0, 2]
    assert sort.merge(data2, data1)[0] == [0, 1, 2]
    assert data1 == [1]
    assert data2 == [0, 2]

    data1 = [0, 2, 3, 9]
    data2 = [1, 4, 5, 6, 7, 8]
    assert sort.merge(data1, data2)[0] == list(range(10))
    assert data1 == [0, 2, 3, 9]
    assert data2 == [1, 4, 5, 6, 7, 8]
    assert sort.merge(data2, data1)[0] == list(range(10))
    assert data1 == [0, 2, 3, 9]
    assert data2 == [1, 4, 5, 6, 7, 8]


def test_merge_sort():
    data = []
    assert sort.merge_sort(data) == (0, 0)
    assert data == []

    data = [123]
    assert sort.merge_sort(data) == (0, 0)
    assert data == [123]

    data = [0, 1]
    assert sort.merge_sort(data) == (0, 0)
    assert data == [0, 1]

    data = [1, 0]
    assert sort.merge_sort(data) == (1, 1)
    assert data == [0, 1]

    data = list(data_14)
    sort.merge_sort(data)
    assert data == list(range(14))
    assert sort.merge_sort(data) == (0, 0)

    sort.merge_sort(data, less_fct=sort.less_reverse)
    assert data == list(reversed(range(14)))
    assert sort.merge_sort(data, less_fct=sort.less_reverse) == (0, 0)

    data = list(data_14_str)
    sort.merge_sort(data)
    assert data == '0 1 10 11 12 13 2 3 4 5 6 7 8 9'.split()
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_10)
    sort.merge_sort(data)
    assert data == list(range(10))
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_100)
    sort.merge_sort(data)
    assert data == list(range(100))
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_1000)
    sort.merge_sort(data)
    assert data == list(range(1000))
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_10_duplicate)
    sort.merge_sort(data)
    assert data == sorted(data_10_duplicate)
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_100_duplicate)
    sort.merge_sort(data)
    assert data == sorted(data_100_duplicate)
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_20_double)
    sort.merge_sort(data)
    assert data == sorted(list(range(10)) + list(range(10)))
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_200_double)
    sort.merge_sort(data)
    assert data == sorted(list(range(100)) + list(range(100)))
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_2000_double)
    sort.merge_sort(data)
    assert data == sorted(list(range(1000)) + list(range(1000)))
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_famous)
    sort.merge_sort(data)
    assert data == sorted(data)
    assert sort.merge_sort(data) == (0, 0)

    data = list(data_films)
    sort.merge_sort(data)
    assert data == sorted(data)
    assert sort.merge_sort(data) == (0, 0)


def test_partition():
    data = [0, 0]
    assert sort.partition(data, 0, 1) == (1, 2, 1)
    assert data == [0, 0]

    data = [0, 0, 0]
    assert sort.partition(data, 0, 2) == (1, 4, 2)
    assert data == [0, 0, 0]

    data = [0, 1]
    assert sort.partition(data, 0, 1) == (0, 3, 1)
    assert data == [0, 1]

    data = [1, 0]
    assert sort.partition(data, 0, 1) == (1, 2, 1)
    assert data == [0, 1]

    data = [0, 1, 2]
    assert sort.partition(data, 0, 2) == (0, 4, 1)
    assert data == [0, 1, 2]

    data = [0, 2, 1]
    assert sort.partition(data, 0, 2) == (0, 4, 1)
    assert data == [0, 2, 1]

    data = [1, 0, 2]
    assert sort.partition(data, 0, 2) == (1, 4, 1)
    assert data == [0, 1, 2]

    data = [1, 2, 0]
    assert sort.partition(data, 0, 2) == (1, 4, 2)
    assert data == [0, 1, 2]

    data = [2, 0, 1]
    assert sort.partition(data, 0, 2) == (2, 3, 1)
    assert data == [1, 0, 2]

    data = [2, 1, 0]
    assert sort.partition(data, 0, 2) == (2, 3, 1)
    assert data == [0, 1, 2]

    data = list(data_10)
    pivot = data[0]
    assert sort.partition(data, 0, len(data) - 1)[0] == pivot
    assert data[pivot] == pivot
    assert sorted(data[:pivot]) == list(range(pivot))
    assert sorted(data[pivot + 1:]) == list(range(pivot + 1, 10))

    data = list(data_100)
    pivot = data[0]
    assert sort.partition(data, 0, len(data) - 1)[0] == pivot
    assert data[pivot] == pivot
    assert sorted(data[:pivot]) == list(range(pivot))
    assert sorted(data[pivot + 1:]) == list(range(pivot + 1, 100))

    data = list(data_1000)
    pivot = data[0]
    assert sort.partition(data, 0, len(data) - 1)[0] == pivot
    assert data[pivot] == pivot
    assert sorted(data[:pivot]) == list(range(pivot))
    assert sorted(data[pivot + 1:]) == list(range(pivot + 1, 1000))

    data = list(data_10_duplicate)
    pivot = data[0]
    i_pivot = sort.partition(data, 0, len(data) - 1)[0]
    assert data[i_pivot] == pivot
    for i in range(i_pivot):
        assert data[i] <= pivot
    for i in range(i_pivot + 1, 10):
        assert data[i] >= pivot

    data = list(data_100_duplicate)
    pivot = data[0]
    i_pivot = sort.partition(data, 0, len(data) - 1)[0]
    assert data[i_pivot] == pivot
    for i in range(i_pivot):
        assert data[i] <= pivot
    for i in range(i_pivot + 1, 100):
        assert data[i] >= pivot

    data = list(data_1000_duplicate)
    pivot = data[0]
    i_pivot = sort.partition(data, 0, len(data) - 1)[0]
    assert data[i_pivot] == pivot
    for i in range(i_pivot):
        assert data[i] <= pivot
    for i in range(i_pivot + 1, 1000):
        assert data[i] >= pivot

    data = list(data_20_double)
    pivot = data[0]
    i_pivot = sort.partition(data, 0, len(data) - 1)[0]
    assert (i_pivot == 2*pivot) or (i_pivot == 2*pivot + 1)
    assert data[i_pivot] == pivot
    for i in range(i_pivot):
        assert data[i] <= pivot
    for i in range(i_pivot + 1, 20):
        assert data[i] >= pivot

    data = list(data_200_double)
    pivot = data[0]
    i_pivot = sort.partition(data, 0, len(data) - 1)[0]
    assert (i_pivot == 2*pivot) or (i_pivot == 2*pivot + 1)
    assert data[i_pivot] == pivot
    for i in range(i_pivot):
        assert data[i] <= pivot
    for i in range(i_pivot + 1, 200):
        assert data[i] >= pivot

    data = list(data_2000_double)
    pivot = data[0]
    i_pivot = sort.partition(data, 0, len(data) - 1)[0]
    assert (i_pivot == 2*pivot) or (i_pivot == 2*pivot + 1)
    assert data[i_pivot] == pivot
    for i in range(i_pivot):
        assert data[i] <= pivot
    for i in range(i_pivot + 1, 2000):
        assert data[i] >= pivot


def test_partition_3_way():
    data = [0, 0]
    assert sort.partition_3_way(data, 0, 1) == (0, 1, 2, 0)
    assert data == [0, 0]

    data = [0, 0, 0]
    assert sort.partition_3_way(data, 0, 2) == (0, 2, 4, 0)
    assert data == [0, 0, 0]

    data = [0, 1]
    assert sort.partition_3_way(data, 0, 1) == (0, 0, 2, 1)
    assert data == [0, 1]

    data = [1, 0]
    assert sort.partition_3_way(data, 0, 1) == (1, 1, 1, 1)
    assert data == [0, 1]

    data = [0, 1, 2]
    assert sort.partition_3_way(data, 0, 2) == (0, 0, 4, 2)
    assert data == [0, 2, 1]

    data = [0, 2, 1]
    assert sort.partition_3_way(data, 0, 2) == (0, 0, 4, 2)
    assert data == [0, 1, 2]

    data = [1, 0, 2]
    assert sort.partition_3_way(data, 0, 2) == (1, 1, 3, 2)
    assert data == [0, 1, 2]

    data = [1, 2, 0]
    assert sort.partition_3_way(data, 0, 2) == (1, 1, 3, 2)
    assert data == [0, 1, 2]

    data = [2, 0, 1]
    assert sort.partition_3_way(data, 0, 2) == (2, 2, 2, 2)
    assert data == [0, 1, 2]

    data = [2, 1, 0]
    assert sort.partition_3_way(data, 0, 2) == (2, 2, 2, 2)
    assert data == [1, 0, 2]

    data = list(data_10)
    pivot = data[0]
    first, last, _, _ = sort.partition_3_way(data, 0, len(data) - 1)
    assert first == pivot
    assert last == pivot
    assert data[first] == pivot
    assert sorted(data[:pivot]) == list(range(pivot))
    assert sorted(data[pivot + 1:]) == list(range(pivot + 1, 10))

    data = list(data_100)
    pivot = data[0]
    first, last, _, _ = sort.partition_3_way(data, 0, len(data) - 1)
    assert first == pivot
    assert last == pivot
    assert data[first] == pivot
    assert sorted(data[:pivot]) == list(range(pivot))
    assert sorted(data[pivot + 1:]) == list(range(pivot + 1, 100))

    data = list(data_1000)
    pivot = data[0]
    first, last, _, _ = sort.partition_3_way(data, 0, len(data) - 1)
    assert first == pivot
    assert last == pivot
    assert data[pivot] == pivot
    assert sorted(data[:pivot]) == list(range(pivot))
    assert sorted(data[pivot + 1:]) == list(range(pivot + 1, 1000))

    data = list(data_10_duplicate)
    pivot = data[0]
    first, last, _, _ = sort.partition_3_way(data, 0, len(data) - 1)
    assert 0 <= first < last < 10
    for i in range(first):
        assert data[i] < pivot
    for i in range(first, last):
        assert data[i] == pivot
    for i in range(last + 1, 10):
        assert data[i] > pivot

    data = list(data_100_duplicate)
    pivot = data[0]
    first, last, _, _ = sort.partition_3_way(data, 0, len(data) - 1)
    assert 0 <= first < last < 100
    for i in range(first):
        assert data[i] < pivot
    for i in range(first, last):
        assert data[i] == pivot
    for i in range(last + 1, 100):
        assert data[i] > pivot

    data = list(data_1000_duplicate)
    pivot = data[0]
    first, last, _, _ = sort.partition_3_way(data, 0, len(data) - 1)
    assert 0 <= first < last < 1000
    for i in range(first):
        assert data[i] < pivot
    for i in range(first, last):
        assert data[i] == pivot
    for i in range(last + 1, 1000):
        assert data[i] > pivot

    data = list(data_20_double)
    pivot = data[0]
    first, last, _, _ = sort.partition_3_way(data, 0, len(data) - 1)
    assert first == 2*pivot
    assert last == first + 1
    for i in range(first):
        assert data[i] < pivot
    for i in range(first, last):
        assert data[i] == pivot
    for i in range(last + 1, 20):
        assert data[i] > pivot

    data = list(data_200_double)
    pivot = data[0]
    first, last, _, _ = sort.partition_3_way(data, 0, len(data) - 1)
    assert first == 2*pivot
    assert last == first + 1
    for i in range(first):
        assert data[i] < pivot
    for i in range(first, last):
        assert data[i] == pivot
    for i in range(last + 1, 200):
        assert data[i] > pivot

    data = list(data_2000_double)
    pivot = data[0]
    first, last, _, _ = sort.partition_3_way(data, 0, len(data) - 1)
    assert first == 2*pivot
    assert last == first + 1
    for i in range(first):
        assert data[i] < pivot
    for i in range(first, last):
        assert data[i] == pivot
    for i in range(last + 1, 2000):
        assert data[i] > pivot


def test_partition_5_way():
    data = [0, 0]
    assert sort.partition_5_way(data, 0, 1) == (0, 0, 1, 1, 1, 0)
    assert data == [0, 0]

    data = [0, 0, 0]
    assert sort.partition_5_way(data, 0, 2) == (0, 0, 1, 2, 4, 1)
    assert data == [0, 0, 0]

    data = [0, 1]
    assert sort.partition_5_way(data, 0, 1) == (0, 0, 1, 1, 1, 0)
    assert data == [0, 1]

    data = [1, 0]
    assert sort.partition_5_way(data, 0, 1) == (0, 0, 1, 1, 1, 1)
    assert data == [0, 1]

    data = [0, 1, 2]
    assert sort.partition_5_way(data, 0, 2) == (0, 0, 2, 2, 4, 0)
    assert data == [0, 1, 2]

    data = [0, 2, 1]
    assert sort.partition_5_way(data, 0, 2) == (0, 0, 1, 1, 4, 2)
    assert data == [0, 1, 2]

    data = [1, 0, 2]
    assert sort.partition_5_way(data, 0, 2) == (1, 1, 2, 2, 2, 2)
    assert data == [0, 1, 2]

    data = [1, 2, 0]
    assert sort.partition_5_way(data, 0, 2) == (0, 0, 1, 1, 4, 3)
    assert data == [0, 1, 2]

    data = [2, 0, 1]
    assert sort.partition_5_way(data, 0, 2) == (1, 1, 2, 2, 2, 3)
    assert data == [0, 1, 2]

    data = [2, 1, 0]
    assert sort.partition_5_way(data, 0, 2) == (0, 0, 2, 2, 4, 1)
    assert data == [0, 1, 2]

    data = list(data_10)
    pivot_p = data[-1]
    pivot_q = data[0]
    assert pivot_p < pivot_q
    first_p, last_p, first_q, last_q, _, _ \
        = sort.partition_5_way(data, 0, len(data) - 1)
    assert first_p == pivot_p
    assert last_p == pivot_p
    assert data[first_p] == pivot_p
    assert sorted(data[:pivot_p]) == list(range(pivot_p))
    assert sorted(data[pivot_p + 1:]) == list(range(pivot_p + 1, 10))
    assert first_q == pivot_q
    assert last_q == pivot_q
    assert data[first_q] == pivot_q
    assert sorted(data[:pivot_q]) == list(range(pivot_q))
    assert sorted(data[pivot_q + 1:]) == list(range(pivot_q + 1, 10))

    data = list(data_100)
    pivot_p = data[-1]
    pivot_q = data[0]
    assert pivot_p < pivot_q
    first_p, last_p, first_q, last_q, _, _ \
        = sort.partition_5_way(data, 0, len(data) - 1)
    assert first_p == pivot_p
    assert last_p == pivot_p
    assert data[first_p] == pivot_p
    assert sorted(data[:pivot_p]) == list(range(pivot_p))
    assert sorted(data[pivot_p + 1:]) == list(range(pivot_p + 1, 100))
    assert first_q == pivot_q
    assert last_q == pivot_q
    assert data[first_q] == pivot_q
    assert sorted(data[:pivot_q]) == list(range(pivot_q))
    assert sorted(data[pivot_q + 1:]) == list(range(pivot_q + 1, 100))

    data = list(data_1000)
    pivot_p = data[0]
    pivot_q = data[-1]
    assert pivot_p < pivot_q
    first_p, last_p, first_q, last_q, _, _ \
        = sort.partition_5_way(data, 0, len(data) - 1)
    assert first_p == pivot_p
    assert last_p == pivot_p
    assert data[first_p] == pivot_p
    assert sorted(data[:pivot_p]) == list(range(pivot_p))
    assert sorted(data[pivot_p + 1:]) == list(range(pivot_p + 1, 1000))
    assert first_q == pivot_q
    assert last_q == pivot_q
    assert data[first_q] == pivot_q
    assert sorted(data[:pivot_q]) == list(range(pivot_q))
    assert sorted(data[pivot_q + 1:]) == list(range(pivot_q + 1, 1000))

    data = list(data_10_duplicate)
    pivot_p = data[0]
    pivot_q = data[-1]
    assert pivot_p < pivot_q
    first_p, last_p, first_q, last_q, _, _ \
        = sort.partition_5_way(data, 0, len(data) - 1)
    assert 0 <= first_p < last_p < 10
    for i in range(first_p):
        assert data[i] < pivot_p
    for i in range(first_p, last_p):
        assert data[i] == pivot_p
    for i in range(last_p + 1, 10):
        assert data[i] > pivot_p
    assert 0 <= first_q < last_q < 10
    for i in range(first_q):
        assert data[i] < pivot_q
    for i in range(first_q, last_q):
        assert data[i] == pivot_q
    for i in range(last_q + 1, 10):
        assert data[i] > pivot_q

    data = list(data_100_duplicate)
    pivot_p = data[0]
    pivot_q = data[-1]
    assert pivot_p <= pivot_q
    first_p, last_p, first_q, last_q, _, _ \
        = sort.partition_5_way(data, 0, len(data) - 1)
    assert 0 <= first_p <= last_p < 100
    for i in range(first_p):
        assert data[i] < pivot_p
    for i in range(first_p, last_p):
        assert data[i] == pivot_p
    for i in range(last_p + 1, 100):
        assert data[i] >= pivot_p
    assert 0 <= first_q < last_q < 100
    for i in range(first_q):
        assert data[i] <= pivot_q
    for i in range(first_q, last_q):
        assert data[i] == pivot_q
    for i in range(last_q + 1, 100):
        assert data[i] > pivot_q

    data = list(data_1000_duplicate)
    pivot_p = data[0]
    pivot_q = data[-1]
    assert pivot_p <= pivot_q
    first_p, last_p, first_q, last_q, _, _ \
        = sort.partition_5_way(data, 0, len(data) - 1)
    assert 0 <= first_p <= last_p < 100
    for i in range(first_p):
        assert data[i] < pivot_p
    for i in range(first_p, last_p):
        assert data[i] == pivot_p
    for i in range(last_p + 1, 1000):
        assert data[i] >= pivot_p
    assert 0 <= first_q < last_q < 1000
    for i in range(first_q):
        assert data[i] <= pivot_q
    for i in range(first_q, last_q):
        assert data[i] == pivot_q
    for i in range(last_q + 1, 1000):
        assert data[i] > pivot_q

    data = list(data_20_double)
    pivot_p = data[-1]
    pivot_q = data[0]
    assert pivot_p < pivot_q
    first_p, last_p, first_q, last_q, _, _ \
        = sort.partition_5_way(data, 0, len(data) - 1)
    assert first_p == 2*pivot_p
    assert last_p == first_p + 1
    for i in range(first_p):
        assert data[i] < pivot_p
    for i in range(first_p, last_p):
        assert data[i] == pivot_p
    for i in range(last_p + 1, 20):
        assert data[i] > pivot_p
    assert first_q == 2*pivot_q
    assert last_q == first_q + 1
    for i in range(first_q):
        assert data[i] < pivot_q
    for i in range(first_q, last_q):
        assert data[i] == pivot_q
    for i in range(last_q + 1, 20):
        assert data[i] > pivot_q

    data = list(data_200_double)
    pivot_p = data[-1]
    pivot_q = data[0]
    assert pivot_p < pivot_q
    first_p, last_p, first_q, last_q, _, _ \
        = sort.partition_5_way(data, 0, len(data) - 1)
    assert first_p == 2*pivot_p
    assert last_p == first_p + 1
    for i in range(first_p):
        assert data[i] < pivot_p
    for i in range(first_p, last_p):
        assert data[i] == pivot_p
    for i in range(last_p + 1, 200):
        assert data[i] > pivot_p
    assert first_q == 2*pivot_q
    assert last_q == first_q + 1
    for i in range(first_q):
        assert data[i] < pivot_q
    for i in range(first_q, last_q):
        assert data[i] == pivot_q
    for i in range(last_q + 1, 20):
        assert data[i] > pivot_q

    data = list(data_2000_double)
    pivot_p = data[0]
    pivot_q = data[-1]
    assert pivot_p < pivot_q
    first_p, last_p, first_q, last_q, _, _ \
        = sort.partition_5_way(data, 0, len(data) - 1)
    assert first_p == 2*pivot_p
    assert last_p == first_p + 1
    for i in range(first_p):
        assert data[i] < pivot_p
    for i in range(first_p, last_p):
        assert data[i] == pivot_p
    for i in range(last_p + 1, 2000):
        assert data[i] > pivot_p
    assert first_q == 2*pivot_q
    assert last_q == first_q + 1
    for i in range(first_q):
        assert data[i] < pivot_q
    for i in range(first_q, last_q):
        assert data[i] == pivot_q
    for i in range(last_q + 1, 20):
        assert data[i] > pivot_q


def test_quick_sort():
    data = []
    assert sort.quick_sort(data) == (0, 0, 0, 0)
    assert data == []

    data = [123]
    assert sort.quick_sort(data) == (0, 0, 0, 0)
    assert data == [123]

    data = [0, 1]
    sort.quick_sort(data)
    assert data == [0, 1]

    data = [1, 0]
    sort.quick_sort(data)
    assert data == [0, 1]

    data = list(data_14)
    sort.quick_sort(data)
    assert data == list(range(14))

    sort.quick_sort(data, less_fct=sort.less_reverse)
    assert data == list(reversed(range(14)))

    data = list(data_14_str)
    sort.quick_sort(data)
    assert data == '0 1 10 11 12 13 2 3 4 5 6 7 8 9'.split()

    data = list(data_10)
    sort.quick_sort(data)
    assert data == list(range(10))

    data = list(data_100)
    sort.quick_sort(data)
    assert data == list(range(100))

    data = list(data_1000)
    sort.quick_sort(data)
    assert data == list(range(1000))

    data = list(data_10_duplicate)
    sort.quick_sort(data)
    assert data == sorted(data_10_duplicate)

    data = list(data_100_duplicate)
    sort.quick_sort(data)
    assert data == sorted(data_100_duplicate)

    data = list(data_1000_duplicate)
    sort.quick_sort(data)
    assert data == sorted(data_1000_duplicate)

    data = list(data_20_double)
    sort.quick_sort(data)
    assert data == sorted(list(range(10)) + list(range(10)))

    data = list(data_200_double)
    sort.quick_sort(data)
    assert data == sorted(list(range(100)) + list(range(100)))

    data = list(data_2000_double)
    sort.quick_sort(data)
    assert data == sorted(list(range(1000)) + list(range(1000)))

    data = list(data_famous)
    sort.quick_sort(data)
    assert data == sorted(data)

    data = list(data_films)
    sort.quick_sort(data)
    assert data == sorted(data)


def test_quick_sort_3_way():
    data = []
    assert sort.quick_sort_3_way(data) == (0, 0, 0, 0)
    assert data == []

    data = [123]
    assert sort.quick_sort_3_way(data) == (0, 0, 0, 0)
    assert data == [123]

    data = [0, 1]
    sort.quick_sort_3_way(data)
    assert data == [0, 1]

    data = [1, 0]
    sort.quick_sort_3_way(data)
    assert data == [0, 1]

    data = list(data_14)
    sort.quick_sort_3_way(data)
    assert data == list(range(14))

    sort.quick_sort_3_way(data, less_fct=sort.less_reverse)
    assert data == list(reversed(range(14)))

    data = list(data_14_str)
    sort.quick_sort_3_way(data)
    assert data == '0 1 10 11 12 13 2 3 4 5 6 7 8 9'.split()

    data = list(data_10)
    sort.quick_sort_3_way(data)
    assert data == list(range(10))

    data = list(data_100)
    sort.quick_sort_3_way(data)
    assert data == list(range(100))

    data = list(data_1000)
    sort.quick_sort_3_way(data)
    assert data == list(range(1000))

    data = list(data_10_duplicate)
    sort.quick_sort_3_way(data)
    assert data == sorted(data_10_duplicate)

    data = list(data_100_duplicate)
    sort.quick_sort_3_way(data)
    assert data == sorted(data_100_duplicate)

    data = list(data_1000_duplicate)
    sort.quick_sort_3_way(data)
    assert data == sorted(data_1000_duplicate)

    data = list(data_20_double)
    sort.quick_sort_3_way(data)
    assert data == sorted(list(range(10)) + list(range(10)))

    data = list(data_200_double)
    sort.quick_sort_3_way(data)
    assert data == sorted(list(range(100)) + list(range(100)))

    data = list(data_2000_double)
    sort.quick_sort_3_way(data)
    assert data == sorted(list(range(1000)) + list(range(1000)))

    data = list(data_famous)
    sort.quick_sort_3_way(data)
    assert data == sorted(data)

    data = list(data_films)
    sort.quick_sort_3_way(data)
    assert data == sorted(data)


def test_quick_sort_5_way():
    data = []
    assert sort.quick_sort_5_way(data) == (0, 0, 0, 0)
    assert data == []

    data = [123]
    assert sort.quick_sort_5_way(data) == (0, 0, 0, 0)
    assert data == [123]

    data = [0, 1]
    sort.quick_sort_5_way(data)
    assert data == [0, 1]

    data = [1, 0]
    sort.quick_sort_5_way(data)
    assert data == [0, 1]

    data = list(data_14)
    sort.quick_sort_5_way(data)
    assert data == list(range(14))

    sort.quick_sort_5_way(data, less_fct=sort.less_reverse)
    assert data == list(reversed(range(14)))

    data = list(data_14_str)
    sort.quick_sort_5_way(data)
    assert data == '0 1 10 11 12 13 2 3 4 5 6 7 8 9'.split()

    data = list(data_10)
    sort.quick_sort_5_way(data)
    assert data == list(range(10))

    data = list(data_100)
    sort.quick_sort_5_way(data)
    assert data == list(range(100))

    data = list(data_1000)
    sort.quick_sort_5_way(data)
    assert data == list(range(1000))

    data = list(data_10_duplicate)
    sort.quick_sort_5_way(data)
    assert data == sorted(data_10_duplicate)

    data = list(data_100_duplicate)
    sort.quick_sort_5_way(data)
    assert data == sorted(data_100_duplicate)

    data = list(data_1000_duplicate)
    sort.quick_sort_5_way(data)
    assert data == sorted(data_1000_duplicate)

    data = list(data_20_double)
    sort.quick_sort_5_way(data)
    assert data == sorted(list(range(10)) + list(range(10)))

    data = list(data_200_double)
    sort.quick_sort_5_way(data)
    assert data == sorted(list(range(100)) + list(range(100)))

    data = list(data_2000_double)
    sort.quick_sort_5_way(data)
    assert data == sorted(list(range(1000)) + list(range(1000)))

    data = list(data_famous)
    sort.quick_sort_5_way(data)
    assert data == sorted(data)

    data = list(data_films)
    sort.quick_sort_5_way(data)
    assert data == sorted(data)


def test_selection_sort():
    data = []
    assert sort.selection_sort(data) == (0, 0)
    assert data == []

    data = [123]
    assert sort.selection_sort(data) == (0, 0)
    assert data == [123]

    data = [0, 1]
    assert sort.selection_sort(data) == (1, 0)
    assert data == [0, 1]

    data = [1, 0]
    assert sort.selection_sort(data) == (1, 1)
    assert data == [0, 1]

    data = list(data_14)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == list(range(14))
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    nb_comparisons, _ = sort.selection_sort(data, less_fct=sort.less_reverse)
    assert data == list(reversed(range(14)))
    assert sort.selection_sort(data, less_fct=sort.less_reverse) \
        == (nb_comparisons, 0)

    data = list(data_14_str)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == '0 1 10 11 12 13 2 3 4 5 6 7 8 9'.split()
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_10)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == list(range(10))
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_100)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == list(range(100))
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_1000)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == list(range(1000))
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_10_duplicate)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == sorted(data_10_duplicate)
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_100_duplicate)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == sorted(data_100_duplicate)
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_20_double)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == sorted(list(range(10)) + list(range(10)))
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_200_double)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == sorted(list(range(100)) + list(range(100)))
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_2000_double)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == sorted(list(range(1000)) + list(range(1000)))
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_famous)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == sorted(data)
    assert sort.selection_sort(data) == (nb_comparisons, 0)

    data = list(data_films)
    nb_comparisons, _ = sort.selection_sort(data)
    assert data == sorted(data)
    assert sort.selection_sort(data) == (nb_comparisons, 0)


def test_shell_gap_sequence():
    assert tuple(sort.shell_gap_sequence(1)) == ()

    assert tuple(sort.shell_gap_sequence(9)) == (1, 4, 7)
    assert tuple(sort.shell_gap_sequence(10)) == (1, 4, 7)
    assert tuple(sort.shell_gap_sequence(11)) == (1, 4, 7, 10)

    for length in range(1, 100):
        assert tuple(sort.shell_gap_sequence(length)) \
            == tuple(range(1, length, 3))

    assert tuple(sort.shell_gap_sequence(1, 5)) == ()

    assert tuple(sort.shell_gap_sequence(10, 5)) == (1, 6)
    assert tuple(sort.shell_gap_sequence(11, 5)) == (1, 6)
    assert tuple(sort.shell_gap_sequence(12, 5)) == (1, 6, 11)

    for length in range(1, 100):
        assert tuple(sort.shell_gap_sequence(length, 5)) \
            == tuple(range(1, length, 5))

    for step in range(2, 100):
        for length in range(1, 100):
            assert tuple(sort.shell_gap_sequence(length, step)) \
                == tuple(range(1, length, step))


def test_shell_sort():
    data = []
    assert sort.shell_sort(data) == (0, 0)
    assert data == []

    data = [123]
    assert sort.shell_sort(data) == (0, 0)
    assert data == [123]

    data = [0, 1]
    assert sort.shell_sort(data) == (1, 0)
    assert data == [0, 1]

    data = [1, 0]
    assert sort.shell_sort(data) == (1, 1)
    assert data == [0, 1]

    data = list(data_14)
    sort.shell_sort(data)
    assert data == list(range(14))
    assert sort.shell_sort(data)[1] == 0

    sort.shell_sort(data, less_fct=sort.less_reverse)
    assert data == list(reversed(range(14)))
    assert sort.shell_sort(data, less_fct=sort.less_reverse)[1] == 0

    data = list(data_14_str)
    sort.shell_sort(data)
    assert data == '0 1 10 11 12 13 2 3 4 5 6 7 8 9'.split()
    assert sort.shell_sort(data)[1] == 0

    data = list(data_10)
    sort.shell_sort(data)
    assert data == list(range(10))
    assert sort.shell_sort(data)[1] == 0

    data = list(data_100)
    sort.shell_sort(data)
    assert data == list(range(100))
    assert sort.shell_sort(data)[1] == 0

    data = list(data_1000)
    sort.shell_sort(data)
    assert data == list(range(1000))
    assert sort.shell_sort(data)[1] == 0

    data = list(data_10_duplicate)
    sort.shell_sort(data)
    assert data == sorted(data_10_duplicate)
    assert sort.shell_sort(data)[1] == 0

    data = list(data_100_duplicate)
    sort.shell_sort(data)
    assert data == sorted(data_100_duplicate)
    assert sort.shell_sort(data)[1] == 0

    data = list(data_20_double)
    sort.shell_sort(data)
    assert data == sorted(list(range(10)) + list(range(10)))
    assert sort.shell_sort(data)[1] == 0

    data = list(data_200_double)
    sort.shell_sort(data)
    assert data == sorted(list(range(100)) + list(range(100)))
    assert sort.shell_sort(data)[1] == 0

    data = list(data_2000_double)
    sort.shell_sort(data)
    assert data == sorted(list(range(1000)) + list(range(1000)))
    assert sort.shell_sort(data)[1] == 0

    data = list(data_famous)
    sort.shell_sort(data)
    assert data == sorted(data)
    assert sort.shell_sort(data)[1] == 0

    data = list(data_films)
    sort.shell_sort(data)
    assert data == sorted(data)
    assert sort.shell_sort(data)[1] == 0

    data = list(data_10)
    sort.shell_sort(data, gap_sequence=sort.shell_gap_sequence(10, 7))
    assert data == list(range(10))
    assert sort.shell_sort(data)[1] == 0

    data = list(data_100)
    sort.shell_sort(data, gap_sequence=sort.shell_gap_sequence(100, 7))
    assert data == list(range(100))
    assert sort.shell_sort(data)[1] == 0

    data = list(data_1000)
    sort.shell_sort(data, gap_sequence=sort.shell_gap_sequence(1000, 7))
    assert data == list(range(1000))
    assert sort.shell_sort(data)[1] == 0

    data = list(data_100_duplicate)
    sort.shell_sort(data, gap_sequence=sort.shell_gap_sequence(100, 7))
    assert data == sorted(data_100_duplicate)
    assert sort.shell_sort(data)[1] == 0


def test_shuffle():
    data = []
    sort.shuffle(data)
    assert data == []

    data = [666]
    sort.shuffle(data)
    assert data == [666]

    data = list(data_10)
    sort.shuffle(data)
    assert sorted(data) == list(range(10))

    data = list(data_100)
    sort.shuffle(data)
    assert sorted(data) == list(range(100))

    data = list(data_1000)
    sort.shuffle(data)
    assert sorted(data) == list(range(1000))

    data = list(data_10000)
    sort.shuffle(data)
    assert sorted(data) == list(range(10000))

    data = list(data_10_duplicate)
    sort.shuffle(data)
    assert sorted(data) == sorted(data_10_duplicate)

    data = list(data_100_duplicate)
    sort.shuffle(data)
    assert sorted(data) == sorted(data_100_duplicate)

    data = list(data_200_double)
    sort.shuffle(data)
    assert sorted(data) == sorted(list(range(100)) + list(range(100)))

    data = list(data_2000_double)
    sort.shuffle(data)
    assert sorted(data) == sorted(list(range(1000)) + list(range(1000)))

    data = list(data_20000_double)
    sort.shuffle(data)
    assert sorted(data) == sorted(list(range(10000)) + list(range(10000)))

    for i in range(200):
        data = list(range(i))
        sort.shuffle(data)
        if i >= 100:
            assert data != list(range(i))
        assert sorted(data) == list(range(i))


#
# Main
######
if __name__ == '__main__':
    # Run all test_...() functions (useful if pytest miss)
    for s in sorted(dir()):
        if s[:5] == 'test_':
            print(s, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[s]()
            print(file=sys.stderr)
