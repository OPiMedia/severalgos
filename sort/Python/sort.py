#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Sort (October 17, 2016)
====
* Insertion Sort

* Merge Sort

* Quick Sort

* Selection Sort

* Shell Sort

* Knuth Shuffle

Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2013, 2014, 2015, 2016 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import collections
import random


#
# Functions
############
def less(a, b):
    """
    If `a < b`
    then return `True`,
    else return `False`.

    :param a: Object comparable by <
    :param b: Object comparable by <

    :return: bool
    """
    return a < b


def less_reverse(a, b):
    """
    If `b < a`
    then return `True`,
    else return `False`.

    :param a: Object comparable by <
    :param b: Object comparable by <

    :return: bool
    """
    return b < a


def less_reverse_str_lower(a, b):
    """
    If `b.lower() < a.lower()`
    then return `True`,
    else return `False`.

    :param a: str
    :param b: str

    :return: bool
    """
    assert isinstance(a, str), type(a)
    assert isinstance(b, str), type(b)

    return b.lower() < a.lower()


def less_reverse_str_upper(a, b):
    """
    If `a.upper() < b.upper()`
    then return `True`,
    else return `False`.

    :param a: str
    :param b: str

    :return: bool
    """
    assert isinstance(a, str), type(a)
    assert isinstance(b, str), type(b)

    return a.upper() < b.upper()


def less_str_lower(a, b):
    """
    If `b.lower() < a.lower()`
    then return `True`,
    else return `False`.

    :param a: str
    :param b: str

    :return: bool
    """
    assert isinstance(a, str), type(a)
    assert isinstance(b, str), type(b)

    return b.lower() < a.lower()


def less_str_upper(a, b):
    """
    If `a.upper() < b.upper()`
    then return `True`,
    else return `False`.

    :param a: str
    :param b: str

    :return: bool
    """
    assert isinstance(a, str), type(a)
    assert isinstance(b, str), type(b)

    return a.upper() < b.upper()


def insertion_sort(data, less_fct=less):
    """
    Sort the list `data` by insertion sort algorithm,
    and return (nb of comparisons, nb of swaps).

    :warning: Side effects: `data` is modified.

    O(n^2) comparisons
    O(n^2) swaps

    Insertion Sort:
    https://en.wikipedia.org/wiki/Insertion_sort
    http://www.sorting-algorithms.com/insertion-sort

    See week 2 of *Algorithms, Part I*
    (Robert Sedgewick, Kevin Wayne, Coursera 2013)
    https://www.coursera.org/course/algs4partI

    http://algs4.cs.princeton.edu/21elementary/

    :param data: list
    :param less_fct: function (Object, Object) -> bool

    :return: (int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert callable(less_fct), type(less_fct)

    nb_comparisons = 0
    nb_swaps = 0

    for i in range(len(data)):
        for j in range(i, 0, -1):
            nb_comparisons += 1
            if less_fct(data[j], data[j - 1]):
                data[j - 1], data[j] = data[j], data[j - 1]
                nb_swaps += 1
            else:
                break

    return (nb_comparisons, nb_swaps)


def is_sorted(data, less_fct=less):
    """
    If `data` is sorted
    then return `True`,
    else return `False`.

    O(n) comparisons

    :param data: Sequence
    :param less_fct: function (Object, Object) -> bool

    :return: bool
    """
    assert isinstance(data, collections.Sequence), type(data)
    assert callable(less_fct), type(less_fct)

    for i in range(len(data) - 1):
        if less_fct(data[i + 1], data[i]):
            return False

    return True


def merge(data1, data2, less_fct=less):
    """
    Return (merged list, nb of comparisons).

    Merged list is sorted list of all elements of data1 and data2.

    :param data1: sorted Sequence
    :param data2: sorted Sequence
    :param less_fct: function (Object, Object) -> bool

    :return: (list, int >= 0)
    """
    assert isinstance(data1, collections.Sequence), type(data1)
    assert isinstance(data2, collections.Sequence), type(data2)
    assert callable(less_fct), type(less_fct)

    assert is_sorted(data1, less_fct=less_fct)
    assert is_sorted(data2, less_fct=less_fct)

    nb_comparisons = 0

    data = []

    i1 = 0
    i2 = 0
    for _ in range(len(data1) + len(data2)):
        if i1 >= len(data1):
            data.append(data2[i2])
            i2 += 1
        elif i2 >= len(data2):
            data.append(data1[i1])
            i1 += 1
        else:
            nb_comparisons += 1
            if less_fct(data2[i2], data1[i1]):
                data.append(data2[i2])
                i2 += 1
            else:
                data.append(data1[i1])
                i1 += 1

    assert is_sorted(data, less_fct=less_fct)

    return (data, nb_comparisons)


def merge_sort(data, less_fct=less):
    """
    Sort the list `data` by merge sort algorithm,
    and return (nb of comparisons, nb of merges).

    :warning: Side effects: `data` is modified.

    O(n lg(n)) comparisons

    Merge Sort:
    https://en.wikipedia.org/wiki/Merge_sort
    http://www.sorting-algorithms.com/merge-sort

    See week 3 of *Algorithms, Part I*
    (Robert Sedgewick, Kevin Wayne, Coursera 2013)
    https://www.coursera.org/course/algs4partI

    http://algs4.cs.princeton.edu/22mergesort/

    :param data: list
    :param less_fct: function (Object, Object) -> bool

    :return: (int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert callable(less_fct), type(less_fct)

    def merge_sort_rec(data, low, high):
        if high <= low:
            return

        middle = low + (high - low)//2

        merge_sort_rec(data, low, middle)       # left half
        merge_sort_rec(data, middle + 1, high)  # right half

        if less_fct(data[middle + 1], data[middle]):
            subdata, nb_comparisons = merge(data[low: middle + 1],
                                            data[middle + 1:high + 1],
                                            less_fct=less_fct)
            data[low: high + 1] = subdata
            nbs['comparisons'] += nb_comparisons
            nbs['merges'] += 1

    nbs = {'comparisons': 0,
           'merges': 0}

    merge_sort_rec(data, 0, len(data) - 1)

    return (nbs['comparisons'], nbs['merges'])


def partition(data, low, high, less_fct=less):
    """
    Partition the sublist `data[low:high + 1]` around a pivot p
    and return (index of pivot, nb of comparisons, nb of swaps).

    The partitioned sublist is such that: <= pivot, pivot, >= pivot.
    | -+---------+-+----------+-
    |  |_ <=     |p|<= _      |
    | -+---------+-+----------+-
    |   |         |          |
    |   low       index      high

    :warning: Side effects: `data[low:high + 1]` is modified.

    O(n) with n = high + 1 - low

    :param data: list
    :param low: 0 <= int <= high
    :param high: low < int < len(data)
    :param less_fct: function (Object, Object) -> bool

    :return: (low <= int <= high, int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert isinstance(low, int), type(low)
    assert isinstance(high, int), type(high)
    assert 0 <= low < high < len(data), (low, high, len(data))
    assert callable(less_fct), type(less_fct)

    nb_comparisons = 0
    nb_swaps = 0

    pivot = data[low]

    i = low
    j = high + 1

    # +----------------------+
    # |p|?                   |
    # +----------------------+
    #  |                    | |
    #  i                    | j
    #  low                  high

    while True:
        # +-+-----+-------+------+
        # |p|_ <= |?      |<= _  |
        # +-+-----+-------+------+
        #  |     |         |    |
        #  low   i         j    high

        i += 1
        while (i <= j) and less_fct(data[i], pivot):  # _ < p
            i += 1
            if i > high:
                break
            nb_comparisons += 1
        nb_comparisons += 1

        # +-+-----+-+-----+------+
        # |p|_ <= |>|?    |<= _  |
        # +-+-----+-+-----+------+
        #  |       |       |    |
        #  low     i       j    high

        j -= 1
        while (i <= j) and less_fct(pivot, data[j]):  # p < _
            j -= 1
            if i < low:
                break
            nb_comparisons += 1
        nb_comparisons += 1

        if i >= j:
            break

        # +-+-----+-+---+-+------+
        # |p|_ <= |>|?  |<|<= _  |
        # +-+-----+-+---+-+------+
        #  |       |     |      |
        #  low     i     j      high

        data[j], data[i] = data[i], data[j]
        nb_swaps += 1

    data[j], data[low] = data[low], data[j]
    nb_swaps += 1

    return (j, nb_comparisons, nb_swaps)


def partition_3_way(data, low, high, less_fct=less):
    """
    Partition the sublist `data[low:high + 1]` around a pivot p
    and return (first index of pivot, last index of pivot,
                nb of comparisons, nb of swaps).

    The partitioned sublist is such that:
    | -+------+-------------+------+-
    |  |_ <   |p           p|< _   |
    | -+------+-------------+------+-
    |   |      |           |      |
    |   low    first       last   high

    :warning: Side effects: `data[low:high + 1]` is modified.

    O(n) with n = high + 1 - low

    :param data: list
    :param low: 0 <= int <= high
    :param high: low < int < len(data)
    :param less_fct: function (Object, Object) -> bool

    :return: (low <= int <= high, low <= int <= high, int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert isinstance(low, int), type(low)
    assert isinstance(high, int), type(high)
    assert 0 <= low < high < len(data), (low, high, len(data))
    assert callable(less_fct), type(less_fct)

    nb_comparisons = 0
    nb_swaps = 0

    pivot = data[low]

    first = low
    last = high

    i = low + 1

    # +---------------------------+
    # |p|?                        |
    # +---------------------------+
    #  | |                       |
    #  | i                       |
    #  low                       high
    #  first                     last

    while i <= last:
        # +------+------+------+------+
        # |_ <   |p    p|?     |< _   |
        # +------+------+------+------+
        #  |      |      |    |      |
        #  low    first  i    last   high

        if less_fct(data[i], pivot):    # _ < p
            data[i], data[first] = data[first], data[i]
            first += 1
            i += 1
            nb_comparisons += 1
            nb_swaps += 1
        elif less_fct(pivot, data[i]):  # p < _
            data[last], data[i] = data[i], data[last]
            last -= 1
            nb_comparisons += 2
            nb_swaps += 1
        else:                           # p
            i += 1
            nb_comparisons += 2

    return (first, last, nb_comparisons, nb_swaps)


def partition_5_way(data, low, high, less_fct=less):
    """
    Partition the sublist `data[low:high + 1]` with two pivots p, q
    and return (first index of pivot p, last index of pivot p,
                first index of pivot q, last index of pivot q,
                nb of comparisons, nb of swaps).

    The partitioned sublist is such that:
    | -+------+---------+---------------+---------+------+-
    |  |  <   |p       p|p < _ < q      |q       q|< _   |
    | -+------+---------+---------------+---------+------+-
    |   |      |       |                 |       |      |
    |   low    first_p last_p            first_q last_q high

    :warning: Side effects: `data[low:high + 1]` is modified.

    O(n) with n = high + 1 - low

    :param data: list
    :param low: 0 <= int <= high
    :param high: low < int < len(data)
    :param less_fct: function (Object, Object) -> bool

    :return: (low <= int <= high, low <= int <= high,
              low <= int <= high, low <= int <= high,
              int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert isinstance(low, int), type(low)
    assert isinstance(high, int), type(high)
    assert 0 <= low < high < len(data), (low, high, len(data))
    assert callable(less_fct), type(less_fct)

    nb_comparisons = 1
    nb_swaps = 0

    if less_fct(data[high], data[low]):
        data[high], data[low] = data[low], data[high]
        nb_swaps += 1

    pivot_p = data[low]
    pivot_q = data[high]

    first_p = low
    last_p = low

    first_q = high
    last_q = high

    i = low + 1

    # +-+---------------------------------------------+-+
    # |p|?                                            |q|
    # +-+---------------------------------------------+-+
    #  | |                                             |
    #  | i                                             |
    #  low                                             high
    #  first_p                                         first_q
    #  last_p                                          last_q

    while i < first_q:
        # +------+---------+----------+----+---------+------+
        # |_ <   |p       p|p < _ < q |?   |q       q|< _   |
        # +------+---------+----------+----+---------+------+
        #  |      |       |            |    |       |      |
        #  low    first_p last_p       i    first_q last_q high

        if less_fct(data[i], pivot_p):    # _ < p
            data[i], data[first_p] = data[first_p], data[i]
            data[i], data[last_p + 1] = data[last_p + 1], data[i]
            first_p += 1
            last_p += 1
            i += 1
            nb_comparisons += 1
            nb_swaps += 2
        elif less_fct(data[i], pivot_q):  # p <= _ < q
            if less_fct(pivot_p, data[i]):  # p < _ < q
                i += 1
            else:                           # p
                data[i], data[last_p + 1] = data[last_p + 1], data[i]
                last_p += 1
                i += 1
                nb_swaps += 1
            nb_comparisons += 3
        else:                             # q <= _
            if less_fct(pivot_q, data[i]):  # q < _
                data[last_q], data[i] = data[i], data[last_q]
                data[first_q - 1], data[i] = data[i], data[first_q - 1]
                first_q -= 1
                last_q -= 1
                nb_swaps += 2
            else:                           # q
                assert not less_fct(data[i], pivot_q)

                data[first_q - 1], data[i] = data[i], data[first_q - 1]
                first_q -= 1
                nb_swaps += 1
            nb_comparisons += 3

    return (first_p, last_p, first_q, last_q, nb_comparisons, nb_swaps)


def quick_sort(data, less_fct=less):
    """
    Sort the list `data` by quick sort algorithm,
    and return (nb of comparisons, nb of swaps,
                nb recursive calls, recursive depth).

    :warning: Side effects: `data` is modified.

    O(n^2) comparisons, O(n lg(n)) in average

    Quick Sort:
    https://en.wikipedia.org/wiki/Quicksort
    http://www.sorting-algorithms.com/quick-sort

    See week 2 of *Algorithms, Part I*
    (Robert Sedgewick, Kevin Wayne, Coursera 2013)
    https://www.coursera.org/course/algs4partI

    http://algs4.cs.princeton.edu/23quicksort/

    :param data: list
    :param less_fct: function (Object, Object) -> bool

    :return: (int >= 0, int >= 0, int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert callable(less_fct), type(less_fct)

    def quick_sort_rec(data, low, high):
        if low >= high:
            return 0

        index_pivot, nb_comparisons, nb_swaps = partition(data, low, high,
                                                          less_fct=less_fct)

        nbs['comparisons'] += nb_comparisons
        nbs['swaps'] += nb_swaps

        depth_a = quick_sort_rec(data, low, index_pivot - 1)
        depth_b = quick_sort_rec(data, index_pivot + 1, high)

        nbs['calls'] += 2

        return max((depth_a, depth_b)) + 1

    nbs = {'comparisons': 0,
           'swaps': 0,
           'calls': 0}

    shuffle(data)
    depth = quick_sort_rec(data, 0, len(data) - 1)

    return (nbs['comparisons'], nbs['swaps'], nbs['calls'], depth)


def quick_sort_3_way(data, less_fct=less):
    """
    Sort the list `data` by quick sort algorithm,
    and return (nb of comparisons, nb of swaps,
                nb recursive calls, recursive depth).

    :warning: Side effects: `data` is modified.

    O(n^2) comparisons, O(n lg(n)) in average

    Quick Sort:
    https://en.wikipedia.org/wiki/Quicksort
    http://www.sorting-algorithms.com/quick-sort

    See week 2 of *Algorithms, Part I*
    (Robert Sedgewick, Kevin Wayne, Coursera 2013)
    https://www.coursera.org/course/algs4partI

    http://algs4.cs.princeton.edu/23quicksort/

    :param data: list
    :param less_fct: function (Object, Object) -> bool

    :return: (int >= 0, int >= 0, int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert callable(less_fct), type(less_fct)

    def quick_sort_rec(data, low, high):
        if low >= high:
            return 0

        first, last, nb_comparisons, nb_swaps \
            = partition_3_way(data, low, high, less_fct=less_fct)

        nbs['comparisons'] += nb_comparisons
        nbs['swaps'] += nb_swaps

        depth_a = quick_sort_rec(data, low, first - 1)
        depth_b = quick_sort_rec(data, last + 1, high)

        nbs['calls'] += 2

        return max((depth_a, depth_b)) + 1

    nbs = {'comparisons': 0,
           'swaps': 0,
           'calls': 0}

    shuffle(data)
    depth = quick_sort_rec(data, 0, len(data) - 1)

    return (nbs['comparisons'], nbs['swaps'], nbs['calls'], depth)


def quick_sort_5_way(data, less_fct=less):
    """
    Sort the list `data` by quick sort algorithm with 2 pivots,
    and return (nb of comparisons, nb of swaps,
                nb recursive calls, recursive depth).

    :warning: Side effects: `data` is modified.

    O(n^2) comparisons, O(n lg(n)) in average

    Quick Sort:
    https://en.wikipedia.org/wiki/Quicksort
    http://www.sorting-algorithms.com/quick-sort

    See week 2 of *Algorithms, Part I*
    (Robert Sedgewick, Kevin Wayne, Coursera 2013)
    https://www.coursera.org/course/algs4partI

    http://algs4.cs.princeton.edu/23quicksort/

    :param data: list
    :param less_fct: function (Object, Object) -> bool

    :return: (int >= 0, int >= 0, int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert callable(less_fct), type(less_fct)

    def quick_sort_rec(data, low, high):
        if low >= high:
            return 0

        first_p, last_p, first_q, last_q, nb_comparisons, nb_swaps \
            = partition_5_way(data, low, high, less_fct=less_fct)

        nbs['comparisons'] += nb_comparisons
        nbs['swaps'] += nb_swaps

        depth_a = quick_sort_rec(data, low, first_p - 1)
        depth_b = quick_sort_rec(data, last_p + 1, first_q - 1)
        depth_c = quick_sort_rec(data, last_q + 1, high)

        nbs['calls'] += 3

        return max((depth_a, depth_b, depth_c)) + 1

    nbs = {'comparisons': 0,
           'swaps': 0,
           'calls': 0}

    shuffle(data)
    depth = quick_sort_rec(data, 0, len(data) - 1)

    return (nbs['comparisons'], nbs['swaps'], nbs['calls'], depth)


def read_data(file, convert=None):
    """
    Read the file
    and return a list of lines (without \n).

    If `convert` is not `None`
    then each line is converted.

    :param file: str or (read file object)
    :param convert: function str -> Object

    :return: list
    """
    assert isinstance(file, str) or hasattr(file, 'read'), type(file)
    assert (convert is None) or callable(convert), type(convert)

    data = []

    fin = (open(file) if isinstance(file, str)
           else file)

    for line in fin:
        if line[-1:] == '\n':
            line = line[:-1]
        data.append(line if convert is None
                    else convert(line))

    if isinstance(file, str):
        fin.close()

    return data


def selection_sort(data, less_fct=less):
    """
    Sort the list `data` by selection sort algorithm,
    and return (nb of comparisons, nb of swaps).

    :warning: Side effects: `data` is modified.

    O(n^2) comparisons
    O(n) swaps

    Selection Sort:
    https://en.wikipedia.org/wiki/Selection_sort
    http://www.sorting-algorithms.com/selection-sort

    See week 2 of *Algorithms, Part I*
    (Robert Sedgewick, Kevin Wayne, Coursera 2013)
    https://www.coursera.org/course/algs4partI

    http://algs4.cs.princeton.edu/21elementary/

    :param data: list
    :param less_fct: function (Object, Object) -> bool

    :return: (int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert callable(less_fct), type(less_fct)

    nb_comparisons = 0
    nb_swaps = 0

    for i in range(len(data)):
        j_min = i
        for j in range(i + 1, len(data)):  # search smallest
            if less_fct(data[j], data[j_min]):
                j_min = j
            nb_comparisons += 1

        if j_min != i:
            data[i], data[j_min] = data[j_min], data[i]
            nb_swaps += 1

    return (nb_comparisons, nb_swaps)


def shell_gap_sequence(length, step=3):
    """
    Return 1, step + 1, 2 step + 1, 3 step + 1, ... until < length.

    :param length: int >= 1
    :param step: int >= 2

    :return: sequence of (0 < int < length)
    """
    assert isinstance(length, int), type(length)
    assert length >= 1, length

    assert isinstance(step, int), type(step)
    assert step >= 2, step

    value = 1
    while value < length:
        yield value

        value += step


def shell_sort(data, less_fct=less, gap_sequence=None):
    """
    Sort the list `data` by Shell sort algorithm with `gap_sequence`,
    and return (nb of comparisons, nb of swaps).

    If `gap_sequence` is `None`
    then use sequence [1, 4, 13, 40, ..., s_i = 3*s_{i-1} + 1]
         with s_i < `len(data)` <= s_{i+1}.

    :warning: Side effects: `data` is modified.

    O(n^(3/2)) comparisons (with 3*x + 1 increment)

    Shell Sort:
    https://en.wikipedia.org/wiki/Shellsort
    http://www.sorting-algorithms.com/shell-sort

    See week 2 of *Algorithms, Part I*
    (Robert Sedgewick, Kevin Wayne, Coursera 2013)
    https://www.coursera.org/course/algs4partI

    http://algs4.cs.princeton.edu/21elementary/

    :param data: list
    :param less_fct: function (Object, Object) -> bool
    :param gap_sequence: None or (Iterable of (0 < int < len(data)))

    :return: (int >= 0, int >= 0)
    """
    assert isinstance(data, list), type(data)
    assert callable(less_fct), type(less_fct)
    assert (gap_sequence is None) \
        or isinstance(gap_sequence, collections.Iterable), type(gap_sequence)

    nb_comparisons = 0
    nb_swaps = 0

    if gap_sequence is None:
        gap_sequence = (shell_gap_sequence(len(data)) if len(data) > 0
                        else [])

    for h in gap_sequence:
        for i in range(h, len(data)):
            for j in range(i, h - 1, -h):
                nb_comparisons += 1
                if less_fct(data[j], data[j - h]):
                    data[j - h], data[j] = data[j], data[j - h]
                    nb_swaps += 1
                else:
                    break

    return (nb_comparisons, nb_swaps)


def shuffle(data):
    """
    Shuffle `data` by Knuth algorithm.

    :warning: Side effects: `data` is modified.

    O(n) comparisons
    O(n) swaps

    See week 2 of *Algorithms, Part I*
    (Robert Sedgewick, Kevin Wayne, Coursera 2013)
    https://www.coursera.org/course/algs4partI

    :param data: list
    """
    assert isinstance(data, list), type(data)

    for i in range(1, len(data)):
        j = random.randrange(i + 1)
        data[i], data[j] = data[j], data[i]


#
# Main
#######
def main():
    """
    Main
    """
    import sys
    import time

    # Params
    convert = None
    less_fct = less
    fast = False
    src = sys.stdin
    stdout = sys.stdout

    for param in sys.argv[1:]:
        if param[:2] == '--':
            if param == '--fast':
                fast = True
            elif param == '--float':
                convert = float
            elif param == '--int':
                convert = int
            elif param == '--lower':
                convert = None
                less_fct = less_str_lower
            elif param == '--no-stdout':
                stdout = None
            elif param == '--reverse':
                less_fct = less_reverse
            elif param == '--reverse-lower':
                convert = None
                less_fct = less_reverse_str_lower
            elif param == '--reverse-upper':
                convert = None
                less_fct = less_reverse_str_upper
            elif param == '--upper':
                convert = None
                less_fct = less_str_upper
            else:
                print('! Unknow param "{}"'.format(param), file=sys.stderr)
        elif param[:1] == '-':
            if param == '-':
                src = sys.stdin
            else:
                print('! Unknow param "{}"'.format(param), file=sys.stderr)
        else:
            src = param
    sys.stderr.flush()

    # Read
    data_src = read_data(src, convert=convert)

    # Merge Sort
    data = list(data_src)

    start_time = (time.perf_counter() if sys.version_info >= (3, 3)
                  else time.clock())
    nb_comparisons, nb_merges = merge_sort(data, less_fct=less_fct)
    end_time = (time.perf_counter() if sys.version_info >= (3, 3)
                else time.clock())

    if stdout is not None:
        for item in data:
            print(item, file=stdout)
        stdout.flush()

    print('Python:', sys.version, file=sys.stderr)
    print('    ', sys.version_info, file=sys.stderr)
    if __debug__:
        print('     Debug mode', file=sys.stderr)
    print('* Merge Sort:       #: {0}'
          '\t# comparisons: {1} = {0}*{2:.2f}\t# merges: {3} = {0}*{4:.2f}'
          '\tTime: {5:.5f}'
          .format(len(data),
                  nb_comparisons, nb_comparisons/len(data),
                  nb_merges, nb_merges/len(data),
                  end_time - start_time),
          file=sys.stderr)
    sys.stderr.flush()

    assert len(data) == len(data_src)
    assert is_sorted(data)

    if fast:
        exit()

    # Quick Sort
    data = list(data_src)

    start_time = (time.perf_counter() if sys.version_info >= (3, 3)
                  else time.clock())
    nb_comparisons, nb_swaps, nb_recursive_calls, recursive_depth \
        = quick_sort(data, less_fct=less_fct)
    end_time = (time.perf_counter() if sys.version_info >= (3, 3)
                else time.clock())

    print('* Quick Sort:       #: {0}'
          '\t# comparisons: {1} = {0}*{2:.2f}\t# swaps: {3} = {0}*{4:.2f}'
          '\t# recursive calls: {5}\trecursive depth: {6}'
          '\tTime: {7:.5f}'
          .format(len(data),
                  nb_comparisons, nb_comparisons/len(data),
                  nb_swaps, nb_swaps/len(data),
                  nb_recursive_calls, recursive_depth,
                  end_time - start_time),
          file=sys.stderr)
    sys.stderr.flush()

    assert len(data) == len(data_src)
    assert is_sorted(data)

    # Quick Sort 3-way
    data = list(data_src)

    start_time = (time.perf_counter() if sys.version_info >= (3, 3)
                  else time.clock())
    nb_comparisons, nb_swaps, nb_recursive_calls, recursive_depth \
        = quick_sort_3_way(data, less_fct=less_fct)
    end_time = (time.perf_counter() if sys.version_info >= (3, 3)
                else time.clock())

    print('* Quick Sort 3-way: #: {0}'
          '\t# comparisons: {1} = {0}*{2:.2f}\t# swaps: {3} = {0}*{4:.2f}'
          '\t# recursive calls: {5}\trecursive depth: {6}'
          '\tTime: {7:.5f}'
          .format(len(data),
                  nb_comparisons, nb_comparisons/len(data),
                  nb_swaps, nb_swaps/len(data),
                  nb_recursive_calls, recursive_depth,
                  end_time - start_time),
          file=sys.stderr)
    sys.stderr.flush()

    assert len(data) == len(data_src)
    assert is_sorted(data)

    # Quick Sort 5-way
    data = list(data_src)

    start_time = (time.perf_counter() if sys.version_info >= (3, 3)
                  else time.clock())
    nb_comparisons, nb_swaps, nb_recursive_calls, recursive_depth \
        = quick_sort_5_way(data, less_fct=less_fct)
    end_time = (time.perf_counter() if sys.version_info >= (3, 3)
                else time.clock())

    print('* Quick Sort 5-way: #: {0}'
          '\t# comparisons: {1} = {0}*{2:.2f}\t# swaps: {3} = {0}*{4:.2f}'
          '\t# recursive calls: {5}\trecursive depth: {6}'
          '\tTime: {7:.5f}'
          .format(len(data),
                  nb_comparisons, nb_comparisons/len(data),
                  nb_swaps, nb_swaps/len(data),
                  nb_recursive_calls, recursive_depth,
                  end_time - start_time),
          file=sys.stderr)
    sys.stderr.flush()

    assert len(data) == len(data_src)
    assert is_sorted(data)

    if len(data_src) < 100000:
        # Shell Sort
        data = list(data_src)

        start_time = (time.perf_counter() if sys.version_info >= (3, 3)
                      else time.clock())
        nb_comparisons, nb_swaps = shell_sort(data, less_fct=less_fct)
        end_time = (time.perf_counter() if sys.version_info >= (3, 3)
                    else time.clock())

        print('* Shell Sort:       #: {0}'
              '\t# comparisons: {1} = {0}*{2:.2f}\t# swaps: {3} = {0}*{4:.2f}'
              '\tTime: {5:.5f}'
              .format(len(data),
                      nb_comparisons, nb_comparisons/len(data),
                      nb_swaps, nb_swaps/len(data),
                      end_time - start_time),
              file=sys.stderr)
        sys.stderr.flush()

        assert len(data) == len(data_src)
        assert is_sorted(data)

    if len(data_src) < 10000:
        # Insertion Sort
        data = list(data_src)

        start_time = (time.perf_counter() if sys.version_info >= (3, 3)
                      else time.clock())
        nb_comparisons, nb_swaps = insertion_sort(data, less_fct=less_fct)
        end_time = (time.perf_counter() if sys.version_info >= (3, 3)
                    else time.clock())

        print('* Insertion Sort:   #: {0}'
              '\t# comparisons: {1} = {0}*{2:.2f}\t# swaps: {3} = {0}*{4:.2f}'
              '\tTime: {5:.5f}'
              .format(len(data),
                      nb_comparisons, nb_comparisons/len(data),
                      nb_swaps, nb_swaps/len(data),
                      end_time - start_time),
              file=sys.stderr)
        sys.stderr.flush()

        assert len(data) == len(data_src)
        assert is_sorted(data)

        # Selection Sort
        data = list(data_src)

        start_time = (time.perf_counter() if sys.version_info >= (3, 3)
                      else time.clock())
        nb_comparisons, nb_swaps = selection_sort(data)
        end_time = (time.perf_counter() if sys.version_info >= (3, 3)
                    else time.clock())

        print('* Selection Sort:   #: {0}'
              '\t# comparisons: {1} = {0}*{2:.2f}\t# swaps: {3} = {0}*{4:.2f}'
              '\tTime: {5:.5f}'
              .format(len(data),
                      nb_comparisons, nb_comparisons/len(data),
                      nb_swaps, nb_swaps/len(data),
                      end_time - start_time),
              file=sys.stderr)
        sys.stderr.flush()

        assert len(data) == len(data_src)
        assert is_sorted(data)


if __name__ == '__main__':
    main()
