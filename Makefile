# Makefile of severalgos --- February 15, 2015

.SUFFIXES:

7Z       = 7z
ATTRIB   = attrib
CAT      = cat
CD       = cd
CHECKTXT = checkTxtPy.py
CP       = cp -p
ECHO     = echo
MKDIR    = mkdir -p
MV       = mv
PYTHON2  = python2
PYTHON3  = python3
PYTEST2  = py.test-2.7
PYTEST3  = py.test-3.3
RM       = rm -f
RMDIR    = rmdir
SHELL    = sh



########
# Test #
########
.PHONY: checkTxt pytest pytest2 pytest3

checkTxt:
	@$(CHECKTXT) "*.py" "*/*.py" "*/*/*.py" "*.rst" "*.txt" "*/*.txt" "*/*/*.txt" "*/*/*/*.txt" */Doxyfile */Makefile */*.c */*.h

pytest:	pytest2 pytest3

pytest2:
	-$(PYTEST2) -v

pytest3:
	-$(PYTEST3) -v



#########
# Clean #
#########
.PHONY:	clean distclean
clean:
	$(RM) -r *.pyc *.pyo */*.pyc */*.pyo */*/*.pyc */*/*.pyo */*/*/*.pyc */*/*/*.pyo */*/*/*/*.pyc */*/*/*/*.pyo */*/*/*/*/*.pyc */*/*/*/*/*.pyo
	-$(RMDIR) __pycache__ */__pycache__ */*/__pycache__ */*/*/__pycache__ */*/*/*/__pycache__

distclean:	clean
