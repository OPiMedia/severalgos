.. -*- restructuredtext -*-

=====================
severalgos/ Fibonacci
=====================

C++, Prolog and Python implementations of Fibonacci numbers computation.


* **Mathematical** relations:
  *Fibonacci numbers - several relations to implement good algorithms*
  https://bitbucket.org/OPiMedia/severalgos/src/master/Fibonacci/math/Fibonacci.pdf

* The **C++ HTML online documentation**
  http://www.opimedia.be/DS/online-documentations/severalgos/Fibonacci-cpp/html/



GMP
https://gmplib.org/
