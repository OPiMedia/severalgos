/* -*- coding: latin-1 -*- */
/** \file prog_benchmark_gmp.cpp
 * \brief
 * Usage: prog_benchmark_gmp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iomanip>
#include <iostream>
#include <string>
#include <utility>

#include <gmpxx.h>  // NOLINT

#include "benchmark.hpp"
#include "fibonacci_gmp.hpp"



const unsigned int DEFAULT_NB(10000);



/** \brief
 * Return the duration in milliseconds
 * of nb computations \a fct(n),
 * with \f$n \approx 2^i\f$.
 */
std::chrono::milliseconds
bench_function(mpz_class (*fct)(unsigned int),
               unsigned int i,
               unsigned int nb = DEFAULT_NB) {
  unsigned int n((static_cast<unsigned int>(1) << i) - 1);
  const unsigned int mask((static_cast<unsigned int>(1) << (i + 1)) - 1);

  const std::chrono::steady_clock::time_point
    start(std::chrono::steady_clock::now());

  while (nb-- > 0) {
    fct(n);
    n = (n + nb) & mask;
  }

  return round<std::chrono::milliseconds>(std::chrono::steady_clock::now()
                                          - start);
}


/** \brief
 * Return the duration in milliseconds
 * of nb computations \a fct(n),
 * with \f$n \approx 2^i\f$.
 */
std::chrono::milliseconds
bench_function(std::pair<mpz_class, mpz_class> (*fct)(unsigned int),
               unsigned int i,
               unsigned int nb = DEFAULT_NB) {
  unsigned int n((static_cast<unsigned int>(1) << i) - 1);
  const unsigned int mask((static_cast<unsigned int>(1) << (i + 1)) - 1);

  const std::chrono::steady_clock::time_point
    start(std::chrono::steady_clock::now());

  while (nb-- > 0) {
    fct(n);
    n = (n + nb) & mask;
  }

  return round<std::chrono::milliseconds>(std::chrono::steady_clock::now()
                                          - start);
}



/* ******
 * Main *
 ********/
int
main() {
  const std::chrono::steady_clock::time_point
    total_start(std::chrono::steady_clock::now());

  std::chrono::milliseconds duration;

  std::cout << "i  mpz__gmp   mpz_pair__iter_lg_table   mpz_pair__iter_lg   mpz_pair__rec_lg_table   mpz_pair__rec_lg   mpz__iter_lg_optimized_table   mpz__iter_lg_optimized   mpz__iter_lg_table   mpz__iter_lg   mpz__iter_n   mpz__pair__rec_n   mpz__rec_exp"  // NOLINT
            << std::endl;

  for (unsigned int i(0); i <= 16; ++i) {
    std::cout << std::setw(2) << i << ' ';
    std::cout.flush();

    duration = bench_function(fibonacci_gmp::fibonacci_mpz__gmp, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    if (i <= 15) {
      duration = bench_function
        (fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();

      duration = bench_function(fibonacci_gmp::fibonacci_mpz_pair__iter_lg, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();

      duration = bench_function
        (fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();

      duration = bench_function(fibonacci_gmp::fibonacci_mpz_pair__rec_lg, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();

      duration = bench_function
        (fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();

      duration = bench_function(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized,
                                i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();

      duration = bench_function(fibonacci_gmp::fibonacci_mpz__iter_lg_table, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();

      duration = bench_function(fibonacci_gmp::fibonacci_mpz__iter_lg, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();
    }

    if (i <= 13) {
      duration = bench_function(fibonacci_gmp::fibonacci_mpz__iter_n, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();
    }

    if (i <= 11) {
      duration = bench_function(fibonacci_gmp::fibonacci_mpz_pair__rec_n, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();
    }

    if (i <= 3) {
      duration = bench_function(fibonacci_gmp::fibonacci_mpz__rec_exp, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();
    } else if (i == 4) {
      std::cout << ' ' << std::setw(10) << 256084500;
      std::cout.flush();
    }

    std::cout << std::endl;
    std::cout.flush();
  }

  std::cerr
    << "--- Total: "
    << round<std::chrono::seconds>(std::chrono::steady_clock::now()
                                   - total_start).count()
    << "s ---" << std::endl;

  return EXIT_SUCCESS;
}
