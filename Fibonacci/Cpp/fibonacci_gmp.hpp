/* -*- coding: latin-1 -*- */
/** \file fibonacci_gmp.hpp
 * \brief
 * fibonacci_gmp.hpp (February 27, 2015)
 * Implementations using GMP
 * https://gmplib.org/
 *
 * \warning
 * Complexity informations in each function
 * considers that operations on large numbers
 * are in constant time and constant space (which is obviously false).
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef FIBONACCI_FIBONACCI_GMP_H_
#define FIBONACCI_FIBONACCI_GMP_H_

#include <cassert>

#include <limits>
#include <utility>

#include <gmpxx.h>  // NOLINT



namespace fibonacci_gmp {

  /* **********************
   * Functions prototypes *
   ************************/
  /** \brief
   * Return the number of bits of the binary representation of \a n.
   *
   * If \a n == 0 then return 0.
   *
   * \pre T must be an unsigned integer type.
   */
  inline
  size_t
  bit_length(const mpz_class& n);


  /** \brief
   * Return \f$F_n\f$ forall n integer.
   *
   * \f$F_{-n} = -(-1)^n F_n\f$
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   *
   * \param n >= std::numeric_limits<signed int>::min() + 1
   */
  inline
  mpz_class
  fibonacci_mpz(signed int n);


  /** \brief
   * Return \f$F_n\f$.
   *
   * Alias to \a fibonacci_mpz_pair().second.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): "1"
   */
  inline
  mpz_class
  fibonacci_mpz(unsigned int n);


  /** \brief
   * Return \f$(F_{n-1}, F_n\f$ forall n integer.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   *
   * \param n >= std::numeric_limits<signed int>::min() + 2
   */
  inline
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair(signed int n);


  /** \brief
   * Return \f$(F_{n-1}, F_n\f$.
   *
   * Alias to \a fibonacci_mpz_pair__iter_lg_table().
   *
   * Time O(n):  lg(n)\n
   * Space O(n): "1"
   */
  inline
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair(unsigned int n);


  /** \brief
   * Return \f$F_{2^n}\f$.
   *
   * Alias to \a fibonacci_mpz_pow2__iter_lucas_n_table().
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  inline
  mpz_class
  fibonacci_mpz_pow2(unsigned int n);


  /** \brief
   * Return (\f$F_{2^n - 1}, F_{2^n}\f$).
   *
   * Alias to \a fibonacci_mpz_pow2_pair__iter_n_table().
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  inline
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair(unsigned int n);


  /** \brief
   * Return \f$L_n\f$ forall n integer.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   *
   * \param n >= std::numeric_limits<signed int>::min() + 2
   */
  inline
  mpz_class
  lucas_mpz(signed int n);


  /** \brief
   * Return \f$(L_{n-1}, L_n)\f$ forall n integer.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   *
   * \param n >= std::numeric_limits<signed int>::min() + 3
   */
  inline
  std::pair<mpz_class, mpz_class>
  lucas_mpz_pair(signed int n);



  /* ******************************************************
   * Functions prototypes: real and worse implementations *
   ********************************************************/

  /** \brief
   * Return \f$F_n\f$.
   *
   * Simple call to GMP function mpz_fib_ui().
   *
   * Time O(n):  ?\n
   * Space O(n): ?
   */
  mpz_class
  fibonacci_mpz__gmp(unsigned int n);


  /** \brief
   * Return \f$F_n\f$.
   *
   * Algorithm:\n
   * Compute successively:\n
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^{lg(n)+1} - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^{lg(n)+1}}\f$
   *
   * With relations:\n
   *   \f$F_{2^{i+1} - 1} = F^2_{2^i} + F^2_{2^i - 1}\f$\n
   *   \f$F_{2^{i+1}}     = 2 F_{2^i} F_{2^i - 1} + F^2_{2^i}\f$
   *
   * For each ith bit of n equal to 1, add the corresponding \f$F_{2^i}\f$\n
   * with relations:\n
   *   \f$F_{k + 2^i - 1} = F_k F_{2^i} + F_{k-1} F_{2^i - 1}\f$\n
   *   \f$F_{k + 2^i}     = F_k F_{2^i} + F_{k-1} F_{2^i} + F_k F_{2^i - 1}\f$
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   */
  mpz_class
  fibonacci_mpz__iter_lg(unsigned int n);


  /** \brief
   * Return \f$F_n\f$.
   *
   * Same algorithm as \a fibonacci_mpz__iter_lg()
   * but with precaculated tables.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): "1"
   */
  mpz_class
  fibonacci_mpz__iter_lg_table(unsigned int n);


  /** \brief
   * Return \f$F_n\f$.
   *
   * Algorithm:\n
   * Compute successively:\n
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^{lg(n)+1} - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^{lg(n)+1}}\f$
   *
   * With relations:\n
   *   \f$F_{2^{i+1} - 1} = F^2_{2^i} + F^2_{2^i - 1}\f$\n
   *   \f$F_{2^{i+1}}     = 2 F_{2^i} F_{2^i - 1} + F^2_{2^i}\f$
   *
   * For each ith bit of n equal to 1, add the corresponding \f$F_{2^i}\f$\n
   * with relations:\n
   *   \f$F_{k + 2^i - 1} = F_k F_{2^i} + F_{k-1} F_{2^i - 1}\f$\n
   *   \f$F_{k + 2^i}     = F_k F_{2^i} + F_{k-1} F_{2^i} + F_k F_{2^i - 1}\f$
   *
   * Process before the first consecutive bits equals to 0...0 or 1...1.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   */
  mpz_class
  fibonacci_mpz__iter_lg_optimized(unsigned int n);


  /** \brief
   * Return \f$F_n\f$.
   *
   * Same algorithm as \a fibonacci_mpz__iter_lg_optimized()
   * but with precaculated table of F_{2^i}.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): "1"
   */
  mpz_class
  fibonacci_mpz__iter_lg_optimized_table(unsigned int n);


  /** \brief
   * Return \f$F_n\f$.
   *
   * Algorithm:\n
   * Simple iteration\n
   * with usage of the recursive definition \f$F_{n+2} = F_{n+1} + F_n\f$.
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  mpz_class
  fibonacci_mpz__iter_n(unsigned int n);


  /** \brief
   * Return \f$F_n\f$.
   *
   * From a certain \a n, recursion fails.
   *
   * Algorithm:\n
   * Simple usage of the recursive definition \f$F_{n+2} = F_{n+1} + F_n\f$\n
   * (twice calls on each step).
   *
   * Time O(n):  2^n\n
   * Space O(n): n
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  mpz_class
  fibonacci_mpz__rec_exp(unsigned int n);


  /** \brief
   * Return \f$F_n\f$.
   *
   * \pre n < FIBONACCI_MPZ_TABLE_SIZE
   *
   * Time O(n):  "1"\n
   * Space O(n): "1"
   */
  inline
  mpz_class
  fibonacci_mpz__table_const(unsigned int n);


  /** \brief
   * Return (\f$F_{n-1}, F_n\f$).
   *
   * Simple call to GMP function mpz_fib2_ui().
   *
   * Time O(n):  ?\n
   * Space O(n): ?
   */
  inline
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__gmp(unsigned int n);


  /** \brief
   * Return (\f$F_{n-1}, F_n\f$).
   *
   * Algorithm:\n
   * Iterate on each bit of n, with relations:
   *   \f$F_{2n-1} = F^2_n + F^2_{n-1}\f$\n
   *   \f$F_{2n}   = 2 F_n F_{n-1} + F^2_n\f$\n
   *   \f$F_{2n+1} = (F_n + F_{n-1})^2 + F^2_n\f$
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__iter_lg(unsigned int n);


  /** \brief
   * Return (\f$F_{n-1}, F_n\f$).
   *
   * Same algorithm as \a fibonacci_mpz_pair__iter_lg()
   * but with the precaculated table.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__iter_lg_table(unsigned int n);


  /** \brief
   * Return (\f$F_{n-1}, F_n\f$).
   *
   * Algorithm:\n
   * Recursive call with relations:\n
   *   \f$F_{2n-1} = F^2_n + F^2_{n-1}\f$\n
   *   \f$F_{2n}   = 2 F_n F_{n-1} + F^2_n\f$\n
   *   \f$F_{2n+1} = (F_n + F_{n-1})^2 + F^2_n\f$
   *
   * Time O(n):  lg(n)\n
   * Space O(n): lg(n)
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__rec_lg(unsigned int n);


  /** \brief
   * Return (\f$F_{n-1}, F_n\f$).
   *
   * Same algorithm as \a fibonacci_mpz_pair__rec_lg()
   * but with the precaculated table.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): lg(n)
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__rec_lg_table(unsigned int n);


  /** \brief
   * Return \f$(F_{n-1}, F_n\f$.
   *
   * From a certain \a n, recursion fails.
   *
   * Algorithm:\n
   * Simple usage of the recursive definition \f$F_{n+2} = F_{n+1} + F_n\f$\n
   * (but only one call on each step).
   *
   * Time O(n):  n\n
   * Space O(n): n
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__rec_n(unsigned int n);


  /** \brief
   * Return \f$F_{2^n}\f$.
   *
   * Algorithm:\n
   * Product of Lucas numbers algorithm.\n
   * Compute successively:
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^n - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^n}\f$
   *
   * With relations:\n
   *   \f$F_{2n} = L_n F_n\f$\n
   *   \f$L_{2n} = L^2_n - 2\f$ if n >= 2
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  mpz_class
  fibonacci_mpz_pow2__iter_lucas_n(unsigned int n);


  /** \brief
   * Return \f$F_{2^n}\f$.
   *
   * Same algorithm as \a fibonacci_mpz_pow2__iter_lucas_n()
   * but with precaculated table.
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  mpz_class
  fibonacci_mpz_pow2__iter_lucas_n_table(unsigned int n);


  /** \brief
   * Return \f$F_{2^n}\f$.
   *
   * Algorithm:\n
   * Product of Lucas numbers algorithm by Takahashi:\n
   * *A fast algorithm for computing large Fibonacci numbers* (Daisuke Takahashi)\n
   * http://www.ii.uni.wroc.pl/~lorys/IPL/article75-6-1.pdf
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  mpz_class
  fibonacci_mpz_pow2__iter_takahashi_n(unsigned int n);


  /** \brief
   * Return \f$F_{2^n}\f$.
   *
   * \pre n < FIBONACCI_POW2_MPZ_TABLE_SIZE
   *
   * Time O(n):  "1"\n
   * Space O(n): "1"
   */
  inline
  mpz_class
  fibonacci_mpz_pow2__table_const(unsigned int n);


  /** \brief
   * Return (\f$F_{2^n - 1}, F_{2^n}\f$).
   *
   * Algorithm:\n
   * Compute successively:
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^n - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^n}\f$
   *
   * With relations:\n
   *   \f$F_{2^{i+1} - 1} = F^2_{2^i} + F^2_{2^i - 1}\f$\n
   *   \f$F_{2^{i+1}}     = 2 F_{2^i} F_{2^i - 1} + F^2_{2^i}\f$
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__iter_n(unsigned int n);


  /** \brief
   * Return (\f$F_{2^n - 1}, F_{2^n}\f$).
   *
   * Same algorithm as \a fibonacci_mpz_pow2_pair__iter_n()
   * but with precaculated table.
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__iter_n_table(unsigned int n);


  /** \brief
   * Return (\f$F_{2^n - 1}, F_{2^n}\f$).
   *
   * Algorithm:\n
   * Product of Lucas numbers algorithm.\n
   * Compute successively:
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^n - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^n}\f$
   *
   * With relations:\n
   *   \f$F_{2n} = L_n F_n\f$\n
   *   \f$L_{2n} = L^2_n - 2\f$ if n >= 2
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__iter_lucas_n(unsigned int n);


  /** \brief
   * Return (\f$F_{2^n - 1}, F_{2^n}\f$).
   *
   * Same algorithm as \a fibonacci_mpz_pow2_pair__iter_lucas_n()
   * but with precaculated table.
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__iter_lucas_n_table(unsigned int n);


  /** \brief
   * Return (\f$F_{2^n - 1}, F_{2^n}\f$).
   *
   * \pre n < FIBONACCI_POW2_MPZ_TABLE_SIZE
   *
   * Time O(n):  "1"\n
   * Space O(n): "1"
   */
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__table_const(unsigned int n);



  /* *****************
   * Global variable *
   *******************/

  /** \brief Precalculated table of Fibonacci numbers. */
  extern const mpz_class FIBONACCI_MPZ_TABLE[];


  /** \brief
   * Exponent of the bigger index is a power of 2
   * = lg(FIBONACCI_MPZ_TABLE_LG_SIZE).
   */
  extern const unsigned int FIBONACCI_MPZ_TABLE_LG_SIZE;


  /** \brief
   * Nb elements of FIBONACCI_MPZ_TABLE.
   *
   * \pre >= 3
   */
  extern const unsigned int FIBONACCI_MPZ_TABLE_SIZE;


  /** \brief
   * Precalculated table of Fibonacci numbers (\f$F_{2^n - 1}, F_{2^n}\f$).
   */
  extern const std::pair<mpz_class, mpz_class> FIBONACCI_POW2_MPZ_TABLE[];


  /** \brief
   * Nb elements of FIBONACCI_POW2_MPZ_TABLE.
   *
   * \pre >= 2
   */
  extern const unsigned int FIBONACCI_POW2_MPZ_TABLE_SIZE;



  /* *********************************
   * Inline functions implementation *
   ***********************************/
  inline
  size_t
  bit_length(const mpz_class& n) {
    assert(n >= 0);

    return (n != 0
            ? mpz_sizeinbase(n.get_mpz_t(), 2)
            : 0);
  }


  inline
  mpz_class
  fibonacci_mpz(signed int n) {
    assert(n >= std::numeric_limits<signed int>::min() + 1);

    return (n >= 0
            ? fibonacci_mpz(static_cast<unsigned int>(n))  // not negative
            : ((static_cast<unsigned int>(-n) & 1) != 0
               ? fibonacci_mpz(static_cast<unsigned int>(-n))      // odd
               : -fibonacci_mpz(static_cast<unsigned int>(-n))));  // even
  }


  inline
  mpz_class
  fibonacci_mpz(unsigned int n) {
    return fibonacci_mpz_pair(n).second;
  }


  inline
  mpz_class
  fibonacci_mpz__gmp(unsigned int n) {
    assert(std::numeric_limits<unsigned long int>::digits >= 64);  // NOLINT

    mpz_class f_n;

    mpz_fib_ui(f_n.get_mpz_t(), n);

    return f_n;
  }


  inline
  mpz_class
  fibonacci_mpz__table_const(unsigned int n) {
    assert(n < FIBONACCI_MPZ_TABLE_SIZE);

    return FIBONACCI_MPZ_TABLE[n];
  }


  inline
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair(signed int n) {
    if (n >= 0) {
      return fibonacci_mpz_pair(static_cast<unsigned int>(n));
    } else {
      assert(n >= std::numeric_limits<signed int>::min() + 2);

      const unsigned int abs_n(-n);

      std::pair<mpz_class, mpz_class>
        f_n__f_n_p1(fibonacci_mpz_pair(static_cast<unsigned int>(abs_n + 1)));

      return ((abs_n & 1) == 0
              ? std::make_pair(f_n__f_n_p1.second,
                               static_cast<mpz_class>(-f_n__f_n_p1.first))
              : std::make_pair(static_cast<mpz_class>(-f_n__f_n_p1.second),
                               f_n__f_n_p1.first));
    }
  }


  inline
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair(unsigned int n) {
    return fibonacci_mpz_pair__iter_lg_table(n);
  }


  inline
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__gmp(unsigned int n) {
    assert(std::numeric_limits<unsigned long int>::digits >= 64);  // NOLINT

    std::pair<mpz_class, mpz_class> f_n_1__f_n;

    mpz_fib2_ui(f_n_1__f_n.second.get_mpz_t(),
                f_n_1__f_n.first.get_mpz_t(), n);

    return f_n_1__f_n;
  }


  inline
  mpz_class
  fibonacci_mpz_pow2(unsigned int n) {
    return fibonacci_mpz_pow2__iter_lucas_n_table(n);
  }


  inline
  mpz_class
  fibonacci_mpz_pow2__table_const(unsigned int n) {
    assert(n < FIBONACCI_POW2_MPZ_TABLE_SIZE);

    return fibonacci_mpz_pow2_pair__table_const(n).second;
  }


  inline
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair(unsigned int n) {
    return fibonacci_mpz_pow2_pair__iter_n_table(n);
  }


  inline
  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__table_const(unsigned int n) {
    assert(n < FIBONACCI_POW2_MPZ_TABLE_SIZE);

    return FIBONACCI_POW2_MPZ_TABLE[n];
  }


  inline
  mpz_class
  lucas_mpz(signed int n) {
    assert(n >= std::numeric_limits<signed int>::min() + 2);

    std::pair<mpz_class, mpz_class> f_n_1__f_n(fibonacci_mpz_pair(n));

    return f_n_1__f_n.first*2 + f_n_1__f_n.second;
  }


  inline
  std::pair<mpz_class, mpz_class>
  lucas_mpz_pair(signed int n) {
    assert(n >= std::numeric_limits<signed int>::min() + 3);

    std::pair<mpz_class, mpz_class> f_n_2__f_n_1(fibonacci_mpz_pair(n - 1));

    const mpz_class f_n_2 = f_n_2__f_n_1.first;
    const mpz_class f_n_1 = f_n_2__f_n_1.second;

    f_n_2__f_n_1.first = f_n_2*2 + f_n_1;
    f_n_2__f_n_1.second = f_n_1*3 + f_n_2;

    return f_n_2__f_n_1;
  }

}  // namespace fibonacci_gmp

#endif  // FIBONACCI_FIBONACCI_GMP_H_
