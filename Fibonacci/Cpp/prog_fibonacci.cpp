/* -*- coding: latin-1 -*- */
/** \file prog_fibonacci.cpp
 * \brief
 * Usage: prog_fibonacci [n [n [n...]]]
 *
 * If command line arguments n_0 [n_1 [...]] exist\n
 * then print \f$n_i: F_{n_i} \mod 2^{64}\f$ for each i,\n
 * else print \f$n_i: F_{n_i} \mod 2^{64}\f$ binary size for i from 0 to 50.
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iomanip>
#include <iostream>

#include "fibonacci.hpp"



int
main(int argc, const char* const argv[]) {
  if (argc > 1) {
    for (int i(1); i < argc; ++i) {
      const signed int n(atoi(argv[i]));

      std::cout << n << ": " << fibonacci::fibonacci_int64(n) << std::endl;
      std::cout.flush();
    }
  } else {
    std::cout << "n  F_n          binary size" << std::endl;
    for (unsigned int n(0); n <= 50; ++n) {
      const uint64_t f_n(fibonacci::fibonacci_uint64(n));

      std::cout << std::setw(2)  << n << ' '
                << std::setw(11) << f_n << ' '
                << std::setw(2)  << fibonacci::bit_length(f_n) << std::endl;
    }
  }

  return EXIT_SUCCESS;
}
