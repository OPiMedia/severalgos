/* -*- coding: latin-1 -*- */
/** \file prog_lucas_gmp.cpp
 * \brief
 * Usage: prog_lucas_gmp [n [n [n...]]]
 *
 * If command line arguments n_0 [n_1 [...]] exist\n
 * then print \f$n_i: L_{n_i}\f$ for each i,\n
 * else print \f$n_i: L_{n_i}\f$ binary size for i from 0 to 100.
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iomanip>
#include <iostream>

#include <gmpxx.h>  // NOLINT

#include "fibonacci_gmp.hpp"



int
main(int argc, const char* const argv[]) {
  if (argc > 1) {
    for (int i(1); i < argc; ++i) {
      const signed int n(atoi(argv[i]));

      std::cout << n << ": " << fibonacci_gmp::lucas_mpz(n) << std::endl;
      std::cout.flush();
    }
  } else {
    std::cout << "n   L_n                    binary size" << std::endl;
    for (unsigned int n(0); n <= 100; ++n) {
      const mpz_class l_n(fibonacci_gmp::lucas_mpz(n));

      std::cout << std::setw(3)  << n << ' '
                << std::setw(21) << l_n << ' '
                << std::setw(2)  << fibonacci_gmp::bit_length(l_n) << std::endl;
    }
  }

  return EXIT_SUCCESS;
}
