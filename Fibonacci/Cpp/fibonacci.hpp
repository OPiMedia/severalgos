/* -*- coding: latin-1 -*- */
/** \mainpage
 * Module fibonacci (February 29, 2016)
 *
 * Several implementation of Fibonacci numbers computation.
 *
 * Fibonacci numbers: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377...\n
 *   \f$F_0 = 0\f$\n
 *   \f$F_1 = 1\f$\n
 *   \f$F_{n+2} = F_{n+1} + F_n   \qquad\forall n\f$ integer
 *
 * https://oeis.org/A000045
 *
 * Lucas numbers: 2, 1, 3, 4, 7, 11, 18, 29, 47, 76, 123, 199, 322, 521, 843...\n
 *   \f$L_0 = 2\f$\n
 *   \f$L_1 = 1\f$\n
 *   \f$L_{n+2} = L_{n+1} + L_n   \qquad\forall n\f$ integer
 *
 * https://oeis.org/A000032
 *
 * See
\htmlonly
<ul>
<li>
  <strong>Mathematical</strong> relations:
  <i>Fibonacci numbers &mdash; several relations to implement good algorithms</i>
  <strong><a href="https://bitbucket.org/OPiMedia/severalgos/src/master/Fibonacci/math/Fibonacci.pdf"><tt>.pdf</tt></a></strong>
</li>
<li>
  Complete <strong>C++ and Python sources</strong> on
  <strong><a href="https://bitbucket.org/OPiMedia/severalgos/src/master/Fibonacci/">Bitbucket</a></strong>
</li>
<li>
  This
  <strong><a href="http://www.opimedia.be/DS/online-documentations/severalgos/Fibonacci-cpp/html/">C++ HTML online documentation</a></strong>
</li>
</ul>
\endhtmlonly
 *
 * \warning
 * Complexity informations in each function
 * considers that operations on large numbers
 * are in constant time and constant space (which is obviously false).
 *
 * \verbatim
Implementations                                | Time O(n)     | Space O(n) | Implementation using GMP
-----------------------------------------------+---------------+------------+--------------------------------------------
fibonacci_uint64_pair__iter_lg_table           | lg(n)         | "1"        | fibonacci_mpz_pair__iter_lg_table
fibonacci_uint64_pair__iter_lg                 | lg(n)         | 1          | fibonacci_mpz_pair__iter_lg
fibonacci_uint64_pair__rec_lg_table            | lg(n)         | lg(n)      | fibonacci_mpz_pair__rec_lg_table
fibonacci_uint64_pair__rec_lg                  | lg(n)         | lg(n)      | fibonacci_mpz_pair__rec_lg
fibonacci_uint64__iter_lg_optimized_table      | lg(n)         | "1"        | fibonacci_mpz__iter_lg_optimized_table
fibonacci_uint64__iter_lg_optimized            | lg(n)         | 1          | fibonacci_mpz__iter_lg_optimized
fibonacci_uint64__iter_lg_table                | lg(n)         | "1"        | fibonacci_mpz__iter_lg_table
fibonacci_uint64__iter_lg                      | lg(n)         | 1          | fibonacci_mpz__iter_lg
fibonacci_uint64__iter_n                       | n             | 1          | fibonacci_mpz__iter_n
fibonacci_uint64_pair__rec_n                   | n             | n          | fibonacci_mpz_pair__rec_n
fibonacci_uint64__rec_exp                      | 2^n           | n          | fibonacci_mpz__rec_exp
-----------------------------------------------+---------------+------------+--------------------------------------------
fibonacci_uint64_pow2__iter_lucas_n_table      | n             | "1"        | fibonacci_mpz_pow2__iter_lucas_n_table
fibonacci_uint64_pow2__iter_lucas_n            | n             | 1          | fibonacci_mpz_pow2__iter_lucas_n
                                               | n             | 1          | fibonacci_mpz_pow2__iter_takahashi_n
fibonacci_uint64_pow2_pair__iter_lucas_n_table | n             | "1"        | fibonacci_mpz_pow2_pair__iter_lucas_n_table
fibonacci_uint64_pow2_pair__iter_lucas_n       | n             | 1          | fibonacci_mpz_pow2_pair__iter_lucas_n
fibonacci_uint64_pow2_pair__iter_n_table       | n             | "1"        | fibonacci_mpz_pow2_pair__iter_n_table
fibonacci_uint64_pow2_pair__iter_n             | n             | 1          | fibonacci_mpz_pow2_pair__iter_n
\endverbatim

Benchmark with Debian Wheezy - **GCC** 4.7.2 64 bits
(uint64 and \htmlonly<a href="https://gmplib.org/">GMP</a>\endhtmlonly) :
\htmlonly<br />
  <img src="benchmark_Debian_gcc4_7_2_64bits.svg" alt="Benchmark GCC Fibonacci" />
  <img src="benchmark_gmp_Debian_gcc4_7_2_64bits.svg" alt="Benchmark GCC Fibonacci GMP" /><br />
  <img src="benchmark_pow2_Debian_gcc4_7_2_64bits.svg" alt="Benchmark GCC Fibonacci pow2" />
  <img src="benchmark_pow2_gmp_Debian_gcc4_7_2_64bits.svg" alt="Benchmark GCC Fibonacci pow2 GMP" />
\endhtmlonly

Benchmark with Debian Wheezy - **Clang** 3.3 64 bits (but GMP compiled with GCC)
\htmlonly<br />
  <img src="benchmark_Debian_clang3_3_64bits.svg" alt="Benchmark Clang Fibonacci" />
  <img src="benchmark_gmp_Debian_clang3_3_64bits.svg" alt="Benchmark Clang Fibonacci GMP" /><br />
  <img src="benchmark_pow2_Debian_clang3_3_64bits.svg" alt="Benchmark Clang Fibonacci" />
  <img src="benchmark_pow2_gmp_Debian_clang3_3_64bits.svg" alt="Benchmark Clang Fibonacci GMP" />
\endhtmlonly
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015, 2016 Olivier Pirson
 * http://www.opimedia.be/
 */

/** \file fibonacci.hpp
 * \brief Function definitions.
 *
 * \warning
 * Complexity informations in each function
 * considers that operations on large numbers
 * are in constant time and constant space (which is obviously false).
 */

#ifndef FIBONACCI_FIBONACCI_H_
#define FIBONACCI_FIBONACCI_H_

#include <cassert>
#include <cstdint>

#include <limits>
#include <utility>



namespace fibonacci {

  /* **********************
   * Functions prototypes *
   ************************/

  /** \brief
   * Return the number of bits of the binary representation of \a n.
   *
   * If \a n == 0 then return 0.
   *
   * \pre T must be an unsigned integer type.
   *
   * \return <= std::numeric_limits<T>::digits
   */
  template <typename T>
  unsigned int
  bit_length(T n);


  /** \brief
   * Forall n integer, if -92 <= n <= 92
   * then return \f$F_n\f$,
   * else return \f$F_n \mod 2^{64}\f$.
   *
   * \warning Maybe sign problem with n < -92.
   *
   * \f$F_{-n} = -(-1)^n F_n\f$
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   *
   * \param n >= std::numeric_limits<signed int>::min() + 1
   */
  inline
  int64_t
  fibonacci_int64(signed int n);


  /** \brief
   * Forall n integer, if -91 <= n <= 92
   * then return \f$(F_{n-1} \mod 2^{64}, F_n)\f$,
   * else return \f$(F_{n-1} \mod 2^{64}, F_n \mod 2^{64})\f$.
   *
   * \warning Maybe sign problem with n < -91.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   *
   * \param n >= std::numeric_limits<signed int>::min() + 2
   */
  inline
  std::pair<int64_t, int64_t>
  fibonacci_int64_pair(signed int n);


  /** \brief
   * If n <= 93
   * then return \f$F_n\f$,
   * else return \f$F_n \mod 2^{64}\f$.
   *
   * Alias to \a fibonacci_uint64_pair().second.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): "1"
   */
  inline
  uint64_t
  fibonacci_uint64(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$(F_{n-1} \mod 2^{64}, F_n)\f$,
   * else return \f$(F_{n-1} \mod 2^{64}, F_n \mod 2^{64})\f$.
   *
   * Alias to \a fibonacci_uint64_pair__iter_lg_table().
   *
   * Time O(n):  lg(n)\n
   * Space O(n): "1"
   */
  inline
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair(unsigned int n);


  /** \brief
   * If n <= 6
   * then return \f$F_{2^n}\f$,
   * else return \f$F_{2^n} \mod 2^{64}\f$.
   *
   * Alias to \a fibonacci_uint64_pow2__iter_lucas_n_table().
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  inline
  uint64_t
  fibonacci_uint64_pow2(unsigned int n);


  /** \brief
   * If n <= 6
   * then return (\f$F_{2^n - 1}, F_{2^n}\f$),
   * then return (\f$F_{2^n - 1} \mod 2^{64}, F_{2^n} \mod 2^{64}\f$).
   *
   * Alias to \a fibonacci_uint64_pow2_pair__iter_n_table().
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  inline
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair(unsigned int n);


  /** \brief
   * Forall n integer, if -90 <= n <= 90
   * then return \f$L_n\f$,
   * else return \f$L_n \mod 2^{64}\f$.
   *
   * \warning Maybe problem with n < -90.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   *
   * \param n >= std::numeric_limits<signed int>::min() + 2
   */
  inline
  int64_t
  lucas_int64(signed int n);


  /** \brief
   * Forall n integer, if -89 <= n <= 90
   * then return \f$(L_{n-1}, L_n)\f$,
   * else return \f$(L_{n-1} \mod 2^{64}, L_n \mod 2^{64})\f$.
   *
   * \warning Maybe problem with n < -89.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   *
   * \param n >= std::numeric_limits<signed int>::min() + 3
   */
  inline
  std::pair<int64_t, int64_t>
  lucas_int64_pair(signed int n);



  /* ******************************************************
   * Functions prototypes: real and worse implementations *
   ********************************************************/

  /** \brief
   * If n <= 93
   * then return \f$F_n\f$,
   * else return \f$F_n \mod 2^{64}\f$.
   *
   * Algorithm:\n
   * Compute successively:\n
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^{lg(n)+1} - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^{lg(n)+1}}\f$
   *
   * With relations:\n
   *   \f$F_{2^{i+1} - 1} = F^2_{2^i} + F^2_{2^i - 1}\f$\n
   *   \f$F_{2^{i+1}}     = 2 F_{2^i} F_{2^i - 1} + F^2_{2^i}\f$
   *
   * For each ith bit of n equal to 1, add the corresponding \f$F_{2^i}\f$\n
   * with relations:\n
   *   \f$F_{k + 2^i - 1} = F_k F_{2^i} + F_{k-1} F_{2^i - 1}\f$\n
   *   \f$F_{k + 2^i}     = F_k F_{2^i} + F_{k-1} F_{2^i} + F_k F_{2^i - 1}\f$
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   */
  uint64_t
  fibonacci_uint64__iter_lg(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$F_n\f$,
   * else return \f$F_n \mod 2^{64}\f$.
   *
   * Same algorithm as \a fibonacci_uint64__iter_lg()
   * but with precaculated tables.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): "1"
   */
  uint64_t
  fibonacci_uint64__iter_lg_table(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$F_n\f$,
   * else return \f$F_n \mod 2^{64}\f$.
   *
   * Algorithm:\n
   * Compute successively:\n
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^{lg(n)+1} - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^{lg(n)+1}}\f$
   *
   * With relations:\n
   *   \f$F_{2^{i+1} - 1} = F^2_{2^i} + F^2_{2^i - 1}\f$\n
   *   \f$F_{2^{i+1}}     = 2 F_{2^i} F_{2^i - 1} + F^2_{2^i}\f$
   *
   * For each ith bit of n equal to 1, add the corresponding \f$F_{2^i}\f$\n
   * with relations:\n
   *   \f$F_{k + 2^i - 1} = F_k F_{2^i} + F_{k-1} F_{2^i - 1}\f$\n
   *   \f$F_{k + 2^i}     = F_k F_{2^i} + F_{k-1} F_{2^i} + F_k F_{2^i - 1}\f$
   *
   * Process before the first consecutive bits equals to 0...0 or 1...1.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   */
  uint64_t
  fibonacci_uint64__iter_lg_optimized(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$F_n\f$,
   * else return \f$F_n \mod 2^{64}\f$.
   *
   * Same algorithm as \a fibonacci_uint64__iter_lg_optimized()
   * but with precaculated table of F_{2^i}.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): "1"
   */
  uint64_t
  fibonacci_uint64__iter_lg_optimized_table(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$F_n\f$,
   * else return \f$F_n \mod 2^{64}\f$.
   *
   * Algorithm:\n
   * Simple iteration\n
   * with usage of the recursive definition \f$F_{n+2} = F_{n+1} + F_n\f$.
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  uint64_t
  fibonacci_uint64__iter_n(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$F_n\f$,
   * else return \f$F_n \mod 2^{64}\f$.
   *
   * From a certain \a n, recursion fails.
   *
   * Algorithm:\n
   * Simple usage of the recursive definition \f$F_{n+2} = F_{n+1} + F_n\f$\n
   * (twice calls on each step).
   *
   * Time O(n):  2^n\n
   * Space O(n): n
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  uint64_t
  fibonacci_uint64__rec_exp(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$F_n\f$,
   * else return \f$F_n \mod 2^{64}\f$.
   *
   * \pre n < FIBONACCI_UINT64_TABLE_SIZE
   *
   * Time O(n):  "1"\n
   * Space O(n): "1"
   */
  inline
  uint64_t
  fibonacci_uint64__table_const(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$(F_{n-1} \mod 2^{64}, F_n)\f$,
   * else return \f$(F_{n-1} \mod 2^{64}, F_n \mod 2^{64})\f$.
   *
   * Algorithm:\n
   * Iterate on each bit of n, with relations:\n
   *   \f$F_{2n-1} = F^2_n + F^2_{n-1}\f$\n
   *   \f$F_{2n}   = 2 F_n F_{n-1} + F^2_n\f$\n
   *   \f$F_{2n+1} = (F_n + F_{n-1})^2 + F^2_n\f$
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   */
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__iter_lg(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$(F_{n-1} \mod 2^{64}, F_n)\f$,
   * else return \f$(F_{n-1} \mod 2^{64}, F_n \mod 2^{64})\f$.
   *
   * Same algorithm as \a fibonacci_uint64_pair__iter_lg()
   * but with the precaculated table.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): 1
   */
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__iter_lg_table(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$(F_{n-1} \mod 2^{64}, F_n)\f$,
   * else return \f$(F_{n-1} \mod 2^{64}, F_n \mod 2^{64})\f$.
   *
   * Algorithm:\n
   * Recursive call with relations:\n
   *   \f$F_{2n-1} = F^2_n + F^2_{n-1}\f$\n
   *   \f$F_{2n}   = 2 F_n F_{n-1} + F^2_n\f$\n
   *   \f$F_{2n+1} = (F_n + F_{n-1})^2 + F^2_n\f$
   *
   * Time O(n):  lg(n)\n
   * Space O(n): lg(n)
   */
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__rec_lg(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$(F_{n-1} \mod 2^{64}, F_n)\f$,
   * else return \f$(F_{n-1} \mod 2^{64}, F_n \mod 2^{64})\f$.
   *
   * Same algorithm as \a fibonacci_uint64_pair__rec_lg()
   * but with the precaculated table.
   *
   * Time O(n):  lg(n)\n
   * Space O(n): lg(n)
   */
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__rec_lg_table(unsigned int n);


  /** \brief
   * If n <= 93
   * then return \f$(F_{n-1} \mod 2^{64}, F_n)\f$,
   * else return \f$(F_{n-1} \mod 2^{64}, F_n \mod 2^{64})\f$.
   *
   * From a certain \a n, recursion fails.
   *
   * Algorithm:\n
   * Simple usage of the recursive definition \f$F_{n+2} = F_{n+1} + F_n\f$\n
   * (but only one call on each step).
   *
   * Time O(n):  n\n
   * Space O(n): n
   */
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__rec_n(unsigned int n);


  /** \brief
   * If n <= 6
   * then return \f$F_{2^n}\f$,
   * else return \f$F_{2^n} \mod 2^{64}\f$.
   *
   * Algorithm:\n
   * Product of Lucas numbers algorithm.\n
   * Compute successively:
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^n - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^n}\f$
   *
   * With relations:\n
   *   \f$F_{2n} = L_n F_n\f$\n
   *   \f$L_{2n} = L^2_n - 2\f$ if n >= 2
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  uint64_t
  fibonacci_uint64_pow2__iter_lucas_n(unsigned int n);


  /** \brief
   * If n <= 6
   * then return \f$F_{2^n}\f$,
   * else return \f$F_{2^n} \mod 2^{64}\f$.
   *
   * Same algorithm as \a fibonacci_uint64_pow2__iter_lucas_n()
   * but with precaculated table.
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  uint64_t
  fibonacci_uint64_pow2__iter_lucas_n_table(unsigned int n);


  /** \brief
   * If n <= 6
   * then return \f$F_{2^n}\f$,
   * then return \f$F_{2^n} \mod 2^{64}\f$.
   *
   * \pre n < FIBONACCI_UINT64_TABLE_SIZE
   *
   * Time O(n):  "1"\n
   * Space O(n): "1"
   */
  inline
  uint64_t
  fibonacci_uint64_pow2__table_const(unsigned int n);


  /** \brief
   * If n <= 6
   * then return (\f$F_{2^n - 1}, F_{2^n}\f$),
   * then return (\f$F_{2^n - 1} \mod 2^{64}, F_{2^n} \mod 2^{64}\f$).
   *
   * Algorithm:\n
   * Compute successively:
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^n - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^n}\f$
   *
   * With relations:\n
   *   \f$F_{2^{i+1} - 1} = F^2_{2^i} + F^2_{2^i - 1}\f$\n
   *   \f$F_{2^{i+1}}     = 2 F_{2^i} F_{2^i - 1} + F^2_{2^i}\f$
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__iter_n(unsigned int n);


  /** \brief
   * If n <= 6
   * then return (\f$F_{2^n - 1}, F_{2^n}\f$),
   * then return (\f$F_{2^n - 1} \mod 2^{64}, F_{2^n} \mod 2^{64}\f$).
   *
   * Same algorithm as \a fibonacci_uint64_pow2_pair__iter_n()
   * but with precaculated table.
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__iter_n_table(unsigned int n);


  /** \brief
   * If n <= 6
   * then return (\f$F_{2^n - 1}, F_{2^n}\f$),
   * then return (\f$F_{2^n - 1} \mod 2^{64}, F_{2^n} \mod 2^{64}\f$).
   *
   * Algorithm:\n
   * Product of Lucas numbers algorithm.\n
   * Compute successively:
   *   \f$F_0, F_1, F_3, F_7, F_15, F_31, F_63, ..., F_{2^n - 1}\f$\n
   *   \f$F_1, F_2, F_4, F_8, F_16, F_32, F_64, ..., F_{2^n}\f$
   *
   * With relations:\n
   *   \f$F_{2n} = L_n F_n\f$\n
   *   \f$L_{2n} = L^2_n - 2\f$ if n >= 2
   *
   * Time O(n):  n\n
   * Space O(n): 1
   */
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__iter_lucas_n(unsigned int n);


  /** \brief
   * If n <= 6
   * then return (\f$F_{2^n - 1}, F_{2^n}\f$),
   * then return (\f$F_{2^n - 1} \mod 2^{64}, F_{2^n} \mod 2^{64}\f$).
   *
   * Same algorithm as \a fibonacci_uint64_pow2_pair__iter_lucas_n()
   * but with precaculated table.
   *
   * Time O(n):  n\n
   * Space O(n): "1"
   */
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__iter_lucas_n_table(unsigned int n);


  /** \brief
   * If n <= 6
   * then return (\f$F_{2^n - 1}, F_{2^n}\f$),
   * then return (\f$F_{2^n - 1} \mod 2^{64}, F_{2^n} \mod 2^{64}\f$).
   *
   * \pre n < FIBONACCI_UINT64_TABLE_SIZE
   *
   * Time O(n):  "1"\n
   * Space O(n): "1"
   */
  inline
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__table_const(unsigned int n);



  /* ******************
   * Global constants *
   ********************/

  /** \brief Precalculated table of Fibonacci numbers. */
  extern const uint64_t FIBONACCI_UINT64_TABLE[];


  /** \brief
   * Exponent of the bigger index is a power of 2
   * = lg(FIBONACCI_UINT64_TABLE_LG_SIZE).
   */
  extern const unsigned int FIBONACCI_UINT64_TABLE_LG_SIZE;


  /** \brief
   * Number of elements of FIBONACCI_UINT64_TABLE.
   *
   * \pre >= 3
   */
  extern const unsigned int FIBONACCI_UINT64_TABLE_SIZE;


  /** \brief
   * Precalculated table of Fibonacci numbers (\f$F_{2^n - 1}, F_{2^n}\f$).
   */
  extern const std::pair<uint64_t, uint64_t> FIBONACCI_POW2_UINT64_TABLE[];


  /** \brief
   * Nb elements of FIBONACCI_POW2_UINT64_TABLE.
   *
   * \pre >= 2
   */
  extern const unsigned int FIBONACCI_POW2_UINT64_TABLE_SIZE;



  /* *********************************
   * Inline functions implementation *
   ***********************************/

#if __GNUC__
#  ifndef __has_builtin
#    define __has_builtin__TO_KILL
#    define __has_builtin(x) 1
#  endif
#else
#  ifndef __has_builtin
#    define __has_builtin__TO_KILL
#    define __has_builtin(x) 0
#  endif
#endif


#if __GNUC__ || __clang__
#  if __has_builtin(__builtin_clz)
  inline
  unsigned int
  bit_length(unsigned int n) {
    return (n != 0
            ? std::numeric_limits<unsigned int>::digits - __builtin_clz(n)
            : 0);
  }
#  endif
#endif


#if __GNUC__ || __clang__
#  if __has_builtin(__builtin_clz)
  inline
  unsigned int
  bit_length(uint64_t n) {
    assert(std::numeric_limits<unsigned long>::digits >= 64);  // NOLINT

    return (n != 0
            ? std::numeric_limits<unsigned long>::digits - __builtin_clzl(n)  // NOLINT
            : 0);
  }
#  endif
#endif


#ifdef __has_builtin__TO_KILL
#  undef __has_builtin__TO_KILL
#  undef __has_builtin
#endif


  template <typename T>
  unsigned int
  bit_length(T n) {
    assert(!std::numeric_limits<T>::is_signed);

    if (n != 0) {
      unsigned int length(std::numeric_limits<T>::digits);
      T pow2(static_cast<T>(1) << (length - 1));

      while ((n & pow2) == 0) {
        --length;
        pow2 >>= 1;
      }

      return length;
    } else {
      return 0;
    }
  }


  inline
  int64_t
  fibonacci_int64(signed int n) {
    assert(n >= std::numeric_limits<signed int>::min() + 1);

    return (n >= 0
            ? fibonacci_uint64(n)  // not negative
            : ((static_cast<unsigned int>(-n) & 1) != 0
               ? fibonacci_uint64(static_cast<unsigned int>(-n))      // odd
               : -fibonacci_uint64(static_cast<unsigned int>(-n))));  // even
  }


  inline
  std::pair<int64_t, int64_t>
  fibonacci_int64_pair(signed int n) {
    if (n >= 0) {
      return fibonacci_uint64_pair(n);
    } else {
      assert(n >= std::numeric_limits<signed int>::min() + 2);

      const unsigned int abs_n(-n);

      std::pair<int64_t, int64_t> f_n__f_n_p1(fibonacci_int64_pair(abs_n + 1));

      return ((abs_n & 1) == 0
              ? std::make_pair(f_n__f_n_p1.second,
                               -f_n__f_n_p1.first)
              : std::make_pair(-f_n__f_n_p1.second,
                               f_n__f_n_p1.first));
    }
  }


  inline
  uint64_t
  fibonacci_uint64__table_const(unsigned int n) {
    assert(n < FIBONACCI_UINT64_TABLE_SIZE);

    return FIBONACCI_UINT64_TABLE[n];
  }


  inline
  uint64_t
  fibonacci_uint64(unsigned int n) {
    return fibonacci_uint64_pair(n).second;
  }


  inline
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair(unsigned int n) {
    return fibonacci_uint64_pair__iter_lg_table(n);
  }


  inline
  uint64_t
  fibonacci_uint64_pow2(unsigned int n) {
    return fibonacci_uint64_pow2__iter_lucas_n_table(n);
  }


  inline
  uint64_t
  fibonacci_uint64_pow2__table_const(unsigned int n) {
    assert(n < FIBONACCI_POW2_UINT64_TABLE_SIZE);

    return fibonacci_uint64_pow2_pair__table_const(n).second;
  }


  inline
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair(unsigned int n) {
    return fibonacci_uint64_pow2_pair__iter_n_table(n);
  }


  inline
  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__table_const(unsigned int n) {
    assert(n < FIBONACCI_POW2_UINT64_TABLE_SIZE);

    return FIBONACCI_POW2_UINT64_TABLE[n];
  }


  inline
  int64_t
  lucas_int64(signed int n) {
    assert(n >= std::numeric_limits<signed int>::min() + 2);

    std::pair<int64_t, int64_t> f_n_1__f_n(fibonacci_int64_pair(n));

    return f_n_1__f_n.first*2 + f_n_1__f_n.second;
  }


  inline
  std::pair<int64_t, int64_t>
  lucas_int64_pair(signed int n) {
    assert(n >= std::numeric_limits<signed int>::min() + 3);

    std::pair<int64_t, int64_t> f_n_2__f_n_1(fibonacci_int64_pair(n - 1));

    const int64_t f_n_2 = f_n_2__f_n_1.first;
    const int64_t f_n_1 = f_n_2__f_n_1.second;

    f_n_2__f_n_1.first = f_n_2*2 + f_n_1;
    f_n_2__f_n_1.second = f_n_1*3 + f_n_2;

    return f_n_2__f_n_1;
  }

}  // namespace fibonacci

#endif  // FIBONACCI_FIBONACCI_H_
