/* -*- coding: latin-1 -*- */
/** \file benchmark.hpp
 * \brief
 * Round function to chrono::duration.
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef FIBONACCI_BENCHMARK_H_
#define FIBONACCI_BENCHMARK_H_

#include <chrono>  // NOLINT


/** \brief
 * Round a duration to nearest.
 *
 * From
 * http://howardhinnant.github.io/duration_io/chrono_util.html
 */
template <class To, class Rep, class Period>
To
round(const std::chrono::duration<Rep, Period>& d) {
  const To t0(std::chrono::duration_cast<To>(d));
  const To t1(t0 + To(1));
  const auto diff0(d - t0);
  const auto diff1(t1 - d);

  return (((diff0 < diff1)
           || ((diff0 == diff1) && !(t0.count() & 1)))
          ? t0
          : t1);
}

#endif  // FIBONACCI_BENCHMARK_H_
