/* -*- coding: latin-1 -*- */
/** \file prog_benchmark_pow2.cpp
 * \brief
 * Usage: prog_benchmark_pow2
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iomanip>
#include <iostream>
#include <string>
#include <utility>

#include "benchmark.hpp"
#include "fibonacci.hpp"



const unsigned int DEFAULT_NB(1000000);



/** \brief
 * Return the duration in milliseconds
 * of nb computations \a fct(n),
 * with \f$n \approx 2^i\f$.
 */
std::chrono::milliseconds
bench_function(uint64_t (*fct)(unsigned int),
               unsigned int i,
               unsigned int nb = DEFAULT_NB) {
  const std::chrono::steady_clock::time_point
    start(std::chrono::steady_clock::now());

  while (nb-- > 0) {
    fct(i);
  }

  return round<std::chrono::milliseconds>(std::chrono::steady_clock::now()
                                          - start);
}


/** \brief
 * Return the duration in milliseconds
 * of nb computations \a fct(n),
 * with \f$n \approx 2^i\f$.
 */
std::chrono::milliseconds
bench_function(std::pair<uint64_t, uint64_t> (*fct)(unsigned int),
               unsigned int i,
               unsigned int nb = DEFAULT_NB) {
  const std::chrono::steady_clock::time_point
    start(std::chrono::steady_clock::now());

  while (nb-- > 0) {
    fct(i);
  }

  return round<std::chrono::milliseconds>(std::chrono::steady_clock::now()
                                          - start);
}



/* ******
 * Main *
 ********/
int
main() {
  const std::chrono::steady_clock::time_point
    total_start(std::chrono::steady_clock::now());

  std::chrono::milliseconds duration;

  std::cout << "i   uint64_pow2__iter_lucas_n_table   uint64_pow2__iter_lucas_n   uint64_pow2_pair__iter_lucas_n_table   uint64_pow2_pair__iter_lucas_n   uint64_pow2_pair__iter_n_table   uint64_pow2_pair__iter_n"  // NOLINT
            << std::endl;

  for (unsigned int i(0); i <= 31; ++i) {
    std::cout << std::setw(2) << i << ' ';
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64_pow2__iter_lucas_n_table, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64_pow2__iter_lucas_n, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n_table, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64_pow2_pair__iter_n_table, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64_pow2_pair__iter_n, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    std::cout << std::endl;
    std::cout.flush();
  }

  std::cerr
    << "--- Total: "
    << round<std::chrono::seconds>(std::chrono::steady_clock::now()
                                   - total_start).count()
    << "s ---" << std::endl;

  return EXIT_SUCCESS;
}
