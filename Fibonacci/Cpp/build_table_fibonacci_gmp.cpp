/* -*- coding: latin-1 -*- */
/** \file build_table_fibonacci_gmp.cpp
 * \brief
 * Usage: build_table_fibonacci_gmp [nb=1025 [exponent=64]]
 *
 * If exponent = 0
 * then print C/C++ table elements F_n for n = 0..(nb-1),
 * else print C/C++ table elements F_n % 2^{exponent} for n = 0..(nb-1).
 *
 * Each result that requires > 64 bits
 * is printed as a mpz_class() (GMP big integer).
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>
#include <cstdlib>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <limits>

#include <gmpxx.h>  // NOLINT



size_t
bit_length(const mpz_class& n) {
  assert(n >= 0);

  return (n != 0
          ? mpz_sizeinbase(n.get_mpz_t(), 2)
          : 0);
}


mpz_class
fibonacci_mpz__iter_n(unsigned int n) {
  mpz_class f_k_1(1);  // F_{k-1}
  mpz_class f_k(0);    // F_k

  while (n-- > 0) {
    swap(f_k, f_k_1);
    f_k += f_k_1;
  }

  return f_k;
}



int
main(int argc, const char* const argv[]) {
  unsigned int mod_exponent(64);
  mpz_class mod_pow2(0);
  unsigned int nb(1025);
  unsigned int previous_bit_length_required(1);

  if (argc > 1) {
    nb = atoi(argv[1]);
    if (argc > 2) {
      mod_exponent = atoi(argv[2]);
    }
  }

  if (mod_exponent > 0) {
    mod_pow2 = (mpz_class(1) << mod_exponent);
  }


  // Header
  std::cout << "/* Table of F_0, ..., F_n, ..., F_{" << nb - 1 << '}';
  if (mod_exponent > 0) {
    std::cout << " modulo 2^{" << mod_exponent << '}';
  }
  std::cout << " */";


  // Table
  for (unsigned int n(0); n < nb; ++n) {
    if ((n % 10) == 0) {  // F_{10*i}
      if (n == 0) {
        std::cout << std::endl
                  << "/* F_0 */" << std::endl;
      } else {
        std::cout << std::endl
                  << "/* F_{" <<  n << "} */" << std::endl;
      }
    }

    const mpz_class f_n(fibonacci_mpz__iter_n(n));
    const mpz_class mod_f_n(mod_exponent == 0
                            ? f_n
                            : f_n % mod_pow2);  // modulo 2^{mod_exponent}

    if (bit_length(mod_f_n) <= 64) {  // primitive type
      std::cout << mod_f_n;
      if (bit_length(mod_f_n)
          > static_cast<unsigned int>(std::numeric_limits<int>::digits)) {  // u
        std::cout << 'u';
        if (bit_length(mod_f_n)
            > static_cast<unsigned int>
            (std::numeric_limits<unsigned int>::digits)) {  // ul
          std::cout << 'l';
        }
      }
    } else {                                         // big int
      std::cout << "mpz_class(\"" << mod_f_n << "\")";
    }
    if (n + 1 < nb) {  // not last
      std::cout << ',';
    }

    if (bit_length(mod_f_n) > previous_bit_length_required) {
      previous_bit_length_required *= 2;
      std::cout << "  /* 1st element that requires "
                << previous_bit_length_required << " bits */";
    }

    if (mod_f_n != f_n) {  // F_n > 2^{mod_exponent}
      std::cout << "  /* " << f_n << " % 2^{" << mod_exponent << "} */";
    }
    std::cout << std::endl;
  }


  return EXIT_SUCCESS;
}
