/* -*- coding: latin-1 -*- */
/** \file prog_lucas.cpp
 * \brief
 * Usage: prog_lucas [n [n [n...]]]
 *
 * If command line arguments n_0 [n_1 [...]] exist\n
 * then print \f$n_i: L_{n_i} \mod 2^{64}\f$ for each i,\n
 * else print \f$n_i: L_{n_i} \mod 2^{64}\f$ binary size for i from 0 to 50.
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iomanip>
#include <iostream>

#include "fibonacci.hpp"



int
main(int argc, const char* const argv[]) {
  if (argc > 1) {
    for (int i(1); i < argc; ++i) {
      const signed int n(atoi(argv[i]));

      std::cout << n << ": " << fibonacci::lucas_int64(n) << std::endl;
      std::cout.flush();
    }
  } else {
    std::cout << "n  L_n          binary size" << std::endl;
    for (unsigned int n(0); n <= 50; ++n) {
      const uint64_t l_n(fibonacci::lucas_int64(n));

      std::cout << std::setw(2)  << n << ' '
                << std::setw(11) << l_n << ' '
                << std::setw(2)  << fibonacci::bit_length(l_n) << std::endl;
    }
  }

  return EXIT_SUCCESS;
}
