/* -*- coding: latin-1 -*- */
/** \file test_fibonacci.hpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cmath>

#include <limits>
#include <utility>

#include <cxxtest/TestSuite.h>  // NOLINT // CxxTest http://cxxtest.com/

#include "benchmark.hpp"
#include "fibonacci.hpp"



#define SHOW_FUNC_NAME {                                  \
  std::cout << std::endl                                  \
            << "=== " << __func__ << " ===" << std::endl; \
  std::cout.flush();                                      \
}



std::chrono::steady_clock::time_point total_start;



class Test__fibonacci : public CxxTest::TestSuite {
 public:
  void test_start() {
    total_start = std::chrono::steady_clock::now();
  }


  void test_types() {
    SHOW_FUNC_NAME;

    TS_ASSERT_LESS_THAN_EQUALS(32, std::numeric_limits<unsigned int>::digits);

    TS_ASSERT_LESS_THAN_EQUALS(std::numeric_limits<signed int>::digits,
                               std::numeric_limits<unsigned int>::digits);
    TS_ASSERT_EQUALS(-(std::numeric_limits<signed int>::min() + 1),
                     std::numeric_limits<signed int>::max())

    TS_ASSERT_EQUALS(std::numeric_limits<uint64_t>::digits, 64);
    TS_ASSERT(!std::numeric_limits<uint64_t>::is_signed);

    TS_ASSERT_EQUALS(std::numeric_limits<int64_t>::digits, 63);
    TS_ASSERT(std::numeric_limits<int64_t>::is_signed);
    TS_ASSERT_EQUALS(-(std::numeric_limits<int64_t>::min() + 1),
                     std::numeric_limits<int64_t>::max());
  }


  void test__FIBONACCI_UINT64_TABLE() {
    SHOW_FUNC_NAME;

    assert(fibonacci::FIBONACCI_UINT64_TABLE_SIZE >= 3);
    assert((1u << fibonacci::FIBONACCI_UINT64_TABLE_LG_SIZE) + 1
           <= fibonacci::FIBONACCI_UINT64_TABLE_SIZE);
    assert(fibonacci::FIBONACCI_UINT64_TABLE_SIZE
           < (1u << (fibonacci::FIBONACCI_UINT64_TABLE_LG_SIZE + 1)));

    TS_ASSERT_EQUALS(fibonacci::FIBONACCI_UINT64_TABLE[0], 0);
    TS_ASSERT_EQUALS(fibonacci::FIBONACCI_UINT64_TABLE[1], 1);
    TS_ASSERT_EQUALS(fibonacci::FIBONACCI_UINT64_TABLE[2], 1);

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < fibonacci::FIBONACCI_UINT64_TABLE_SIZE; ++n) {
      TS_ASSERT_EQUALS(fibonacci::FIBONACCI_UINT64_TABLE[n], f_n);

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__FIBONACCI_POW2_UINT64_TABLE_SIZE() {
    SHOW_FUNC_NAME;

    assert(fibonacci::FIBONACCI_POW2_UINT64_TABLE_SIZE >= 2);

    TS_ASSERT_EQUALS(fibonacci::FIBONACCI_POW2_UINT64_TABLE[0],
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::FIBONACCI_POW2_UINT64_TABLE[1],
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));

    uint64_t f_2i_1(0);  // F_{2^i - 1}
    uint64_t f_2i(1);    // F_{2^i}

    for (unsigned int i(0); i < fibonacci::FIBONACCI_POW2_UINT64_TABLE_SIZE;
         ++i) {
      TS_ASSERT_EQUALS(fibonacci::FIBONACCI_POW2_UINT64_TABLE[i],
                       std::make_pair(f_2i_1, f_2i));

      const uint64_t sqr_f_2i(f_2i*f_2i);

      f_2i = ((f_2i*f_2i_1) << 1) + sqr_f_2i;  // F_{2^{i+1}}
      f_2i_1 = sqr_f_2i + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
    }
  }


  void test__bit_length__unsigned_int() {
    SHOW_FUNC_NAME;

    TS_ASSERT(!std::numeric_limits<unsigned int>::is_signed);

    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<unsigned int>(0)), 0);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<unsigned int>(1)), 1);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<unsigned int>(2)), 2);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<unsigned int>(3)), 2);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<unsigned int>(4)), 3);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<unsigned int>(5)), 3);

    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<unsigned int>(-1)),
                     std::numeric_limits<unsigned int>::digits);
    TS_ASSERT_EQUALS
      (fibonacci::bit_length((static_cast<unsigned int>(-1)) >> 1),
       std::numeric_limits<unsigned int>::digits - 1);

    unsigned int pow2(1);

    for (unsigned int i(0) ;
         i < static_cast<unsigned int>(std::numeric_limits<unsigned int>
                                       ::digits);
         ++i, pow2 *= 2) {
      TS_ASSERT_EQUALS(fibonacci::bit_length(pow2 - 1), i);
      TS_ASSERT_EQUALS(fibonacci::bit_length(pow2), i + 1);
      if (i > 0) {
        TS_ASSERT_EQUALS(fibonacci::bit_length(pow2 + 1), i + 1);
      }
    }

    TS_ASSERT_EQUALS(pow2, 0);

    for (unsigned int n(0); n < 1000000; ++n) {
      unsigned int length(fibonacci::bit_length(n));

      TS_ASSERT_LESS_THAN_EQUALS(0, length);
      TS_ASSERT_LESS_THAN_EQUALS(length, 20);
      if (n > 0) {
        TS_ASSERT_EQUALS
          (length,
           static_cast<unsigned int>(std::floor(std::log2(n))) + 1);
      }

      length = fibonacci::bit_length(std::numeric_limits<unsigned int>::max()
                                     - n);

      TS_ASSERT_LESS_THAN_EQUALS(0, length);
      TS_ASSERT_LESS_THAN_EQUALS(length,
                                 std::numeric_limits<unsigned int>::digits);
      TS_ASSERT_EQUALS
        (length,
         static_cast<unsigned int>
         (std::floor(std::log2(std::numeric_limits<unsigned int>::max() - n)))
         + 1);
    }
  }


  void test__bit_length__uint64() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(std::numeric_limits<uint64_t>::digits, 64);
    TS_ASSERT(!std::numeric_limits<uint64_t>::is_signed);

    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<uint64_t>(0)), 0);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<uint64_t>(1)), 1);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<uint64_t>(2)), 2);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<uint64_t>(3)), 2);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<uint64_t>(4)), 3);
    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<uint64_t>(5)), 3);

    TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<uint64_t>(-1)),
                     std::numeric_limits<uint64_t>::digits);
    TS_ASSERT_EQUALS(fibonacci::bit_length((static_cast<uint64_t>(-1)) >> 1),
                     std::numeric_limits<uint64_t>::digits - 1);

    uint64_t pow2(1);

    for (unsigned int i(0); i < 64; ++i, pow2 *= 2) {
      TS_ASSERT_EQUALS(fibonacci::bit_length(pow2 - 1), i);
      TS_ASSERT_EQUALS(fibonacci::bit_length(pow2), i + 1);
      if (i > 0) {
        TS_ASSERT_EQUALS(fibonacci::bit_length(pow2 + 1), i + 1);
      }
    }

    TS_ASSERT_EQUALS(pow2, 0);

    for (unsigned int n(0); n < 1000000; ++n) {
      unsigned int length(fibonacci::bit_length(static_cast<uint64_t>(n)));

      TS_ASSERT_LESS_THAN_EQUALS(0, length);
      TS_ASSERT_LESS_THAN_EQUALS(length, 20);
      if (n > 0) {
        TS_ASSERT_EQUALS
          (length,
           static_cast<unsigned int>(std::floor(std::log2(n))) + 1);
      }

      length = fibonacci::bit_length(std::numeric_limits<uint64_t>::max() - n);

      TS_ASSERT_LESS_THAN_EQUALS(0, length);
      TS_ASSERT_LESS_THAN_EQUALS(length, 64);
      if (n < 10000) {
        TS_ASSERT_EQUALS
          (length,
           static_cast<unsigned int>
           (std::floor(std::log2(std::numeric_limits<uint64_t>::max() - n))));
      }
    }

    for (unsigned int n(0); n < 1000000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::bit_length(static_cast<uint64_t>(n)),
                       fibonacci::bit_length(n));

      TS_ASSERT_EQUALS
        (fibonacci::bit_length(static_cast<uint64_t>
                               ((std::numeric_limits<unsigned int>::max()
                                 - n))),
         fibonacci::bit_length(std::numeric_limits<unsigned int>::max() - n));
    }
  }


  void test__fibonacci_int64() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(0), 0);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(2), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(3), 2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(4), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(10), 55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(100),
                     3736710778780434371ull);  // 354224848179261915075 % 2^64

    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-2), -1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-3), 2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-4), -3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-9), 34);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-10), -55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-99), 16008811023750101250ull);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-100), -3736710778780434371ll);

    int64_t f_n_1(fibonacci::fibonacci_uint64(1001));
    int64_t f_n(-fibonacci::fibonacci_uint64(1000));

    for (signed int n(-1000); n < 1000; ++n) {
      TS_ASSERT_EQUALS(abs(fibonacci::fibonacci_int64(-n)), abs(f_n));

      TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(n), f_n);

      if ((-92 <= n) && (n <= 92)) {
        if ((n >= 0) || ((n % 2) != 0)) {  // not negative or odd
          TS_ASSERT_LESS_THAN_EQUALS(0, f_n);
        } else {                           // negative and even
          TS_ASSERT_LESS_THAN(f_n, 0);
        }
      }

      const int64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_int64_pair() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(0),
                     std::make_pair(static_cast<int64_t>(1),
                                    static_cast<int64_t>(0)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(1),
                     std::make_pair(static_cast<int64_t>(0),
                                    static_cast<int64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(2),
                     std::make_pair(static_cast<int64_t>(1),
                                    static_cast<int64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(3),
                     std::make_pair(static_cast<int64_t>(1),
                                    static_cast<int64_t>(2)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(4),
                     std::make_pair(static_cast<int64_t>(2),
                                    static_cast<int64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(10),
                     std::make_pair(static_cast<int64_t>(34),
                                    static_cast<int64_t>(55)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_int64_pair(100),
       // 218922995834555169026 % 2^64, 354224848179261915075 % 2^64
       std::make_pair(static_cast<int64_t>(16008811023750101250ull),
                      static_cast<int64_t>(3736710778780434371ull)));

    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(-1),
                     std::make_pair(static_cast<int64_t>(-1),
                                    static_cast<int64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(-2),
                     std::make_pair(static_cast<int64_t>(2),
                                    static_cast<int64_t>(-1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(-3),
                     std::make_pair(static_cast<int64_t>(-3),
                                    static_cast<int64_t>(2)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(-4),
                     std::make_pair(static_cast<int64_t>(5),
                                    static_cast<int64_t>(-3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(-9),
                     std::make_pair(static_cast<int64_t>(-55),
                                    static_cast<int64_t>(34)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(-10),
                     std::make_pair(static_cast<int64_t>(89),
                                    static_cast<int64_t>(-55)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_int64_pair(-99),
       // -354224848179261915075 % 2^64, 218922995834555169026 % 2^64
       std::make_pair(static_cast<int64_t>(-3736710778780434371ll),
                      static_cast<int64_t>(16008811023750101250ull)));
    TS_ASSERT_EQUALS
       (fibonacci::fibonacci_int64_pair(-100),
       // 573147844013817084101 % 2^64, -354224848179261915075 % 2^64
        std::make_pair(static_cast<int64_t>(1298777728820984005ull),
                       static_cast<int64_t>(-3736710778780434371ll)));

    int64_t f_n_1(fibonacci::fibonacci_uint64(1001));
    int64_t f_n(-fibonacci::fibonacci_uint64(1000));

    for (signed int n(-1000); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_int64_pair(n),
                       std::make_pair(f_n_1, f_n));

      const int64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(0), 0);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(2), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(3), 2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(4), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(10), 55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(100),
                     3736710778780434371ull);  // 354224848179261915075 % 2^64

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(n), f_n);

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64__iter_lg() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(0), 0);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(2), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(3), 2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(4), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(10), 55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(100),
                     3736710778780434371ull);  // 354224848179261915075 % 2^64

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(n), f_n);

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64__iter_lg_table() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(0), 0);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(2), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(3), 2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(4), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(10), 55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(100),
                     3736710778780434371ull);  // 354224848179261915075 % 2^64

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(n), f_n);

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64__iter_lg_optimized() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(0), 0);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(2), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(3), 2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(4), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(10), 55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(100),
                     3736710778780434371ull);  // 354224848179261915075 % 2^64

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(n), f_n);

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64__iter_lg_optimized_table() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(0),
                     0);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(1),
                     1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(2),
                     1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(3),
                     2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(4),
                     3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(10),
                     55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(100),
                     3736710778780434371ull);  // 354224848179261915075 % 2^64

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(n),
                       f_n);

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64__iter_n() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_n(0), 0);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_n(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_n(2), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_n(3), 2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_n(4), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_n(10), 55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_n(100),
                     3736710778780434371ull);  // 354224848179261915075 % 2^64

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_n(n), f_n);

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64__rec_exp() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(0), 0);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(2), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(3), 2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(4), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(10), 55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(20), 6765);
    // TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(100),
    //                 3736710778780434371ull);  // 354224848179261915075 % 2^64

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(n), f_n);

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64__table_const() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__table_const(0), 0);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__table_const(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__table_const(2), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__table_const(3), 2);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__table_const(4), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__table_const(10), 55);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__table_const(20), 6765);

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < fibonacci::FIBONACCI_UINT64_TABLE_SIZE; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__table_const(n), f_n);

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64_pair()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair(0),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(0)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair(1),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair(2),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair(3),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(2)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair(4),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair(10),
                     std::make_pair(static_cast<uint64_t>(34),
                                    static_cast<uint64_t>(55)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pair(100),
       // 218922995834555169026 % 2^64, 354224848179261915075 % 2^64
       std::make_pair(static_cast<uint64_t>(16008811023750101250ull),
                      static_cast<uint64_t>(3736710778780434371ull)));

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair(n),
                       std::make_pair(f_n_1, f_n));

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64_pair__iter_lg()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(0),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(0)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(1),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(2),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(3),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(2)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(4),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(10),
                     std::make_pair(static_cast<uint64_t>(34),
                                    static_cast<uint64_t>(55)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pair__iter_lg(100),
       // 218922995834555169026 % 2^64, 354224848179261915075 % 2^64
       std::make_pair(static_cast<uint64_t>(16008811023750101250ull),
                      static_cast<uint64_t>(3736710778780434371ull)));

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(n),
                       std::make_pair(f_n_1, f_n));

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64_pair__iter_lg_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(0),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(0)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(1),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(2),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(3),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(2)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(4),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(10),
                     std::make_pair(static_cast<uint64_t>(34),
                                    static_cast<uint64_t>(55)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pair__iter_lg_table(100),
       // 218922995834555169026 % 2^64, 354224848179261915075 % 2^64
       std::make_pair(static_cast<uint64_t>(16008811023750101250ull),
                      static_cast<uint64_t>(3736710778780434371ull)));

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(n),
                       std::make_pair(f_n_1, f_n));

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64_pair__rec_lg()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(0),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(0)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(1),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(2),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(3),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(2)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(4),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(10),
                     std::make_pair(static_cast<uint64_t>(34),
                                    static_cast<uint64_t>(55)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pair__rec_lg(100),
       // 218922995834555169026 % 2^64, 354224848179261915075 % 2^64
       std::make_pair(static_cast<uint64_t>(16008811023750101250ull),
                      static_cast<uint64_t>(3736710778780434371ull)));

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(n),
                       std::make_pair(f_n_1, f_n));

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64_pair__rec_lg_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(0),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(0)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(1),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(2),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(3),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(2)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(4),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(10),
                     std::make_pair(static_cast<uint64_t>(34),
                                    static_cast<uint64_t>(55)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pair__rec_lg_table(100),
       // 218922995834555169026 % 2^64, 354224848179261915075 % 2^64
       std::make_pair(static_cast<uint64_t>(16008811023750101250ull),
                      static_cast<uint64_t>(3736710778780434371ull)));

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(n),
                       std::make_pair(f_n_1, f_n));

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64_pair__rec_n()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_n(0),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(0)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_n(1),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_n(2),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_n(3),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(2)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_n(4),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_n(10),
                     std::make_pair(static_cast<uint64_t>(34),
                                    static_cast<uint64_t>(55)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pair__rec_n(100),
       // 218922995834555169026 % 2^64, 354224848179261915075 % 2^64
       std::make_pair(static_cast<uint64_t>(16008811023750101250ull),
                      static_cast<uint64_t>(3736710778780434371ull)));

    uint64_t f_n_1(1);
    uint64_t f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_n(n),
                       std::make_pair(f_n_1, f_n));

      const uint64_t t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_uint64_pow2()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(0), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(2), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(3), 21);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(4), 987);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(7),
                     // 251728825683549488150424261 % 2^64
                     18154666814248790725ull);

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci::fibonacci_uint64_pow2(n),
         fibonacci::fibonacci_uint64(1u << n));
    }
  }


  void test__fibonacci_uint64_pow2__iter_lucas_n()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n(0), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n(1), 1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n(2), 3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n(3), 21);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n(4), 987);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n(7),
                     // 251728825683549488150424261 % 2^64
                     18154666814248790725ull);

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci::fibonacci_uint64_pow2__iter_lucas_n(n),
         fibonacci::fibonacci_uint64(1u << n));
    }
  }


  void test__fibonacci_uint64_pow2__iter_lucas_n_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n_table(0),
                     1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n_table(1),
                     1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n_table(2),
                     3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n_table(3),
                     21);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n_table(4),
                     987);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n_table(7),
                     // 251728825683549488150424261 % 2^64
                     18154666814248790725ull);

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci::fibonacci_uint64_pow2__iter_lucas_n_table(n),
         fibonacci::fibonacci_uint64(1u << n));
    }
  }


  void test__fibonacci_uint64_pow2__table_const() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__table_const(0),
                     1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__table_const(1),
                     1);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__table_const(2),
                     3);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__table_const(3),
                     21);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__table_const(4),
                     987);
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__table_const(7),
                     // 251728825683549488150424261 % 2^64
                     18154666814248790725ull);

    for (unsigned int n(0);
         n < fibonacci::FIBONACCI_POW2_UINT64_TABLE_SIZE; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci::fibonacci_uint64_pow2__table_const(n),
         fibonacci::fibonacci_uint64(1u << n));
    }
  }


  void test__fibonacci_uint64_pow2_pair()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair(0),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair(1),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair(2),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair(3),
                     std::make_pair(static_cast<uint64_t>(13),
                                    static_cast<uint64_t>(21)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair(4),
                     std::make_pair(static_cast<uint64_t>(610),
                                    static_cast<uint64_t>(987)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair(7),
       // 155576970220531065681649693 % 2^64, 251728825683549488150424261 % 2^64
       std::make_pair(static_cast<uint64_t>(8394940206042357789ull),
                      static_cast<uint64_t>(18154666814248790725ull)));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci::fibonacci_uint64_pow2_pair(n),
         fibonacci::fibonacci_uint64_pair(1u << n));
    }
  }


  void test__fibonacci_uint64_pow2_pair__iter_lucas_n()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n(0),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n(1),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n(2),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n(3),
                     std::make_pair(static_cast<uint64_t>(13),
                                    static_cast<uint64_t>(21)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n(4),
                     std::make_pair(static_cast<uint64_t>(610),
                                    static_cast<uint64_t>(987)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n(7),
       // 155576970220531065681649693 % 2^64, 251728825683549488150424261 % 2^64
       std::make_pair(static_cast<uint64_t>(8394940206042357789ull),
                      static_cast<uint64_t>(18154666814248790725ull)));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n(n),
         fibonacci::fibonacci_uint64_pair(1u << n));
    }
  }


  void test__fibonacci_uint64_pow2_pair__iter_lucas_n_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n_table(0),
       std::make_pair(static_cast<uint64_t>(0),
                      static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n_table(1),
       std::make_pair(static_cast<uint64_t>(1),
                      static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n_table(2),
       std::make_pair(static_cast<uint64_t>(2),
                      static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n_table(3),
       std::make_pair(static_cast<uint64_t>(13),
                      static_cast<uint64_t>(21)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n_table(4),
       std::make_pair(static_cast<uint64_t>(610),
                      static_cast<uint64_t>(987)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n_table(7),
       // 155576970220531065681649693 % 2^64, 251728825683549488150424261 % 2^64
       std::make_pair(static_cast<uint64_t>(8394940206042357789ull),
                      static_cast<uint64_t>(18154666814248790725ull)));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n_table(n),
         fibonacci::fibonacci_uint64_pair(1u << n));
    }
  }


  void test__fibonacci_uint64_pow2_pair__iter_n()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n(0),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n(1),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n(2),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n(3),
                     std::make_pair(static_cast<uint64_t>(13),
                                    static_cast<uint64_t>(21)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n(4),
                     std::make_pair(static_cast<uint64_t>(610),
                                    static_cast<uint64_t>(987)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair__iter_n(7),
       // 155576970220531065681649693 % 2^64, 251728825683549488150424261 % 2^64
       std::make_pair(static_cast<uint64_t>(8394940206042357789ull),
                      static_cast<uint64_t>(18154666814248790725ull)));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci::fibonacci_uint64_pow2_pair__iter_n(n),
         fibonacci::fibonacci_uint64_pair(1u << n));
    }
  }


  void test__fibonacci_uint64_pow2_pair__iter_n_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n_table(0),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n_table(1),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n_table(2),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n_table(3),
                     std::make_pair(static_cast<uint64_t>(13),
                                    static_cast<uint64_t>(21)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__iter_n_table(4),
                     std::make_pair(static_cast<uint64_t>(610),
                                    static_cast<uint64_t>(987)));
    TS_ASSERT_EQUALS
      (fibonacci::fibonacci_uint64_pow2_pair__iter_n_table(7),
       // 155576970220531065681649693 % 2^64, 251728825683549488150424261 % 2^64
       std::make_pair(static_cast<uint64_t>(8394940206042357789ull),
                      static_cast<uint64_t>(18154666814248790725ull)));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci::fibonacci_uint64_pow2_pair__iter_n_table(n),
         fibonacci::fibonacci_uint64_pair(1u << n));
    }
  }


  void test__fibonacci_uint64_pow2_pair__table_const() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__table_const(0),
                     std::make_pair(static_cast<uint64_t>(0),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__table_const(1),
                     std::make_pair(static_cast<uint64_t>(1),
                                    static_cast<uint64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__table_const(2),
                     std::make_pair(static_cast<uint64_t>(2),
                                    static_cast<uint64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__table_const(3),
                     std::make_pair(static_cast<uint64_t>(13),
                                    static_cast<uint64_t>(21)));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__table_const(4),
                     std::make_pair(static_cast<uint64_t>(610),
                                    static_cast<uint64_t>(987)));

    for (unsigned int n(0);
         n < fibonacci::FIBONACCI_POW2_UINT64_TABLE_SIZE; ++n) {
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2_pair__table_const(n),
                       fibonacci::fibonacci_uint64_pair(1u << n));
    }
  }


  void test__lucas_int64()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::lucas_int64(0), 2);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(1), 1);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(2), 3);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(3), 4);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(4), 7);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(10), 123);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(100),
                     17307588752571085255ull);  // 792070839848372253127 % 2^64

    TS_ASSERT_EQUALS(fibonacci::lucas_int64(-1), -1);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(-2), 3);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(-3), -4);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(-4), 7);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(-9), -76);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(-10), 123);
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(-99),
                     8535389466189232508ll);  // -489526700523968661124 % 2^64
    TS_ASSERT_EQUALS(fibonacci::lucas_int64(-100),
                     17307588752571085255ull);  // 792070839848372253127 % 2^64

    int64_t l_n_1(-fibonacci::lucas_int64(1001));
    int64_t l_n(fibonacci::lucas_int64(1000));

    for (signed int n(-1000); n < 1000; ++n) {
      TS_ASSERT_EQUALS(abs(fibonacci::lucas_int64(-n)), abs(l_n));

      TS_ASSERT_EQUALS(fibonacci::lucas_int64(n), l_n);

      if ((-90 <= n) && (n <= 90)) {
        if ((n >= 0) || ((n % 2) == 0)) {  // not negative or even
          TS_ASSERT_LESS_THAN_EQUALS(0, l_n);
        } else {                           // negative and odd
          TS_ASSERT_LESS_THAN(l_n, 0);
        }
      }

      const int64_t t(l_n_1);

      l_n_1 = l_n;
      l_n += t;
    }
  }


  void test__lucas_int64_pair()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(0),
                     std::make_pair(static_cast<int64_t>(-1),
                                    static_cast<int64_t>(2)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(1),
                     std::make_pair(static_cast<int64_t>(2),
                                    static_cast<int64_t>(1)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(2),
                     std::make_pair(static_cast<int64_t>(1),
                                    static_cast<int64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(3),
                     std::make_pair(static_cast<int64_t>(3),
                                    static_cast<int64_t>(4)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(4),
                     std::make_pair(static_cast<int64_t>(4),
                                    static_cast<int64_t>(7)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(10),
                     std::make_pair(static_cast<int64_t>(76),
                                    static_cast<int64_t>(123)));
    TS_ASSERT_EQUALS
      (fibonacci::lucas_int64_pair(100),
       // 489526700523968661124 % 2^64, 792070839848372253127 % 2^64
       std::make_pair(static_cast<int64_t>(9911354607520319108ull),
                      static_cast<int64_t>(17307588752571085255ull)));

    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(-1),
                     std::make_pair(static_cast<int64_t>(3),
                                    static_cast<int64_t>(-1)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(-2),
                     std::make_pair(static_cast<int64_t>(-4),
                                    static_cast<int64_t>(3)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(-3),
                     std::make_pair(static_cast<int64_t>(7),
                                    static_cast<int64_t>(-4)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(-4),
                     std::make_pair(static_cast<int64_t>(-11),
                                    static_cast<int64_t>(7)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(-9),
                     std::make_pair(static_cast<int64_t>(123),
                                    static_cast<int64_t>(-76)));
    TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(-10),
                     std::make_pair(static_cast<int64_t>(-199),
                                    static_cast<int64_t>(123)));
    // 792070839848372253127 % 2^64, -489526700523968661124 % 2^64
    TS_ASSERT_EQUALS
      (fibonacci::lucas_int64_pair(-99),
       std::make_pair(static_cast<int64_t>(17307588752571085255ull),
                      static_cast<int64_t>(8535389466189232508ll)));
    // -1281597540372340914251% 2^64, 792070839848372253127 % 2^64
    TS_ASSERT_EQUALS
      (fibonacci::lucas_int64_pair(-100),
       std::make_pair(static_cast<int64_t>(9674544787327698869ull),
                      static_cast<int64_t>(17307588752571085255ull)));

    int64_t l_n_1(-fibonacci::lucas_int64(1001));
    int64_t l_n(fibonacci::lucas_int64(1000));

    for (signed int n(-1000); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci::lucas_int64_pair(n),
                       std::make_pair(l_n_1, l_n));

      const int64_t t(l_n_1);

      l_n_1 = l_n;
      l_n += t;
    }
  }


  void test_check_implementions()  {
    SHOW_FUNC_NAME;

    // First 10000 n
    for (unsigned int n(0); n < 10000; ++n) {
      const std::pair <uint64_t, uint64_t>
        p(fibonacci::fibonacci_uint64_pair(n));

      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_n(n), p.second);

      if (n < 20) {
        TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__rec_exp(n), p.second);
      }

      if (n < fibonacci::FIBONACCI_UINT64_TABLE_SIZE) {
        TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__table_const(n), p.second);
      }

      TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-static_cast<signed int>(n)),
                       (n % 2 == 0
                        ? -p.second    // even
                        : p.second));  // odd

      TS_ASSERT_EQUALS
        (p.first, fibonacci::fibonacci_int64(static_cast<signed int>(n) - 1));
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(n), p);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(n), p);

      if (n < 100) {
        TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_n(n), p);
      }
    }

    // 20000 n around max signed int
    for (unsigned int
           n(static_cast<unsigned int>(std::numeric_limits<signed int>::max())
             - 1000);
         n < static_cast<unsigned int>(std::numeric_limits<signed int>::max())
           + 10000;
         ++n) {
      const std::pair <uint64_t, uint64_t>
        p(fibonacci::fibonacci_uint64_pair(n));

      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(n), p.second);
      if (n
          < static_cast<unsigned int>(std::numeric_limits<signed int>::max())) {
        TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(n), p.second);
      }
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(n),
                       p.second);

      if (n
          < static_cast<unsigned int>(std::numeric_limits<signed int>::max())) {
        TS_ASSERT_EQUALS
          (fibonacci::fibonacci_int64(-static_cast<signed int>(n)),
           (n % 2 == 0
            ? -p.second    // even
            : p.second));  // odd

        TS_ASSERT_EQUALS
          (p.first, fibonacci::fibonacci_int64(static_cast<signed int>(n) - 1));
      }

      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(n), p);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(n), p);
    }

    // Last 10000 n for unsigned int
    for (unsigned int n(std::numeric_limits<unsigned int>::max());
         n > std::numeric_limits<unsigned int>::max() - 10000; --n) {
      const std::pair <uint64_t, uint64_t>
        p(fibonacci::fibonacci_uint64_pair(n));

      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_table(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64__iter_lg_optimized_table(n),
                       p.second);

      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__iter_lg_table(n), p);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pair__rec_lg_table(n), p);
    }

    // Powers of 2
    for (unsigned int n(0); n < 32; ++n) {
      const std::pair <uint64_t, uint64_t>
        p(fibonacci::fibonacci_uint64_pow2_pair(n));

      TS_ASSERT_EQUALS
        (p, fibonacci::fibonacci_uint64_pair(1u << n));

      TS_ASSERT_EQUALS
        (p, fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n(n));
      TS_ASSERT_EQUALS
        (p, fibonacci::fibonacci_uint64_pow2_pair__iter_lucas_n_table(n));
      TS_ASSERT_EQUALS
        (p, fibonacci::fibonacci_uint64_pow2_pair__iter_n(n));
      TS_ASSERT_EQUALS
        (p, fibonacci::fibonacci_uint64_pow2_pair__iter_n_table(n));
      if (n < fibonacci::FIBONACCI_POW2_UINT64_TABLE_SIZE) {
        TS_ASSERT_EQUALS
          (p, fibonacci::fibonacci_uint64_pow2_pair__table_const(n));
      }

      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(n), p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__iter_lucas_n_table(n),
                       p.second);
      if (n < fibonacci::FIBONACCI_POW2_UINT64_TABLE_SIZE) {
        TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2__table_const(n),
                         p.second);
      }
    }
  }


  void test_formulas_a_b()  {
    SHOW_FUNC_NAME;

    for (signed int a(-150); a < 150; ++a) {
      const std::pair <int64_t, int64_t> p(fibonacci::fibonacci_int64_pair(a));
      const int64_t f_a_1(p.first);
      const int64_t f_a(p.second);
      const int64_t f_a_p1(f_a + f_a_1);

      for (signed int b(-150); b < 150; ++b) {
        const std::pair <int64_t, int64_t>
          p(fibonacci::fibonacci_int64_pair(b));
        const int64_t f_b_1(p.first);
        const int64_t f_b(p.second);

        // F_{a+b} = F_{a+1} F_b + F_a F_{b-1}   [modulo 2^{64}],
        // forall a, b integer
        TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(a + b),
                         f_a_p1*f_b + f_a*f_b_1);

        // F_{a-b} = {-1}^b {F_a F_{b-1} - F_{a-1} F_b}   [modulo 2^{64}],
        // forall a, b integer
        if ((b % 2) == 0) {  // b even
          TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(a - b),
                           f_a*f_b_1 - f_a_1*f_b);
        } else {             // b odd
          TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(a - b),
                           f_a_1*f_b - f_a*f_b_1);
        }
      }
    }
  }


  void test_formulas_2n()  {
    SHOW_FUNC_NAME;

    for (signed int n(-1000); n < 1000; ++n) {
      std::pair <int64_t, int64_t> p(fibonacci::fibonacci_int64_pair(n));
      const int64_t f_n_1(p.first);
      const int64_t f_n(p.second);
      const int64_t f_n_p1(f_n + f_n_1);

      const int64_t sqr_f_n_1(f_n_1*f_n_1);
      const int64_t sqr_f_n(f_n*f_n);
      const int64_t sqr_f_n_p1(f_n_p1*f_n_p1);

      p = fibonacci::fibonacci_int64_pair(2*n);
      const int64_t f_2n_1(p.first);
      const int64_t f_2n(p.second);
      const int64_t f_2n_p1(f_2n + f_2n_1);

      // F_{2n - 1} = F^2_n + F^2_{n-1}   [modulo 2^{64}], forall n integer
      TS_ASSERT_EQUALS(f_2n_1, sqr_f_n + sqr_f_n_1);

      // F_2n = F^2_{n+1} - F^2_{n-1}
      //      = 2 F_n F_{n-1} + F^2_n   [modulo 2^{64}], forall n integer
      TS_ASSERT_EQUALS(f_2n, sqr_f_n_p1 - sqr_f_n_1);
      TS_ASSERT_EQUALS(f_2n, 2*f_n*f_n_1 + sqr_f_n);

      // F_{2n + 1} = F^2_{n+1} + F^2_n   [modulo 2^{64}], forall n integer
      TS_ASSERT_EQUALS(f_2n_p1, sqr_f_n_p1 + sqr_f_n);
    }
  }


  void test_formulas_pow2()  {
    SHOW_FUNC_NAME;

    for (unsigned int i(0); i < 20; ++i) {
      std::pair <uint64_t, uint64_t>
        p(fibonacci::fibonacci_uint64_pow2_pair(i));
      const uint64_t f_2i_1(p.first);
      const uint64_t f_2i(p.second);
      const uint64_t f_2i_p1(f_2i + f_2i_1);

      const uint64_t sqr_f_2i_1(f_2i_1*f_2i_1);
      const uint64_t sqr_f_2i(f_2i*f_2i);
      const uint64_t sqr_f_2i_p1(f_2i_p1*f_2i_p1);

      p = fibonacci::fibonacci_uint64_pow2_pair(i + 1);

      const uint64_t f_2ip1_1(p.first);
      const uint64_t f_2ip1(p.second);

      // F_{2^{i+1} - 1} = F^2_{2^i} + F^2_{2^i - 1}   [modulo 2^{64}],
      // forall i integer >= 0
      TS_ASSERT_EQUALS(f_2ip1_1, sqr_f_2i + sqr_f_2i_1);

      // F_{2^{i+1}} = F^2_{2^i + 1} - F^2_{2^i - 1}
      //             = 2 F_{2^i} F_{2^i - 1} + F^2_{2^i} [modulo 2^{64}],
      // forall i integer >= 0
      TS_ASSERT_EQUALS(f_2ip1, sqr_f_2i_p1 - sqr_f_2i_1);
      TS_ASSERT_EQUALS(f_2ip1, 2*f_2i*f_2i_1 + sqr_f_2i);

      // F_{2^{i+1) + 1} = F^2_{2^i + 1} + F^2_{2^i}   [modulo 2^{64}],
      // forall i integer >= 0
      TS_ASSERT_EQUALS(f_2ip1 + f_2ip1_1, sqr_f_2i_p1 + sqr_f_2i);
    }
  }


  void test_formulas_n_add_pow2()  {
    SHOW_FUNC_NAME;

    for (signed int n(-100); n < 100; ++n) {
      const std::pair <int64_t, int64_t> p(fibonacci::fibonacci_int64_pair(n));
      const int64_t f_n_1(p.first);
      const int64_t f_n(p.second);
      const int64_t f_n_p1(f_n + f_n_1);

      for (unsigned int i(0); i < 15; ++i) {
        std::pair <uint64_t, uint64_t>
          p(fibonacci::fibonacci_uint64_pow2_pair(i));
        const uint64_t f_2i_1(p.first);
        const uint64_t f_2i(p.second);
        const uint64_t f_2i_p1(f_2i + f_2i_1);

        p = fibonacci::fibonacci_int64_pair(n + (1 << i));

        const int64_t f_n_2i_1(p.first);
        const int64_t f_n_2i(p.second);
        const int64_t f_n_2i_p1(f_n_2i + f_n_2i_1);

        // F_{n + 2^i - 1} = F_n F_{2^i} + F_{n-1} F_{2^i - 1}
        // [modulo 2^{64}], forall n integer, forall i integer >= 0
        TS_ASSERT_EQUALS(f_n_2i_1, f_n*f_2i + f_n_1*f_2i_1);

        // F_{n + 2^i} = F_{n+1} F_{2^i} + F_n F_{2^i - 1}
        //             = F_n F_{2^i} + F_n F_{2^i - 1} + F_{n - 1} F_{2^i}
        // [modulo 2^{64}], forall n integer, forall i integer >= 0
        TS_ASSERT_EQUALS(f_n_2i, f_n_p1*f_2i + f_n*f_2i_1);
        TS_ASSERT_EQUALS(f_n_2i, f_n*f_2i + f_n*f_2i_1 + f_n_1*f_2i);

        // F_{n + 2^i + 1} = F_{n+1} F_{2^i + 1} + F_n F_{2^i}
        // [modulo 2^{64}], forall n integer, forall i integer >= 0
        TS_ASSERT_EQUALS(f_n_2i_p1, f_n_p1*f_2i_p1 + f_n*f_2i);
      }
    }
  }


  void test_formulas_lucas()  {
    SHOW_FUNC_NAME;

    for (signed int n(-1000); n < 1000; ++n) {
      const std::pair <int64_t, int64_t> p(fibonacci::fibonacci_int64_pair(n));
      const int64_t f_n_p1(p.second + p.first);

      // L_n = F_{n + 1} + F_{n - 1}   [modulo 2^{64}], forall n integers
      TS_ASSERT_EQUALS(fibonacci::lucas_int64(n), f_n_p1 + p.first);
    }
  }


  void test_end() {
    std::cout
      << "--- Total: "
      << round<std::chrono::seconds>(std::chrono::steady_clock::now()
                                     - total_start).count()
      << "s ---" << std::endl;
  }
};
