/* -*- coding: latin-1 -*- */
/** \file fibonacci.cpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>

#include <utility>

#include "fibonacci.hpp"


namespace fibonacci {

  const unsigned int FIBONACCI_UINT64_TABLE_LG_SIZE(11);

  const unsigned int FIBONACCI_UINT64_TABLE_SIZE(2049);

  const uint64_t FIBONACCI_UINT64_TABLE[FIBONACCI_UINT64_TABLE_SIZE]{
#include "table_2049_Fn_uint64.h"
  };


  const unsigned int FIBONACCI_POW2_UINT64_TABLE_SIZE(20);

  const std::pair<uint64_t, uint64_t>
  FIBONACCI_POW2_UINT64_TABLE[FIBONACCI_POW2_UINT64_TABLE_SIZE]{
#include "table_20_F2i_uint64.h"
  };



  uint64_t
  fibonacci_uint64__iter_lg(unsigned int n) {
    if (n != 0) {
      // k = 0
      uint64_t f_k_1(1);  // F_{k-1}
      uint64_t f_k(0);    // F_k

      // i = 0
      uint64_t f_2i_1(0);  // F_{2^i - 1}
      uint64_t f_2i(1);    // F_{2^i}

      while (true) {
        if ((n & 1) != 0) {  // ith bit of initial n is 1
          const uint64_t t(f_k*f_2i);

          f_k = t + f_k_1*f_2i + f_k*f_2i_1;  // F_{k + 2^i}
          f_k_1 = t + f_k_1*f_2i_1;           // F_{k + 2^i - 1}
          // k <- k + 2^i = k + 10...0

          if (n == 1) {  // inv: k = initial n
            return f_k;
          }
        }

        const uint64_t t(f_2i*f_2i);

        f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
        f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
        // ++i

        n >>= 1;  // n <- n/2
      }
    } else {
      return 0;
    }
  }


  uint64_t
  fibonacci_uint64__iter_lg_table(unsigned int n) {
    unsigned int i(FIBONACCI_UINT64_TABLE_LG_SIZE);
    const unsigned int k(n & ((1u << i) - 1));  // n % (2^i - 1)

    uint64_t f_k(FIBONACCI_UINT64_TABLE[k]);  // F_k

    n >>= i;  // new initial n

    if (n == 0) {
      return f_k;
    }

    uint64_t f_k_1(k != 0
                   ? FIBONACCI_UINT64_TABLE[k - 1]
                   : 1);                            // F_{k-1}

    std::pair<uint64_t, uint64_t>
      f_2i_1__f_2i(fibonacci_uint64_pow2_pair__table_const(i));

    uint64_t& f_2i_1(f_2i_1__f_2i.first);  // F_{2^i - 1}
    uint64_t& f_2i(f_2i_1__f_2i.second);   // F_{2^i}

    while (true) {
      if ((n & 1) != 0) {  // ith bit of initial n is 1
        const uint64_t t(f_k*f_2i);

        f_k = t + f_k_1*f_2i + f_k*f_2i_1;  // F_{k + 2^i}
        f_k_1 = t + f_k_1*f_2i_1;           // F_{k + 2^i - 1}
        // k <- k + 2^i = k + 10...0

        if (n == 1) {  // inv: k = initial n
          return f_k;
        }
      }

      ++i;
      if (i < FIBONACCI_POW2_UINT64_TABLE_SIZE) {
        f_2i_1__f_2i = fibonacci_uint64_pow2_pair__table_const(i);
      } else {
        const uint64_t t(f_2i*f_2i);

        f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
        f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
      }

      n >>= 1;  // n <- n/2
    }
  }


  uint64_t
  fibonacci_uint64__iter_lg_optimized(unsigned int n) {
    // k = 0
    uint64_t f_k_1(1);  // F_{k-1}
    uint64_t f_k(0);    // F_k

    // i = 0
    uint64_t f_2i_1(0);  // F_{2^i - 1}
    uint64_t f_2i(1);    // F_{2^i}

    const unsigned int odd(n & 1);  // n is odd, its first bit is 1

    // First consecutive bits equals to 0...0 or 1...1
    while (((n & 1) == odd) && (n != 0)) {  // same bit as first_bit
      const uint64_t t(f_2i*f_2i);

      f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
      f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
      // ++i

      n >>= 1;  // n <- n/2
    }

    if (odd) {            // (i + 1) first consecutive bits equals to 01...1
      f_k_1 = f_2i - f_2i_1;  // F_{2^i - 2}
      f_k = f_2i_1;           // F_{2^i - 1}
    } else if (n != 0) {  // (i + 1) first consecutive bits equals to 10...0
      f_k_1 = f_2i_1;  // F_{2^i - 1}
      f_k = f_2i;      // F_{2^i}
    }

    // Remaining bits
    n >>= 1;  // n <- n/2
    while (n != 0) {
      const uint64_t t(f_2i*f_2i);

      f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
      f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
      // ++i

      if ((n & 1) != 0) {  // ith bit of initial n is 1
        const uint64_t t(f_k*f_2i);

        f_k = t + f_k_1*f_2i + f_k*f_2i_1;  // F_{k + 2^i}
        f_k_1 = t + f_k_1*f_2i_1;           // F_{k + 2^i - 1}
        // k <- k + 2^i = k + 10...0
      }

      n >>= 1;  // n <- n/2
    }

    // inv: k = initial n

    return f_k;
  }


  uint64_t
  fibonacci_uint64__iter_lg_optimized_table(unsigned int n) {
    // k = 0
    uint64_t f_k_1(1);  // F_{k-1}
    uint64_t f_k(0);    // F_k

    unsigned int i(0);

    const unsigned int odd(n & 1);  // n is odd, its first bit is 1

    // First consecutive bits equals to 0...0 or 1...1
    while (((n & 1) == odd) && (n != 0)) {  // same bit as first_bit
      ++i;
      n >>= 1;  // n <- n/2
    }

    std::pair<uint64_t, uint64_t> f_2i_1__f_2i;
    uint64_t& f_2i_1(f_2i_1__f_2i.first);  // F_{2^i - 1}
    uint64_t& f_2i(f_2i_1__f_2i.second);   // F_{2^i}

    if (i < FIBONACCI_POW2_UINT64_TABLE_SIZE) {
      f_2i_1__f_2i = fibonacci_uint64_pow2_pair__table_const(i);
    } else {
      f_2i_1__f_2i = fibonacci_uint64_pow2_pair__table_const
        (FIBONACCI_POW2_UINT64_TABLE_SIZE - 1);

      for (unsigned int j(FIBONACCI_POW2_UINT64_TABLE_SIZE - 1); j < i; ++j) {
        const uint64_t t(f_2i*f_2i);

        f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{j+1}}
        f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{j+1} - 1}
      }
    }

    if (odd) {            // (i + 1) first consecutive bits equals to 01...1
      f_k_1 = f_2i - f_2i_1;  // F_{2^i - 2}
      f_k = f_2i_1;           // F_{2^i - 1}
    } else if (n != 0) {  // (i + 1) first consecutive bits equals to 10...0
      f_k_1 = f_2i_1;  // F_{2^i - 1}
      f_k = f_2i;      // F_{2^i}
    }

    // Remaining bits
    n >>= 1;  // n <- n/2
    while (n != 0) {
      ++i;
      if (i < FIBONACCI_POW2_UINT64_TABLE_SIZE) {
        f_2i_1__f_2i = fibonacci_uint64_pow2_pair__table_const(i);
      } else {
        const uint64_t t(f_2i*f_2i);

        f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
        f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
      }

      if ((n & 1) != 0) {  // ith bit of initial n is 1
        const uint64_t t(f_k*f_2i);

        f_k = t + f_k_1*f_2i + f_k*f_2i_1;  // F_{k + 2^i}
        f_k_1 = t + f_k_1*f_2i_1;           // F_{k + 2^i - 1}
        // k <- k + 2^i = k + 10...0
      }

      n >>= 1;  // n <- n/2
    }

    // inv: k = initial n

    return f_k;
  }


  uint64_t
  fibonacci_uint64__iter_n(unsigned int n) {
    uint64_t f_k_1(1);  // F_{k-1}
    uint64_t f_k(0);    // F_k

    while (n-- > 0) {
      const uint64_t t(f_k + f_k_1);

      f_k_1 = f_k;
      f_k = t;
    }

    return f_k;
  }


  uint64_t
  fibonacci_uint64__rec_exp(unsigned int n) {
    return (n <= 1
            ? n
            : (fibonacci_uint64__rec_exp(n - 1)
               + fibonacci_uint64__rec_exp(n - 2)));
  }


  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__iter_lg(unsigned int n) {
    uint64_t f_k_1(1);
    uint64_t f_k(0);

    if (n != 0) {
      unsigned int pow2(1ul << (bit_length(n) - 1));

      while (pow2 != 0) {
        uint64_t sqr_f_k(f_k*f_k);

        if ((n & pow2) == 0) {  // bit 0
          f_k = ((f_k*f_k_1) << 1) + sqr_f_k;
          f_k_1 = sqr_f_k + f_k_1*f_k_1;
          // k <- k*2
        } else {                // bit 1
          const uint64_t f_k_p1(f_k + f_k_1);

          f_k_1 = ((f_k*f_k_1) << 1) + sqr_f_k;
          f_k = f_k_p1*f_k_p1 + sqr_f_k;
          // k <- k*2 + 1
        }

        pow2 >>= 1;
      }
    }

    return std::make_pair(f_k_1, f_k);
  }


  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__iter_lg_table(unsigned int n) {
    if (n < FIBONACCI_UINT64_TABLE_SIZE) {  // F_n in table
      return std::make_pair((n != 0
                             ? FIBONACCI_UINT64_TABLE[n - 1]
                             : 1),
                            FIBONACCI_UINT64_TABLE[n]);
    } else {                                // F_n not in table
      assert(bit_length(n) > FIBONACCI_UINT64_TABLE_LG_SIZE);

      const unsigned int initial_n(n);
      const unsigned int nb(bit_length(n) - FIBONACCI_UINT64_TABLE_LG_SIZE);

      n >>= nb;

      assert(n != 0);

      uint64_t f_k_1(FIBONACCI_UINT64_TABLE[n - 1]);  // F_{k-1}
      uint64_t f_k(FIBONACCI_UINT64_TABLE[n]);        // F_k

      unsigned int pow2(1ul << (nb - 1));

      while (pow2 != 0) {
        uint64_t sqr_f_k(f_k*f_k);

        if ((initial_n & pow2) == 0) {  // bit 0
          f_k = ((f_k*f_k_1) << 1) + sqr_f_k;
          f_k_1 = sqr_f_k + f_k_1*f_k_1;
          // k <- k*2
        } else {                        // bit 1
          const uint64_t f_k_p1(f_k + f_k_1);

          f_k_1 = ((f_k*f_k_1) << 1) + sqr_f_k;
          f_k = f_k_p1*f_k_p1 + sqr_f_k;
          // k <- k*2 + 1
        }

        pow2 >>= 1;
      }

      return std::make_pair(f_k_1, f_k);
    }
  }


  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__rec_lg(unsigned int n) {
    if (n != 0) {
      // F_{k-1}, F_k with k = n/2
      std::pair<uint64_t, uint64_t>
        f_k_1__f_k(fibonacci_uint64_pair__rec_lg(n >> 1));

      const uint64_t f_k_1(f_k_1__f_k.first);
      const uint64_t f_k(f_k_1__f_k.second);

      const uint64_t sqr_f_k = f_k*f_k;

      if ((n & 1) == 0) {  // even
        f_k_1__f_k.first = sqr_f_k + f_k_1*f_k_1;
        f_k_1__f_k.second = ((f_k*f_k_1) << 1) + sqr_f_k;
      } else {             // odd
        f_k_1__f_k.first = ((f_k*f_k_1) << 1) + sqr_f_k;

        const uint64_t f_k_p1(f_k + f_k_1);

        f_k_1__f_k.second = f_k_p1*f_k_p1 + sqr_f_k;
      }

      return f_k_1__f_k;
    } else {
      return std::make_pair(1, 0);
    }
  }


  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__rec_lg_table(unsigned int n) {
    if (n >= FIBONACCI_UINT64_TABLE_SIZE) {
      // F_{k-1}, F_k with k = n/2
      std::pair<uint64_t, uint64_t>
        f_k_1__f_k(fibonacci_uint64_pair__rec_lg_table(n >> 1));

      const uint64_t f_k_1(f_k_1__f_k.first);
      const uint64_t f_k(f_k_1__f_k.second);

      const uint64_t sqr_f_k = f_k*f_k;

      if ((n & 1) == 0) {  // even
        f_k_1__f_k.first = sqr_f_k + f_k_1*f_k_1;
        f_k_1__f_k.second = ((f_k*f_k_1) << 1) + sqr_f_k;
      } else {             // odd
        f_k_1__f_k.first = ((f_k*f_k_1) << 1) + sqr_f_k;

        const uint64_t f_k_p1(f_k + f_k_1);

        f_k_1__f_k.second = f_k_p1*f_k_p1 + sqr_f_k;
      }

      return f_k_1__f_k;
    } else {
      return std::make_pair((n != 0
                             ? FIBONACCI_UINT64_TABLE[n - 1]
                             : 1),
                            FIBONACCI_UINT64_TABLE[n]);
    }
  }


  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pair__rec_n(unsigned int n) {
    if (n != 0) {
      // F_{k-2}, F_{k-1}
      std::pair<uint64_t, uint64_t>
        f_k_1__f_k(fibonacci_uint64_pair__rec_n(n - 1));

      const uint64_t f_k_1(f_k_1__f_k.first);

      f_k_1__f_k.first = f_k_1__f_k.second;
      f_k_1__f_k.second += f_k_1;

      return f_k_1__f_k;
    } else {
      return std::make_pair(1, 0);
    }
  }


  uint64_t
  fibonacci_uint64_pow2__iter_lucas_n(unsigned int n) {
    if (n > 1) {
      // i = 2
      uint64_t f_2i(3);  // F_{2^i}
      uint64_t l_2i1(3);  // L_{2^{i - 1}}

      n -= 2;
      while (n-- > 0) {
        l_2i1 = l_2i1*l_2i1 - 2;  // L_{2^i}
        f_2i *= l_2i1;  // F_{2^{i + 1}}
      }

      return f_2i;
    } else {
      return 1;
    }
  }


  uint64_t
  fibonacci_uint64_pow2__iter_lucas_n_table(unsigned int n) {
    if (n < FIBONACCI_POW2_UINT64_TABLE_SIZE) {
      return fibonacci_uint64_pow2_pair__table_const(n).second;
    } else {
      // i = FIBONACCI_POW2_UINT64_TABLE_SIZE - 1
      std::pair<uint64_t, uint64_t>
        f_2i_1__f_2i(fibonacci_uint64_pow2_pair__table_const
                     (FIBONACCI_POW2_UINT64_TABLE_SIZE - 1));

      uint64_t& f_2i(f_2i_1__f_2i.second);  // F_{2^i}
      uint64_t l_2i((f_2i_1__f_2i.first << 1) + f_2i);  // L_{2^i}

      n -= FIBONACCI_POW2_UINT64_TABLE_SIZE - 1;
      while (n-- > 0) {
        f_2i *= l_2i;  // F_{2^{i + 1}}
        l_2i = l_2i*l_2i - 2;  // L_{2^{i + 1}}
      }

      return f_2i;
    }
  }


  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__iter_n(unsigned int n) {
    uint64_t f_2i_1(0);  // F_{2^i - 1}
    uint64_t f_2i(1);    // F_{2^i}

    while (n-- > 0) {
      const uint64_t sqr_f_2i(f_2i*f_2i);

      f_2i = ((f_2i*f_2i_1) << 1) + sqr_f_2i;  // F_{2^{i+1}}
      f_2i_1 = sqr_f_2i + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
    }

    return std::make_pair(f_2i_1, f_2i);
  }


  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__iter_n_table(unsigned int n) {
    if (n < FIBONACCI_POW2_UINT64_TABLE_SIZE) {
      return fibonacci_uint64_pow2_pair__table_const(n);
    } else {
      std::pair<uint64_t, uint64_t>
        f_2i_1__f_2i(fibonacci_uint64_pow2_pair__table_const
                     (FIBONACCI_POW2_UINT64_TABLE_SIZE - 1));

      uint64_t& f_2i_1(f_2i_1__f_2i.first);  // F_{2^i - 1}
      uint64_t& f_2i(f_2i_1__f_2i.second);   // F_{2^i}

      n -= FIBONACCI_POW2_UINT64_TABLE_SIZE - 1;
      if (n != 0) {
        while (n-- > 0) {
          const uint64_t sqr_f_2i(f_2i*f_2i);

          f_2i = ((f_2i*f_2i_1) << 1) + sqr_f_2i;  // F_{2^{i+1}}
          f_2i_1 = sqr_f_2i + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
        }
      }

      return f_2i_1__f_2i;
    }
  }


  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__iter_lucas_n(unsigned int n) {
    if (n > 1) {
      // i = 2
      uint64_t f_2i_1(2);  // F_{2^i - 1}
      uint64_t f_2i(3);    // F_{2^i}

      uint64_t l_2i_1(4);  // L_{2^i - 1}
      uint64_t l_2i(7);    // L_{2^i}

      n -= 2;
      while (n-- > 0) {
        const uint64_t l_2i_1_f_2i_1(l_2i_1*f_2i_1);

        f_2i *= l_2i;                   // F_{2^{i + 1}}
        f_2i_1 = f_2i - l_2i_1_f_2i_1;  // F_{2^{i + 1} - 1}

        l_2i_1 = f_2i + l_2i_1_f_2i_1;  // L_{2^{i + 1} - 1}
        l_2i = l_2i*l_2i - 2;           // L_{2^{i + 1}}
      }

      return std::make_pair(f_2i_1, f_2i);
    } else {
      return std::make_pair(n, 1);
    }
  }


  std::pair<uint64_t, uint64_t>
  fibonacci_uint64_pow2_pair__iter_lucas_n_table(unsigned int n) {
    if (n < FIBONACCI_POW2_UINT64_TABLE_SIZE) {
      return fibonacci_uint64_pow2_pair__table_const(n);
    } else {
      // i = FIBONACCI_POW2_UINT64_TABLE_SIZE - 1
      std::pair<uint64_t, uint64_t>
        f_2i_1__f_2i(fibonacci_uint64_pow2_pair__table_const
                     (FIBONACCI_POW2_UINT64_TABLE_SIZE - 1));

      uint64_t& f_2i_1(f_2i_1__f_2i.first);  // F_{2^i - 1}
      uint64_t& f_2i(f_2i_1__f_2i.second);   // F_{2^i}

      uint64_t l_2i_1((f_2i << 1) - f_2i_1);  // L_{2^i - 1}
      uint64_t l_2i((f_2i_1 << 1) + f_2i);    // L_{2^i}

      n -= FIBONACCI_POW2_UINT64_TABLE_SIZE - 1;
      while (n-- > 0) {
        const uint64_t l_2i_1_f_2i_1(l_2i_1*f_2i_1);

        f_2i *= l_2i;                   // F_{2^{i + 1}}
        f_2i_1 = f_2i - l_2i_1_f_2i_1;  // F_{2^{i + 1} - 1}

        l_2i_1 = f_2i + l_2i_1_f_2i_1;  // L_{2^{i + 1} - 1}
        l_2i = l_2i*l_2i - 2;           // L_{2^{i + 1}}
      }

      return std::make_pair(f_2i_1, f_2i);
    }
  }

}  // namespace fibonacci
