/* -*- coding: latin-1 -*- */
/** \file build_table_fibonacci_pow2_gmp.cpp
 * \brief
 * Usage: build_table_fibonacci_pow2_gmp [nb=20 [exponent=64]]
 *
 * If exponent = 0
 * then print C/C++ table elements F_{2^i - 1} for i = 0..(nb-1),
 * else print C/C++ table elements F_{2^i} % 2^{exponent} for i = 0..(nb-1).
 *
 * Each result that requires > 64 bits
 * is printed as a mpz_class() (GMP big integer).
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>
#include <cstdlib>

#include <iomanip>
#include <iostream>
#include <limits>
#include <utility>

#include <gmpxx.h>  // NOLINT



size_t
bit_length(const mpz_class& n) {
  assert(n >= 0);

  return (n != 0
          ? mpz_sizeinbase(n.get_mpz_t(), 2)
          : 0);
}



void
print_fib(bool first, bool last_pair, const mpz_class& fib,
          unsigned int mod_exponent, const mpz_class& mod_pow2) {
  const mpz_class mod_fib(mod_exponent == 0
                          ? fib
                          : fib % mod_pow2);  // modulo 2^{mod_exponent}

  if (first) {
    std::cout << "std::make_pair(";
  }

  if (bit_length(mod_fib) <= 64) {  // primitive type
    std::cout << mod_fib;
    if (bit_length(mod_fib)
        > static_cast<unsigned int>(std::numeric_limits<int>::digits)) {  // u
      std::cout << 'u';
      if (bit_length(mod_fib)
          > static_cast<unsigned int>
          (std::numeric_limits<unsigned int>::digits)) {  // ul
        std::cout << 'l';
      }
    }
  } else {                                         // big int
    std::cout << "mpz_class(\"" << mod_fib << "\")";
  }

  if (!first) {
    std::cout << ')';
  }

  if (!last_pair) {
    std::cout << ',';
  }

  if (mod_fib != fib) {  // fib > 2^{mod_exponent}
    std::cout << "  /* " << fib << " % 2^{" << mod_exponent << "} */";
  }
  std::cout << std::endl;
}


std::pair<mpz_class, mpz_class>
fibonacci_mpz_pow2_pair(unsigned int n) {
  mpz_class f_2i_1(0);  // F_{2^i - 1}
  mpz_class f_2i(1);    // F_{2^i}

  while (n-- > 0) {
    const mpz_class sqr_f_2i(f_2i*f_2i);

    f_2i = ((f_2i*f_2i_1) << 1) + sqr_f_2i;  // F_{2^{i+1}}
    f_2i_1 = sqr_f_2i + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
  }

  return std::make_pair(f_2i_1, f_2i);
}



int
main(int argc, const char* const argv[]) {
  unsigned int mod_exponent(64);
  mpz_class mod_pow2(0);
  unsigned int nb(20);

  if (argc > 1) {
    nb = atoi(argv[1]);
    if (argc > 2) {
      mod_exponent = atoi(argv[2]);
    }
  }

  if (mod_exponent > 0) {
    mod_pow2 = (mpz_class(1) << mod_exponent);
  }


  // Header
  std::cout << "/* Table of F_0, F_1, ..., F_{2^i - 1}, F_{2^i}, ...,  F_{2^{"
            << nb - 1 << "} - 1}, F_{2^{" << nb - 1 << "}}";
  if (mod_exponent > 0) {
    std::cout << " modulo 2^{" << mod_exponent << '}';
  }
  std::cout << " */";


  // Table
  for (unsigned int i(0); i < nb; ++i) {
    if (i == 0) {
      std::cout << std::endl
                << "/* F_0, F_1 */" << std::endl;
    } else {
      std::cout << std::endl
                << "/* F_{2^" <<  i << " - 1}, F_{2^" <<  i << "} */"
                << std::endl;
    }

    const std::pair<mpz_class, mpz_class>
      f_2i_1__f_2i(fibonacci_mpz_pow2_pair(i));

    print_fib(true, false, f_2i_1__f_2i.first,
              mod_exponent, mod_pow2);
    print_fib(false, (i + 1 == nb), f_2i_1__f_2i.second,
              mod_exponent, mod_pow2);
  }

  return EXIT_SUCCESS;
}
