/* -*- coding: latin-1 -*- */
/*
 * test_fibonacci_gmp.hpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cmath>
#include <cstdint>

#include <limits>
#include <utility>

#include <cxxtest/TestSuite.h>  // NOLINT // CxxTest http://cxxtest.com/

#include "benchmark.hpp"
#include "fibonacci.hpp"
#include "fibonacci_gmp.hpp"



#define SHOW_FUNC_NAME {                                  \
  std::cout << std::endl                                  \
            << "=== " << __func__ << " ===" << std::endl; \
  std::cout.flush();                                      \
}



std::chrono::steady_clock::time_point total_start;



class Test__fibonacci_gmp : public CxxTest::TestSuite {
 public:
  void test_start() {
    total_start = std::chrono::steady_clock::now();
  }


  void test_types() {
    SHOW_FUNC_NAME;

    TS_ASSERT_LESS_THAN_EQUALS(32, std::numeric_limits<unsigned int>::digits);

    TS_ASSERT_LESS_THAN_EQUALS(std::numeric_limits<signed int>::digits,
                               std::numeric_limits<unsigned int>::digits);
    TS_ASSERT_EQUALS(-(std::numeric_limits<signed int>::min() + 1),
                     std::numeric_limits<signed int>::max());
  }


  void test__FIBONACCI_MPZ_TABLE() {
    SHOW_FUNC_NAME;

    assert(fibonacci_gmp::FIBONACCI_MPZ_TABLE_SIZE >= 3);
    assert((1u << fibonacci_gmp::FIBONACCI_MPZ_TABLE_LG_SIZE) + 1
           <= fibonacci_gmp::FIBONACCI_MPZ_TABLE_SIZE);
    assert(fibonacci_gmp::FIBONACCI_MPZ_TABLE_SIZE
           < (1u << (fibonacci_gmp::FIBONACCI_MPZ_TABLE_LG_SIZE + 1)));

    TS_ASSERT_EQUALS(fibonacci_gmp::FIBONACCI_MPZ_TABLE[0], 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::FIBONACCI_MPZ_TABLE[1], 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::FIBONACCI_MPZ_TABLE[2], 1);

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < fibonacci_gmp::FIBONACCI_MPZ_TABLE_SIZE; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::FIBONACCI_MPZ_TABLE[n], f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__FIBONACCI_POW2_MPZ_TABLE_SIZE() {
    SHOW_FUNC_NAME;

    assert(fibonacci_gmp::FIBONACCI_POW2_MPZ_TABLE_SIZE >= 2);

    TS_ASSERT_EQUALS(fibonacci_gmp::FIBONACCI_POW2_MPZ_TABLE[0],
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::FIBONACCI_POW2_MPZ_TABLE[1],
                     std::make_pair(mpz_class(1), mpz_class(1)));

    mpz_class f_2i_1(0);  // F_{2^i - 1}
    mpz_class f_2i(1);    // F_{2^i}

    for (unsigned int i(0); i < fibonacci_gmp::FIBONACCI_POW2_MPZ_TABLE_SIZE;
         ++i) {
      TS_ASSERT_EQUALS(fibonacci_gmp::FIBONACCI_POW2_MPZ_TABLE[i],
                       std::make_pair(f_2i_1, f_2i));

      const mpz_class sqr_f_2i(f_2i*f_2i);

      f_2i = ((f_2i*f_2i_1) << 1) + sqr_f_2i;  // F_{2^{i+1}}
      f_2i_1 = sqr_f_2i + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
    }
  }


  void test__bit_length() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(mpz_class(0)), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(mpz_class(1)), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(mpz_class(2)), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(mpz_class(3)), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(mpz_class(4)), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(mpz_class(5)), 3);

    mpz_class pow2(1);

    for (unsigned int i(0); i < 10000; ++i, pow2 *= 2) {
      TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(pow2 - 1), i);
      TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(pow2), i + 1);
      if (i > 0) {
        TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(pow2 + 1), i + 1);
      }
    }

    for (unsigned int n(0); n < 1000000; ++n) {
      unsigned int length(fibonacci_gmp::bit_length(mpz_class(n)));

      TS_ASSERT_LESS_THAN_EQUALS(0, length);
      TS_ASSERT_LESS_THAN_EQUALS(length, 20);
      if (n > 0) {
        TS_ASSERT_EQUALS
          (length,
           static_cast<unsigned int>(std::floor(std::log2(n))) + 1);
      }
    }

    for (unsigned int n(0); n < 1000000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::bit_length(static_cast<mpz_class>(n)),
                       fibonacci::bit_length(n));
    }
  }


  void test__fibonacci_mpz__signed_int() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(0), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(2), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(3), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(4), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(10), 55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(100),
                     mpz_class("354224848179261915075"));

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(-1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(-2), -1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(-3), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(-4), -3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(-9), 34);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(-10), -55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(-99),
                     mpz_class("218922995834555169026"));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(-100),
                     mpz_class("-354224848179261915075"));

    mpz_class f_n_1(fibonacci_gmp::fibonacci_mpz(10001u));
    mpz_class f_n(-fibonacci_gmp::fibonacci_mpz(10000u));

    for (signed int n(-10000); n < 10000; ++n) {
      TS_ASSERT_EQUALS(abs(fibonacci_gmp::fibonacci_mpz(-n)), abs(f_n));

      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(n), f_n);

      if ((-92 <= n) && (n <= 92)) {
        if ((n >= 0) || ((n % 2) != 0)) {  // not negative or odd
          TS_ASSERT_LESS_THAN_EQUALS(0, f_n);
        } else {                           // negative and even
          TS_ASSERT_LESS_THAN(f_n, 0);
        }
      }

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz__unsigned_int() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(0u), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(1u), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(2u), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(3u), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(4u), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(10u), 55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(100u),
                     mpz_class("354224848179261915075"));


    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 10000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(n), f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz__gmp() {
    SHOW_FUNC_NAME;
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__gmp(0), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__gmp(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__gmp(2), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__gmp(3), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__gmp(4), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__gmp(10), 55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__gmp(100),
                     mpz_class("354224848179261915075"));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__gmp(n), f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz__iter_lg() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(0), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(2), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(3), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(4), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(10), 55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(100),
                     mpz_class("354224848179261915075"));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(n), f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz__iter_lg_table() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(0), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(2), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(3), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(4), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(10), 55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(100),
                     mpz_class("354224848179261915075"));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(n), f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz__iter_lg_optimized() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(0), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(2), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(3), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(4), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(10), 55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(100),
                     mpz_class("354224848179261915075"));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(n), f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz__iter_lg_optimized_table() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(0),
                     0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(1),
                     1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(2),
                     1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(3),
                     2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(4),
                     3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(10),
                     55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(100),
                     mpz_class("354224848179261915075"));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(n),
                       f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz__iter_n() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_n(0), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_n(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_n(2), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_n(3), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_n(4), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_n(10), 55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_n(100),
                     mpz_class("354224848179261915075"));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_n(n), f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz__rec_exp() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(0), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(2), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(3), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(4), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(10), 55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(20), 6765);
    // TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(100),
    //                 mpz_class("354224848179261915075"));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(n), f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz__table_const() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__table_const(0), 0);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__table_const(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__table_const(2), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__table_const(3), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__table_const(4), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__table_const(10), 55);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__table_const(20), 6765);

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < fibonacci_gmp::FIBONACCI_MPZ_TABLE_SIZE; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__table_const(n), f_n);

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz_pair__signed_int() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(0),
                     std::make_pair(mpz_class(1), mpz_class(0)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(1),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(2),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(3),
                     std::make_pair(mpz_class(1), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(4),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(10),
                     std::make_pair(mpz_class(34), mpz_class(55)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(100),
                     std::make_pair(mpz_class("218922995834555169026"),
                                    mpz_class("354224848179261915075")));

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(-1),
                     std::make_pair(mpz_class(-1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(-2),
                     std::make_pair(mpz_class(2), mpz_class(-1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(-3),
                     std::make_pair(mpz_class(-3), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(-4),
                     std::make_pair(mpz_class(5), mpz_class(-3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(-9),
                     std::make_pair(mpz_class(-55), mpz_class(34)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(-10),
                     std::make_pair(mpz_class(89), mpz_class(-55)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(-99),
                     std::make_pair(mpz_class("-354224848179261915075"),
                                    mpz_class("218922995834555169026")));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(-100),
                     std::make_pair(mpz_class("573147844013817084101"),
                                    mpz_class("-354224848179261915075")));

    mpz_class f_n_1(fibonacci_gmp::fibonacci_mpz(10001));
    mpz_class f_n(-fibonacci_gmp::fibonacci_mpz(10000));

    for (signed int n(-10000); n < 10000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(n),
                       std::make_pair(f_n_1, f_n));

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz_pair__unsigned_int() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(0u),
                     std::make_pair(mpz_class(1), mpz_class(0)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(1u),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(2u),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(3u),
                     std::make_pair(mpz_class(1), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(4u),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(10u),
                     std::make_pair(mpz_class(34), mpz_class(55)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(100u),
                     std::make_pair(mpz_class("218922995834555169026"),
                                    mpz_class("354224848179261915075")));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 10000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair(n),
                       std::make_pair(f_n_1, f_n));

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz_pair__gmp()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__gmp(0),
                     std::make_pair(mpz_class(1), mpz_class(0)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__gmp(1),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__gmp(2),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__gmp(3),
                     std::make_pair(mpz_class(1), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__gmp(4),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__gmp(10),
                     std::make_pair(mpz_class(34), mpz_class(55)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pair__gmp(100),
       std::make_pair(mpz_class("218922995834555169026"),
                      mpz_class("354224848179261915075")));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__gmp(n),
                       std::make_pair(f_n_1, f_n));

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz_pair__iter_lg()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg(0),
                     std::make_pair(mpz_class(1), mpz_class(0)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg(1),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg(2),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg(3),
                     std::make_pair(mpz_class(1), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg(4),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg(10),
                     std::make_pair(mpz_class(34), mpz_class(55)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pair__iter_lg(100),
       std::make_pair(mpz_class("218922995834555169026"),
                      mpz_class("354224848179261915075")));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg(n),
                       std::make_pair(f_n_1, f_n));

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz_pair__iter_lg_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(0),
                     std::make_pair(mpz_class(1), mpz_class(0)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(1),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(2),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(3),
                     std::make_pair(mpz_class(1), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(4),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(10),
                     std::make_pair(mpz_class(34), mpz_class(55)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(100),
       std::make_pair(mpz_class("218922995834555169026"),
                      mpz_class("354224848179261915075")));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(n),
                       std::make_pair(f_n_1, f_n));

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz_pair__rec_lg()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg(0),
                     std::make_pair(mpz_class(1), mpz_class(0)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg(1),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg(2),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg(3),
                     std::make_pair(mpz_class(1), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg(4),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg(10),
                     std::make_pair(mpz_class(34), mpz_class(55)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pair__rec_lg(100),
       std::make_pair(mpz_class("218922995834555169026"),
                      mpz_class("354224848179261915075")));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg(n),
                       std::make_pair(f_n_1, f_n));

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz_pair__rec_lg_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(0),
                     std::make_pair(mpz_class(1), mpz_class(0)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(1),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(2),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(3),
                     std::make_pair(mpz_class(1), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(4),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(10),
                     std::make_pair(mpz_class(34), mpz_class(55)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(100),
       std::make_pair(mpz_class("218922995834555169026"),
                      mpz_class("354224848179261915075")));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(n),
                       std::make_pair(f_n_1, f_n));

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz_pair__rec_n()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_n(0),
                     std::make_pair(mpz_class(1), mpz_class(0)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_n(1),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_n(2),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_n(3),
                     std::make_pair(mpz_class(1), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_n(4),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_n(10),
                     std::make_pair(mpz_class(34), mpz_class(55)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pair__rec_n(100),
       std::make_pair(mpz_class("218922995834555169026"),
                      mpz_class("354224848179261915075")));

    mpz_class f_n_1(1);
    mpz_class f_n(0);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_n(n),
                       std::make_pair(f_n_1, f_n));

      const mpz_class t(f_n_1);

      f_n_1 = f_n;
      f_n += t;
    }
  }


  void test__fibonacci_mpz_pow2()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2(0), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2(2), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2(3), 21);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2(4), 987);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2(7),
                     mpz_class("251728825683549488150424261"));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2(n),
         fibonacci_gmp::fibonacci_mpz(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2__iter_lucas_n()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n(0), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n(2), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n(3), 21);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n(4), 987);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n(7),
                     mpz_class("251728825683549488150424261"));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n(n),
         fibonacci_gmp::fibonacci_mpz(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2__iter_lucas_n_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n_table(0),
                     1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n_table(1),
                     1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n_table(2),
                     3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n_table(3),
                     21);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n_table(4),
                     987);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n_table(7),
                     mpz_class("251728825683549488150424261"));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n_table(n),
         fibonacci_gmp::fibonacci_mpz(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2__iter_takahashi_n()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_takahashi_n(0),
                     1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_takahashi_n(1),
                     1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_takahashi_n(2),
                     3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_takahashi_n(3),
                     21);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_takahashi_n(4),
                     987);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_takahashi_n(7),
                     mpz_class("251728825683549488150424261"));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2__iter_takahashi_n(n),
         fibonacci_gmp::fibonacci_mpz(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2__table_const()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__table_const(0),
                     1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__table_const(1),
                     1);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__table_const(2),
                     3);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__table_const(3),
                     21);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__table_const(4),
                     987);
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__table_const(7),
                     mpz_class("251728825683549488150424261"));

    for (unsigned int n(0);
         n < fibonacci_gmp::FIBONACCI_POW2_MPZ_TABLE_SIZE; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2__table_const(n),
         fibonacci_gmp::fibonacci_mpz(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2_pair()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair(0),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair(1),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair(2),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair(3),
                     std::make_pair(mpz_class(13), mpz_class(21)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair(4),
                     std::make_pair(mpz_class(610), mpz_class(987)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair(7),
       std::make_pair(mpz_class("155576970220531065681649693"),
                      mpz_class("251728825683549488150424261")));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2_pair(n),
         fibonacci_gmp::fibonacci_mpz_pair(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2_pair__iter_lucas_n()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n(0),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n(1),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n(2),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n(3),
                     std::make_pair(mpz_class(13), mpz_class(21)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n(4),
                     std::make_pair(mpz_class(610), mpz_class(987)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n(7),
       std::make_pair(mpz_class("155576970220531065681649693"),
                      mpz_class("251728825683549488150424261")));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n(n),
         fibonacci_gmp::fibonacci_mpz_pair(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2_pair__iter_lucas_n_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n_table(0),
       std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n_table(1),
       std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n_table(2),
       std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n_table(3),
       std::make_pair(mpz_class(13), mpz_class(21)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n_table(4),
       std::make_pair(mpz_class(610), mpz_class(987)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n_table(7),
       std::make_pair(mpz_class("155576970220531065681649693"),
                      mpz_class("251728825683549488150424261")));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n_table(n),
         fibonacci_gmp::fibonacci_mpz_pair(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2_pair__iter_n()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n(0),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n(1),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n(2),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n(3),
                     std::make_pair(mpz_class(13), mpz_class(21)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n(4),
                     std::make_pair(mpz_class(610), mpz_class(987)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n(7),
       std::make_pair(mpz_class("155576970220531065681649693"),
                      mpz_class("251728825683549488150424261")));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n(n),
         fibonacci_gmp::fibonacci_mpz_pair(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2_pair__iter_n_table()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n_table(0),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n_table(1),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n_table(2),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n_table(3),
                     std::make_pair(mpz_class(13), mpz_class(21)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n_table(4),
                     std::make_pair(mpz_class(610), mpz_class(987)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n_table(7),
       std::make_pair(mpz_class("155576970220531065681649693"),
                      mpz_class("251728825683549488150424261")));

    for (unsigned int n(0); n < 20; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n_table(n),
         fibonacci_gmp::fibonacci_mpz_pair(1u << n));
    }
  }


  void test__fibonacci_mpz_pow2_pair__table_const()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__table_const(0),
                     std::make_pair(mpz_class(0), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__table_const(1),
                     std::make_pair(mpz_class(1), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__table_const(2),
                     std::make_pair(mpz_class(2), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__table_const(3),
                     std::make_pair(mpz_class(13), mpz_class(21)));
    TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2_pair__table_const(4),
                     std::make_pair(mpz_class(610), mpz_class(987)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::fibonacci_mpz_pow2_pair__table_const(7),
       std::make_pair(mpz_class("155576970220531065681649693"),
                      mpz_class("251728825683549488150424261")));

    for (unsigned int n(0);
         n < fibonacci_gmp::FIBONACCI_POW2_MPZ_TABLE_SIZE; ++n) {
      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz_pow2_pair__table_const(n),
         fibonacci_gmp::fibonacci_mpz_pair(1u << n));
    }
  }


  void test__lucas_mpz()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(0), 2);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(1), 1);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(2), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(3), 4);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(4), 7);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(10), 123);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(100),
                     mpz_class("792070839848372253127"));

    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(-1), -1);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(-2), 3);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(-3), -4);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(-4), 7);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(-9), -76);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(-10), 123);
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(-99),
                     mpz_class("-489526700523968661124"));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(-100),
                     mpz_class("792070839848372253127"));

    mpz_class l_n_1(-fibonacci_gmp::lucas_mpz(10001));
    mpz_class l_n(fibonacci_gmp::lucas_mpz(10000));

    for (signed int n(-10000); n < 10000; ++n) {
      TS_ASSERT_EQUALS(abs(fibonacci_gmp::lucas_mpz(-n)), abs(l_n));

      TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(n), l_n);

      if ((n >= 0) || ((n % 2) == 0)) {  // not negative or even
        TS_ASSERT_LESS_THAN_EQUALS(0, l_n);
      } else {                           // negative and odd
        TS_ASSERT_LESS_THAN(l_n, 0);
      }

      const mpz_class t(l_n_1);

      l_n_1 = l_n;
      l_n += t;
    }
  }


  void test__lucas_mpz_pair()  {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(0),
                     std::make_pair(mpz_class(-1), mpz_class(2)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(1),
                     std::make_pair(mpz_class(2), mpz_class(1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(2),
                     std::make_pair(mpz_class(1), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(3),
                     std::make_pair(mpz_class(3), mpz_class(4)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(4),
                     std::make_pair(mpz_class(4), mpz_class(7)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(10),
                     std::make_pair(mpz_class(76), mpz_class(123)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::lucas_mpz_pair(100),
       std::make_pair(mpz_class("489526700523968661124"),
                      mpz_class("792070839848372253127")));

    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(-1),
                     std::make_pair(mpz_class(3), mpz_class(-1)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(-2),
                     std::make_pair(mpz_class(-4), mpz_class(3)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(-3),
                     std::make_pair(mpz_class(7), mpz_class(-4)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(-4),
                     std::make_pair(mpz_class(-11), mpz_class(7)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(-9),
                     std::make_pair(mpz_class(123), mpz_class(-76)));
    TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(-10),
                     std::make_pair(mpz_class(-199), mpz_class(123)));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::lucas_mpz_pair(-99),
       std::make_pair(mpz_class("792070839848372253127"),
                      mpz_class("-489526700523968661124")));
    TS_ASSERT_EQUALS
      (fibonacci_gmp::lucas_mpz_pair(-100),
       std::make_pair(mpz_class("-1281597540372340914251"),
                      mpz_class("792070839848372253127")));

    mpz_class l_n_1(-fibonacci_gmp::lucas_mpz(10001));
    mpz_class l_n(fibonacci_gmp::lucas_mpz(10000));

    for (signed int n(-10000); n < 10000; ++n) {
      TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz_pair(n),
                       std::make_pair(l_n_1, l_n));

      const mpz_class t(l_n_1);

      l_n_1 = l_n;
      l_n += t;
    }
  }


  void test_check_implementions()  {
    SHOW_FUNC_NAME;

    // First 10000 n
    for (unsigned int n(0); n < 10000; ++n) {
      const std::pair <mpz_class, mpz_class>
        p(fibonacci_gmp::fibonacci_mpz_pair(n));

      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(n),
                       p.second & std::numeric_limits<uint64_t>::max());

      TS_ASSERT_EQUALS(p, fibonacci_gmp::fibonacci_mpz_pair__gmp(n));

      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(n), p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(static_cast<signed int>(n)),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(n), p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(n),
                       p.second);

      if (n < 1000) {
        TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_n(n), p.second);
      }

      if (n < 20) {
        TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__rec_exp(n), p.second);
      }

      if (n < fibonacci_gmp::FIBONACCI_MPZ_TABLE_SIZE) {
        TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__table_const(n),
                         p.second);
      }

      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz(-static_cast<signed int>(n)),
         (n % 2 == 0
          ? -p.second    // even
          : p.second));  // odd

      TS_ASSERT_EQUALS
        (p.first, fibonacci_gmp::fibonacci_mpz(static_cast<signed int>(n) - 1));
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(n), p);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(n), p);

      if (n < 100) {
        TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_n(n), p);
      }
    }

    // 500 n around 2^17
    for (unsigned int n((1 << 17) - 250); n < (1 << 17) + 250; ++n) {
      const std::pair <mpz_class, mpz_class>
        p(fibonacci_gmp::fibonacci_mpz_pair(n));

      TS_ASSERT_EQUALS(p, fibonacci_gmp::fibonacci_mpz_pair__gmp(n));

      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(n),
                       p.second & std::numeric_limits<uint64_t>::max());

      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(n), p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(static_cast<signed int>(n)),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg(n), p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_table(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz__iter_lg_optimized_table(n),
                       p.second);

      TS_ASSERT_EQUALS
        (fibonacci_gmp::fibonacci_mpz(-static_cast<signed int>(n)),
         (n % 2 == 0
          ? -p.second    // even
          : p.second));  // odd

      TS_ASSERT_EQUALS
        (p.first, fibonacci_gmp::fibonacci_mpz(static_cast<signed int>(n) - 1));
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__iter_lg_table(n), p);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg(n), p);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pair__rec_lg_table(n), p);
    }

    // Powers of 2
    for (unsigned int n(0); n < 20; ++n) {
      const std::pair <mpz_class, mpz_class>
        p(fibonacci_gmp::fibonacci_mpz_pow2_pair(n));

      TS_ASSERT_EQUALS(p, fibonacci_gmp::fibonacci_mpz_pair__gmp(1u << n));
      TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(n),
                       p.second & std::numeric_limits<uint64_t>::max());

      TS_ASSERT_EQUALS
        (p, fibonacci_gmp::fibonacci_mpz_pair(1u << n));

      TS_ASSERT_EQUALS
        (p, fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n(n));
      TS_ASSERT_EQUALS
        (p, fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_lucas_n_table(n));
      TS_ASSERT_EQUALS
        (p, fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n(n));
      TS_ASSERT_EQUALS
        (p, fibonacci_gmp::fibonacci_mpz_pow2_pair__iter_n_table(n));
      if (n < fibonacci_gmp::FIBONACCI_POW2_MPZ_TABLE_SIZE) {
        TS_ASSERT_EQUALS
          (p, fibonacci_gmp::fibonacci_mpz_pow2_pair__table_const(n));
      }

      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2(n), p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n(n),
                       p.second);
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_lucas_n_table(n),
                       p.second);
      if (n < fibonacci_gmp::FIBONACCI_POW2_MPZ_TABLE_SIZE) {
        TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__table_const(n),
                         p.second);
      }
      TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz_pow2__iter_takahashi_n(n),
                       p.second);
    }
  }


  void test_uint64_bounds_mod()  {
    SHOW_FUNC_NAME;

    const mpz_class mod64("18446744073709551616");

    std::pair <int64_t, int64_t> p64;
    std::pair <uint64_t, uint64_t> pu64;
    std::pair <mpz_class, mpz_class> p;

    /* fibonacci_int64() */
    TS_ASSERT_DIFFERS(fibonacci::fibonacci_int64(-93),
                      fibonacci_gmp::fibonacci_mpz(-93));
    TS_ASSERT_EQUALS(static_cast<uint64_t>(fibonacci::fibonacci_int64(-93)),
                     fibonacci_gmp::fibonacci_mpz(-93) % mod64);

    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(-92),
                     fibonacci_gmp::fibonacci_mpz(-92));

    TS_ASSERT_EQUALS(fibonacci::fibonacci_int64(92),
                     fibonacci_gmp::fibonacci_mpz(92));

    TS_ASSERT_DIFFERS(fibonacci::fibonacci_int64(93),
                      fibonacci_gmp::fibonacci_mpz(93));
    TS_ASSERT_EQUALS(static_cast<uint64_t>(fibonacci::fibonacci_int64(93)),
                     fibonacci_gmp::fibonacci_mpz(93) % mod64);

    /* fibonacci_int64_pair() */
    p64 = fibonacci::fibonacci_int64_pair(-93);
    p = fibonacci_gmp::fibonacci_mpz_pair(-93);

    TS_ASSERT_DIFFERS(p64.first, p.first);
    TS_ASSERT_EQUALS(p64.first, p.first % mod64);

    TS_ASSERT_DIFFERS(p64.second, p.second);
    TS_ASSERT_EQUALS(static_cast<uint64_t>(p64.second), p.second % mod64);

    p64 = fibonacci::fibonacci_int64_pair(-92);
    p = fibonacci_gmp::fibonacci_mpz_pair(-92);

    TS_ASSERT_DIFFERS(p64.first, p.first);
    TS_ASSERT_EQUALS(static_cast<uint64_t>(p64.first), p.first % mod64);

    TS_ASSERT_EQUALS(p64.second, p.second);

    p64 = fibonacci::fibonacci_int64_pair(-91);
    p = fibonacci_gmp::fibonacci_mpz_pair(-91);

    TS_ASSERT_EQUALS(p64.first, p.first);
    TS_ASSERT_EQUALS(p64.second, p.second);

    p64 = fibonacci::fibonacci_int64_pair(92);
    p = fibonacci_gmp::fibonacci_mpz_pair(92);

    TS_ASSERT_EQUALS(p64.first, p.first);
    TS_ASSERT_EQUALS(p64.second, p.second);

    p64 = fibonacci::fibonacci_int64_pair(93);
    p = fibonacci_gmp::fibonacci_mpz_pair(93);

    TS_ASSERT_EQUALS(p64.first, p.first);

    TS_ASSERT_DIFFERS(p64.second, p.second);
    TS_ASSERT_EQUALS(static_cast<uint64_t>(p64.second), p.second % mod64);

    p64 = fibonacci::fibonacci_int64_pair(94);
    p = fibonacci_gmp::fibonacci_mpz_pair(94);

    TS_ASSERT_DIFFERS(p64.first, p.first);
    TS_ASSERT_EQUALS(static_cast<uint64_t>(p64.first), p.first % mod64);

    TS_ASSERT_DIFFERS(p64.second, p.second);
    TS_ASSERT_EQUALS(static_cast<uint64_t>(p64.second), p.second % mod64);

    /* fibonacci_uint64() */
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(93),
                     fibonacci_gmp::fibonacci_mpz(93));

    TS_ASSERT_DIFFERS(fibonacci::fibonacci_uint64(94),
                      fibonacci_gmp::fibonacci_mpz(94));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64(94),
                     fibonacci_gmp::fibonacci_mpz(94) % mod64);

    /* fibonacci_uint64_pair() */
    pu64 = fibonacci::fibonacci_uint64_pair(93);
    p = fibonacci_gmp::fibonacci_mpz_pair(93);

    TS_ASSERT_EQUALS(pu64.first, p.first);
    TS_ASSERT_EQUALS(pu64.second, p.second);

    pu64 = fibonacci::fibonacci_uint64_pair(94);
    p = fibonacci_gmp::fibonacci_mpz_pair(94);

    TS_ASSERT_EQUALS(pu64.first, p.first);

    TS_ASSERT_DIFFERS(pu64.second, p.second);
    TS_ASSERT_EQUALS(pu64.second, p.second % mod64);

    pu64 = fibonacci::fibonacci_uint64_pair(95);
    p = fibonacci_gmp::fibonacci_mpz_pair(95);

    TS_ASSERT_DIFFERS(pu64.first, p.first);
    TS_ASSERT_EQUALS(pu64.first, p.first % mod64);

    TS_ASSERT_DIFFERS(pu64.second, p.second);
    TS_ASSERT_EQUALS(pu64.second, p.second % mod64);

    /* fibonacci_uint64_pow2() */
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(6),
                     fibonacci_gmp::fibonacci_mpz_pow2(6));

    TS_ASSERT_DIFFERS(fibonacci::fibonacci_uint64_pow2(7),
                      fibonacci_gmp::fibonacci_mpz_pow2(7));
    TS_ASSERT_EQUALS(fibonacci::fibonacci_uint64_pow2(7),
                     fibonacci_gmp::fibonacci_mpz_pow2(7) % mod64);

    /* fibonacci_uint64_pow2_pair() */
    pu64 = fibonacci::fibonacci_uint64_pow2_pair(6);
    p = fibonacci_gmp::fibonacci_mpz_pow2_pair(6);

    TS_ASSERT_EQUALS(pu64.first, p.first);
    TS_ASSERT_EQUALS(pu64.second, p.second);

    pu64 = fibonacci::fibonacci_uint64_pow2_pair(7);
    p = fibonacci_gmp::fibonacci_mpz_pow2_pair(7);

    TS_ASSERT_DIFFERS(pu64.first, p.first);
    TS_ASSERT_EQUALS(pu64.first, p.first % mod64);

    TS_ASSERT_DIFFERS(pu64.second, p.second);
    TS_ASSERT_EQUALS(pu64.second, p.second % mod64);

    /* lucas_int64() */
    TS_ASSERT_DIFFERS(fibonacci::lucas_int64(-91),
                      fibonacci_gmp::lucas_mpz(-91));
    //? TS_ASSERT_EQUALS(fibonacci::lucas_int64(-91),
    //                 fibonacci_gmp::lucas_mpz(-91) % mod64);

    TS_ASSERT_EQUALS(fibonacci::lucas_int64(-90),
                     fibonacci_gmp::lucas_mpz(-90));

    TS_ASSERT_EQUALS(fibonacci::lucas_int64(90),
                     fibonacci_gmp::lucas_mpz(90));

    TS_ASSERT_DIFFERS(fibonacci::lucas_int64(91),
                      fibonacci_gmp::lucas_mpz(91));
    TS_ASSERT_EQUALS(static_cast<uint64_t>(fibonacci::lucas_int64(93)),
                     fibonacci_gmp::lucas_mpz(93) % mod64);

    /* lucas_int64_pair() */
    p64 = fibonacci::lucas_int64_pair(-91);
    p = fibonacci_gmp::lucas_mpz_pair(-91);

    TS_ASSERT_DIFFERS(p64.first, p.first);
    TS_ASSERT_EQUALS(static_cast<uint64_t>(p64.first), p.first % mod64);

    TS_ASSERT_DIFFERS(p64.second, p.second);
    //? TS_ASSERT_EQUALS(p64.second, p.second % mod64);

    p64 = fibonacci::lucas_int64_pair(-90);
    p = fibonacci_gmp::lucas_mpz_pair(-90);

    TS_ASSERT_DIFFERS(p64.first, p.first);
    //? TS_ASSERT_EQUALS(p64.first, p.first % mod64);

    TS_ASSERT_EQUALS(p64.second, p.second);

    p64 = fibonacci::lucas_int64_pair(-89);
    p = fibonacci_gmp::lucas_mpz_pair(-89);

    TS_ASSERT_EQUALS(p64.first, p.first);
    TS_ASSERT_EQUALS(p64.second, p.second);

    p64 = fibonacci::lucas_int64_pair(90);
    p = fibonacci_gmp::lucas_mpz_pair(90);

    TS_ASSERT_EQUALS(p64.first, p.first);
    TS_ASSERT_EQUALS(p64.second, p.second);

    p64 = fibonacci::lucas_int64_pair(91);
    p = fibonacci_gmp::lucas_mpz_pair(91);

    TS_ASSERT_EQUALS(p64.first, p.first);

    TS_ASSERT_DIFFERS(p64.second, p.second);
    TS_ASSERT_EQUALS(static_cast<uint64_t>(p64.second), p.second % mod64);

    p64 = fibonacci::lucas_int64_pair(92);
    p = fibonacci_gmp::lucas_mpz_pair(92);

    TS_ASSERT_DIFFERS(p64.first, p.first);
    TS_ASSERT_EQUALS(static_cast<uint64_t>(p64.first), p.first % mod64);

    TS_ASSERT_DIFFERS(p64.second, p.second);
    TS_ASSERT_EQUALS(static_cast<uint64_t>(p64.second), p.second % mod64);
  }

  void test_formulas_a_b()  {
    SHOW_FUNC_NAME;

    for (signed int a(-200); a < 200; ++a) {
      const std::pair <mpz_class, mpz_class>
        p(fibonacci_gmp::fibonacci_mpz_pair(a));
      const mpz_class f_a_1(p.first);
      const mpz_class f_a(p.second);
      const mpz_class f_a_p1(f_a + f_a_1);

      for (signed int b(-200); b < 200; ++b) {
        const std::pair <mpz_class, mpz_class>
          p(fibonacci_gmp::fibonacci_mpz_pair(b));
        const mpz_class f_b_1(p.first);
        const mpz_class f_b(p.second);

        // F_{a+b} = F_{a+1} F_b + F_a F_{b-1}   forall a, b integer
        TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(a + b),
                         f_a_p1*f_b + f_a*f_b_1);

        // F_{a-b} = {-1}^b {F_a F_{b-1} - F_{a-1} F_b}  forall a, b integer
        if ((b % 2) == 0) {  // b even
          TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(a - b),
                           f_a*f_b_1 - f_a_1*f_b);
        } else {             // b odd
          TS_ASSERT_EQUALS(fibonacci_gmp::fibonacci_mpz(a - b),
                           f_a_1*f_b - f_a*f_b_1);
        }
      }
    }
  }


  void test_formulas_2n()  {
    SHOW_FUNC_NAME;

    for (signed int n(-10000); n < 10000; ++n) {
      std::pair <mpz_class, mpz_class> p(fibonacci_gmp::fibonacci_mpz_pair(n));
      const mpz_class f_n_1(p.first);
      const mpz_class f_n(p.second);
      const mpz_class f_n_p1(f_n + f_n_1);

      const mpz_class sqr_f_n_1(f_n_1*f_n_1);
      const mpz_class sqr_f_n(f_n*f_n);
      const mpz_class sqr_f_n_p1(f_n_p1*f_n_p1);

      p = fibonacci_gmp::fibonacci_mpz_pair(2*n);
      const mpz_class f_2n_1(p.first);
      const mpz_class f_2n(p.second);
      const mpz_class f_2n_p1(f_2n + f_2n_1);

      // F_{2n - 1} = F^2_n + F^2_{n-1}   forall n integer
      TS_ASSERT_EQUALS(f_2n_1, sqr_f_n + sqr_f_n_1);

      // F_2n = F^2_{n+1} - F^2_{n-1}
      //      = 2 F_n F_{n-1} + F^2_n   forall n integer
      TS_ASSERT_EQUALS(f_2n, sqr_f_n_p1 - sqr_f_n_1);
      TS_ASSERT_EQUALS(f_2n, 2*f_n*f_n_1 + sqr_f_n);

      // F_{2n + 1} = F^2_{n+1} + F^2_n   forall n integer
      TS_ASSERT_EQUALS(f_2n_p1, sqr_f_n_p1 + sqr_f_n);
    }
  }


  void test_formulas_pow2()  {
    SHOW_FUNC_NAME;

    for (unsigned int i(0); i < 20; ++i) {
      std::pair <mpz_class, mpz_class>
        p(fibonacci_gmp::fibonacci_mpz_pow2_pair(i));
      const mpz_class f_2i_1(p.first);
      const mpz_class f_2i(p.second);
      const mpz_class f_2i_p1(f_2i + f_2i_1);

      const mpz_class sqr_f_2i_1(f_2i_1*f_2i_1);
      const mpz_class sqr_f_2i(f_2i*f_2i);
      const mpz_class sqr_f_2i_p1(f_2i_p1*f_2i_p1);

      p = fibonacci_gmp::fibonacci_mpz_pow2_pair(i + 1);

      const mpz_class f_2ip1_1(p.first);
      const mpz_class f_2ip1(p.second);

      // F_{2^{i+1} - 1} = F^2_{2^i} + F^2_{2^i - 1}   forall i integer >= 0
      TS_ASSERT_EQUALS(f_2ip1_1, sqr_f_2i + sqr_f_2i_1);

      // F_{2^{i+1}} = F^2_{2^i + 1} - F^2_{2^i - 1}
      //             = 2 F_{2^i} F_{2^i - 1} + F^2_{2^i} forall i integer >= 0
      TS_ASSERT_EQUALS(f_2ip1, sqr_f_2i_p1 - sqr_f_2i_1);
      TS_ASSERT_EQUALS(f_2ip1, 2*f_2i*f_2i_1 + sqr_f_2i);

      // F_{2^{i+1) + 1} = F^2_{2^i + 1} + F^2_{2^i}   forall i integer >= 0
      TS_ASSERT_EQUALS(f_2ip1 + f_2ip1_1, sqr_f_2i_p1 + sqr_f_2i);
    }
  }


  void test_formulas_n_add_pow2()  {
    SHOW_FUNC_NAME;

    for (signed int n(-100); n < 100; ++n) {
      const std::pair <mpz_class, mpz_class>
        p(fibonacci_gmp::fibonacci_mpz_pair(n));
      const mpz_class f_n_1(p.first);
      const mpz_class f_n(p.second);
      const mpz_class f_n_p1(f_n + f_n_1);

      for (unsigned int i(0); i < 15; ++i) {
        std::pair <mpz_class, mpz_class>
          p(fibonacci_gmp::fibonacci_mpz_pow2_pair(i));
        const mpz_class f_2i_1(p.first);
        const mpz_class f_2i(p.second);
        const mpz_class f_2i_p1(f_2i + f_2i_1);

        p = fibonacci_gmp::fibonacci_mpz_pair(n + (1 << i));

        const mpz_class f_n_2i_1(p.first);
        const mpz_class f_n_2i(p.second);
        const mpz_class f_n_2i_p1(f_n_2i + f_n_2i_1);

        // F_{n + 2^i - 1} = F_n F_{2^i} + F_{n-1} F_{2^i - 1}
        // forall n integer, forall i integer >= 0
        TS_ASSERT_EQUALS(f_n_2i_1, f_n*f_2i + f_n_1*f_2i_1);

        // F_{n + 2^i} = F_{n+1} F_{2^i} + F_n F_{2^i - 1}
        //             = F_n F_{2^i} + F_n F_{2^i - 1} + F_{n - 1} F_{2^i}
        // forall n integer, forall i integer >= 0
        TS_ASSERT_EQUALS(f_n_2i, f_n_p1*f_2i + f_n*f_2i_1);
        TS_ASSERT_EQUALS(f_n_2i, f_n*f_2i + f_n*f_2i_1 + f_n_1*f_2i);

        // F_{n + 2^i + 1} = F_{n+1} F_{2^i + 1} + F_n F_{2^i}
        // forall n integer, forall i integer >= 0
        TS_ASSERT_EQUALS(f_n_2i_p1, f_n_p1*f_2i_p1 + f_n*f_2i);
      }
    }
  }


  void test_formulas_lucas()  {
    SHOW_FUNC_NAME;

    for (signed int n(-10000); n < 10000; ++n) {
      const std::pair <mpz_class, mpz_class>
        p(fibonacci_gmp::fibonacci_mpz_pair(n));
      const mpz_class f_n_p1(p.second + p.first);

      // L_n = F_{n + 1} + F_{n - 1}   forall n integers
      TS_ASSERT_EQUALS(fibonacci_gmp::lucas_mpz(n), f_n_p1 + p.first);
    }
  }


  void test_end() {
    std::cout
      << "--- Total: "
      << round<std::chrono::seconds>(std::chrono::steady_clock::now()
                                     - total_start).count()
      << "s ---" << std::endl;
  }
};
