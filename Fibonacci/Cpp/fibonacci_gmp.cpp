/* -*- coding: latin-1 -*- */
/** \file fibonacci_gmp.cpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "fibonacci_gmp.hpp"

#include <algorithm>
#include <utility>



namespace fibonacci_gmp {

  const unsigned int FIBONACCI_MPZ_TABLE_LG_SIZE(11);

  const unsigned int FIBONACCI_MPZ_TABLE_SIZE(2049);

  const mpz_class FIBONACCI_MPZ_TABLE[FIBONACCI_MPZ_TABLE_SIZE]{
#include "table_2049_Fn_gmp.h"
  };


  const unsigned int FIBONACCI_POW2_MPZ_TABLE_SIZE(19);

  const std::pair<mpz_class, mpz_class>
  FIBONACCI_POW2_MPZ_TABLE[FIBONACCI_POW2_MPZ_TABLE_SIZE]{
#include "table_20_F2i_gmp.h"
  };



  mpz_class
  fibonacci_mpz__iter_lg(unsigned int n) {
    if (n != 0) {
      // k = 0
      mpz_class f_k_1(1);  // F_{k-1}
      mpz_class f_k(0);    // F_k

      // i = 0
      mpz_class f_2i_1(0);  // F_{2^i - 1}
      mpz_class f_2i(1);    // F_{2^i}

      while (true) {
        if ((n & 1) != 0) {  // ith bit of initial n is 1
          const mpz_class t(f_k*f_2i);

          f_k = t + f_k_1*f_2i + f_k*f_2i_1;  // F_{k + 2^i}
          f_k_1 = t + f_k_1*f_2i_1;           // F_{k + 2^i - 1}
          // k <- k + 2^i = k + 10...0

          if (n == 1) {  // inv: k = initial n
            return f_k;
          }
        }

        const mpz_class t(f_2i*f_2i);

        f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
        f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
        // ++i

        n >>= 1;  // n <- n/2
      }
    } else {
      return 0;
    }
  }


  mpz_class
  fibonacci_mpz__iter_lg_table(unsigned int n) {
    unsigned int i(FIBONACCI_MPZ_TABLE_LG_SIZE);
    const unsigned int k(n & ((1u << i) - 1));  // n % (2^i - 1)

    mpz_class f_k(FIBONACCI_MPZ_TABLE[k]);  // F_k

    n >>= i;  // new initial n

    if (n == 0) {
      return f_k;
    }

    mpz_class f_k_1(k != 0
                   ? FIBONACCI_MPZ_TABLE[k - 1]
                   : 1);                            // F_{k-1}

    std::pair<mpz_class, mpz_class>
      f_2i_1__f_2i(fibonacci_mpz_pow2_pair__table_const(i));

    mpz_class& f_2i_1(f_2i_1__f_2i.first);  // F_{2^i - 1}
    mpz_class& f_2i(f_2i_1__f_2i.second);   // F_{2^i}

    while (true) {
      if ((n & 1) != 0) {  // ith bit of initial n is 1
        const mpz_class t(f_k*f_2i);

        f_k = t + f_k_1*f_2i + f_k*f_2i_1;  // F_{k + 2^i}
        f_k_1 = t + f_k_1*f_2i_1;           // F_{k + 2^i - 1}
        // k <- k + 2^i = k + 10...0

        if (n == 1) {  // inv: k = initial n
          return f_k;
        }
      }

      ++i;
      if (i < FIBONACCI_POW2_MPZ_TABLE_SIZE) {
        f_2i_1__f_2i = fibonacci_mpz_pow2_pair__table_const(i);
        // this copy is mostly useless
      } else {
        const mpz_class t(f_2i*f_2i);

        f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
        f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
      }

      n >>= 1;  // n <- n/2
    }
  }


  mpz_class
  fibonacci_mpz__iter_lg_optimized(unsigned int n) {
    // k = 0
    mpz_class f_k_1(1);  // F_{k-1}
    mpz_class f_k(0);    // F_k

    // i = 0
    mpz_class f_2i_1(0);  // F_{2^i - 1}
    mpz_class f_2i(1);    // F_{2^i}

    const unsigned int odd(n & 1);  // n is odd, its first bit is 1

    // First consecutive bits equals to 0...0 or 1...1
    while (((n & 1) == odd) && (n != 0)) {  // same bit as first_bit
      const mpz_class t(f_2i*f_2i);

      f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
      f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
      // ++i

      n >>= 1;  // n <- n/2
    }

    if (odd) {            // (i + 1) first consecutive bits equals to 01...1
      f_k_1 = f_2i - f_2i_1;  // F_{2^i - 2}
      f_k = f_2i_1;           // F_{2^i - 1}
    } else if (n != 0) {  // (i + 1) first consecutive bits equals to 10...0
      f_k_1 = f_2i_1;  // F_{2^i - 1}
      f_k = f_2i;      // F_{2^i}
    }

    // Remaining bits
    n >>= 1;  // n <- n/2
    while (n != 0) {
      const mpz_class t(f_2i*f_2i);

      f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
      f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
      // ++i

      if ((n & 1) != 0) {  // ith bit of initial n is 1
        const mpz_class t(f_k*f_2i);

        f_k = t + f_k_1*f_2i + f_k*f_2i_1;  // F_{k + 2^i}
        f_k_1 = t + f_k_1*f_2i_1;           // F_{k + 2^i - 1}
        // k <- k + 2^i = k + 10...0
      }

      n >>= 1;  // n <- n/2
    }

    // inv: k = initial n

    return f_k;
  }


  mpz_class
  fibonacci_mpz__iter_lg_optimized_table(unsigned int n) {
    // k = 0
    mpz_class f_k_1(1);  // F_{k-1}
    mpz_class f_k(0);    // F_k

    unsigned int i(0);

    const unsigned int odd(n & 1);  // n is odd, its first bit is 1

    // First consecutive bits equals to 0...0 or 1...1
    while (((n & 1) == odd) && (n != 0)) {  // same bit as first_bit
      ++i;
      n >>= 1;  // n <- n/2
    }

    std::pair<mpz_class, mpz_class> f_2i_1__f_2i;
    mpz_class& f_2i_1(f_2i_1__f_2i.first);  // F_{2^i - 1}
    mpz_class& f_2i(f_2i_1__f_2i.second);   // F_{2^i}

    if (i < FIBONACCI_POW2_MPZ_TABLE_SIZE) {
      f_2i_1__f_2i = fibonacci_mpz_pow2_pair__table_const(i);
    } else {
      f_2i_1__f_2i = fibonacci_mpz_pow2_pair__table_const
        (FIBONACCI_POW2_MPZ_TABLE_SIZE - 1);

      for (unsigned int j(FIBONACCI_POW2_MPZ_TABLE_SIZE - 1); j < i; ++j) {
        const mpz_class t(f_2i*f_2i);

        f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{j+1}}
        f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{j+1} - 1}
      }
    }

    if (odd) {            // (i + 1) first consecutive bits equals to 01...1
      f_k_1 = f_2i - f_2i_1;  // F_{2^i - 2}
      f_k = f_2i_1;           // F_{2^i - 1}
    } else if (n != 0) {  // (i + 1) first consecutive bits equals to 10...0
      f_k_1 = f_2i_1;  // F_{2^i - 1}
      f_k = f_2i;      // F_{2^i}
    }

    // Remaining bits
    n >>= 1;  // n <- n/2
    while (n != 0) {
      ++i;
      if (i < FIBONACCI_POW2_MPZ_TABLE_SIZE) {
        f_2i_1__f_2i = fibonacci_mpz_pow2_pair__table_const(i);
      } else {
        const mpz_class t(f_2i*f_2i);

        f_2i = ((f_2i*f_2i_1) << 1) + t;  // F_{2^{i+1}}
        f_2i_1 = t + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
      }

      if ((n & 1) != 0) {  // ith bit of initial n is 1
        const mpz_class t(f_k*f_2i);

        f_k = t + f_k_1*f_2i + f_k*f_2i_1;  // F_{k + 2^i}
        f_k_1 = t + f_k_1*f_2i_1;           // F_{k + 2^i - 1}
        // k <- k + 2^i = k + 10...0
      }

      n >>= 1;  // n <- n/2
    }

    // inv: k = initial n

    return f_k;
  }


  mpz_class
  fibonacci_mpz__iter_n(unsigned int n) {
    mpz_class f_k_1(1);  // F_{k-1}
    mpz_class f_k(0);    // F_k

    while (n-- > 0) {
      swap(f_k, f_k_1);
      f_k += f_k_1;
    }

    return f_k;
  }


  mpz_class
  fibonacci_mpz__rec_exp(unsigned int n) {
    return (n <= 1
            ? mpz_class(n)
            : fibonacci_mpz__rec_exp(n - 1) + fibonacci_mpz__rec_exp(n - 2));
  }


  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__iter_lg(unsigned int n) {
    mpz_class f_k_1(1);
    mpz_class f_k(0);

    if (n != 0) {
      unsigned int pow2(1ul << (bit_length(n) - 1));

      while (pow2 != 0) {
        mpz_class sqr_f_k(f_k*f_k);

        if ((n & pow2) == 0) {  // bit 0
          f_k = ((f_k*f_k_1) << 1) + sqr_f_k;
          f_k_1 = sqr_f_k + f_k_1*f_k_1;
          // k <- k*2
        } else {                // bit 1
          const mpz_class f_k_p1(f_k + f_k_1);

          f_k_1 = ((f_k*f_k_1) << 1) + sqr_f_k;
          f_k = f_k_p1*f_k_p1 + sqr_f_k;
          // k <- k*2 + 1
        }

        pow2 >>= 1;
      }
    }

    return std::make_pair(f_k_1, f_k);
  }


  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__iter_lg_table(unsigned int n) {
    if (n < FIBONACCI_MPZ_TABLE_SIZE) {  // F_n in table
      return std::make_pair((n != 0
                             ? FIBONACCI_MPZ_TABLE[n - 1]
                             : 1),
                            FIBONACCI_MPZ_TABLE[n]);
    } else {                             // F_n not in table
      assert(bit_length(n) > FIBONACCI_MPZ_TABLE_LG_SIZE);

      const unsigned int initial_n(n);
      const unsigned int nb(bit_length(n) - FIBONACCI_MPZ_TABLE_LG_SIZE);

      n >>= nb;

      assert(n != 0);

      mpz_class f_k_1(FIBONACCI_MPZ_TABLE[n - 1]);  // F_{k-1}
      mpz_class f_k(FIBONACCI_MPZ_TABLE[n]);        // F_k

      unsigned int pow2(1ul << (nb - 1));

      while (pow2 != 0) {
        mpz_class sqr_f_k(f_k*f_k);

        if ((initial_n & pow2) == 0) {  // bit 0
          f_k = ((f_k*f_k_1) << 1) + sqr_f_k;
          f_k_1 = sqr_f_k + f_k_1*f_k_1;
          // k <- k*2
        } else {                        // bit 1
          const mpz_class f_k_p1(f_k + f_k_1);

          f_k_1 = ((f_k*f_k_1) << 1) + sqr_f_k;
          f_k = f_k_p1*f_k_p1 + sqr_f_k;
          // k <- k*2 + 1
        }

        pow2 >>= 1;
      }

      return std::make_pair(f_k_1, f_k);
    }
  }


  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__rec_lg(unsigned int n) {
    if (n != 0) {
      // F_{k-1}, F_k with k = n/2
      std::pair<mpz_class, mpz_class>
        f_k_1__f_k(fibonacci_mpz_pair__rec_lg(n >> 1));

      const mpz_class f_k_1(f_k_1__f_k.first);
      const mpz_class f_k(f_k_1__f_k.second);

      const mpz_class sqr_f_k = f_k*f_k;

      if ((n & 1) == 0) {  // even
        f_k_1__f_k.first = sqr_f_k + f_k_1*f_k_1;
        f_k_1__f_k.second = ((f_k*f_k_1) << 1) + sqr_f_k;
      } else {             // odd
        f_k_1__f_k.first = ((f_k*f_k_1) << 1) + sqr_f_k;

        const mpz_class f_k_p1(f_k + f_k_1);

        f_k_1__f_k.second = f_k_p1*f_k_p1 + sqr_f_k;
      }

      return f_k_1__f_k;
    } else {
      return std::make_pair(1, 0);
    }
  }


  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__rec_lg_table(unsigned int n) {
    if (n >= FIBONACCI_MPZ_TABLE_SIZE) {
      // F_{k-1}, F_k with k = n/2
      std::pair<mpz_class, mpz_class>
        f_k_1__f_k(fibonacci_mpz_pair__rec_lg_table(n >> 1));

      const mpz_class f_k_1(f_k_1__f_k.first);
      const mpz_class f_k(f_k_1__f_k.second);

      const mpz_class sqr_f_k = f_k*f_k;

      if ((n & 1) == 0) {  // even
        f_k_1__f_k.first = sqr_f_k + f_k_1*f_k_1;
        f_k_1__f_k.second = ((f_k*f_k_1) << 1) + sqr_f_k;
      } else {             // odd
        f_k_1__f_k.first = ((f_k*f_k_1) << 1) + sqr_f_k;

        const mpz_class f_k_p1(f_k + f_k_1);

        f_k_1__f_k.second = f_k_p1*f_k_p1 + sqr_f_k;
      }

      return f_k_1__f_k;
    } else {
      return std::make_pair((n != 0
                             ? FIBONACCI_MPZ_TABLE[n - 1]
                             : 1),
                            FIBONACCI_MPZ_TABLE[n]);
    }
  }


  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pair__rec_n(unsigned int n) {
    if (n != 0) {
      // F_{k-2}, F_{k-1}
      std::pair<mpz_class, mpz_class>
        f_k_1__f_k(fibonacci_mpz_pair__rec_n(n - 1));

      const mpz_class f_k_1(f_k_1__f_k.first);

      f_k_1__f_k.first = f_k_1__f_k.second;
      f_k_1__f_k.second += f_k_1;

      return f_k_1__f_k;
    } else {
      return std::make_pair(1, 0);
    }
  }


  mpz_class
  fibonacci_mpz_pow2__iter_lucas_n(unsigned int n) {
    if (n > 1) {
      // i = 2
      mpz_class f_2i(3);  // F_{2^i}
      mpz_class l_2i1(3);  // L_{2^{i - 1}}

      n -= 2;
      while (n-- > 0) {
        l_2i1 = l_2i1*l_2i1 - 2;  // L_{2^i}
        f_2i *= l_2i1;  // F_{2^{i + 1}}
      }

      return f_2i;
    } else {
      return 1;
    }
  }


  mpz_class
  fibonacci_mpz_pow2__iter_lucas_n_table(unsigned int n) {
    if (n < FIBONACCI_POW2_MPZ_TABLE_SIZE) {
      return fibonacci_mpz_pow2_pair__table_const(n).second;
    } else {
      // i = FIBONACCI_POW2_MPZ_TABLE_SIZE - 1
      std::pair<mpz_class, mpz_class>
        f_2i_1__f_2i(fibonacci_mpz_pow2_pair__table_const
                     (FIBONACCI_POW2_MPZ_TABLE_SIZE - 1));

      mpz_class& f_2i(f_2i_1__f_2i.second);  // F_{2^i}
      mpz_class l_2i((f_2i_1__f_2i.first << 1) + f_2i);  // L_{2^i}

      n -= FIBONACCI_POW2_MPZ_TABLE_SIZE - 1;
      while (n-- > 0) {
        f_2i *= l_2i;  // F_{2^{i + 1}}
        l_2i = l_2i*l_2i - 2;  // L_{2^{i + 1}}
      }

      return f_2i;
    }
  }


  mpz_class
  fibonacci_mpz_pow2__iter_takahashi_n(unsigned int n) {
    if (n > 1) {
      // i = 2
      mpz_class f_2i1(1);  // F_{2^{i - 1}}
      mpz_class l_2i1(3);  // L_{2^{i - 1}}

      n -= 2;
      while (n-- > 0) {
        const mpz_class sqr_f_2i1(f_2i1*f_2i1);  // F^2_{2^{i - 1}}

        f_2i1 = (l_2i1 + f_2i1) >> 1;  // F_{2^{i - 1} + 1}
        f_2i1 = ((f_2i1*f_2i1) << 1) - sqr_f_2i1*3 - 2;  // F_{2^i}
        l_2i1 = sqr_f_2i1*5 + 2;  // L_{2^i}
      }

      return l_2i1*f_2i1;
    } else {
      return 1;
    }
  }


  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__iter_lucas_n(unsigned int n) {
    if (n > 1) {
      // i = 2
      mpz_class f_2i_1(2);  // F_{2^i - 1}
      mpz_class f_2i(3);    // F_{2^i}

      mpz_class l_2i_1(4);  // L_{2^i - 1}
      mpz_class l_2i(7);    // L_{2^i}

      n -= 2;
      while (n-- > 0) {
        const mpz_class l_2i_1_f_2i_1(l_2i_1*f_2i_1);

        f_2i *= l_2i;                   // F_{2^{i + 1}}
        f_2i_1 = f_2i - l_2i_1_f_2i_1;  // F_{2^{i + 1} - 1}

        l_2i_1 = f_2i + l_2i_1_f_2i_1;  // L_{2^{i + 1} - 1}
        l_2i = l_2i*l_2i - 2;           // L_{2^{i + 1}}
      }

      return std::make_pair(f_2i_1, f_2i);
    } else {
      return std::make_pair(n, 1);
    }
  }


  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__iter_lucas_n_table(unsigned int n) {
    if (n < FIBONACCI_POW2_MPZ_TABLE_SIZE) {
      return fibonacci_mpz_pow2_pair__table_const(n);
    } else {
      // i = FIBONACCI_POW2_MPZ_TABLE_SIZE - 1
      std::pair<mpz_class, mpz_class>
        f_2i_1__f_2i(fibonacci_mpz_pow2_pair__table_const
                     (FIBONACCI_POW2_MPZ_TABLE_SIZE - 1));

      mpz_class& f_2i_1(f_2i_1__f_2i.first);  // F_{2^i - 1}
      mpz_class& f_2i(f_2i_1__f_2i.second);   // F_{2^i}

      mpz_class l_2i_1((f_2i << 1) - f_2i_1);  // L_{2^i - 1}
      mpz_class l_2i((f_2i_1 << 1) + f_2i);    // L_{2^i}

      n -= FIBONACCI_POW2_MPZ_TABLE_SIZE - 1;
      while (n-- > 0) {
        const mpz_class l_2i_1_f_2i_1(l_2i_1*f_2i_1);

        f_2i *= l_2i;                   // F_{2^{i + 1}}
        f_2i_1 = f_2i - l_2i_1_f_2i_1;  // F_{2^{i + 1} - 1}

        l_2i_1 = f_2i + l_2i_1_f_2i_1;  // L_{2^{i + 1} - 1}
        l_2i = l_2i*l_2i - 2;           // L_{2^{i + 1}}
      }

      return std::make_pair(f_2i_1, f_2i);
    }
  }


  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__iter_n(unsigned int n) {
    mpz_class f_2i_1(0);  // F_{2^i - 1}
    mpz_class f_2i(1);   // F_{2^i}

    while (n-- > 0) {
      const mpz_class sqr_f_2i(f_2i*f_2i);

      f_2i = ((f_2i*f_2i_1) << 1) + sqr_f_2i;  // F_{2^{i+1}}
      f_2i_1 = sqr_f_2i + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
    }

    return std::make_pair(f_2i_1, f_2i);
  }


  std::pair<mpz_class, mpz_class>
  fibonacci_mpz_pow2_pair__iter_n_table(unsigned int n) {
    if (n < FIBONACCI_POW2_MPZ_TABLE_SIZE) {
      return fibonacci_mpz_pow2_pair__table_const(n);
    } else {
      std::pair<mpz_class, mpz_class>
        f_2i_1__f_2i(fibonacci_mpz_pow2_pair__table_const
                     (FIBONACCI_POW2_MPZ_TABLE_SIZE - 1));

      mpz_class& f_2i_1(f_2i_1__f_2i.first);  // F_{2^i - 1}
      mpz_class& f_2i(f_2i_1__f_2i.second);   // F_{2^i}

      n -= FIBONACCI_POW2_MPZ_TABLE_SIZE - 1;
      while (n-- > 0) {
        const mpz_class sqr_f_2i(f_2i*f_2i);

        f_2i = ((f_2i*f_2i_1) << 1) + sqr_f_2i;  // F_{2^{i+1}}
        f_2i_1 = sqr_f_2i + f_2i_1*f_2i_1;       // F_{2^{i+1} - 1}
      }

      return f_2i_1__f_2i;
    }
  }

}  // namespace fibonacci_gmp
