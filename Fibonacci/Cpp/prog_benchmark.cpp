/* -*- coding: latin-1 -*- */
/** \file prog_benchmark.cpp
 * \brief
 * Usage: prog_benchmark
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iomanip>
#include <iostream>
#include <string>
#include <utility>

#include "benchmark.hpp"
#include "fibonacci.hpp"



const unsigned int DEFAULT_NB(1000000);



/** \brief
 * Return the duration in milliseconds
 * of nb computations \a fct(n),
 * with \f$n \approx 2^i\f$.
 */
std::chrono::milliseconds
bench_function(uint64_t (*fct)(unsigned int),
               unsigned int i,
               unsigned int nb = DEFAULT_NB) {
  unsigned int n((static_cast<unsigned int>(1) << i) - 1);
  const unsigned int mask((static_cast<uint64_t>(1) << (i + 1)) - 1);

  const std::chrono::steady_clock::time_point
    start(std::chrono::steady_clock::now());

  while (nb-- > 0) {
    fct(n);
    n = (n + nb) & mask;
  }

  return round<std::chrono::milliseconds>(std::chrono::steady_clock::now()
                                          - start);
}


/** \brief
 * Return the duration in milliseconds
 * of nb computations \a fct(n),
 * with \f$n \approx 2^i\f$.
 */
std::chrono::milliseconds
bench_function(std::pair<uint64_t, uint64_t> (*fct)(unsigned int),
               unsigned int i,
               unsigned int nb = DEFAULT_NB) {
  unsigned int n((static_cast<unsigned int>(1) << i) - 1);
  const unsigned int mask((static_cast<uint64_t>(1) << (i + 1)) - 1);

  const std::chrono::steady_clock::time_point
    start(std::chrono::steady_clock::now());

  while (nb-- > 0) {
    fct(n);
    n = (n + nb) & mask;
  }

  return round<std::chrono::milliseconds>(std::chrono::steady_clock::now()
                                          - start);
}



/* ******
 * Main *
 ********/
int
main() {
  const std::chrono::steady_clock::time_point
    total_start(std::chrono::steady_clock::now());

  std::chrono::milliseconds duration;

  std::cout << "i   uint64_pair__iter_lg_table   uint64_pair__iter_lg   uint64_pair__rec_lg_table   uint64_pair__rec_lg   uint64__iter_lg_optimized_table   uint64__iter_lg_optimized   uint64__iter_lg_table   uint64__iter_lg   uint64__iter_n   uint64_pair__rec_n   uint64__rec_exp"  // NOLINT
            << std::endl;

  for (unsigned int i(0); i <= 31; ++i) {
    std::cout << std::setw(2) << i << ' ';
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64_pair__iter_lg_table, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function(fibonacci::fibonacci_uint64_pair__iter_lg, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64_pair__rec_lg_table, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function(fibonacci::fibonacci_uint64_pair__rec_lg, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64__iter_lg_optimized_table, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function
      (fibonacci::fibonacci_uint64__iter_lg_optimized, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function(fibonacci::fibonacci_uint64__iter_lg_table, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    duration = bench_function(fibonacci::fibonacci_uint64__iter_lg, i);
    std::cout << ' ' << std::setw(10) << duration.count();
    std::cout.flush();

    if (i <= 9) {
      duration = bench_function(fibonacci::fibonacci_uint64__iter_n, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();

      duration = bench_function(fibonacci::fibonacci_uint64_pair__rec_n, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();
    }

    if (i <= 3) {
      duration = bench_function(fibonacci::fibonacci_uint64__rec_exp, i);
      std::cout << ' ' << std::setw(10) << duration.count();
      std::cout.flush();
    }

    std::cout << std::endl;
    std::cout.flush();
  }

  std::cerr
    << "--- Total: "
    << round<std::chrono::seconds>(std::chrono::steady_clock::now()
                                   - total_start).count()
    << "s ---" << std::endl;

  return EXIT_SUCCESS;
}
