#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Usage: prog_fibonacci.py [n [n [n...]]]

If command line arguments n_0 [n_1 [...]] exist
then print "n_i: F_{n_i}" for each i,
else print "n_i: F_{n_i} binary size" for i from 0 to 50.

Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2015 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import print_function

import sys

import fibonacci


#
# Main
######
def main():
    """
    If command line arguments n_0 [n_1 [...]] exist
    then print "n_i: F_n(_i)" for each i,
    else print "n_i: F_n(_i) size" for i from 0 to 50.
    """
    if len(sys.argv) > 1:
        for n in sys.argv[1:]:
            try:
                n = int(n)
                f_n = fibonacci.fibonacci_int(n)
            except ValueError:
                f_n = 'Invalid integer!'

            print('{}: {}'.format(n, f_n))
            sys.stdout.flush()
    else:
        print('n  F_n          binary size')
        for n in range(51):
            f_n = fibonacci.fibonacci(n)
            print('{:2} {:11} {:2}'.format(n, f_n, f_n.bit_length()))


if __name__ == '__main__':
    main()
