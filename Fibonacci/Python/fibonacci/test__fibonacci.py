#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2015 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import print_function

import sys

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)


import fibonacci


def test_memoization_begin():
    if __name__ != '__main__':
        assert len(fibonacci.__fibonacci_memo) == 0


def test_fibonacci():
    assert fibonacci.fibonacci(0) == 0
    assert fibonacci.fibonacci(1) == 1
    assert fibonacci.fibonacci(2) == 1
    assert fibonacci.fibonacci(3) == 2
    assert fibonacci.fibonacci(4) == 3
    assert fibonacci.fibonacci(10) == 55
    assert fibonacci.fibonacci(100) == 354224848179261915075

    f_n_1 = 1
    f_n = 0
    for n in range(1000):
        assert fibonacci.fibonacci(n) == f_n

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci_int():
    assert fibonacci.fibonacci_int(0) == 0
    assert fibonacci.fibonacci_int(1) == 1
    assert fibonacci.fibonacci_int(2) == 1
    assert fibonacci.fibonacci_int(3) == 2
    assert fibonacci.fibonacci_int(4) == 3
    assert fibonacci.fibonacci_int(10) == 55
    assert fibonacci.fibonacci_int(100) == 354224848179261915075

    assert fibonacci.fibonacci_int(-1) == 1
    assert fibonacci.fibonacci_int(-2) == -1
    assert fibonacci.fibonacci_int(-3) == 2
    assert fibonacci.fibonacci_int(-4) == -3
    assert fibonacci.fibonacci_int(-9) == 34
    assert fibonacci.fibonacci_int(-10) == -55
    assert fibonacci.fibonacci_int(-99) == 218922995834555169026
    assert fibonacci.fibonacci_int(-100) == -354224848179261915075

    f_n_1 = fibonacci.fibonacci(1001)
    f_n = -fibonacci.fibonacci(1000)
    for n in range(-1000, 1000):
        assert abs(fibonacci.fibonacci_int(-n)) == abs(f_n)

        assert fibonacci.fibonacci_int(n) == f_n

        if (n >= 0) or (n % 2 != 0):  # not negative or odd
            assert f_n >= 0
        else:                         # negative and even
            assert f_n < 0

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci_int_generator():
    f_gen = fibonacci.fibonacci_int_generator()

    assert next(f_gen) == 0
    assert next(f_gen) == 1
    assert next(f_gen) == 1
    assert next(f_gen) == 2
    assert next(f_gen) == 3
    assert next(f_gen) == 5

    f_gen = fibonacci.fibonacci_int_generator(5)

    assert next(f_gen) == 5
    assert next(f_gen) == 8
    assert next(f_gen) == 13
    assert next(f_gen) == 21
    assert next(f_gen) == 34
    assert next(f_gen) == 55

    f_gen = fibonacci.fibonacci_int_generator(-5)

    assert next(f_gen) == 5
    assert next(f_gen) == -3
    assert next(f_gen) == 2
    assert next(f_gen) == -1
    assert next(f_gen) == 1
    assert next(f_gen) == 0

    for from_n in range(-1000, 1000):
        f_gen = fibonacci.fibonacci_int_generator(-from_n)

        f_n_1, f_n = fibonacci.fibonacci_int_pair(-from_n)
        for _ in range(2000):
            assert next(f_gen) == f_n

            f_n_1, f_n = f_n, f_n + f_n_1

    for from_n in range(-1000, 1000):
        f_n_1, f_n = fibonacci.fibonacci_int_pair(-from_n)
        for f in fibonacci.fibonacci_int_generator(-from_n):
            assert f == f_n

            if f > 10000:
                break

            f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci_int_pair():
    assert fibonacci.fibonacci_int_pair(0) == (1, 0)
    assert fibonacci.fibonacci_int_pair(1) == (0, 1)
    assert fibonacci.fibonacci_int_pair(2) == (1, 1)
    assert fibonacci.fibonacci_int_pair(3) == (1, 2)
    assert fibonacci.fibonacci_int_pair(4) == (2, 3)
    assert fibonacci.fibonacci_int_pair(10) == (34, 55)
    assert fibonacci.fibonacci_int_pair(100) == (218922995834555169026,
                                                 354224848179261915075)

    assert fibonacci.fibonacci_int_pair(-1) == (-1, 1)
    assert fibonacci.fibonacci_int_pair(-2) == (2, -1)
    assert fibonacci.fibonacci_int_pair(-3) == (-3, 2)
    assert fibonacci.fibonacci_int_pair(-4) == (5, -3)
    assert fibonacci.fibonacci_int_pair(-9) == (-55, 34)
    assert fibonacci.fibonacci_int_pair(-10) == (89, -55)
    assert fibonacci.fibonacci_int_pair(-99) == (-354224848179261915075,
                                                 218922995834555169026)
    assert fibonacci.fibonacci_int_pair(-100) == (573147844013817084101,
                                                  -354224848179261915075)

    f_n_1 = fibonacci.fibonacci(1001)
    f_n = -fibonacci.fibonacci(1000)
    for n in range(-1000, 1000):
        assert fibonacci.fibonacci_int_pair(n) == (f_n_1, f_n)

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci__iter_lg():
    assert fibonacci.fibonacci__iter_lg(0) == 0
    assert fibonacci.fibonacci__iter_lg(1) == 1
    assert fibonacci.fibonacci__iter_lg(2) == 1
    assert fibonacci.fibonacci__iter_lg(3) == 2
    assert fibonacci.fibonacci__iter_lg(4) == 3
    assert fibonacci.fibonacci__iter_lg(10) == 55
    assert fibonacci.fibonacci__iter_lg(100) == 354224848179261915075

    f_n_1 = 1
    f_n = 0
    for n in range(1000):
        assert fibonacci.fibonacci__iter_lg(n) == f_n

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci__iter_lg_optimized():
    assert fibonacci.fibonacci__iter_lg_optimized(0) == 0
    assert fibonacci.fibonacci__iter_lg_optimized(1) == 1
    assert fibonacci.fibonacci__iter_lg_optimized(2) == 1
    assert fibonacci.fibonacci__iter_lg_optimized(3) == 2
    assert fibonacci.fibonacci__iter_lg_optimized(4) == 3
    assert fibonacci.fibonacci__iter_lg_optimized(10) == 55
    assert fibonacci.fibonacci__iter_lg_optimized(100) == 354224848179261915075

    f_n_1 = 1
    f_n = 0
    for n in range(1000):
        assert fibonacci.fibonacci__iter_lg_optimized(n) == f_n

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci__iter_n():
    assert fibonacci.fibonacci__iter_n(0) == 0
    assert fibonacci.fibonacci__iter_n(1) == 1
    assert fibonacci.fibonacci__iter_n(2) == 1
    assert fibonacci.fibonacci__iter_n(3) == 2
    assert fibonacci.fibonacci__iter_n(4) == 3
    assert fibonacci.fibonacci__iter_n(10) == 55
    assert fibonacci.fibonacci__iter_n(100) == 354224848179261915075

    f_n_1 = 1
    f_n = 0
    for n in range(1000):
        assert fibonacci.fibonacci__iter_n(n) == f_n

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci_pair():
    assert fibonacci.fibonacci_pair(0) == (1, 0)
    assert fibonacci.fibonacci_pair(1) == (0, 1)
    assert fibonacci.fibonacci_pair(2) == (1, 1)
    assert fibonacci.fibonacci_pair(3) == (1, 2)
    assert fibonacci.fibonacci_pair(4) == (2, 3)
    assert fibonacci.fibonacci_pair(10) == (34, 55)
    assert fibonacci.fibonacci_pair(100) == (218922995834555169026,
                                             354224848179261915075)

    f_n_1 = 1
    f_n = 0
    for n in range(1000):
        assert fibonacci.fibonacci_pair(n) == (f_n_1, f_n)

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci_pair__iter_lg():
    assert fibonacci.fibonacci_pair__iter_lg(0) == (1, 0)
    assert fibonacci.fibonacci_pair__iter_lg(1) == (0, 1)
    assert fibonacci.fibonacci_pair__iter_lg(2) == (1, 1)
    assert fibonacci.fibonacci_pair__iter_lg(3) == (1, 2)
    assert fibonacci.fibonacci_pair__iter_lg(4) == (2, 3)
    assert fibonacci.fibonacci_pair__iter_lg(10) == (34, 55)
    assert fibonacci.fibonacci_pair__iter_lg(100) == (218922995834555169026,
                                                      354224848179261915075)

    f_n_1 = 1
    f_n = 0
    for n in range(1000):
        assert fibonacci.fibonacci_pair__iter_lg(n) == (f_n_1, f_n)

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci_pair__rec_lg():
    assert fibonacci.fibonacci_pair__rec_lg(0) == (1, 0)
    assert fibonacci.fibonacci_pair__rec_lg(1) == (0, 1)
    assert fibonacci.fibonacci_pair__rec_lg(2) == (1, 1)
    assert fibonacci.fibonacci_pair__rec_lg(3) == (1, 2)
    assert fibonacci.fibonacci_pair__rec_lg(4) == (2, 3)
    assert fibonacci.fibonacci_pair__rec_lg(10) == (34, 55)
    assert fibonacci.fibonacci_pair__rec_lg(100) == (218922995834555169026,
                                                     354224848179261915075)

    f_n_1 = 1
    f_n = 0
    for n in range(1000):
        assert fibonacci.fibonacci_pair__rec_lg(n) == (f_n_1, f_n)

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci_pair__rec_n():
    assert fibonacci.fibonacci_pair__rec_n(0) == (1, 0)
    assert fibonacci.fibonacci_pair__rec_n(1) == (0, 1)
    assert fibonacci.fibonacci_pair__rec_n(2) == (1, 1)
    assert fibonacci.fibonacci_pair__rec_n(3) == (1, 2)
    assert fibonacci.fibonacci_pair__rec_n(4) == (2, 3)
    assert fibonacci.fibonacci_pair__rec_n(10) == (34, 55)
    assert fibonacci.fibonacci_pair__rec_n(100) == (218922995834555169026,
                                                    354224848179261915075)

    f_n_1 = 1
    f_n = 0
    for n in range(100):
        assert fibonacci.fibonacci_pair__rec_n(n) == (f_n_1, f_n)

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci_pair_with():
    assert fibonacci.fibonacci_pair_with(0, 1, 0, 0) == (1, 0)
    assert fibonacci.fibonacci_pair_with(0, 1, 0, 1) == (0, 1)
    assert fibonacci.fibonacci_pair_with(0, 1, 0, 2) == (1, 1)
    assert fibonacci.fibonacci_pair_with(0, 1, 0, 3) == (1, 2)
    assert fibonacci.fibonacci_pair_with(0, 1, 0, 4) == (2, 3)
    assert fibonacci.fibonacci_pair_with(0, 1, 0, 10) == (34, 55)
    assert fibonacci.fibonacci_pair_with(0, 1, 0, 100) \
        == (218922995834555169026, 354224848179261915075)

    assert fibonacci.fibonacci_pair_with(1, 0, 1, 0) == (1, 0)
    assert fibonacci.fibonacci_pair_with(1, 0, 1, 1) == (0, 1)
    assert fibonacci.fibonacci_pair_with(1, 0, 1, 2) == (1, 1)
    assert fibonacci.fibonacci_pair_with(1, 0, 1, 3) == (1, 2)
    assert fibonacci.fibonacci_pair_with(1, 0, 1, 4) == (2, 3)
    assert fibonacci.fibonacci_pair_with(1, 0, 1, 10) == (34, 55)
    assert fibonacci.fibonacci_pair_with(1, 0, 1, 100) \
        == (218922995834555169026, 354224848179261915075)

    assert fibonacci.fibonacci_pair_with(2, 1, 1, 0) == (1, 0)
    assert fibonacci.fibonacci_pair_with(2, 1, 1, 1) == (0, 1)
    assert fibonacci.fibonacci_pair_with(2, 1, 1, 2) == (1, 1)
    assert fibonacci.fibonacci_pair_with(2, 1, 1, 3) == (1, 2)
    assert fibonacci.fibonacci_pair_with(2, 1, 1, 4) == (2, 3)
    assert fibonacci.fibonacci_pair_with(2, 1, 1, 10) == (34, 55)
    assert fibonacci.fibonacci_pair_with(2, 1, 1, 100) \
        == (218922995834555169026, 354224848179261915075)

    assert fibonacci.fibonacci_pair_with(3, 1, 2, 0) == (1, 0)
    assert fibonacci.fibonacci_pair_with(3, 1, 2, 1) == (0, 1)
    assert fibonacci.fibonacci_pair_with(3, 1, 2, 2) == (1, 1)
    assert fibonacci.fibonacci_pair_with(3, 1, 2, 3) == (1, 2)
    assert fibonacci.fibonacci_pair_with(3, 1, 2, 4) == (2, 3)
    assert fibonacci.fibonacci_pair_with(3, 1, 2, 10) == (34, 55)
    assert fibonacci.fibonacci_pair_with(3, 1, 2, 100) \
        == (218922995834555169026, 354224848179261915075)

    f_k_1 = 1
    f_k = 0
    for k in range(200):
        for n in range(200):
            assert fibonacci.fibonacci_pair_with(k, f_k_1, f_k, n) \
                == fibonacci.fibonacci_pair(n)

        f_k_1, f_k = f_k, f_k + f_k_1


def test_fibonacci_pow2():
    assert fibonacci.fibonacci_pow2(0) == 1
    assert fibonacci.fibonacci_pow2(1) == 1
    assert fibonacci.fibonacci_pow2(2) == 3
    assert fibonacci.fibonacci_pow2(3) == 21
    assert fibonacci.fibonacci_pow2(4) == 987
    assert fibonacci.fibonacci_pow2(7) == 251728825683549488150424261

    for n in range(20):
        assert fibonacci.fibonacci_pow2(n) == fibonacci.fibonacci(2**n)


def test_fibonacci_pow2_pair():
    assert fibonacci.fibonacci_pow2_pair(0) == (0, 1)
    assert fibonacci.fibonacci_pow2_pair(1) == (1, 1)
    assert fibonacci.fibonacci_pow2_pair(2) == (2, 3)
    assert fibonacci.fibonacci_pow2_pair(3) == (13, 21)
    assert fibonacci.fibonacci_pow2_pair(4) == (610, 987)
    assert fibonacci.fibonacci_pow2_pair(7) == (155576970220531065681649693,
                                                251728825683549488150424261)

    for n in range(20):
        assert fibonacci.fibonacci_pow2_pair(n) \
            == fibonacci.fibonacci_pair(2**n)


def test_fibonacci__rec_exp():
    assert fibonacci.fibonacci__rec_exp(0) == 0
    assert fibonacci.fibonacci__rec_exp(1) == 1
    assert fibonacci.fibonacci__rec_exp(2) == 1
    assert fibonacci.fibonacci__rec_exp(3) == 2
    assert fibonacci.fibonacci__rec_exp(4) == 3
    assert fibonacci.fibonacci__rec_exp(10) == 55
    #assert fibonacci.fibonacci__rec_exp(100) == 354224848179261915075

    f_n_1 = 1
    f_n = 0
    for n in range(20):
        assert fibonacci.fibonacci__rec_exp(n) == f_n

        f_n_1, f_n = f_n, f_n + f_n_1


def test_fibonacci__rec_memoized_n():
    assert fibonacci.fibonacci__rec_memoized_n(0) == 0
    assert fibonacci.fibonacci__rec_memoized_n(1) == 1
    assert fibonacci.fibonacci__rec_memoized_n(2) == 1
    assert fibonacci.fibonacci__rec_memoized_n(3) == 2
    assert fibonacci.fibonacci__rec_memoized_n(4) == 3
    assert fibonacci.fibonacci__rec_memoized_n(10) == 55
    assert fibonacci.fibonacci__rec_memoized_n(100) == 354224848179261915075

    f_n_1 = 1
    f_n = 0
    for n in range(1000):
        assert fibonacci.fibonacci__rec_memoized_n(n) == f_n

        f_n_1, f_n = f_n, f_n + f_n_1


def test_lucas_int():
    assert fibonacci.lucas_int(0) == 2
    assert fibonacci.lucas_int(1) == 1
    assert fibonacci.lucas_int(2) == 3
    assert fibonacci.lucas_int(3) == 4
    assert fibonacci.lucas_int(4) == 7
    assert fibonacci.lucas_int(10) == 123
    assert fibonacci.lucas_int(100) == 792070839848372253127

    assert fibonacci.lucas_int(-1) == -1
    assert fibonacci.lucas_int(-2) == 3
    assert fibonacci.lucas_int(-3) == -4
    assert fibonacci.lucas_int(-4) == 7
    assert fibonacci.lucas_int(-9) == -76
    assert fibonacci.lucas_int(-10) == 123
    assert fibonacci.lucas_int(-99) == -489526700523968661124
    assert fibonacci.lucas_int(-100) == 792070839848372253127

    l_n_1 = -fibonacci.lucas_int(1001)
    l_n = fibonacci.lucas_int(1000)
    for n in range(-1000, 1000):
        assert abs(fibonacci.lucas_int(-n)) == abs(l_n)

        assert fibonacci.lucas_int(n) == l_n

        if (n >= 0) or (n % 2 == 0):  # not negative or even
            assert l_n >= 0
        else:                         # negative and odd
            assert l_n < 0

        l_n_1, l_n = l_n, l_n + l_n_1


def test_lucas_int_pair():
    assert fibonacci.lucas_int_pair(0) == (-1, 2)
    assert fibonacci.lucas_int_pair(1) == (2, 1)
    assert fibonacci.lucas_int_pair(2) == (1, 3)
    assert fibonacci.lucas_int_pair(3) == (3, 4)
    assert fibonacci.lucas_int_pair(4) == (4, 7)
    assert fibonacci.lucas_int_pair(10) == (76, 123)
    assert fibonacci.lucas_int_pair(100) == (489526700523968661124,
                                             792070839848372253127)

    assert fibonacci.lucas_int_pair(-1) == (3, -1)
    assert fibonacci.lucas_int_pair(-2) == (-4, 3)
    assert fibonacci.lucas_int_pair(-3) == (7, -4)
    assert fibonacci.lucas_int_pair(-4) == (-11, 7)
    assert fibonacci.lucas_int_pair(-9) == (123, -76)
    assert fibonacci.lucas_int_pair(-10) == (-199, 123)
    assert fibonacci.lucas_int_pair(-99) == (792070839848372253127,
                                             -489526700523968661124)
    assert fibonacci.lucas_int_pair(-100) == (-1281597540372340914251,
                                              792070839848372253127)

    l_n_1 = -fibonacci.lucas_int(1001)
    l_n = fibonacci.lucas_int(1000)
    for n in range(-1000, 1000):
        assert fibonacci.lucas_int_pair(n) == (l_n_1, l_n)

        l_n_1, l_n = l_n, l_n + l_n_1


def test_check_implementions():
    f_gen = fibonacci.fibonacci_int_generator()

    for n in range(1000):
        p = fibonacci.fibonacci_pair(n)

        assert fibonacci.fibonacci(n) == p[1]
        assert fibonacci.fibonacci_int(n) == p[1]
        assert fibonacci.fibonacci__iter_lg(n) == p[1]
        assert fibonacci.fibonacci__iter_lg_optimized(n) == p[1]
        assert fibonacci.fibonacci__iter_n(n) == p[1]
        if n < 20:
            assert fibonacci.fibonacci__rec_exp(n) == p[1]
        assert fibonacci.fibonacci__rec_memoized_n(n) == p[1]

        assert fibonacci.fibonacci_int(-n) == (-p[1] if n % 2 == 0  # even
                                               else p[1])

        assert next(f_gen) == p[1]

        assert p[0] == fibonacci.fibonacci_int(n - 1)
        assert fibonacci.fibonacci_pair__iter_lg(n) == p
        assert fibonacci.fibonacci_pair__rec_lg(n) == p
        if n < 100:
            assert fibonacci.fibonacci_pair__rec_n(n) == p
        assert fibonacci.fibonacci_pair_with(0, 1, 0, n) == p

    for n in range(20):
        p = fibonacci.fibonacci_pow2_pair(n)

        assert p == fibonacci.fibonacci_pair(2**n)
        assert fibonacci.fibonacci_pow2(n) == p[1]


def test_formulas_a_b():
    for a in range(-150, 150):
        f_a_1, f_a = fibonacci.fibonacci_int_pair(a)
        f_a_p1 = f_a + f_a_1
        for b in range(-150, 150):
            f_b_1, f_b = fibonacci.fibonacci_int_pair(b)

            # F_{a+b} = F_{a+1} F_b + F_a F_{b-1}   forall a, b integer
            assert fibonacci.fibonacci_int(a + b) == f_a_p1*f_b + f_a*f_b_1

            # F_{a-b} = (-1)^b (F_a F_{b-1} - F_{a-1} F_b)  forall a, b integer
            if b % 2 == 0:  # b even
                assert fibonacci.fibonacci_int(a - b) == f_a*f_b_1 - f_a_1*f_b
            else:           # b odd
                assert fibonacci.fibonacci_int(a - b) == f_a_1*f_b - f_a*f_b_1


def test_formulas_2n():
    for n in range(-1000, 1000):
        f_n_1, f_n = fibonacci.fibonacci_int_pair(n)
        f_n_p1 = f_n + f_n_1

        sqr_f_n_1 = f_n_1**2
        sqr_f_n = f_n**2
        sqr_f_n_p1 = f_n_p1**2

        f_2n_1, f_2n = fibonacci.fibonacci_int_pair(2*n)
        f_2n_p1 = f_2n + f_2n_1

        # F_{2n - 1} = F^2_n + F^2_{n-1}   forall n integer
        assert f_2n_1 == sqr_f_n + sqr_f_n_1

        # F_2n = F^2_{n+1} - F^2_{n-1}
        #      = 2 F_n F_{n-1} + F^2_n   forall n integer
        assert f_2n == sqr_f_n_p1 - sqr_f_n_1
        assert f_2n == 2*f_n*f_n_1 + sqr_f_n

        # F_{2n + 1} = F^2_{n+1} + F^2_n   forall n integer
        assert f_2n_p1 == sqr_f_n_p1 + sqr_f_n


def test_formulas_pow2():
    for i in range(20):
        f_2i_1, f_2i = fibonacci.fibonacci_pow2_pair(i)
        f_2i_p1 = f_2i + f_2i_1

        sqr_f_2i_1 = f_2i_1**2
        sqr_f_2i = f_2i**2
        sqr_f_2i_p1 = f_2i_p1**2

        f_2ip1_1, f_2ip1 = fibonacci.fibonacci_pow2_pair(i + 1)

        # F_{2^{i+1} - 1} = F^2_{2^i} + F^2_{2^i - 1}   forall i integer >= 0
        assert f_2ip1_1 == sqr_f_2i + sqr_f_2i_1

        # F_{2^{i+1}} = F^2_{2^i + 1} - F^2_{2^i - 1}
        #             = 2 F_{2^i} F_{2^i - 1} + F^2_{2^i} forall i integer >= 0
        assert f_2ip1 == sqr_f_2i_p1 - sqr_f_2i_1
        assert f_2ip1 == 2*f_2i*f_2i_1 + sqr_f_2i

        # F_{2^{i+1} + 1} = F^2_{2^i + 1} + F^2_{2^i}   forall i integer >= 0
        assert f_2ip1 + f_2ip1_1 == sqr_f_2i_p1 + sqr_f_2i


def test_formulas_n_add_pow2():
    for n in range(-100, 100):
        f_n_1, f_n = fibonacci.fibonacci_int_pair(n)
        f_n_p1 = f_n + f_n_1

        for i in range(15):
            f_2i_1, f_2i = fibonacci.fibonacci_pow2_pair(i)
            f_2i_p1 = f_2i + f_2i_1

            f_n_2i_1, f_n_2i = fibonacci.fibonacci_int_pair(n + 2**i)
            f_n_2i_p1 = f_n_2i + f_n_2i_1

            # F_{n + 2^i - 1} = F_n F_{2^i} + F_{n-1} F_{2^i - 1}
            # forall n integer, forall i integer >= 0
            assert f_n_2i_1 == f_n*f_2i + f_n_1*f_2i_1

            # F_{n + 2^i} = F_{n+1} F_{2^i} + F_n F_{2^i - 1}
            #             = F_n F_{2^i} + F_n F_{2^i - 1} + F_{n-1} F_{2^i}
            # forall n integer, forall i integer >= 0
            assert f_n_2i == f_n_p1*f_2i + f_n*f_2i_1
            assert f_n_2i == f_n*f_2i + f_n*f_2i_1 + f_n_1*f_2i

            # F_{n + 2^i + 1} = F_{n+1} F_{2^i + 1} + F_n F_{2^i}
            # forall n integer, forall i integer >= 0
            assert f_n_2i_p1 == f_n_p1*f_2i_p1 + f_n*f_2i


def test_formulas_lucas():
    for n in range(-1000, 1000):
        f_n_1, f_n = fibonacci.fibonacci_int_pair(n)
        f_n_p1 = f_n + f_n_1

        # L_n = F_{n+1} + F_{n-1}   forall n integers
        assert fibonacci.lucas_int(n) == f_n_p1 + f_n_1


def test_memoization_end():
    if __name__ != '__main__':
        assert len(fibonacci.__fibonacci_memo) \
            == fibonacci.__fibonacci_memo_max_size

        assert fibonacci.__fibonacci_memo.get(999) \
            == fibonacci.fibonacci__iter_n(999)

    fibonacci.fibonacci_memoization_reinit()

    assert len(fibonacci.__fibonacci_memo) == 0

    fibonacci.fibonacci_memoization_reinit(2)

    assert len(fibonacci.__fibonacci_memo) == 0

    assert fibonacci.fibonacci__rec_memoized_n(0) == 0
    assert fibonacci.fibonacci__rec_memoized_n(1) == 1
    assert fibonacci.fibonacci__rec_memoized_n(2) == 1
    assert fibonacci.fibonacci__rec_memoized_n(3) == 2
    assert fibonacci.fibonacci__rec_memoized_n(4) == 3
    assert fibonacci.fibonacci__rec_memoized_n(10) == 55

    assert fibonacci.__fibonacci_memo == {8: 21, 10: 55}


#
# Main
######
if __name__ == '__main__':
    # Run all test_...() functions (useful if pytest miss)
    for s in sorted(dir()):
        if s[:5] == 'test_':
            print(s, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[s]()
            print(file=sys.stderr)
