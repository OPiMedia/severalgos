# -*- coding: latin-1 -*-

"""
fibonacci package (February 21, 2015)

(Required by https://readthedocs.org/ to build documentation.)

Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2015 Olivier Pirson
http://www.opimedia.be/
"""
