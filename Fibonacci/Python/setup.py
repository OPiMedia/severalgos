#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Setup of fibonacci package (June 21, 2020)

(Required by https://readthedocs.org/ to build documentation.)

Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2015, 2020 Olivier Pirson
http://www.opimedia.be/
"""

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


setup(name='fibonacci',
      description='Several implementation of Fibonacci numbers computation.',

      author='Olivier Pirson',
      author_email='olivier.pirson.opi@gmail.com',
      url='https://bitbucket.org/OPiMedia/severalgos/src/master/Fibonacci/Python/',

      packages=['fibonacci'],
      license='GPLv3',
      platforms='any',
      include_package_data=False,

      keywords='Python arithmetic Fibonacci Lucas',
      classifiers=(
          'Development Status :: 5 - Production/Stable',

          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',

          'Natural Language :: English',

          'Operating System :: OS Independent',

          'Programming Language :: Python',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3',

          'Topic :: Scientific/Engineering :: Mathematics'))
