%
% Fibonacci - several relations to implement good algorithms.
%
% Piece of severalgos.
% https://bitbucket.org/OPiMedia/severalgos
%
% GPLv3 --- Copyright (C) 2015, 2016, 2020 Olivier Pirson
% http://www.opimedia.be/
%
\documentclass[a4paper,ps2pdf,11pt]{article}

\date{June 21, 2020}

\usepackage{Fibonacci}

\hyphenpenalty=10000
\sloppy


\begin{document}%
%
\maketitle

\section{Définitions}%
\textsc{Fibonacci} numbers:\\
\index{Fibonacci number@\textsc{Fibonacci} number}%
\index{F_n@$\Fibo_n$}%
\noindent
$\begin{array}{@{}l@{\ }l@{\,}l@{}}%
  & \Fibo_0 & \assign \mathbf{0}\\
  & \Fibo_1 & \assign \mathbf{1}\\
  \forall n \in \integers : & \emphDef{\Fibo_n} & \assign \Fibo_{n-2}\,+\,\Fibo_{n-1}\qquad
  \OEIS{A000045}\\
\end{array}$

\bigskip
\index{Lucas number@\textsc{Lucas} number}%
\index{L_n@$\Lucas_n$}%
\noindent
\textsc{Lucas} numbers:\\
$\begin{array}{@{}l@{\ }l@{\,}l@{}}%
  & \Lucas_0 & \assign 2\\
  & \Lucas_1 & \assign \mathbf{1}\\
  \forall n \in \integers : & \emphDef{\Lucas_n} & \assign \Lucas_{n-2}\,+\,\Lucas_{n-1}\qquad
  \OEIS{A000032}\\
\end{array}$

\medskip
\noindent
$\begin{array}{@{}|@{\,}l@{\,}||@{\,}c@{}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}||%
    @{\,}r@{\,}||@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|%
    @{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|%
    @{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}r@{\,}|@{\,}c@{}|@{}}%
  \hline
  n & \mbox{\tiny\dots} & -4 & -3 & -2 & -1
  & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9
  & 10 & 11 & 12 & 13 & 14 & 15 & 16 & 17 & \mbox{\tiny\dots}\\
  \hline
  \Fibo_n & & -3 & 2 & -1 & 1
  & \mathbf{0} & \mathbf{1} & 1 & 2 & 3 & \mathbf{5} & 8 & 13 & 21 & 34
  & 55 & 89 & 144 & 233 & 377 & 610 & 987 & \nombre{1597} & \\
  \hline
  \Lucas_n & & 7 & -4 & 3 & \mathbf{-1}
  & 2 & \mathbf{1} & 3 & 4 & 7 & 11 & 18 & 29 & 47 & 76
  & 123 & 199 & 322 & 521 & 843 & \nombre{1364} & \nombre{2207} & \nombre{3571} & \\
  \hline
\end{array}$

\bigskip
\noindent
\index{golden ratio}%
\index{phi@$\Gold$}%
\index{phi@$\Gold$!phihat@$\Gold*$}%
Golden ratio:\\
$\begin{array}{@{}l@{\ }l@{\ }r@{\ }l@{\ }l@{\ }l@{\ }l@{}}%
  \emphDef{\Gold} & \assign \frac{1\,+\,\sqrt{5}}{2} = & \nombre{1,618033988}\dots
  & = \Gold^2 & -\ 1 = {\scriptstyle\sqrt{1\,+\,\sqrt{1\,+\,\sqrt{1\,+\,\dots}}}}
  & = \lim\limits_{n\rightarrow\infty} & \frac{\Fibo_{n+1}}{\Fibo_n}\\

  \emphDef{\Gold*} & \assign \frac{1\,-\,\sqrt{5}}{2} = & \nombre{-0,618033988}\dots
  & = \Gold*^2 & -\ 1 = 1 - \Gold = -\sFrac{1}{\Gold}
  & = \lim\limits_{n\rightarrow-\infty} & \frac{\Fibo_{n+1}}{\Fibo_n}\\
\end{array}$



\section{Closed-form expressions}
\noindent
\index{Binet's formula@\textsc{Binet}'s formula}%
$\forall n \in \integers :
\begin{array}[t]{@{}|@{\ }l@{\ }l@{}}%
\Fibo_n
& = \frac{\Gold^n\ -\ \Gold*^n}{\Gold\ -\ \Gold*}
= \frac{\Gold^n\ -\ \Gold*^n}{\sqrt{5}}\qquad
\text{(\textsc{Binet}'s formula)}\\
\Lucas_n
& = \Gold^n + \Gold*^n\\
\end{array}$

\medskip
\noindent
$\forall n \in \integers :
\begin{array}[t]{@{}|@{\ }l@{\ }l@{}}%
\Gold^n
& = \Fibo_n \Gold + \Fibo_{n - 1}\\
\Gold*^n
& = \Fibo_n \Gold* + \Fibo_{n - 1}\\
\end{array}$



\section{Relations used to implement algorithms}
\subsection{\textsc{Fibonacci}}
\noindent
$\forall n \in \integers :\ \Fibo_{-n} = -(-1)^n \Fibo_n$

\medskip
\noindent
$\forall a, b \in \integers :
\begin{array}[t]{@{}|@{\ }l@{}}%
  \Fibo_{a + b} = \Fibo_{a + 1} \Fibo_b\ +\ \Fibo_a \Fibo_{b - 1}\\
  \Fibo_{a - b} = (-1)^b \left[\Fibo_a \Fibo_{b - 1} - \Fibo_{a - 1} \Fibo_b\right]\\
\end{array}$

\medskip
\noindent
\index{Cassini's identity@\textsc{Cassini}'s identity}%
$\forall n \in \integers :\ \Fibo_{n + 1} \Fibo_{n - 1}\ -\ \Fibo^2_n = (-1)^n$\qquad
(\textsc{Cassini}'s identity)

\medskip
\noindent
$\forall n \in \integers :
\begin{array}[t]{@{}|@{\ }l@{\ }l@{\ }l@{\ }l@{}}%
  \Fibo_{2n - 1} & = \Fibo^2_n & +\ \Fibo^2_{n - 1}
  & = \Lucas_{n}\Fibo_{n} -\Lucas_{n - 1}\Fibo_{n - 1}\\[1pt]

  \Fibo_{2n}
  & = \Fibo^2_{n + 1} & -\ \Fibo^2_{n - 1}
  = 2 \Fibo_n \Fibo_{n - 1} + \Fibo^2_n
  & = \Lucas_{n}\Fibo_{n}\\[1pt]

  \Fibo_{2n + 1} & = \Fibo^2_{n + 1} & +\ \Fibo^2_n
  & = 2\Lucas_{n}\Fibo_{n} -\Lucas_{n - 1}\Fibo_{n - 1}
  = \Lucas_{n + 1}\Fibo_{n + 1} -\Lucas_n\Fibo_n\\
\end{array}$

\medskip
\noindent
$\forall i \in \naturals :
\begin{array}[t]{@{}|@{\ }l@{\ }l@{\ }l@{\ }l@{}}%
  \Fibo_{2^{i+1} - 1} & = \Fibo^2_{2^i} & +\ \Fibo^2_{2^i - 1}
  & = \Lucas_{2^i} \Fibo_{2^i} - \Lucas_{2^i - 1} \Fibo_{2^i - 1}\\[2pt]

  \Fibo_{2^{i+1}}
  & = \Fibo^2_{2^i + 1} & -\ \Fibo^2_{2^i - 1}
  = 2 \Fibo_{2^i} \Fibo_{2^i - 1} + \Fibo^2_{2^i}
  & = \Lucas_{2^i} \Fibo_{2^i}\\[2pt]

  \Fibo_{2^{i+1} + 1} & = \Fibo^2_{2^i + 1} & +\ \Fibo^2_{2^i}\\
\end{array}$

\medskip
\noindent
$\forall n \in \integers, \forall i \in \naturals :
\begin{array}[t]{@{}|@{\ }l@{\ }l@{\ }l@{}}%
  \Fibo_{n + 2^i - 1} & = \Fibo_n \Fibo_{2^i} & +\ \Fibo_{n - 1} \Fibo_{2^i - 1}\\

  \Fibo_{n + 2^i} & = \Fibo_{n + 1} \Fibo_{2^i} & +\ \Fibo_n \Fibo_{2^i - 1}
  = \Fibo_n \Fibo_{2^i} + \Fibo_n \Fibo_{2^i - 1} + \Fibo_{n - 1} \Fibo_{2^i}\\

  \Fibo_{n + 2^i + 1} & = \Fibo_{n + 1} \Fibo_{2^i + 1} & +\ \Fibo_n \Fibo_{2^i}\\
\end{array}$

\medskip
\noindent
$\forall i \in \naturals :\ %
\Fibo_{2^i} = \prod\limits_{k=0}^{i-1} \Lucas_{2^k}$

\medskip
\noindent
$\forall n \in \integers, \forall i \in \naturals :\ %
\Fibo_{2^i n} = \left[\prod\limits_{k=0}^{i-1} \Lucas_{2^k n}\right] \Fibo_n$

\medskip
\noindent
$\forall n, k \in \integers :\ \Fibo_{n + k}\ -\ (-1)^k \Fibo_{n - k} = \Lucas_n \Fibo_k$


\subsection{\textsc{Lucas}}
\noindent
$\forall n \in \integers :\ %
\Lucas_n = \Fibo_{n + 1} + \Fibo_{n - 1} = \Fibo_{n + 2} - \Fibo_{n - 2} = 2 \Fibo_{n - 1} + \Fibo_n$

\medskip
\noindent
$\forall n \in \integers :\ \Lucas_{-n} = (-1)^n \Lucas_n$

\medskip
\noindent
$\forall a, b \in \integers :
\begin{array}[t]{@{}|@{\ }l@{}}%
  \Lucas_{a + b} = \frac{\Lucas_a \Lucas_b\ +\ 5 \Fibo_a \Fibo_b}{2}\\[2pt]
  \Lucas_{a - b} = (-1)^b \frac{\Lucas_a \Lucas_b\ -\ 5 \Fibo_a \Fibo_b}{2}\\
\end{array}$

\medskip
\noindent
$\forall n \in \integers :
\begin{array}[t]{@{}|@{\ }l@{\ }l@{\ }l@{}}%
  \Lucas_{2n - 1}
  & = \Lucas^2_n\ -\ \Lucas^2_{n - 1}\ -\ (-1)^n 4
  & = \Lucas_n \Fibo_n\ +\ \Lucas_{n - 1} \Fibo_{n - 1}\\[1pt]

  \Lucas_{2n}
  & = \Lucas^2_n\ -\ (-1)^n 2
  & = \Lucas_{n + 1} \Fibo_{n + 1}\ -\ \Lucas_{n - 1} \Fibo_{n - 1}\\
  \Lucas_{2n + 1}
  & = \Lucas^2_{n + 1}\ -\ \Lucas^2_n\ +\ (-1)^n 4
  & = \Lucas_{n + 1} \Fibo_{n + 1}\ +\ \Lucas_n \Fibo_n\\
\end{array}$

\medskip
\noindent
$
\begin{array}[t]{@{\ }l@{\ }l@{\ }l@{}}%
  \forall i \in \naturals :
  & \Lucas_{2^{i+1} - 1}
  & = \Lucas_{2^i} \Fibo_{2^i} + \Lucas_{2^i - 1} \Fibo_{2^i - 1}\\[2pt]

  \forall i \in \naturals* :
  & \Lucas_{2^{i+1}}
  & = \Lucas^2_{2^i}\ -\ 2\\
\end{array}$



\section{Other relations, maybe useful}
\noindent
\index{Mersenne number@\textsc{Mersenne} number}%
\index{M_n@$\Mersenne_n$}%
\noindent
\textsc{Mersenne} numbers:\\
$\forall i \in \naturals :\ \emphDef{\Mersenne_i} \assign 2^i - 1 = \base{\underbrace{1\dots1}_i}
\qquad\OEIS{A000225}$

\medskip
\noindent
$\forall i, j \in \naturals, i \leq j :\ %
\base{\underbrace{1\dots1\underbrace{0\dots0}_i}_j} = \Mersenne_j - \Mersenne_i = 2^i \Mersenne_{j - i} = 2^j - 2^i$


\subsection{\textsc{Fibonacci}}
\noindent
$\forall i \in \naturals*, \forall j \in \naturals :
\begin{array}[t]{@{}|@{\ }l@{\ }l@{}}%
  \Fibo_{2^j - 2^i - 1}
  & = \Fibo_{2^j - 1} \Fibo_{2^i + 1} - \Fibo_{2^j} \Fibo_{2^i}\\
  & = \Fibo_{2^j - 1} \Fibo_{2^i - 1} - (\Fibo_{2^j} - \Fibo_{2^j - 1}) \Fibo_{2^i}\\

  \Fibo_{2^j - 2^i} & = \Fibo_{2^j} \Fibo_{2^i - 1} - \Fibo_{2^j - 1} \Fibo_{2^i}\\

  \Fibo_{2^j - 2^i + 1}
  & = \Fibo_{2^j + 1} \Fibo_{2^i - 1} - \Fibo_{2^j} \Fibo_{2^i}\\
  & = \Fibo_{2^j - 1} \Fibo_{2^i - 1} - \Fibo_{2^j} (\Fibo_{2^i} - \Fibo_{2^i - 1})\\
\end{array}$

\medskip
\noindent
$\forall n \in \integers :
\begin{array}[t]{@{}|@{\ }l@{\ }l@{\qquad}l@{\ }@{}}%
  \Fibo_{3n}
  & = 2 \Lucas_n \Fibo^2_n\ +\ \Lucas_{n - 2} \Fibo_n \Fibo_{n - 1}
  & \equiv 0[2]\\[1pt]

  \Fibo_{3n + 1}
  & = \Lucas_{n + 1} \Fibo^2_{n + 1}\ +\ \Lucas_n \Fibo_n \Fibo_{n - 1}
  & \equiv 1 [2]\\[1pt]

  \Fibo_{3n + 2}
  & = \Fibo^2_{n + 1} (\Lucas_{n + 1} + \Fibo_n) + \Fibo^3_n
  & \equiv 1 [2]\\
\end{array}$


\subsection{When a number is a \textsc{Fibonacci} number?}
\noindent
$\forall n \in \naturals :\\
n$ is a \textsc{Fibonacci} number
$\iff 5n^2 + 4$ or $5n^2 - 4$ is a perfect square



\section{Some relations on more general applications}
\noindent
Let $\emphDef{f} \in \reals^{\integers}$
such that $\forall n \in \integers :\ f(n) = f(n - 1) + f(n - 2)$

\bigskip
\noindent
$\forall n \in \integers :\ f(n) = f(0) \Fibo_{n - 1}\ +\ f(1) \Fibo_n$

\medskip
\noindent
$\forall n \in \integers :\ f(-n)
= (-1)^n \left[f(0) \Fibo_{n + 1}\ -\ f(1) \Fibo_n\right]
= (-1)^n \left[f(0) \Fibo_{n - 1}\ -\ f(-1) \Fibo_n\right]$

\medskip
\noindent
$f = f(1) \Fibo
\iff \forall n \in \integers :\ f(-n) = -(-1)^n f(n)$\\
$f = f(1) \Lucas
\iff
\forall n \in \integers :\ f(-n) = (-1)^n f(n)$

\bigskip
\noindent
$\forall a, b \in \integers :\ f(a + b)
\begin{array}[t]{@{\ }l@{\ }l@{}}%
  = f(a + 1) \Fibo_b & +\ f(a) \Fibo_{b - 1}\\
  = f(a) \Fibo_{b + 1} & +\ f(a - 1) \Fibo_b\\
\end{array}$

\medskip
\noindent
$\forall n \in \integers :
\begin{array}[t]{@{}|@{\ }l@{\ }l@{\ }l@{}}%
  f(2n - 1) & = f(n) \Fibo_n & +\ f(n - 1) \Fibo_{n - 1}\\
  f(2n) & = f(n + 1) \Fibo_n & +\ f(n) \Fibo_{n - 1}\\
  f(2n + 1) & = f(n + 1) \Fibo_{n + 1} & +\ f(n) \Fibo_n\\
\end{array}$



\begin{thebibliography}{9}%
\addcontentsline{toc}{section}{Références}%
\bibitem[MathWorld]{MathWorld}
  \textbf{\href{http://mathworld.wolfram.com/FibonacciNumber.html}{\textsc{Fibonacci} Number}}
  \footnote{\httpShow{mathworld.wolfram.com/FibonacciNumber.html}}
  and
  \textbf{\href{http://mathworld.wolfram.com/LucasNumber.html}{\textsc{Lucas} Number}}
  \footnote{\httpShow{mathworld.wolfram.com/LucasNumber.html}}
  (MathWorld)

\bibitem[OEIS]{OEIS}
  \textbf{\href{http://oeis.org/}{The On-Line Encyclopedia of Integer Sequences}}
  \footnote{\httpShow{oeis.org/}}

\bibitem[severalgo/\textsc{Fibonacci}]{severalgoFibonacci}
  Complete \textbf{C++ and Python sources} on
  \href{https://bitbucket.org/OPiMedia/severalgos/src/master/Fibonacci/}{Bitbucket}
  \footnote{\httpsShow{bitbucket.org/OPiMedia/severalgos/src/master/Fibonacci/}}

  The \href{http://www.opimedia.be/DS/online-documentations/severalgos/Fibonacci-cpp/html/}{C++ HTML online documentation}
  \footnote{\httpShow{www.opimedia.be/DS/online-documentations/severalgos/Fibonacci-cpp/html/}}

\bibitem[\textsc{Takahashi}]{Takahashi}
  \textbf{A fast algorithm for computing large Fibonacci numbers}\\
  (Daisuke \textsc{Takahashi}, 2000)
  \href{http://www.ii.uni.wroc.pl/~lorys/IPL/article75-6-1.pdf}{\texttt{.pdf}}
  \footnote{\httpShow{www.ii.uni.wroc.pl/~lorys/IPL/article75-6-1.pdf}}

\bibitem[testing \textsc{Fibonacci}]{testingFibonacci}
  \href{http://mathlesstraveled.com/2016/02/27/testing-fibonacci-numbers-the-proofs/}{\textbf{Testing \textsc{Fibonacci} numbers: the proofs}}\\
  (Brent \textsc{Yorgey}, February 27, 2016)
  \footnote{\httpShow{mathlesstraveled.com/2016/02/27/testing-fibonacci-numbers-the-proofs/}}

\end{thebibliography}



\addcontentsline{toc}{section}{Index}%
\printindex



\addcontentsline{toc}{section}{\contentsname}%
\tableofcontents
%
\end{document}
