:- module(fibonacci, [fibonacci__exp/2,
                      fibonacci/2, fibonacci/3]).
/** <module> ancestor example

Functions to calculate Fibonacci numbers in Prolog.
https://oeis.org/A000045

Open HTML dynamic documentation in a browser:
$ prolog --pldoc fibonacci.pl

To run all unit tests:
?- run_tests.

@author Olivier Pirson --- http://www.opimedia.be/
@license GPLv3 --- Copyright (C) 2018 Olivier Pirson
@version June 19, 2018
*/

%% fibonacci__exp(+N, ?F_N)
% Fibonacci number F_N.
% O(2^N)
%
% @param N Integer >= 0
% @param F_N the Nth Fibonacci number F_N
fibonacci__exp(0, 0).
fibonacci__exp(1, 1).
fibonacci__exp(N, F_N) :- N > 1,
                          N_2 is N - 2,
                          fibonacci__exp(N_2, F_N_2),
                          N_1 is N - 1,
                          fibonacci__exp(N_1, F_N_1),
                          F_N is F_N_1 + F_N_2.


%% fibonacci(+N, ?F_N1, ?F_N)
% Fibonacci numbers F_{N-1} and F_N.
% O(N)
%
% @param N Integer >= 0
% @param F_N_1 the (N-1)th Fibonacci number F_{N-1}
% @param F_N the Nth Fibonacci number F_N
fibonacci(0, 1, 0).
fibonacci(N, F_N_1, F_N) :- N > 0,
                            K is N - 1,
                            fibonacci(K, F_K_1, F_K),
                            F_N_1 is F_K,
                            F_N is F_K + F_K_1.


%% fibonacci(+N, ?F_N)
% Fibonacci number F_N.
% O(N)
%
% @param N Integer >= 0
% @param F_N the Nth Fibonacci number F_N
fibonacci(N, F_N) :- fibonacci(N, _F_N_1, F_N).



% %%%%%%%%%%%%
% Unit Tests %
% %%%%%%%%%%%%

:- begin_tests(fibonacci__exp).

test(fibonacci__exp) :-
    assertion(fibonacci__exp(0, 0)),
    assertion(fibonacci__exp(1, 1)),
    assertion(fibonacci__exp(2, 1)),
    assertion(fibonacci__exp(3, 2)),
    assertion(fibonacci__exp(4, 3)),
    assertion(fibonacci__exp(5, 5)),
    assertion(fibonacci__exp(10, 55)),
    assertion(fibonacci__exp(20, 6765)).

:- end_tests(fibonacci__exp).



:- begin_tests(fibonacci).

test(fibonacci_3) :-
    assertion(fibonacci(0, 1, 0)),
    assertion(fibonacci(1, 0, 1)),
    assertion(fibonacci(2, 1, 1)),
    assertion(fibonacci(3, 1, 2)),
    assertion(fibonacci(4, 2, 3)),
    assertion(fibonacci(5, 3, 5)),
    assertion(fibonacci(10, 34, 55)),
    assertion(fibonacci(20, 4181, 6765)).

test(fibonacci_2) :-
    assertion(fibonacci(0, 0)),
    assertion(fibonacci(1, 1)),
    assertion(fibonacci(2, 1)),
    assertion(fibonacci(3, 2)),
    assertion(fibonacci(4, 3)),
    assertion(fibonacci(5, 5)),
    assertion(fibonacci(10, 55)),
    assertion(fibonacci(20, 6765)).

:- end_tests(fibonacci).
