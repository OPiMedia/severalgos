# -*- coding: latin-1 -*-

"""
point2d (January 7, 2025)

Functions and class to manipulate 2D points.

In the documentation of this module,
*valid coordinates* mean
a sequence of at least 2 int or float.
If this sequence have more 2 items, only the 2 firsts are used.

Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2014, 2025 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import collections
import math


#
# Functions
###########
def distance(a, b=(0, 0)):
    """
    Return the Euclidian distance between `a` and `b`.
    This is the 2-norm distance:
    ((a_x - b_x)^2 + (y_x - b_y)^2)^{1/2}

    :param a: valid coordinates
    :param b: valid coordinates

    :return: float >= 0
    """
    assert isinstance(a, collections.Sequence), type(a)
    assert len(a) >= 2, a

    assert isinstance(b, collections.Sequence), type(b)
    assert len(b) >= 2, b

    return math.hypot(a[0] - b[0], a[1] - b[1])


def distance2(a, b=(0, 0)):
    """
    Return the square of the Euclidian distance between `a` and `b`:
    (a_x - b_x)^2 + (y_x - b_y)^2

    :param a: valid coordinates
    :param b: valid coordinates

    :return: (int or float) >= 0
    """
    assert isinstance(a, collections.Sequence), type(a)
    assert len(a) >= 2, a

    assert isinstance(b, collections.Sequence), type(b)
    assert len(b) >= 2, b

    return (a[0] - b[0])**2 + (a[1] - b[1])**2


def distance_manhattan(a, b=(0, 0)):
    """
    Return the Manhattan distance between `a` and `b`.
    This is the 1-norm distance:
    |a_x - b_x| + |y_x - b_y|

    :param a: valid coordinates
    :param b: valid coordinates

    :return: (int or float) >= 0
    """
    assert isinstance(a, collections.Sequence), type(a)
    assert len(a) >= 2, a

    assert isinstance(b, collections.Sequence), type(b)
    assert len(b) >= 2, b

    return abs(a[0] - b[0]) + abs(a[1] - b[1])


def distance_p_norm(a, b, p):
    """
    Return the p-norm distance between `a` and `b`.
    (|a_x - b_x|^p + |y_x - b_y|^p)^{1/p}

    :param a: valid coordinates
    :param b: valid coordinates
    :param p: (int or float) >= 1

    :return: (int or float) >= 0
    """
    assert isinstance(a, collections.Sequence), type(a)
    assert len(a) >= 2, a

    assert isinstance(b, collections.Sequence), type(b)
    assert len(b) >= 2, b

    assert (isinstance(p, int) or isinstance(p, float)), type(p)
    assert p >= 1, p

    return (abs(a[0] - b[0])**p + abs(a[1] - b[1])**p)**(1/p)


def floats_equal(x, y, epsilon=1e-10):
    """
    If `x` and `y` are equal with a relative precision of `epsilon`
    then return `True`,
    else return `False`.

    See http://realtimecollisiondetection.net/blog/?p=89

    :param x: int or float
    :param y: int or float
    :param epsilon: float >= 0

    :return: bool
    """
    assert (isinstance(x, int) or isinstance(x, float)), type(x)
    assert (isinstance(y, int) or isinstance(y, float)), type(y)

    assert isinstance(epsilon, float), type(epsilon)
    assert epsilon >= 0, epsilon

    return abs(x - y) <= max(1, abs(x), abs(y))*epsilon


def generator_points_coordinate(points):
    """
    Return a generator that yields
    the sequence of separated coordinates of
    the sequence of points.

    For example, if points = ((0, 1), (2, 3), (4, 5))
    then return 0, 1, 2, 3, 4, 5 and then raise StopIteration.

    :param points: Iterable of (valid coordinates)

    :return: int or float

    :raise: StopIteration if all points are returned
    """
    assert isinstance(points, collections.Iterable), type(points)

    for point in points:
        assert isinstance(point, collections.Sequence), type(point)
        assert len(point) >= 2, point
        assert (isinstance(point[0], int) or isinstance(point[0], float)), \
            type(point)
        assert (isinstance(point[1], int) or isinstance(point[1], float)), \
            type(point)

        yield point[0]

        yield point[1]

    raise StopIteration


def points_equal(a, b, epsilon=1e-10):
    """
    If `a` and `b` are equal with a relative precision of `epsilon`
    then return `True`,
    else return `False`.

    :param a: valid coordinates
    :param b: valid coordinates
    :param epsilon: float >= 0

    :return: bool
    """
    assert isinstance(a, collections.Sequence), type(a)
    assert len(a) >= 2, a
    assert isinstance(a[0], int) or isinstance(a[0], float), type(a[0])
    assert isinstance(a[1], int) or isinstance(a[1], float), type(a[1])

    assert isinstance(b, collections.Sequence), type(b)
    assert len(b) >= 2, b
    assert isinstance(b[0], int) or isinstance(b[0], float), type(b[0])
    assert isinstance(b[1], int) or isinstance(b[1], float), type(b[1])

    assert isinstance(epsilon, float), type(epsilon)
    assert epsilon >= 0, epsilon

    return (floats_equal(a[0], b[0], epsilon=epsilon)
            and floats_equal(a[1], b[1], epsilon=epsilon))


def rotate(point, angle, center=(0, 0)):
    """
    Return the point after
    a rotation of `angle` radians in the counterclockwise
    around the center `center`.

    :param point: valid coordinates
    :param angle: int or float
    :param center: valid coordinates

    :return: (int , int) or (float, float)
    """
    assert isinstance(point, collections.Sequence), type(point)
    assert len(point) >= 2, point

    assert isinstance(angle, int) or isinstance(angle, float), type(angle)

    assert isinstance(center, collections.Sequence), type(center)
    assert len(center) >= 2, center
    assert isinstance(center[0], int) or isinstance(center[0], float), \
        type(center[0])
    assert isinstance(center[1], int) or isinstance(center[1], float), \
        type(center[1])

    if angle == 0:
        return point
    else:
        x = point[0] - center[0]
        y = point[1] - center[1]
        angle = math.atan2(y, x) + angle
        radius = distance((x, y))

        return (math.cos(angle)*radius + center[0],
                math.sin(angle)*radius + center[1])


def rotate90(point, center=(0, 0)):
    """
    Return the point after
    a rotation of 90 degrees in the counterclockwise
    around the center `center`.

    :param point: valid coordinates
    :param center: valid coordinates

    :return: (int, int) or (float, float)
    """
    assert isinstance(point, collections.Sequence), type(point)
    assert len(point) >= 2, point

    assert isinstance(center, collections.Sequence), type(center)
    assert len(center) >= 2, center
    assert isinstance(center[0], int) or isinstance(center[0], float), \
        type(center[0])
    assert isinstance(center[1], int) or isinstance(center[1], float), \
        type(center[1])

    return (-point[1] + center[1] + center[0],
            point[0] - center[0] + center[1])


def rotate180(point, center=(0, 0)):
    """
    Return the point after
    a rotation of 180 degrees
    around the center `center`.

    :param point: valid coordinates
    :param center: valid coordinates

    :return: (int, int) or (float, float)
    """
    assert isinstance(point, collections.Sequence), type(point)
    assert len(point) >= 2, point

    assert isinstance(center, collections.Sequence), type(center)
    assert len(center) >= 2, center
    assert isinstance(center[0], int) or isinstance(center[0], float), \
        type(center[0])
    assert isinstance(center[1], int) or isinstance(center[1], float), \
        type(center[1])

    return (-point[0] + center[0]*2,
            -point[1] + center[1]*2)


def rotate270(point, center=(0, 0)):
    """
    Return the point after
    a rotation of 270 degrees in the counterclockwise
    (-90 degrees in clockwise)
    around the center `center`.

    :param point: valid coordinates
    :param center: valid coordinates

    :return: (int, int) or (float, float)
    """
    assert isinstance(point, collections.Sequence), type(point)
    assert len(point) >= 2, point

    assert isinstance(center, collections.Sequence), type(center)
    assert len(center) >= 2, center
    assert isinstance(center[0], int) or isinstance(center[0], float), \
        type(center[0])
    assert isinstance(center[1], int) or isinstance(center[1], float), \
        type(center[1])

    return (point[1] - center[1] + center[0],
            -point[0] + center[0] + center[1])
