# -*- coding: latin-1 -*-

"""
Points2d (April 21, 2014)

Functions and class to manipulate sequences of 2D points.

In the documentation of this module,
*valid coordinates* mean
a sequence of at least 2 int or float.
If this sequence have more 2 items, only the 2 firsts are used.

Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2014 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import array
import collections
import math

import point2d


#
# Global constant
#################
ITEM_TYPES = ('b', 'B', 'h', 'H', 'i', 'I', 'l', 'L',
              'f', 'd')
"""
List of valid typecode to initialize Points2d.
"""

try:
    TMP_ARRAY = array.array('q', (666, ))
    del TMP_ARRAY
    TMP_ARRAY = array.array('Q', (666, ))
    del TMP_ARRAY
    ITEM_TYPES = ITEM_TYPES + ('q', 'Q')
except ValueError:
    pass


#
# Class
#######
class Points2d(collections.Sequence):
    """
    Sequence of 2D points
    """

    @classmethod
    def polygon(cls, nb_points, radius=1, angle=0, center=(0, 0)):
        """
        Return a regular polygon of `nb_points` vertices
        in `radius` distance of the center `center`.

        The first point
        are an angle of `angle` radians in the counterclockwise.

        :param nb_points: int >= 0
        :param radius: (int >= 0) or (float >= 0)
        :param angle: int or float
        :param center: valid coordinates

        :return: Points2d
        """
        assert isinstance(nb_points, int), type(nb_points)
        assert nb_points >= 0, nb_points

        assert isinstance(radius, int) or isinstance(radius, float), \
            type(radius)
        assert radius >= 0, radius

        assert isinstance(angle, int) or isinstance(angle, float), type(angle)

        assert isinstance(center, collections.Sequence), type(center)
        assert len(center) >= 2, center
        assert isinstance(center[0], int) or isinstance(center[0], float), \
            type(center[0])
        assert isinstance(center[1], int) or isinstance(center[1], float), \
            type(center[1])

        points = [(center[0] + math.cos(angle + math.pi*2*i/nb_points)*radius,
                   center[1] + math.sin(angle + math.pi*2*i/nb_points)*radius)
                  for i in range(nb_points)]
        points = Points2d(points, item_type='d')

        return points

    def __init__(self, points,
                 item_type=None, distance_fcts=(point2d.distance,
                                                point2d.distance2)):
        """
        Initialize the sequence of points with `points`.

        Points are stored in `array.array`.

        If item_type is not None
        then use it to specified the type of items in the `array.array`.
        else uses the minimum necessary type.

        See https://docs.python.org/3/library/array.html#module-array

        distance_fcts ???

        :param points: Iterable of (valid coordinates)
        :param item_type: None or (str in ITEM_TYPES)
        :param distance_fcts: function ((int or float),
                                        (int or float)) -> (int or float)
                              or (function ((int or float),
                                            (int or float)) -> (int or float),
                                  function ((int or float),
                                            (int or float)) -> (int or float))
        """
        assert isinstance(points, collections.Iterable), type(points)
        assert (item_type is None) or isinstance(item_type, str), \
            type(item_type)

        if item_type is None:  # search the minimum necessary type
            if not isinstance(points, collections.Sequence):
                points = tuple(points)

            min_coordinate = 0
            max_coordinate = 0
            for x, y in points:
                if isinstance(x, float) or isinstance(y, float):
                    item_type = 'd'  # double

                    break

                min_coordinate = min(min_coordinate, x, y)
                max_coordinate = min(max_coordinate, x, y)

            if item_type is None:
                if min_coordinate < 0:  # some coordinate are negative
                    max_coordinate = max(max_coordinate, -min_coordinate)
                    if max_coordinate <= (1 << 7) - 1:
                        item_type = 'b'  # signed char
                    elif max_coordinate <= (1 << 15) - 1:
                        item_type = 'h'  # signed short
                    elif max_coordinate <= (1 << 31) - 1:
                        item_type = 'i'  # signed int
                    elif max_coordinate <= (1 << 63) - 1:
                        item_type = 'l'  # signed long
                    else:
                        item_type = 'd'  # double
                else:                   # all coordinate are positive
                    if max_coordinate <= (1 << 8) - 1:
                        item_type = 'B'  # unsigned char
                    elif max_coordinate <= (1 << 16) - 1:
                        item_type = 'H'  # unsigned short
                    elif max_coordinate <= (1 << 32) - 1:
                        item_type = 'I'  # unsigned int
                    elif max_coordinate <= (1 << 64) - 1:
                        item_type = 'L'  # unsigned long
                    else:
                        item_type = 'd'  # double

        assert item_type in ITEM_TYPES, item_type

        self.__array = array.array(item_type,
                                   point2d.generator_points_coordinate(points))

        if isinstance(distance_fcts, collections.Sequence):
            assert len(distance_fcts) >= 2, distance_fcts
            assert callable(distance_fcts[0]), type(distance_fcts[0])
            assert callable(distance_fcts[1]), type(distance_fcts[1])

            self.__distance_fct = distance_fcts[0]
            self.__distance_associate_fct = distance_fcts[1]
        else:
            assert callable(distance_fcts), type(distance_fcts)

            self.__distance_fct = distance_fcts
            self.__distance_associate_fct = lambda x, y: distance_fcts(x, y)**2

        self.__hash = None

    def __contains__(self, point):
        """
        If ``point`` is a point of this Points2d
        then return `True`,
        else return `False`.

        :param point: validate coordinates

        :return: bool
        """
        assert isinstance(point, collections.Sequence), type(point)
        assert len(point) >= 2, point
        assert isinstance(point[0], int) or isinstance(point[0], float), \
            type(point[0])
        assert isinstance(point[1], int) or isinstance(point[1], float), \
            type(point[1])

        for i in range(self.__array.buffer_info()[1], 2):
            if ((self.__array[i] == point[0])
                    and (self.__array[i + 1] == point[1])):
                return True

        return False

    def __getitem__(self, i):
        """
        Return the ith point of the sequence of points.

        :param i: int or slice

        :return: (int, int) or (float, float)
                 or (tuple of ((int, int) or (float, float)))

        :raise: TypeError if `i` have an invalid type
        """
        if isinstance(i, int):
            i *= 2

            return (self.__array[i], self.__array[i + 1])
        elif isinstance(i, slice):
            return NotImplemented
        else:
            raise TypeError

    def __iter__(self):
        """
        Return an iterator that yields
        the sequence of points.

        :return: (int, int) or (float, float)

        :raise: StopIteration if all points are returned
        """
        i = 0
        while i < self.__array.buffer_info()[1]:
            yield (self.__array[i], self.__array[i + 1])

            i += 2

        raise StopIteration

    def __len__(self):
        """
        Return the number of points.

        :return: int >= 0
        """
        return self.__array.buffer_info()[1]//2

    def __reversed__(self):
        """
        Return an iterator that yields
        the sequence of points in reversed order.

        :return: (int, int) or (float, float)

        :raise: StopIteration if all points are returned
        """
        i = self.__array.buffer_info()[1] - 1
        while i >= 0:
            yield (self.__array[i - 1], self.__array[i])

            i -= 2

        raise StopIteration

    def distance(self, i, j):
        """
        Return the distance between `i` and `j`.
        (By default the Euclidian distance.)

        :param i: 0 <= int < len(self)
        :param j: 0 <= int < len(self)

        :return: float >= 0
        """
        assert isinstance(i, int), type(i)
        assert 0 <= i < len(self), i

        assert isinstance(j, int), type(j)
        assert 0 <= j < len(self), j

        return self.__distance_fct(self[i], self[j])

    def distance_associate(self, i, j):
        """
        Return
        result of the associate function of distance between `i` and `j`.
        (By default the square of Euclidian distance.)

        :param i: 0 <= int < len(self)
        :param j: 0 <= int < len(self)

        :return: float >= 0
        """
        assert isinstance(i, int), type(i)
        assert 0 <= i < len(self), i

        assert isinstance(j, int), type(j)
        assert 0 <= j < len(self), j

        return self.__distance_associate_fct(self[i], self[j])

    def draw(self):
        """
        Draw all points with matplotlib.

        .. warning:: Require NetworkX_ and matplotlib_.

        .. _NetworkX: http://networkx.github.io/
        .. _matplotlib: http://matplotlib.org/
        """
        import networkx  # do without ???
        import matplotlib.pyplot

        graph = networkx.Graph()
        graph.add_nodes_from(range(len(self)))

        networkx.draw(graph, pos=self)
        matplotlib.pyplot.show()

    def item_type(self):
        """
        Return item_type used.
        See `__init__()`.

        :return: str in ITEM_TYPES
        """
        return self.__array.typecode
