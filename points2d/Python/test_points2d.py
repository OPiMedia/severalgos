#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2014, 2015 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import math
import sys

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import point2d
import points2d

EMPTY = ()

HLINE10 = [(i, 0) for i in range(-10, 11)]
VLINE10 = [(0, i) for i in range(-10, 11)]
DLINE10 = [(i, i) for i in range(-10, 11)]

SQUARE = ((0, 0), (1, 0), (1, 1), (0, 1))
LITTLE_SQUARE = ((0, 0), (0.5, 0), (0.5, 0.5), (0, 0.5))

EXAMPLES = (EMPTY,
            HLINE10, VLINE10, DLINE10,
            SQUARE, LITTLE_SQUARE)

ALL = set()
for ps in EXAMPLES:
    ALL |= set(ps)
points = frozenset(ps)

del ps


def test_Points2d():  # ???
    for ps in EXAMPLES:
        points = points2d.Points2d(ps)

        assert tuple(points) == tuple(ps)

    ps = SQUARE
    ps = [point2d.rotate(point, math.pi/2, center=(0.5, 0.5))
          for point in SQUARE]

    points = points2d.Points2d(ps)
    # points.draw()

    points = points2d.Points2d.polygon(4)
    # points.draw()


def test_Points2d_distance():  # ???
    for ps in EXAMPLES:
        points = points2d.Points2d(ps)

        for i, a in enumerate(ps):
            for j, b in enumerate(ps):
                assert points.distance(i, j) == point2d.distance(a, b)


def test_Points2d_distance_associate():  # ???
    pass


def test_polygon():  # ???
    pass


# ???


#
# Main
######
if __name__ == '__main__':
    # Run all test_...() functions (useful if pytest miss)
    for s in sorted(dir()):
        if s[:5] == 'test_':
            print(s, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[s]()
            print(file=sys.stderr)
