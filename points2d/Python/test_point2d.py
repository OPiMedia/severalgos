#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Piece of severalgos.
https://bitbucket.org/OPiMedia/severalgos

GPLv3 --- Copyright (C) 2014 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import math
import sys

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import point2d

EMPTY = ()

HLINE10 = [(i, 0) for i in range(-10, 11)]
VLINE10 = [(0, i) for i in range(-10, 11)]
DLINE10 = [(i, i) for i in range(-10, 11)]

SQUARE = ((0, 0), (1, 0), (1, 1), (0, 1))
LITTLE_SQUARE = ((0, 0), (0.5, 0), (0.5, 0.5), (0, 0.5))

EXAMPLES = (EMPTY,
            HLINE10, VLINE10, DLINE10,
            SQUARE, LITTLE_SQUARE)

ALL = set()
for ps in EXAMPLES:
    ALL |= set(ps)
points = frozenset(ps)

del ps


def test_distance():
    assert point2d.distance((0, 0), (1, 0)) == 1
    assert point2d.distance((0, 0), (-1, 0)) == 1
    assert point2d.distance((0, 0), (0, 1)) == 1
    assert point2d.distance((0, 0), (0, -1)) == 1

    for a in ALL:
        assert point2d.distance(a, a) == 0
        assert point2d.distance(a) == math.hypot(a[0], a[1])
        for b in ALL:
            assert point2d.distance(a, b) == math.hypot(a[0] - b[0],
                                                        a[1] - b[1])
            assert point2d.distance(b, a) == point2d.distance(a, b)

    for a in HLINE10:
        assert point2d.distance(a) == abs(a[0])
        for b in HLINE10:
            assert point2d.distance(a, b) == abs(a[0] - b[0])

    for a in VLINE10:
        assert point2d.distance(a) == abs(a[1])
        for b in VLINE10:
            assert point2d.distance(a, b) == abs(a[1] - b[1])

    for a in DLINE10:
        assert point2d.distance(a) == math.sqrt((abs(a[0])**2)*2)
        for b in DLINE10:
            assert point2d.distance(a, b) == math.sqrt((abs(a[0] - b[0])**2)*2)


def test_distance2():
    assert point2d.distance2((1, 0)) == 1
    assert point2d.distance2((-1, 0)) == 1
    assert point2d.distance2((0, 1)) == 1
    assert point2d.distance2((0, -1)) == 1

    for a in ALL:
        assert point2d.distance2(a, a) == 0
        for b in ALL:
            assert math.sqrt(point2d.distance2(a, b)) \
                == point2d.distance(a, b)
            assert point2d.distance2(b, a) == point2d.distance2(a, b)

    for a in HLINE10:
        assert point2d.distance2(a) == a[0]**2
        for b in HLINE10:
            assert point2d.distance2(a, b) == (a[0] - b[0])**2

    for a in VLINE10:
        assert point2d.distance2(a) == a[1]**2
        for b in VLINE10:
            assert point2d.distance2(a, b) == (a[1] - b[1])**2

    for a in DLINE10:
        assert point2d.distance2(a) == (abs(a[0])**2)*2
        for b in DLINE10:
            assert point2d.distance2(a, b) == (abs(a[0] - b[0])**2)*2


def test_distance_manhattan():
    assert point2d.distance_manhattan((0, 0), (1, 0)) == 1
    assert point2d.distance_manhattan((0, 0), (-1, 0)) == 1
    assert point2d.distance_manhattan((0, 0), (0, 1)) == 1
    assert point2d.distance_manhattan((0, 0), (0, -1)) == 1

    for a in ALL:
        assert point2d.distance_manhattan(a, a) == 0
        assert point2d.distance_manhattan(a) == abs(a[0]) + abs(a[1])
        for b in ALL:
            assert point2d.distance_manhattan(a, b) \
                == abs(a[0] - b[0]) + abs(a[1] - b[1])
            assert point2d.distance_manhattan(b, a) \
                == point2d.distance_manhattan(a, b)

    for a in HLINE10:
        assert point2d.distance_manhattan(a) == abs(a[0])
        for b in HLINE10:
            assert point2d.distance_manhattan(a, b) == abs(a[0] - b[0])

    for a in VLINE10:
        assert point2d.distance_manhattan(a) == abs(a[1])
        for b in VLINE10:
            assert point2d.distance_manhattan(a, b) == abs(a[1] - b[1])

    for a in DLINE10:
        assert point2d.distance_manhattan(a) == abs(a[0])*2
        for b in DLINE10:
            assert point2d.distance_manhattan(a, b) == abs(a[0] - b[0])*2


def test_distance_p_norm():
    for a in ALL:
        for b in ALL:
            assert point2d.floats_equal(point2d.distance_p_norm(a, b, 1),
                                        point2d.distance_manhattan(a, b))
            assert point2d.floats_equal(point2d.distance_p_norm(a, b, 2),
                                        point2d.distance(a, b))

    for p in range(1, 10):
        assert point2d.distance_p_norm((0, 0), (1, 0), p) == 1
        assert point2d.distance_p_norm((0, 0), (-1, 0), p) == 1
        assert point2d.distance_p_norm((0, 0), (0, 1), p) == 1
        assert point2d.distance_p_norm((0, 0), (0, -1), p) == 1

        for a in ALL:
            assert point2d.distance_p_norm(a, a, p) == 0
            assert point2d.distance_p_norm(a, (0, 0), p) \
                == (abs(a[0])**p + abs(a[1])**p)**(1.0/p)
            for b in ALL:
                assert point2d.distance_p_norm(a, b, p) \
                    == (abs(a[0] - b[0])**p + abs(a[1] - b[1])**p)**(1.0/p)
            assert point2d.distance_p_norm(b, a, p) \
                == point2d.distance_p_norm(a, b, p)

        for a in HLINE10:
            assert point2d.floats_equal(point2d.distance_p_norm(a, (0, 0), p),
                                        abs(a[0]))
            for b in HLINE10:
                assert point2d.floats_equal(point2d.distance_p_norm(a, b, p),
                                            abs(a[0] - b[0]))

        for a in VLINE10:
            assert point2d.floats_equal(point2d.distance_p_norm(a, (0, 0), p),
                                        abs(a[1]))
            for b in VLINE10:
                assert point2d.floats_equal(point2d.distance_p_norm(a, b, p),
                                            abs(a[1] - b[1]))


def test_floats_equal():  # ???
    for i in range(-10, 11):
        for j in range(-10, 11):
            assert point2d.floats_equal(i, j) == (i == j)
            assert point2d.floats_equal(float(i), j) == (i == j)
            assert point2d.floats_equal(i, float(j)) == (i == j)

            for d in range(-10, 11):
                if d != 0:
                    assert point2d.floats_equal(i/d, j/d) == (i == j)
                    assert point2d.floats_equal(i/d, j/d) \
                        == point2d.floats_equal(j/d, i/d)


def test_generator_points_coordinate():  # ???
    for ps in EXAMPLES:
        s = []
        for point in ps:
            s.extend(point)

        assert list(point2d.generator_points_coordinate(ps)) == s


def test_points_equal():  # ???
    pass


def test_rotate():  # ???
    for point in ALL:
        assert point2d.rotate(point, 0) == point
        assert point2d.points_equal(point2d.rotate(point, math.pi*2), point)
        assert point2d.points_equal(point2d.rotate(point, -math.pi*2), point)


def test_rotate90():
    for point in ALL:
        assert point2d.points_equal(point2d.rotate90(point),
                                    point2d.rotate(point, math.pi/2))

        assert point2d.points_equal(
            point2d.rotate90(point2d.rotate90(
                point2d.rotate90(point2d.rotate90(point)))), point)

        for center in ALL:
            assert point2d.points_equal(point2d.rotate90(point, center=center),
                                        point2d.rotate(point, math.pi/2,
                                                       center=center))

            assert point2d.points_equal(
                point2d.rotate90(
                    point2d.rotate90(
                        point2d.rotate90(
                            point2d.rotate90(point, center=center),
                            center=center),
                        center=center),
                    center=center),
                point)


def test_rotate180():
    for point in ALL:
        assert point2d.points_equal(point2d.rotate180(point),
                                    point2d.rotate(point, math.pi))

        assert point2d.points_equal(
            point2d.rotate180(point2d.rotate180(point)), point)

        for center in ALL:
            assert point2d.points_equal(point2d.rotate180(point,
                                                          center=center),
                                        point2d.rotate(point, math.pi,
                                                       center=center))

            assert point2d.points_equal(
                point2d.rotate180(
                    point2d.rotate180(point, center=center),
                    center=center),
                point)


def test_rotate270():
    for point in ALL:
        assert point2d.points_equal(point2d.rotate270(point),
                                    point2d.rotate(point, -math.pi/2))

        assert point2d.points_equal(
            point2d.rotate270(point2d.rotate270(
                point2d.rotate270(point2d.rotate270(point)))), point)

        for center in ALL:
            assert point2d.points_equal(point2d.rotate270(point,
                                                          center=center),
                                        point2d.rotate(point, -math.pi/2,
                                                       center=center))

            assert point2d.points_equal(
                point2d.rotate270(
                    point2d.rotate270(
                        point2d.rotate270(
                            point2d.rotate270(point, center=center),
                            center=center),
                        center=center),
                    center=center),
                point)


#
# Main
######
if __name__ == '__main__':
    # Run all test_...() functions (useful if pytest miss)
    for s in sorted(dir()):
        if s[:5] == 'test_':
            print(s, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[s]()
            print(file=sys.stderr)
