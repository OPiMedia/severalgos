# -*- coding: utf-8 -*-

"""
graph.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 3, 2020
"""

import bisect

from typing import Dict, Iterable, Tuple


#
# Class
#######

class Graph:
    """
    Simple implementation of undirected graph.

    Use to try Dijkstra's Shortest Path First algorithm.
    """

    def __init__(self, nb_node: int, edges: Iterable[Iterable[int]]) -> None:
        def get_edge(edge: Iterable[int]) -> Tuple[int, int, int]:
            edge = tuple(edge)

            assert 2 <= len(edge) <= 3, edge
            assert 0 <= edge[0] < nb_node
            assert 0 <= edge[1] < nb_node

            return (min(edge[:2]), max(edge[:2]), (sum(edge[:2]) % 9
                                                   if len(edge) < 3
                                                   else edge[2]))

        self._nb_node = nb_node

        self._seq_edges: Tuple[Tuple[int, int, int], ...] = \
            tuple(sorted(get_edge(edge) for edge in edges))

        self._nb_edge = len(self._seq_edges)

        self._edges: Tuple[Dict[int, int]] = tuple(dict()  # type: ignore
                                                   for _ in range(nb_node))
        for edge in self._seq_edges:
            a, b, weight = edge

            assert 0 <= a < b < nb_node
            assert b not in self._edges[a]
            assert a not in self._edges[b]

            self._edges[a][b] = weight
            self._edges[b][a] = weight

        self._distances = [0] * nb_node
        self.compute_distance_dijkstra()

    def __str__(self) -> str:
        return '\n'.join('{} - {} [{}]'.format(a, b, weight)
                         for a, b, weight in self.edges())

    def compute_distance_dijkstra(self, source: int = 0) -> None:
        """
        Set self._distances[] with distance to node source.

        Adapted from
        "Algorithms Unlocked" (Thomas H. Cormen, The MIT Press, 2013) p.98
        """
        assert 0 <= source < self.nb_node()

        for node in self.nodes():
            self._distances[node] = None  # type: ignore

        self._distances[source] = 0

        priority = [(0, source)]

        remains = set(self.nodes())
        remains.remove(source)

        while priority or remains:
            node = (priority.pop(0)[1] if priority
                    else remains.pop())

            for neighbour in self.neighbours(node):
                new_dist = self.distance(node) + self.weight(node, neighbour)
                if ((self._distances[neighbour] is None) or
                        (self._distances[neighbour] > new_dist)):
                    keep = self._distances[neighbour]
                    self._distances[neighbour] = new_dist

                    if keep is None:
                        remains.remove(neighbour)
                    else:
                        del priority[
                            bisect.bisect_left(priority, (keep, neighbour))]

                    item = (self._distances[neighbour], neighbour)
                    i = bisect.bisect_left(priority, item)
                    priority.insert(i, item)

    def distance(self, node: int) -> int:
        assert 0 <= node < self.nb_node()

        assert isinstance(self._distances[node], int)

        return self._distances[node]

    def dot(self) -> str:
        seq = ["""graph "Graph" {
  node [shape=circle]
"""]

        for node in self.nodes():
            seq.append('{} [label="{}"];'
                       .format(node,
                               r'{}\n{}'.format(node, self.distance(node))))

        max_weight = max(weight for _, _, weight in self.edges())

        for a, b, weight in self.edges():
            style = (' [penwidth={},label={}]'.format(
                1 + weight * 4 / (max_weight - 1), weight)
                     if weight != 0
                     else ' [color=gray,penwidth=0.5,label={}]'.format(weight))
            seq.append('{} -- {}{};'.format(a, b, style))

        seq.append('}')

        return '\n'.join(seq)

    def edges(self) -> Tuple[Tuple[int, int, int], ...]:
        return self._seq_edges

    def nb_edge(self) -> int:
        return self._nb_edge

    def nb_node(self) -> int:
        return self._nb_node

    def neighbours(self, node: int) -> Iterable[int]:
        assert 0 <= node < self.nb_node()

        return self._edges[node].keys()

    def nodes(self) -> Iterable[int]:
        return range(self.nb_node())

    def weight(self, a: int, b: int) -> int:
        assert 0 <= a < self.nb_node()
        assert 0 <= b < self.nb_node()

        return self._edges[a][b]

    def write_dot(self, filename: str = 'graph.dot') -> None:
        with open(filename, 'w') as fout:
            print(self.dot(), file=fout)
