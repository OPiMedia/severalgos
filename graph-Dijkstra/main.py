#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
main to run graph.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 3, 2020
"""

import graph


#
# Functions
###########


#
# Main
######
def main():
    """Run main work."""
    gr = graph.Graph(10, ((0, 1), (1, 2), (0, 2), (2, 4), (7, 9),
                          (1, 5), (2, 6), (5, 7), (1, 8), (8, 9),
                          (3, 1), (7, 0), (3, 9), (4, 7), (9, 5), (4, 5)))

    print(str(gr))

    for node in gr.nodes():
        print('{}: {{{}}}'
              .format(node, ','.join(map(str, gr.neighbours(node)))))

    gr.write_dot()


if __name__ == '__main__':
    main()
