/* -*- coding: latin-1 -*- */
/*
 * test_prime64constiterator.hpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cmath>

#include <algorithm>
#include <limits>
#include <utility>
#include <vector>

#include <cxxtest/TestSuite.h>  // NOLINT // CxxTest http://cxxtest.com/

#include "benchmark.hpp"
#include "primes/prime64constiterator.hpp"



#define SHOW_FUNC_NAME {                                  \
  std::cout << std::endl                                  \
            << "=== " << __func__ << " ===" << std::endl; \
  std::cout.flush();                                      \
}



std::chrono::steady_clock::time_point total_start;



class Test__prime64constiterator : public CxxTest::TestSuite {
 public:
  void test_start() {
    total_start = std::chrono::steady_clock::now();
  }


  void test__Prime64ConstIterator() {
    SHOW_FUNC_NAME;

    {
      primes::Prime64ConstIterator it;
      TS_ASSERT_EQUALS(*it, 2);
    }

    {
      primes::Prime64ConstIterator it(0);
      TS_ASSERT_EQUALS(*it, 2);
    }

    {
      primes::Prime64ConstIterator it(1);
      TS_ASSERT_EQUALS(*it, 3);
    }

    {
      primes::Prime64ConstIterator it(2);
      TS_ASSERT_EQUALS(*it, 5);
    }

    {
      primes::Prime64ConstIterator it(3);
      TS_ASSERT_EQUALS(*it, 7);
    }

    {
      primes::Prime64ConstIterator it(4);
      TS_ASSERT_EQUALS(*it, 11);
    }

    {
      primes::Prime64ConstIterator it(9);
      TS_ASSERT_EQUALS(*it, 29);
    }

    {
      primes::Prime64ConstIterator it(99);
      TS_ASSERT_EQUALS(*it, 541);
    }

    {
      primes::Prime64ConstIterator it(999);
      TS_ASSERT_EQUALS(*it, 7919);
    }

    {
      primes::Prime64ConstIterator it(9999);
      TS_ASSERT_EQUALS(*it, 104729);
    }

    {
      primes::Prime64ConstIterator it(99999);
      TS_ASSERT_EQUALS(*it, 1299709);
    }

    for (unsigned int i(0);
         i < std::max(1000u, 2*primes::table_primes_size()); ++i) {
      primes::Prime64ConstIterator it(i);

      TS_ASSERT_EQUALS(*it, primes::prime_ith(i));
    }
  }


  void test__cbegin() {
    SHOW_FUNC_NAME;

    // ???
  }


  void test__cend() {
    SHOW_FUNC_NAME;

    // ???
  }


  void test__operator_dec() {
    SHOW_FUNC_NAME;

    // ???
  }


  void test__operator_dec_postfix() {
    SHOW_FUNC_NAME;

    // ???
  }


  void test__operator_inc() {
    SHOW_FUNC_NAME;

    primes::Prime64ConstIterator it;

    for (unsigned int i(0); i < 1000; ++i) {
      TS_ASSERT_EQUALS(*it, primes::prime_ith(i));
      ++it;
    }

    for (unsigned int k(0); k < 100; ++k) {
      primes::Prime64ConstIterator it(k);

      for (unsigned int i(0); i < 100; ++i) {
        TS_ASSERT_EQUALS(*it, primes::prime_ith(k + i));
        ++it;
      }
    }
  }


  void test__operator_inc_postfix() {
    SHOW_FUNC_NAME;

    // ???
  }


  void test__to_ith() {
    SHOW_FUNC_NAME;

    // ???
  }


  void test_end() {
    std::cout
      << "--- Total: "
      << round<std::chrono::seconds>(std::chrono::steady_clock::now()
                                     - total_start).count()
      << "s ---" << std::endl;
  }
};
