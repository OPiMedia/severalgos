/* -*- coding: latin-1 -*- */
/** \mainpage
 * Module primes (January 7, 2025)
 *
 * Prime numbers sequence: http://oeis.org/A000040
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015, 2025 Olivier Pirson
 * http://www.opimedia.be/
 */

/** \file primes/primes.hpp
 * \brief Function definitions.
 */

#ifndef PRIMES_PRIMES_H_
#define PRIMES_PRIMES_H_

#include <cassert>
#include <cstdint>

#include <utility>
#include <vector>

#include "exceptions.hpp"



namespace primes {
  /* **************
   * Public types *
   ****************/
  /** \brief Type of modulo difference. */
  typedef unsigned int modulo_type;


  /** \brief Type of natural number on 32 bits. */
  typedef uint32_t nat32_type;


  /** \brief Type of natural number on 64 bits. */
  typedef uint64_t nat64_type;



  /* **********************
   * Functions prototypes *
   ************************/

  /** \brief
   * Return \f$\lfloor\sqrt{n}\rfloor\f$,
   * the square root of \a n rounded down.
   */
  inline
  nat64_type
  floor_sqrt(nat64_type n) noexcept;


  /** \brief
   * Return \f$a \sqcap b \f$,
   * the Great Common Divisor of \a a and \a b.
   */
  inline
  nat64_type
  gcd(nat64_type a, nat64_type b) noexcept;


  /** \brief
   * Return true if \a n is a composite number,\n
   * else return false.
   *
   * 0 and 1 are neither prime, neither composite.
   *
   * Composite numbers sequence: https://oeis.org/A002808
   *
   * Time O(n): 1 if n is 0, 1 or even,\n
   *            lg(table_primes_size()) if n <= last prime in the table,\n
   *            \f$\sqrt(n)\f$ else
   */
  inline
  bool
  is_composite(nat64_type n) noexcept;


  /** \brief
   * Return true if \f$a \perp b \f$ (\a a and \a b are coprime),
   * else return false.
   *
   * is_coprime(0, 0) return false.
   */
  inline
  bool
  is_coprime(nat64_type a, nat64_type b) noexcept;


  /** \brief
   * Return true if \a n is prime number,\n
   * else return false.
   *
   * 0 and 1 are neither prime, neither composite.
   *
   * Prime numbers sequence: http://oeis.org/A000040
   *
   * Time O(n): 1 if n is even,\n
   *            lg(table_primes_size()) if n <= last prime in the table,\n
   *            \f$\sqrt(n)\f$ else
   */
  bool
  is_prime(nat64_type n) noexcept;


  /** \brief
   * If n is prime,
   * then return i for n the ith prime number,
   * else raise an exception.
   *
   * See \a prime_to_ith().
   */
  unsigned int
  maybe_prime_to_ith(nat64_type n) throw(NotPrimeException);


  /** \brief
   * Return \f$base^{exp}\f$ modulo \f$2^{64}\f$.
   */
  nat64_type
  pow(nat64_type base, nat64_type exp) noexcept;


  /** \brief
   * Return ordered list of the primary \f$(p_i, \alpha_i)\f$
   * where \f$p_i\f$ are a prime number
   * and \f$\alpha_i\f$ is correspondent exponent
   * in the prime factorization of n.
   *
   * For example:\n
   * For n = 1, return an empty list.\n
   * For n = 2, return ((2, 1)).\n
   * For n = 250, return ((5, 2), (10, 1)) because \f$250 = 5^2 10^1\f$.
   *
   * \param n != 0
   */
  std::vector<std::pair<nat64_type, nat64_type>>
  primaries(nat64_type n) noexcept;


  /** \brief
   * Return the product \f$\prod_i p_i^{\alpha_i}\f$ modulo \f$2^{64}\f$.
   *
   * This is the inverse function of f \a primaries().
   *
   * \pre primaries must be
   *      a correct ordered list of primaries \f$(p_i, \alpha_i)\f$
   *      (in particular each \f$p_i\f$ must be a different prime number
   *       and each \f$\alpha_i\f$ must be != 0)
   */
  nat64_type
  primaries_prod(std::vector<std::pair<nat64_type, nat64_type>> primaries)
  noexcept;


  /** \brief
   * Return the ith prime number.
   *
   * For example:
   * For i =  0, return  2.\n
   * For i =  1, return  3.\n
   * For i =  2, return  5.\n
   * For i =  9, return 29.\n
   * For i = 10, return 31.
   *
   * \pre i such that the ith prime number \f$< 2^{64}\f$.
   *
   * The bigger prime number \f$< 2^{64}\f$
   * is \f$2^{64} - 59 = 18446744073709551557\f$.
   *
   * Time O(i): 1 if i < table_primes_size(),\n
   *            \f$n^{3/2}\f$ else
   *
   * Primes just less than a power of two:
   * http://primes.utm.edu/lists/2small/0bit.html
   *
   * Number of primes \f$\leq 2^n\f$:
   * https://oeis.org/A007053
   */
  nat64_type
  prime_ith(unsigned int i) noexcept;


  /** \brief
   * Return i for p the ith prime number.
   *
   * This is the inverse function of f \a prime_ith().
   *
   * For example:
   * For i =  2, return  0.\n
   * For i =  3, return  1.\n
   * For i =  5, return  2.\n
   * For i = 29, return  9.\n
   * For i = 31, return 10.
   *
   * \pre p must be a prime number (see \a maybe_prime_to_ith())
   */
  unsigned int
  prime_to_ith(nat64_type p) noexcept;


  /** \brief
   * Return the ith primorial modulo \f$2^{64}\f$.
   *
   * The primorial is the product of the first prime numbers.
   *
   * Primorial numbers sequence: http://oeis.org/A002110
   */
  nat64_type
  primorial(unsigned int i) noexcept;


  /** \brief
   * Return n*n modulo \f$2^{64}\f$.
   */
  inline
  nat64_type
  sqr(nat64_type n) noexcept;


  /** \brief
   * Return the modulo used by \p table_modulo_diff().
   */
  inline
  modulo_type
  table_modulo() noexcept;


  /** \brief
   * Return the difference between two remains modulo \a table_modulo()
   * that must be considered to walk potential prime numbers.
   */
  inline
  modulo_type
  table_modulo_diff(unsigned int i) noexcept;


  /** \brief
   * Return the size of the table used by \p table_modulo_diff().
   */
  inline
  unsigned int
  table_modulo_diffs_size() noexcept;


  /** \brief
   * Return the ith prime number (from a precalculated table).
   *
   * \param i < table_primes_size()
   *
   * Time O(i): 1
   */
  inline
  nat32_type
  table_prime_ith(unsigned int i) noexcept;


  /** \brief
   * Return the last prime number in the precalculated table.
   *
   * Time O(i): 1
   */
  inline
  nat32_type
  table_prime_last() noexcept;


  /** \brief
   * Return a const iterator to the first prime number in the table.
   */
  inline
  std::vector<nat32_type>::const_iterator
  table_primes_cbegin() noexcept;


  /** \brief
   * Return a const iterator to the after last prime number in the table.
   */
  inline
  std::vector<nat32_type>::const_iterator
  table_primes_cend() noexcept;


  /** \brief
   * Return the size of the table used by \p table_prime_ith().
   */
  inline
  unsigned int
  table_primes_size() noexcept;


  /** \brief
   * Shrink precalculated tables to their minimum size.
   */
  void
  tables_shrink_to_min() noexcept;



  /* **************************
   * Private global constants *
   ****************************/
  /** \brief
   * \f$2^{32} - 5 = 4294967291\f$,
   * the bigger prime number \f$< 2^{32}\f$
   */
  extern const nat32_type __last_nat32_prime;


  /** \brief
   * \f$2^{64} - 59 = 18446744073709551557\f$,
   * the bigger prime number \f$< 2^{64}\f$
   */
  extern const nat64_type __last_nat64_prime;



  /* **************************
   * Private global variables *
   ****************************/
  /** \brief
   * Modulo used by the pecalculated table \a __modulo_diffs.
   *
   * \pre Must be a primorial number >= 2.
   */
  extern modulo_type __modulo;


  /** \brief
   * Precalculated table of difference of modulos
   * which should be considered to walk potential prime numbers.
   */
  extern std::vector<modulo_type> __modulo_diffs;


  /** \brief Precalculated table of first prime numbers.
   *
   * \pre The last item must be equal a multiple \a __modulo add 1.
   */
  extern std::vector<nat32_type> __primes;

}  // namespace primes


#include "primes__inline.hpp"


#endif  // PRIMES_PRIMES_H_
