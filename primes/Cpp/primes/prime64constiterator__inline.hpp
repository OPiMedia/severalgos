/* -*- coding: latin-1 -*- */
/** \file primes/prime64constiterator__inline.hpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

namespace primes {
  /* ****************************
   * Class Prime64ConstIterator *
   ******************************/

  /* *************************
   * Public static functions *
   ***************************/
  inline
  Prime64ConstIterator
  Prime64ConstIterator::cbegin() noexcept {
    return Prime64ConstIterator();
  }


  inline
  Prime64ConstIterator
  Prime64ConstIterator::cend() noexcept {
    return _END_CONST_ITERATOR;
  }



  /* ******************
   * Public operators *
   ********************/
  inline
  Prime64ConstIterator::const_nat64_type&
  Prime64ConstIterator::operator*() const noexcept {
    assert(prime_ != 0);

    return prime_;
  }


  inline
  Prime64ConstIterator::const_iterator
  Prime64ConstIterator::operator++(int) noexcept {  // NOLINT
    assert(prime_ != 0);

    const_iterator keep(*this);

    ++(*this);

    return keep;
  }


  inline
  Prime64ConstIterator::const_iterator
  Prime64ConstIterator::operator--(int) noexcept {  // NOLINT
    assert(ith_ != 0);

    const_iterator keep(*this);

    --(*this);

    return keep;
  }



  /* ***************
   * Public method *
   *****************/
  /** \brief
   * Return i for p the ith prime number.
   */
  inline
  unsigned int
  Prime64ConstIterator::to_ith() noexcept {
    return ith_;
  }

}  // namespace primes
