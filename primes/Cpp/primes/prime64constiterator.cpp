/* -*- coding: latin-1 -*- */
/** \file primes/prime64constiterator.cpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <limits>

#include "prime64constiterator.hpp"



namespace primes {
  /* *************************
   * Private global constant *
   ***************************/
  const Prime64ConstIterator
  _END_CONST_ITERATOR(std::numeric_limits<unsigned int>::max());



  /* ********************
   * Public constructor *
   **********************/
  Prime64ConstIterator::Prime64ConstIterator(unsigned int i) noexcept
  : ith_(i), prime_(0) {
    if (i < std::numeric_limits<unsigned int>::max()) {
      prime_ = prime_ith(i);
    }
  }



  /* ******************
   * Public operators *
   ********************/
  Prime64ConstIterator::const_iterator&
  Prime64ConstIterator::operator++() noexcept {
    assert(prime_ != 0);

    ++ith_;

    if (ith_ < table_primes_size()) {          // in table
      prime_ = table_prime_ith(ith_);
    } else if (prime_ < __last_nat64_prime) {  // outside table but in 64 bits
      // Search the current modulo
      modulo_type mod(prime_ % table_modulo());
      unsigned int mod_i(0);

      while (mod > 1) {
        mod -= table_modulo_diff(mod_i);
        ++mod_i;
      }

      // Search next prime
      do {
        prime_ += table_modulo_diff(mod_i);
        ++mod_i;
        if (mod_i >= table_modulo_diffs_size()) {
          mod_i = 0;
        }
      } while (!is_prime(prime_));
    } else {                                   // too big
      prime_ = 0;
    }

    return *this;
  }


  Prime64ConstIterator::const_iterator&
  Prime64ConstIterator::operator--() noexcept {
    assert(ith_ != 0);

    --ith_;

    if (ith_ < table_primes_size()) {  // in table
      prime_ = table_prime_ith(ith_);
    } else {                           // outside table
      // Search the current modulo
      modulo_type mod(prime_ % table_modulo());
      unsigned int mod_i(0);

      while (mod > 1) {
        mod -= table_modulo_diff(mod_i);
        ++mod_i;
      }

      // Search next prime
      do {
        prime_ -= table_modulo_diff(mod_i);
        if (mod_i != 0) {
          mod_i = table_modulo_diffs_size() - 1;
        } else {
          --mod_i;
        }
      } while (!is_prime(prime_));
    }

    return *this;
  }

}  // namespace primes
