/* -*- coding: latin-1 -*- */
/** \file primes/primes_inline.hpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>

#include <algorithm>
#include <vector>



namespace primes {
  /* ******************
   * Public functions *
   ********************/
  inline
  nat64_type
  floor_sqrt(nat64_type n) noexcept {
    return std::sqrt(static_cast<long double>(n));
  }


  inline
  nat64_type
  gcd(nat64_type a, nat64_type b) noexcept {
    return std::__gcd(a, b);
  }


  inline
  bool
  is_composite(nat64_type n) noexcept {
    return (n >= 2) && !is_prime(n);
  }


  inline
  bool
  is_coprime(nat64_type a, nat64_type b) noexcept {
    return gcd(a, b) == 1;
  }


  inline
  nat64_type
  sqr(nat64_type n) noexcept {
    return n*n;
  }


  inline
  modulo_type
  table_modulo() noexcept {
    return __modulo;
  }


  inline
  modulo_type
  table_modulo_diff(unsigned int i) noexcept {
    assert(i < table_modulo_diffs_size());

    return __modulo_diffs[i];
  }


  inline
  unsigned int
  table_modulo_diffs_size() noexcept {
    return __modulo_diffs.size();
  }


  inline
  nat32_type
  table_prime_ith(unsigned int i) noexcept {
    assert(i < table_primes_size());

    return __primes[i];
  }


  inline
  nat32_type
  table_prime_last() noexcept {
    return __primes[table_primes_size() - 1];
  }


  inline
  std::vector<nat32_type>::const_iterator
  table_primes_cbegin() noexcept {
    return __primes.cbegin();
  }


  inline
  std::vector<nat32_type>::const_iterator
  table_primes_cend() noexcept {
    return __primes.cend();
  }


  inline
  unsigned int
  table_primes_size() noexcept {
    return __primes.size();
  }

}  // namespace primes
