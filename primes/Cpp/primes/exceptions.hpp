/* -*- coding: latin-1 -*- */
/** \file primes/exceptions.hpp
 * \brief Function definitions.
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PRIMES_EXCEPTIONS_H_
#define PRIMES_EXCEPTIONS_H_

#include <stdexcept>
#include <string>



namespace primes {
/* *******************
 * Classes exception *
 *********************/

/** \brief
 * General exception.
 */
class PrimesException : public std::runtime_error {
 public:
  explicit PrimesException(const std::string& message);
};



/** \brief
 * 
 * Exception raised when a non-prime number is used
 * and a prime number is required.
 */
class NotPrimeException : public PrimesException {
 public:
  explicit NotPrimeException(const std::string& message);
};
}  // namespace primes


#endif  // PRIMES_EXCEPTIONS_H_
