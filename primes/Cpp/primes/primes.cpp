/* -*- coding: latin-1 -*- */
/** \file primes/primes.cpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cmath>

#include <algorithm>
#include <string>
#include <utility>
#include <vector>

#include "primes.hpp"



#if defined(__MINGW32__) || defined(__MINGW64__)
#include <sstream>
/*
  Workaround to MinGW bug:
  http://stackoverflow.com/a/24008447/1333666
*/
namespace std {
  template <typename T>
  std::string
  to_string(T value) {
    std::ostringstream os;

    os << value;

    return os.str();
  }
}  // namespace std
#endif



namespace primes {

  /* **************************
   * Private global constants *
   ****************************/
  const nat32_type __last_nat32_prime(4294967291u);


  const nat64_type __last_nat64_prime(18446744073709551557ul);



  /* **************************
   * Private global variables *
   ****************************/
  modulo_type __modulo(6);

  std::vector<modulo_type> __modulo_diffs{4, 2};

  std::vector<nat32_type> __primes{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31};



  /* ***********
   * Functions *
   *************/
  bool
  is_prime(nat64_type n) noexcept {
    if ((n & 1) == 0) {  // n is even
      return (n == 2);
    } else if (n <= table_prime_last()) {
                         // n maybe in table
      return std::binary_search(table_primes_cbegin() + 1,
                                table_primes_cend(), n);
    } else {             // n outside table
      const nat64_type n_sqrt(floor_sqrt(n));

      // Try divides by primes in table
      for (auto it_p(primes::table_primes_cbegin() + 1);
           (it_p != primes::table_primes_cend()) && (*it_p <= n_sqrt); ++it_p) {
        if ((n % *it_p) == 0) {
          return false;
        }
      }

      // Try divides by some integers outside table
      nat64_type d(table_prime_last());

      assert(d % table_modulo() == 1);

      unsigned int mod_i(0);

      while (d <= n_sqrt) {
        d += table_modulo_diff(mod_i);
        ++mod_i;
        if (mod_i >= table_modulo_diffs_size()) {
          mod_i = 0;
        }

        if (n % d == 0) {
          return false;
        }
      }

      // n is prime
      return true;
    }
  }


  unsigned int
  maybe_prime_to_ith(nat64_type n) throw(NotPrimeException) {
    if ((n & 1) == 0) {                    // n is even
      if (n == 2) {
        return 0;
      }
    } else if (n <= table_prime_last()) {  // n in table
      auto it(std::lower_bound(table_primes_cbegin() + 1,
                               table_primes_cend(), n));

      if ((it != table_primes_cend()) && (*it == n)) {
        return it - table_primes_cbegin();
      }
    } else {                               // n outside table
      if (is_prime(n)) {
        unsigned int i(table_primes_size());

        nat64_type k(table_prime_last());

        assert(k % table_modulo() == 1);

        unsigned int mod_i(0);

        while (true) {
          k += table_modulo_diff(mod_i);
          ++mod_i;
          if (mod_i >= table_modulo_diffs_size()) {
            mod_i = 0;
          }

          if (is_prime(k)) {
            if (k >= n) {
              if (k > n) {
                break;
              } else {
                return i;
              }
            }
            ++i;
          }
        }
      }
    }

    throw NotPrimeException(std::string(__func__) + "(): "
                            + std::to_string(n) + " is NOT prime!");
  }


  nat64_type
  pow(nat64_type base, nat64_type exp) noexcept {
    return (exp != 0
            ? (exp & 1
               ? sqr(pow(base, exp/2))*base  // exp is odd
               : sqr(pow(base, exp/2)))      // exp is even
            : 1);
  }


  std::vector<std::pair<nat64_type, nat64_type>>
  primaries(nat64_type n) noexcept {
    assert(n != 0);

    std::vector<std::pair<nat64_type, nat64_type>> v;

    nat64_type nb(0);


    // Try divides by power of 2
    while ((n & 1) == 0) {
      ++nb;
      n >>= 1;
    }
    if (nb != 0) {
      v.push_back(std::make_pair(2, nb));
    }


    // Try divides by power of primes >= 3 in table
    for (auto it_p(primes::table_primes_cbegin() + 1);
         it_p != primes::table_primes_cend(); ++it_p) {
      if (n % *it_p == 0) {
        n /= *it_p;
        nb = 1;
        while (n % *it_p == 0) {
          ++nb;
          n /= *it_p;
        }

        v.push_back(std::make_pair(*it_p, nb));

        if (n == 1) {
          break;
        }
      }
    }


    // Try divides by power of some integers outside table
    nat64_type d(table_prime_last());

    assert(d % table_modulo() == 1);

    unsigned int mod_i(0);

    while (d <= n) {
      d += table_modulo_diff(mod_i);
      ++mod_i;
      if (mod_i >= table_modulo_diffs_size()) {
        mod_i = 0;
      }

      if (n % d == 0) {  // d is a prime divisor of n
        n /= d;
        nb = 1;
        while (n % d == 0) {
          ++nb;
          n /= d;
        }

        v.push_back(std::make_pair(d, nb));

        if (n == 1) {
          break;
        }
      }
    }

    assert(n != 0);

    return v;
  }


  nat64_type
  primaries_prod(std::vector<std::pair<nat64_type, nat64_type>> primaries)
  noexcept {
    nat64_type n(1);

    auto it(primaries.cbegin());

    if ((it != primaries.cend()) && (it->first == 2)) {
      n <<= it->second;
      ++it;
    }

    for ( ; it != primaries.cend(); ++it) {
      n *= primes::pow(it->first, it->second);
    }

    return n;
  }


  nat64_type
  prime_ith(unsigned int i) noexcept {
    if (i < table_primes_size()) {  // in table
      return table_prime_ith(i);
    } else {                        // outside table
      i -= table_primes_size();

      nat64_type n(table_prime_last());

      assert(n % table_modulo() == 1);

      unsigned int mod_i(0);

      while (true) {
        n += table_modulo_diff(mod_i);
        ++mod_i;
        if (mod_i >= table_modulo_diffs_size()) {
          mod_i = 0;
        }

        if (is_prime(n)) {
          if (i == 0) {
            return n;
          }
          --i;

          assert(n < __last_nat64_prime);
        }
      }
    }
  }


  unsigned int
  prime_to_ith(nat64_type p) noexcept {
    if (p <= table_prime_last()) {  // p in table
      auto it(std::lower_bound(table_primes_cbegin(), table_primes_cend(), p));

      assert((it != table_primes_cend()) && (*it == p));

      return it - table_primes_cbegin();
    } else {                        // p outside table
      unsigned int i(table_primes_size());

      nat64_type n(table_prime_last());

      assert(n % table_modulo() == 1);

      unsigned int mod_i(0);

      while (true) {
        n += table_modulo_diff(mod_i);
        ++mod_i;
        if (mod_i >= table_modulo_diffs_size()) {
          mod_i = 0;
        }

        if (is_prime(n)) {
          if (n == p) {
            return i;
          }
          ++i;
        }
      }
    }
  }


  nat64_type
  primorial(unsigned int i) noexcept {
    nat64_type p(1);

    if (i < table_primes_size()) {
      for (unsigned int j(0); j < i; ++j) {
        p *= table_prime_ith(j);
      }
    } else {
      for (unsigned int j(0) ; j < table_primes_size(); ++j) {
        p *= table_prime_ith(j);
      }

      nat64_type n(table_prime_last());
      unsigned int mod_i(0);

      i -= table_primes_size();
      while (i > 0) {
        n += table_modulo_diff(mod_i);
        if (is_prime(n)) {
          p *= n;
          --i;
        }

        ++mod_i;
        if (mod_i >= table_modulo_diffs_size()) {
          mod_i = 0;
        }
      }
    }

    return p;
  }


  void
  tables_shrink_to_min() noexcept {
    if (table_modulo() > 2) {
      // {2}
      __modulo_diffs.resize(1);
      __modulo_diffs.shrink_to_fit();
      __modulo = 2;
      __modulo_diffs[0] = 2;
    }

    if (table_primes_size() > 2) {
      // {2, 3}
      __primes.resize(2);
      __primes.shrink_to_fit();
    }
  }

}  // namespace primes
