/* -*- coding: latin-1 -*- */
/** \file exceptions.cpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "exceptions.hpp"

#include <string>


namespace primes {
  /* *******************
   * Classes exception *
   *********************/
  PrimesException::PrimesException(const std::string& message)
    : std::runtime_error(message) {
  }


  NotPrimeException::NotPrimeException(const std::string& message)
    : PrimesException(message) {
  }
}  // namespace primes
