/* -*- coding: latin-1 -*- */
/** \file primes/prime64constiterator.hpp
 * \brief Function definitions.
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PRIMES_PRIME64CONSTITERATOR_H_
#define PRIMES_PRIME64CONSTITERATOR_H_

#include <cassert>
#include <cstdint>

#include <utility>
#include <vector>

#include "primes.hpp"



namespace primes {
 /* ***************************
  * Class Prime64ConstIterator *
  *****************************/
class Prime64ConstIterator {
 public:
  /* **************
   * Public types *
   ****************/
  /** \brief Type of iterator. */
  typedef Prime64ConstIterator const_iterator;


  /** \brief Type of natural number on 64 bits. */
  typedef uint64_t nat64_type;


  /** \brief Type of const natural number on 64 bits. */
  typedef const uint64_t const_nat64_type;



  /* *************************
   * Public static functions *
   ***************************/
  /** \brief
   * Return a const iterator to the first prime number.
   */
  static inline
  Prime64ConstIterator
  cbegin() noexcept;


  /** \brief
   * Return a const iterator to the after last prime number.
   */
  static inline
  Prime64ConstIterator
  cend() noexcept;



  /* *********************
   * Public constructor *
   ***********************/
  /** \ brief
   * Construct an iterator to the ith prime number.
   *
   * \pre i such that the ith prime number \f$< 2^{64}\f$.
   *
   * In fact, if i == std::numeric_limits<unsigned int>::max(),
   * then create a end iterator.
   */
  explicit Prime64ConstIterator(unsigned int i = 0) noexcept;



  /* ******************
   * Public operators *
   ********************/
  /** \Brief
   * Return a reference on current pointed item.
   *
   * \pre Must be != cend()
   */
  inline
  const_nat64_type&
  operator*() const noexcept;


  /** \Brief
   * **Prefix** move to the next prime number.
   *
   * \pre Must be != cend()
   */
  const_iterator&
  operator++() noexcept;


  /** \Brief
   * **Postfix** move to the next prime number.
   *
   * \pre Must be != cend()
   */
  inline
  const_iterator
  operator++(int) noexcept;


  /** \Brief
   * **Prefix** move to the previous prime number.
   *
   * \pre Must be != cbegin()
   */
  const_iterator&
  operator--() noexcept;


  /** \Brief
   * **Postfix** move to the previous prime number.
   *
   * \pre Must be != cbegin()
   */
  inline
  const_iterator
  operator--(int) noexcept;



  /* ***************
   * Public method *
   *****************/
  /** \brief
   * Return i for p the ith prime number.
   */
  inline
  unsigned int
  to_ith() noexcept;



 private:
  /* *******************
   * Private variables *
   *********************/
  /** \brief
   * The i such that \a prime_ is the ith prime number (if prime_ != 0).
   */
  unsigned int ith_;


  /** \brief
   * The current prime number
   * or 0 to indicate the end iterator.
   */
  nat64_type prime_;
};



/* *************************
 * Private global constant *
 ***************************/
extern const Prime64ConstIterator _END_CONST_ITERATOR;

}  // namespace primes


#include "prime64constiterator__inline.hpp"


#endif  // PRIMES_PRIMECONSTITERATOR_H_
