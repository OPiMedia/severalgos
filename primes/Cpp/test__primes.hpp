/* -*- coding: latin-1 -*- */
/*
 * test_primes.hpp
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cmath>

#include <algorithm>
#include <limits>
#include <utility>
#include <vector>

#include <cxxtest/TestSuite.h>  // NOLINT // CxxTest http://cxxtest.com/

#include "benchmark.hpp"
#include "primes/primes.hpp"



#define SHOW_FUNC_NAME {                                  \
  std::cout << std::endl                                  \
            << "=== " << __func__ << " ===" << std::endl; \
  std::cout.flush();                                      \
}



std::chrono::steady_clock::time_point total_start;



class Test__primes : public CxxTest::TestSuite {
 public:
  void test_start() {
    total_start = std::chrono::steady_clock::now();
  }


  void test____last_nat32_prime() {
    SHOW_FUNC_NAME;

    primes::nat32_type n(primes::__last_nat32_prime);

    n += 5;
    TS_ASSERT_EQUALS(n, 0);
  }


  void test____last_nat64_prime() {
    SHOW_FUNC_NAME;

    primes::nat64_type n(primes::__last_nat64_prime);

    n += 59;
    TS_ASSERT_EQUALS(n, 0);
  }


  void test____modulo() {
    SHOW_FUNC_NAME;

    TS_ASSERT_LESS_THAN_EQUALS(2, primes::__modulo);


    for (unsigned int i(0); i <= 16; ++i) {
      if (primes::__modulo == primes::primorial(i)) {
        return;
      }
    }
    TS_ASSERT(false);
  }


  void test____primes() {
    SHOW_FUNC_NAME;

    TS_ASSERT_LESS_THAN_EQUALS(2, primes::__primes.size());
    TS_ASSERT_LESS_THAN_EQUALS(primes::__primes.size(),
                               primes::table_primes_size());

    TS_ASSERT_EQUALS(primes::__primes[0], 2);
    TS_ASSERT_EQUALS(primes::__primes[1], 3);

    TS_ASSERT_EQUALS(primes::__primes[primes::__primes.size() - 1]
                     % primes::table_modulo(),
                     1);

    for (unsigned int i(1); i < primes::__primes.size() - 1; ++i) {
      const unsigned int p(primes::__primes[i]);
      const unsigned int q(primes::__primes[i + 1]);

      // p and q are odds
      TS_ASSERT_DIFFERS(p % 2, 0);
      TS_ASSERT_DIFFERS(q % 2, 0);

      // Strictly increasing table
      TS_ASSERT_LESS_THAN(p, q);

      // p and q are primes
      for (unsigned int d(p + 2); d < p; d += 2) {
        TS_ASSERT_DIFFERS(p % d, 0);
        TS_ASSERT_DIFFERS(q % d, 0);
      }

      // Odd between p and q are composites
      for (unsigned int n(p + 2); n < q; n += 2) {
        bool composite(false);
        unsigned int n_sqrt(primes::floor_sqrt(n));

        for (unsigned int d(3); d <= n_sqrt; d += 2) {
          if (n % d == 0) {
            composite = true;

            break;
          }
        }

        TS_ASSERT(composite);
      }
    }
  }


  void test____modulo_diffs() {
    SHOW_FUNC_NAME;

    TS_ASSERT_LESS_THAN_EQUALS(2, primes::__modulo);

    TS_ASSERT_LESS_THAN_EQUALS(1, primes::__modulo_diffs.size());
    TS_ASSERT_LESS_THAN_EQUALS(primes::__modulo_diffs.size(),
                               primes::table_modulo_diffs_size());

    if (primes::__modulo_diffs.size() == 1) {
      TS_ASSERT_EQUALS(primes::__modulo_diffs[0], 2);
    } else if (primes::__modulo_diffs.size() == 2) {
      TS_ASSERT_EQUALS(primes::__modulo_diffs[0], 4);
      TS_ASSERT_EQUALS(primes::__modulo_diffs[1], 2);
    }

    primes::modulo_type sum(0);

    for (unsigned int i(0); i < primes::__modulo_diffs.size(); ++i) {
      TS_ASSERT_LESS_THAN(0, primes::__modulo_diffs[i]);
      TS_ASSERT_LESS_THAN_EQUALS(primes::__modulo_diffs[i], primes::__modulo);
      sum += primes::__modulo_diffs[i];
    }
    TS_ASSERT_EQUALS(sum, primes::__modulo);
  }


  void test__floor_sqrt() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::floor_sqrt(0), 0);
    TS_ASSERT_EQUALS(primes::floor_sqrt(1), 1);
    TS_ASSERT_EQUALS(primes::floor_sqrt(2), 1);
    TS_ASSERT_EQUALS(primes::floor_sqrt(3), 1);
    TS_ASSERT_EQUALS(primes::floor_sqrt(4), 2);

    // sqrt(2^{2i}) = 2^i
    for (primes::nat64_type base(0); base < 100; ++base) {
      for (primes::nat64_type exp(0); exp < 10; exp += 2) {
        const primes::nat64_type sqr_half(primes::pow(base, exp/2));

        TS_ASSERT_EQUALS(primes::floor_sqrt(primes::pow(base, exp)),
                         sqr_half);
        if ((base > 1) && (exp > 1)) {
          for (unsigned int k(1); k < exp/2 - 1; ++k) {
            TS_ASSERT_EQUALS(primes::floor_sqrt(primes::pow(base, exp) - k),
                             sqr_half - 1);
            TS_ASSERT_EQUALS(primes::floor_sqrt(primes::pow(base, exp) + k),
                             sqr_half);
          }
        }
      }
    }

    // floor(sqrt(n))^2 <= n < (floor(sqrt(n)) + 1)^2
    for (primes::nat64_type n(0); n < 10000; ++n) {
      const primes::nat64_type sqrt(primes::floor_sqrt(n));

      TS_ASSERT_LESS_THAN_EQUALS(static_cast<long double>(sqrt),
                                 std::sqrt(static_cast<long double>(n)));
      TS_ASSERT_LESS_THAN(std::sqrt(static_cast<long double>(n)),
                          static_cast<long double>(sqrt + 1));

      TS_ASSERT_LESS_THAN_EQUALS(sqrt*sqrt, n);
      TS_ASSERT_LESS_THAN(n, (sqrt + 1)*(sqrt + 1));
    }

    for (unsigned int i(0); i < 64; ++i) {
      const primes::nat64_type k(static_cast<primes::nat64_type>(1) << i);

      for (primes::nat64_type n(k - 100000); n < k + 100000; ++n) {
        const primes::nat64_type sqrt(primes::floor_sqrt(n));

        TS_ASSERT_LESS_THAN_EQUALS(static_cast<long double>(sqrt),
                                   std::sqrt(static_cast<long double>(n)));
        TS_ASSERT_LESS_THAN(std::sqrt(static_cast<long double>(n)),
                            static_cast<long double>(sqrt + 1));

        TS_ASSERT_LESS_THAN_EQUALS(sqrt*sqrt, n);
        TS_ASSERT_LESS_THAN(n, (sqrt + 1)*(sqrt + 1));
      }
    }

    for (primes::nat64_type n(std::numeric_limits<primes::nat64_type>::max());
         n > std::numeric_limits<primes::nat64_type>::max() - 100000; --n) {
      const primes::nat64_type sqrt(primes::floor_sqrt(n));

      TS_ASSERT_LESS_THAN_EQUALS(static_cast<long double>(sqrt),
                                 std::sqrt(static_cast<long double>(n)));
      TS_ASSERT_LESS_THAN(std::sqrt(static_cast<long double>(n)),
                          static_cast<long double>(sqrt + 1));

      const primes::nat64_type sqr(sqrt*sqrt);
      const primes::nat64_type sqr_p1((sqrt + 1)*(sqrt + 1));

      TS_ASSERT_LESS_THAN_EQUALS(sqr, n);
      if (sqr_p1 > sqr) {
        TS_ASSERT_LESS_THAN(n, sqr_p1);
      }
    }
  }


  void test__gcd() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::gcd(0, 0), 0);
    TS_ASSERT_EQUALS(primes::gcd(1, 0), 1);
    TS_ASSERT_EQUALS(primes::gcd(0, 1), 1);

    TS_ASSERT_EQUALS(partition::gcd(7, 50), 1);
    TS_ASSERT_EQUALS(partition::gcd(21, 50), 1);

    TS_ASSERT_EQUALS(primes::gcd(15, 50), 5);
    TS_ASSERT_EQUALS(primes::gcd(30, 50), 10);

    for (unsigned int n(0); n < 1000; ++n) {
      TS_ASSERT_EQUALS(primes::gcd(n, 0), n);
      TS_ASSERT_EQUALS(primes::gcd(0, n), n);
    }

    for (unsigned int a(0); a < 1000; ++a) {
      for (unsigned int b(0); b < 1000; ++b) {
        TS_ASSERT_EQUALS(primes::gcd(a, b), primes::gcd(b, a));
      }
    }

    for (auto it_p(primes::table_primes_cbegin());
         it_p != primes::table_primes_cend(); ++it_p) {
      for (auto it_q(primes::table_primes_cbegin());
           it_q != primes::table_primes_cend(); ++it_q) {
        if (it_p == it_q) {
          TS_ASSERT_EQUALS(primes::gcd(*it_p, *it_q), *it_p);
        } else {
          TS_ASSERT_EQUALS(primes::gcd(*it_p, *it_q), 1);
        }
      }
    }
  }


  void test__is_composite() {
    SHOW_FUNC_NAME;

    TS_ASSERT(!primes::is_composite(0));
    TS_ASSERT(!primes::is_composite(1));

    TS_ASSERT(primes::is_composite(4));
    TS_ASSERT(primes::is_composite(6));
    TS_ASSERT(primes::is_composite(8));

    TS_ASSERT(primes::is_composite(15));
    TS_ASSERT(primes::is_composite(63));
    TS_ASSERT(primes::is_composite(255));
    TS_ASSERT(primes::is_composite(511));
    TS_ASSERT(primes::is_composite(1023));
    TS_ASSERT(primes::is_composite(2047));
    TS_ASSERT(primes::is_composite(4095));
    TS_ASSERT(primes::is_composite(16383));

    TS_ASSERT(!primes::is_composite(2));
    TS_ASSERT(!primes::is_composite(3));
    TS_ASSERT(!primes::is_composite(5));
    TS_ASSERT(!primes::is_composite(7));
    TS_ASSERT(!primes::is_composite(11));

    TS_ASSERT(!primes::is_composite(31));
    TS_ASSERT(!primes::is_composite(127));
    TS_ASSERT(!primes::is_composite(8191));

    for (auto it_p(primes::table_primes_cbegin());
         it_p != primes::table_primes_cend(); ++it_p) {
      TS_ASSERT(!primes::is_composite(*it_p));
    }
  }


  void test__is_coprime() {
    SHOW_FUNC_NAME;


    TS_ASSERT(!primes::is_coprime(0, 0));
    TS_ASSERT(primes::is_coprime(1, 0));
    TS_ASSERT(primes::is_coprime(0, 1));

    TS_ASSERT(!primes::is_coprime(15, 50));
    TS_ASSERT(!primes::is_coprime(30, 50));

    for (unsigned int n(2); n < 1000; ++n) {
      TS_ASSERT(!primes::is_coprime(n, 0));
      TS_ASSERT(!primes::is_coprime(0, n));
    }

    for (unsigned int a(0); a < 1000; ++a) {
      for (unsigned int b(0); b < 1000; ++b) {
        TS_ASSERT_EQUALS(primes::is_coprime(a, b), primes::is_coprime(b, a));
      }
    }

    for (auto it_p(primes::table_primes_cbegin());
         it_p != primes::table_primes_cend(); ++it_p) {
      for (auto it_q(primes::table_primes_cbegin());
           it_q != primes::table_primes_cend(); ++it_q) {
        if (it_p == it_q) {
          TS_ASSERT(!primes::is_coprime(*it_p, *it_q));
        } else {
          TS_ASSERT(primes::is_coprime(*it_p, *it_q));
        }
      }
    }
  }


  void test__is_prime() {
    SHOW_FUNC_NAME;

    TS_ASSERT(primes::is_prime(2));
    TS_ASSERT(primes::is_prime(3));
    TS_ASSERT(primes::is_prime(5));
    TS_ASSERT(primes::is_prime(7));
    TS_ASSERT(primes::is_prime(11));

    TS_ASSERT(primes::is_prime(31));
    TS_ASSERT(primes::is_prime(127));
    TS_ASSERT(primes::is_prime(8191));

    TS_ASSERT(!primes::is_prime(0));
    TS_ASSERT(!primes::is_prime(1));
    TS_ASSERT(!primes::is_prime(4));
    TS_ASSERT(!primes::is_prime(6));
    TS_ASSERT(!primes::is_prime(8));

    TS_ASSERT(!primes::is_prime(15));
    TS_ASSERT(!primes::is_prime(63));
    TS_ASSERT(!primes::is_prime(255));
    TS_ASSERT(!primes::is_prime(511));
    TS_ASSERT(!primes::is_prime(1023));
    TS_ASSERT(!primes::is_prime(2047));
    TS_ASSERT(!primes::is_prime(4095));
    TS_ASSERT(!primes::is_prime(16383));

    for (auto it_p(primes::table_primes_cbegin());
         it_p != primes::table_primes_cend(); ++it_p) {
      TS_ASSERT(primes::is_prime(*it_p));
    }
  }


  void test__maybe_prime_to_ith() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(2), 0);
    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(3), 1);
    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(5), 2);
    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(7), 3);
    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(11), 4);

    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(      29),      9);  // NOLINT
    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(     541),     99);  // NOLINT
    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(    7919),    999);  // NOLINT
    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(  104729),   9999);  // NOLINT
    TS_ASSERT_EQUALS(primes::maybe_prime_to_ith( 1299709),  99999);  // NOLINT
    // TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(15485863), 999999);  // NOLINT

    for (unsigned int i(0); i < primes::table_primes_size(); ++i) {
      TS_ASSERT_EQUALS(primes::maybe_prime_to_ith(primes::table_prime_ith(i)),
                       i);
    }

    try {
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(0),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(1),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(4),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(6),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(8),
                       primes::NotPrimeException);

      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(10),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(100),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(1000),
                       primes::NotPrimeException);

      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(255),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(511),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(1023),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(2047),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(4095),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(65535),
                       primes::NotPrimeException);
      TS_ASSERT_THROWS(primes::maybe_prime_to_ith(4294967295u),
                       primes::NotPrimeException);
    } catch (const std::exception& e) {
    }
  }


  void test__pow() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::pow(0, 0), 1);
    TS_ASSERT_EQUALS(primes::pow(0, 1), 0);
    TS_ASSERT_EQUALS(primes::pow(0, 2), 0);

    TS_ASSERT_EQUALS(primes::pow(1, 0), 1);
    TS_ASSERT_EQUALS(primes::pow(1, 1), 1);
    TS_ASSERT_EQUALS(primes::pow(1, 2), 1);

    TS_ASSERT_EQUALS(primes::pow(2, 0), 1);
    TS_ASSERT_EQUALS(primes::pow(2, 1), 2);
    TS_ASSERT_EQUALS(primes::pow(2, 2), 4);
    TS_ASSERT_EQUALS(primes::pow(2, 3), 8);
    TS_ASSERT_EQUALS(primes::pow(2, 10), 1024);

    TS_ASSERT_EQUALS(primes::pow(3, 0), 1);
    TS_ASSERT_EQUALS(primes::pow(3, 1), 3);
    TS_ASSERT_EQUALS(primes::pow(3, 2), 9);
    TS_ASSERT_EQUALS(primes::pow(3, 3), 27);

    for (primes::nat64_type base(0); base < 1000; ++base) {
      TS_ASSERT_EQUALS(primes::pow(base, 0), 1);

      for (primes::nat64_type exp(1); exp < 1000; ++exp) {
        TS_ASSERT_EQUALS(primes::pow(base, exp),
                         primes::pow(base, exp - 1)*base);
      }
    }
  }


  void test__primaries() {
    SHOW_FUNC_NAME;

    std::vector<std::pair<primes::nat64_type, primes::nat64_type>> ps;

    TS_ASSERT_EQUALS(primes::primaries(1), ps);

    ps = {std::pair<primes::nat64_type, primes::nat64_type>(2, 1)};
    TS_ASSERT_EQUALS(primes::primaries(2), ps);

    ps = {std::pair<primes::nat64_type, primes::nat64_type>(2, 2)};
    TS_ASSERT_EQUALS(primes::primaries(4), ps);

    ps = {std::pair<primes::nat64_type, primes::nat64_type>(2, 3),
          std::pair<primes::nat64_type, primes::nat64_type>(3, 1)};
    TS_ASSERT_EQUALS(primes::primaries(24), ps);

    for (auto it_p(primes::table_primes_cbegin());
         it_p != primes::table_primes_cend(); ++it_p) {
      // p^1
      ps = {std::pair<primes::nat64_type, primes::nat64_type>(*it_p, 1)};
      TS_ASSERT_EQUALS(primes::primaries(*it_p), ps);

      unsigned int n(1);

      // p^i
      for (unsigned int i(1); i < 5; ++i) {
        n *= *it_p;
        ps = {std::pair<primes::nat64_type, primes::nat64_type>(*it_p, i)};
        TS_ASSERT_EQUALS(primes::primaries(n), ps);
      }
    }

    for (auto it_p(primes::table_primes_cbegin());
         it_p != primes::table_primes_cend(); ++it_p) {
      for (auto it_q(it_p + 1); it_q != primes::table_primes_cend(); ++it_q) {
      // p^1 and q^1
        ps = {std::pair<primes::nat64_type, primes::nat64_type>(*it_p, 1),
              std::pair<primes::nat64_type, primes::nat64_type>(*it_q, 1)};
        TS_ASSERT_EQUALS(primes::primaries(*it_p * *it_q), ps);
      }
    }
  }


  void test__primaries_prod() {
    SHOW_FUNC_NAME;

    std::vector<std::pair<primes::nat64_type, primes::nat64_type>> ps;
    TS_ASSERT_EQUALS(primes::primaries_prod(ps), 1);

    ps = {std::pair<primes::nat64_type, primes::nat64_type>(2, 1)};
    TS_ASSERT_EQUALS(primes::primaries_prod(ps), 2);

    ps = {std::pair<primes::nat64_type, primes::nat64_type>(2, 2)};
    TS_ASSERT_EQUALS(primes::primaries_prod(ps), 4);

    ps = {std::pair<primes::nat64_type, primes::nat64_type>(2, 3),
          std::pair<primes::nat64_type, primes::nat64_type>(3, 1)};
    TS_ASSERT_EQUALS(primes::primaries_prod(ps), 24);

    for (unsigned int n(1); n < 10000; ++n) {
      TS_ASSERT_EQUALS(primes::primaries_prod(primes::primaries(n)), n);
    }
  }


  void test__prime_ith() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::prime_ith(0), 2);
    TS_ASSERT_EQUALS(primes::prime_ith(1), 3);
    TS_ASSERT_EQUALS(primes::prime_ith(2), 5);
    TS_ASSERT_EQUALS(primes::prime_ith(3), 7);
    TS_ASSERT_EQUALS(primes::prime_ith(4), 11);

    // https://primes.utm.edu/nthprime/index.php#nth
    TS_ASSERT_EQUALS(primes::prime_ith(     9),       29);  // NOLINT
    TS_ASSERT_EQUALS(primes::prime_ith(    99),      541);  // NOLINT
    TS_ASSERT_EQUALS(primes::prime_ith(   999),     7919);  // NOLINT
    TS_ASSERT_EQUALS(primes::prime_ith(  9999),   104729);  // NOLINT
    TS_ASSERT_EQUALS(primes::prime_ith( 99999),  1299709);  // NOLINT
    // TS_ASSERT_EQUALS(primes::prime_ith(999999), 15485863);  // NOLINT

    for (unsigned int i(0); i < primes::table_primes_size(); ++i) {
      TS_ASSERT_EQUALS(primes::prime_ith(i), primes::table_prime_ith(i));
    }

    for (unsigned int i(0); i < 1000; ++i) {
      const unsigned int p(primes::prime_ith(i));
      const unsigned int q(primes::prime_ith(i + 1));

      // p is prime
      TS_ASSERT(primes::is_prime(p));

      // q are odds
      TS_ASSERT_DIFFERS(q % 2, 0);

      // Strictly increasing table
      TS_ASSERT_LESS_THAN(p, q);

      // Odd between p and q are composites
      for (unsigned int n(p + 2); n < q; n += 2) {
        TS_ASSERT(primes::is_composite(n));
      }
    }
  }


  void test__prime_to_ith() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::prime_to_ith(2), 0);
    TS_ASSERT_EQUALS(primes::prime_to_ith(3), 1);
    TS_ASSERT_EQUALS(primes::prime_to_ith(5), 2);
    TS_ASSERT_EQUALS(primes::prime_to_ith(7), 3);
    TS_ASSERT_EQUALS(primes::prime_to_ith(11), 4);

    TS_ASSERT_EQUALS(primes::prime_to_ith(      29),      9);  // NOLINT
    TS_ASSERT_EQUALS(primes::prime_to_ith(     541),     99);  // NOLINT
    TS_ASSERT_EQUALS(primes::prime_to_ith(    7919),    999);  // NOLINT
    TS_ASSERT_EQUALS(primes::prime_to_ith(  104729),   9999);  // NOLINT
    TS_ASSERT_EQUALS(primes::prime_to_ith( 1299709),  99999);  // NOLINT
    // TS_ASSERT_EQUALS(primes::prime_to_ith(15485863), 999999);  // NOLINT

    for (unsigned int i(0); i < primes::table_primes_size(); ++i) {
      TS_ASSERT_EQUALS(primes::prime_to_ith(primes::table_prime_ith(i)), i);
    }
  }


  void test__primorial() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::primorial(0), 1);
    TS_ASSERT_EQUALS(primes::primorial(1), 2);
    TS_ASSERT_EQUALS(primes::primorial(2), 6);
    TS_ASSERT_EQUALS(primes::primorial(3), 30);
    TS_ASSERT_EQUALS(primes::primorial(4), 210);
    TS_ASSERT_EQUALS(primes::primorial(10), 6469693230ul);
  }


  void test__sqr() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::sqr(0), 0);
    TS_ASSERT_EQUALS(primes::sqr(1), 1);
    TS_ASSERT_EQUALS(primes::sqr(2), 4);
    TS_ASSERT_EQUALS(primes::sqr(3), 9);
    TS_ASSERT_EQUALS(primes::sqr(4), 16);

    for (unsigned int i(0); i < 10000; ++i) {
      TS_ASSERT_EQUALS(primes::sqr(i), i*i);
    }
  }


  void test__table_modulo() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::table_modulo(), primes::__modulo);
    TS_ASSERT_LESS_THAN_EQUALS(2, primes::table_modulo());


    for (unsigned int i(0); i <= 16; ++i) {
      if (primes::table_modulo() == primes::primorial(i)) {
        return;
      }
    }
    TS_ASSERT(false);
  }


  void test__table_modulo_diffs_size() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::table_modulo_diffs_size(),
                     primes::__modulo_diffs.size());
    TS_ASSERT_LESS_THAN_EQUALS(1, primes::table_modulo_diffs_size());
  }


  void test__table_prime_ith() {
    SHOW_FUNC_NAME;

    TS_ASSERT_LESS_THAN_EQUALS(2, primes::table_primes_size());

    TS_ASSERT_EQUALS(primes::table_prime_ith(0), 2);
    TS_ASSERT_EQUALS(primes::table_prime_ith(1), 3);

    TS_ASSERT_EQUALS(primes::table_prime_ith(primes::__primes.size() - 1)
                     % primes::table_modulo(),
                     1);

    for (unsigned int i(0); i < primes::table_primes_size(); ++i) {
      TS_ASSERT_EQUALS(primes::table_prime_ith(i), primes::__primes[i]);
    }

    for (unsigned int i(1); i < primes::table_primes_size() - 1; ++i) {
      const unsigned int p(primes::table_prime_ith(i));
      const unsigned int q(primes::table_prime_ith(i + 1));

      // p and q are odds
      TS_ASSERT_DIFFERS(p % 2, 0);
      TS_ASSERT_DIFFERS(q % 2, 0);

      // Strictly increasing table
      TS_ASSERT_LESS_THAN(p, q);

      // p and q are primes
      for (unsigned int d(p + 2); d < p; d += 2) {
        TS_ASSERT_DIFFERS(p % d, 0);
        TS_ASSERT_DIFFERS(q % d, 0);
      }

      // Odd between p and q are composites
      for (unsigned int n(p + 2); n < q; n += 2) {
        bool composite(false);
        unsigned int n_sqrt(primes::floor_sqrt(n));

        for (unsigned int d(3); d <= n_sqrt; d += 2) {
          if (n % d == 0) {
            composite = true;

            break;
          }
        }

        TS_ASSERT(composite);
      }
    }
  }


  void test__table_prime_last() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::table_prime_last(),
                     primes::table_prime_ith(primes::table_primes_size() - 1));

    TS_ASSERT_LESS_THAN(2, primes::table_prime_last());
  }


  void test__table_primes_cbegin__cend() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::table_primes_cbegin(),
                     primes::__primes.cbegin());
    TS_ASSERT_EQUALS(primes::table_primes_cend(),
                     primes::__primes.cend());

    TS_ASSERT_EQUALS(*primes::table_primes_cbegin(), 2);
    TS_ASSERT_EQUALS(*(primes::table_primes_cbegin() + 1), 3);
  }


  void test__table_primes_size() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(primes::table_primes_size(), primes::__primes.size());
    TS_ASSERT_LESS_THAN_EQUALS(2, primes::table_primes_size());
  }


  void test__tables_shrink_to_min() {
    SHOW_FUNC_NAME;

    primes::tables_shrink_to_min();

    TS_ASSERT_EQUALS(primes::table_modulo_diffs_size(), 1);
    TS_ASSERT_EQUALS(primes::__modulo_diffs,
                     std::vector<primes::modulo_type>{2});

    TS_ASSERT_EQUALS(primes::table_primes_size(), 2);
    TS_ASSERT_EQUALS(primes::__primes,
                     (std::vector<primes::nat32_type>{2, 3}));
  }


  void test_end() {
    std::cout
      << "--- Total: "
      << round<std::chrono::seconds>(std::chrono::steady_clock::now()
                                     - total_start).count()
      << "s ---" << std::endl;
  }
};
