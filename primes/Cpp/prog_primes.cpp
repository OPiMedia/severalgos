/* -*- coding: latin-1 -*- */
/** \file prog_primes.cpp
 * \brief
 * Usage: prog_primes nb
 *     or prog_primes first last
 *
 * Print the first nb primes numbers
 * or the prime numbers from first to last.
 *
 * Piece of severalgos.
 * https://bitbucket.org/OPiMedia/severalgos
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iostream>

#include "primes/primes.hpp"



int
main(int argc, const char* const argv[]) {
  if (argc > 1) {
    unsigned int first(0);
    unsigned int end;

    if (argc == 2) {
      end = atoi(argv[1]);
    } else {
      first = atoi(argv[1]);
      end = atoi(argv[2]) + 1;
    }

    for (unsigned int i(first); i < end; ++i) {
      std::cout << i << ": " << primes::prime_ith(i) << std::endl;
      std::cout.flush();
    }
  }

  return EXIT_SUCCESS;
}
